import Vue from 'vue';
import CompanySearch from './vue-components/company-search/main.vue';

const companySearch = document.getElementById('company-search');
const companies = document.querySelector('[data-accounting-companies]');
if (companySearch && companies) {
  const companiesParsed = JSON.parse(companies.innerHTML);

  // eslint-disable-next-line no-unused-vars
  const companySearchInstance = new Vue({
    el: companySearch,
    components: {
      CompanySearch
    },
    data() {
      return {
        companies: Object.freeze(companiesParsed)
      };
    }
  });
}
