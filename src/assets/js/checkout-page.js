import Vue from 'vue';
import Vuelidate from 'vuelidate';
import VueI18n from 'vue-i18n';

import { CURRENT_LANGUAGE } from './utils';
import Checkout from './vue-components/checkout/checkout.vue';

Vue.use(Vuelidate);
Vue.use(VueI18n);

// Create VueI18n instance
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});

// Checkout page
if (document.getElementById('sevDesk-checkout')) {
  // eslint-disable-next-line no-unused-vars
  const checkoutInstance = new Vue({
    i18n,
    el: '#sevDesk-checkout',
    components: { 'l-checkout': Checkout }
  });
}
