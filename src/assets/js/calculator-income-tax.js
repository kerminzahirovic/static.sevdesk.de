import Vue from 'vue';
import VueI18n from 'vue-i18n';

import { CURRENT_LANGUAGE } from './utils';
import calculatorIncomeTax from './vue-components/calculator-income-tax.vue';

// Create VueI18n instance
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});


const incomeTaxCalculatorElem = document.getElementById('vue-calculator-income-tax');
if (incomeTaxCalculatorElem) {
  // eslint-disable-next-line no-unused-vars
  const incomeTaxCalculatorInstance = new Vue({
    i18n,
    el: incomeTaxCalculatorElem,
    components: { 'calculator-income-tax': calculatorIncomeTax }
  });
}
