import Vue from 'vue';
import Vuelidate from 'vuelidate';

import BranchFeedback from './vue-components/branch-feedback.vue';
import './components/hero-typed';

Vue.use(Vuelidate);

// Vue branch feedback
if (document.getElementById('branch-feedback')) {
  // eslint-disable-next-line no-unused-vars
  const branchFeedbackInstance = new Vue({
    el: '#branch-feedback',
    components: { 'c-branch-feedback': BranchFeedback }
  });
}
