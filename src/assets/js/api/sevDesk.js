import axios from 'axios';
import qs from 'qs';

// const baseURL = 'https://groot.s.sevenit.biz';
const baseURL = 'https://my.sevdesk.de';

if (baseURL === 'https://groot.s.sevenit.biz') {
  console.error('api is pointing to groot');
}

// axios instance
const http = axios.create({
  baseURL
});

/**
 * Create account
 * @param {Object} data
 */
function createAccount(data) {
  const params = qs.stringify(data);
  return http.post('/register/save', params);
}

/**
 * Create account from checkout
 * @param {Object} data
 */
function createAccountCheckout(data) {
  const params = qs.stringify(data);
  return http.post('/register/coupon', params);
}

/**
 * Check if email already exits
 */
function checkEmail(email) {
  return http.get(`/default/register/checkemail/email/${email}`);
}

/**
 * Redirect user to login
 */
function toLogin(email) {
  window.location = `${baseURL}/#/login?message=loginexists&email=${email}`;
}

/**
 * Redirect user to mobile thank you page
 * because the web app is not yet optimized
 */
function toMobileThankYou(url) {
  return function() {
    // window.analytics.indenify() is called in create account
    window.analytics.track('mobileSignup');
    setTimeout(function() {
      window.location = url;
    }, 200);
  };
}

/**
 * Redirect to app
 * @param {string} hash
 */
function toApp(hash) {
  if (hash) {
    window.location.href = `${baseURL}/#/login?hash_login=${hash}`;
  } else {
    window.location.href = `${baseURL}/#/login`;
  }
}

/**
 * Update billing details
 * @param {Object} tokens {cftToken, apiToken}
 * @param {Object} customerData
 */
function updateSubscriptionBilling(tokens, customerData) {
  console.log('tokens', tokens);
  // ISSUE: do we need to wrap customerData in a another object {data: customerData} ?
  const params = qs.stringify(customerData);
  return http.post(
    `/api/v1/SubscriptionType/Factory/updateBillingInformation?cft=${
      tokens.cftToken
    }&token=${tokens.apiToken}`,
    params
  );
}

/**
 * Create checkout order
 *
 * @param {object} tokens {cftToken, apiToken}
 * @param {object} orderData
 * @returns {promise} order
 */
function createOrder(tokens, orderData) {
  const params = qs.stringify(orderData);
  console.log('order data', params);
  return http.post(
    `/api/v1/SubscriptionType/Factory/createOrder?cft=${
      tokens.cftToken
    }&token=${tokens.apiToken}`,
    params
  );
}

/**
 * User login
 *
 * @param {string} user
 * @param {string} pass
 * @returns {promise} response.data = { status: 'success', user: {apiToken} }
 */
function login(email, password) {
  return http.get('/auth/authenticate/', {
    params: {
      username_login: email,
      password_login: password
    }
  });
}

/**
 * getAutoLoginOTT one time token for login
 * in the second http call it gets the activeUser.id
 *
 * @param {object} tokens {cftToken, apiToken}
 * @returns {promise} response.data.objects.token
 */
function getAutoLoginOTT(tokens) {
  return http
    .get('/api/v1/SevUser', {
      params: {
        cft: tokens.cftToken,
        token: tokens.apiToken
      }
    })
    .then(response => {
      const activeUser = response.data.user.objects[0];
      return http.post(
        `/api/v1/SevUser/${activeUser.id}/getQRLoginData?cft=${
          tokens.cftToken
        }&token=${tokens.apiToken}`
      );
    });
}

/**
 * verifyOTT
 * verify hash after account creatin
 *
 * @param {any} hash
 * @returns {promise}
 */
function verifyOTT(hash) {
  return http.get('/api/v1/SevUser/Factory/verifyOTT', {
    params: {
      ott: hash
    }
  });
}

/**
 * Check coupon for checkout
 *
 * @param {object} tokens {cftToken, apiToken}
 * @param {object} data { planVariantId: planVariant, coupon: coupon }
 * @returns
 */
function checkCoupon(tokens, data) {
  const params = qs.stringify(data);
  return http.post(
    `/api/v1/SubscriptionType/Factory/checkCoupon?cft=${
      tokens.cftToken
    }&token=${tokens.apiToken}`,
    params
  );
}

/**
 * upgradeSuccessRedirect
 *
 * @param {string} ott one time token for login
 */
function upgradeSuccessRedirect(ott) {
  window.location.href = `${baseURL}/#/admin/subscription/upgradeSuccess?hash_login=${ott}`;
}

/**
 * Get checkout plans
 *
 * @param {object} data planVariantId, coupon, time
 * @returns {promise} plans
 */
function getCheckoutPlans(data) {
  return axios.post(
    'https://p2g9q0m4mc.execute-api.eu-central-1.amazonaws.com/sevDesk/coupon/isvalid',
    data
  );
}

export default {
  baseURL,
  createAccount,
  createAccountCheckout,
  login,
  checkEmail,
  toLogin,
  toApp,
  toMobileThankYou,
  checkCoupon,
  updateSubscriptionBilling,
  getAutoLoginOTT,
  verifyOTT,
  createOrder,
  upgradeSuccessRedirect,
  getCheckoutPlans
};
