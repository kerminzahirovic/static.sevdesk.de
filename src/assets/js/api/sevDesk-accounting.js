import axios from 'axios';
import qs from 'qs';

const baseURL = 'https://steuerberater.sevdesk.de';
// axios instance
const http = axios.create({
  baseURL
});

/**
 * Create account
 * @param {Object} data
 */
function createAccount(data) {
  const params = qs.stringify(data);
  return http.post('/register/save', params);
}

/**
 * Redirect to app
 * @param {string} hash
 */
function toApp(hash) {
  if (hash) {
    window.location.href = `${baseURL}/app/#/login?token=${hash}`;
  } else {
    window.location.href = `${baseURL}/app/#/login`;
  }
}

/**
 * Redirect to app new
 * @param {string} url
 */
function toApp2(url) {
  if (url) {
    window.location.href = url;
  } else {
    window.location.href = `${baseURL}/app/#/login`;
  }
}


export default {
  baseURL,
  createAccount,
  toApp,
  toApp2
};
