/**
 * Check if analytics available
 */
function checkSegmentAnalytics() {
  if (typeof window.analytics === 'object') {
    return true;
  }
  return false;
}

/**
 * Get referral
 */
function getReferral() {
  if (!checkSegmentAnalytics()) {
    return false;
  }
  const referredBy = window.analytics.user()._getTraits().referredByHash;
  const referredByPartner = window.analytics.user()._getTraits().referredByPartner;
  return { referredBy, referredByPartner };
}

/**
 * User Created
 * @param {Object} data
 * @param {string} email
 * @param {string} source
 * @param {Function} cb
 */
function trackUserCreated(data, email, source, cb) {
  const { clientID, hash } = data;

  if (!checkSegmentAnalytics()) {
    setTimeout(() => {
      if (cb && typeof cb === 'function') {
        cb(hash);
      }
    }, 600);

    return;
  }
  let user = window.analytics.user();
  let id = user.id() === null ? user.id() : user.anonymousId();

  if (user._getTraits() && user._getTraits().CompanyId) {
    window.analytics.reset();
    user = window.analytics.user();
    id = user.id();
  }

  window.analytics.alias(clientID, id);

  const customTraits = {
    createdAt: new Date().toISOString(),
    lead_quality: 'SQL',
    email
  };
  window.analytics.identify(clientID, customTraits, null, () => {
    setTimeout(() => {
      if (cb && typeof cb === 'function') {
        cb(hash);
      }
    }, 600);
  });

  if (source) {
    window.analytics.track('SignUp', { source });
  } else {
    window.analytics.track('SignUp');
  }
}


/**
 * User Created Simple
 * @param {Object} data
 * @param {string} email
 * @param {string} source
 * @param {Function} cb
 */
function trackUserCreatedSimple(data, email, source, cb) {
  const { clientID, hash } = data;

  if (!checkSegmentAnalytics()) {
    setTimeout(() => {
      if (cb && typeof cb === 'function') {
        cb(hash);
      }
    }, 600);

    return;
  }
  let user = window.analytics.user();
  let id = user.id() === null ? user.id() : user.anonymousId();

  if (user._getTraits() && user._getTraits().CompanyId) {
    window.analytics.reset();
    user = window.analytics.user();
    id = user.id();
  }

  window.analytics.alias(clientID, id);

  const customTraits = {
    createdAt: new Date().toISOString(),
    lead_quality: 'SQL',
    email
  };
  window.analytics.identify(clientID, customTraits, null, () => {
    setTimeout(() => {
      if (cb && typeof cb === 'function') {
        cb(hash);
      }
    }, 600);
  });

  if (source) {
    window.analytics.track('SignUp', { source });
  } else {
    window.analytics.track('SignUp');
  }
}

/**
 * Track pages
 */
function trackPageViewed() {
  window.analytics.identify();
  window.analytics.track(window.location.href, { nonInteraction: 1 });
}

/**
 * Track Custom Event
 * this should be called after already calling window.analytics.identify();
 */
function trackCustomEvent(eventName) {
  window.analytics.track(eventName);
}

/**
 * Cross domain tracking segment and google analytics
 *
 */
function crossDomainTracking() {
  if (typeof window.ga != 'function') {
    return false;
  }
  window.ga('require', 'linker');
  window.ga('linker:autoLink', [
    'sevdesk.de',
    'sevdesk.ch',
    'sevdesk.at',
    'sevdesk.gr',
    'sevdesk.es',
    'sevdesk.uk',
    'sevdesk.com',
    'sevdesk.fr',
    'sevdesk.it'
  ]);
  return true;
}

/**
 * Bounce rate
 */
function bounceRate() {
  setTimeout(() => {
    window.analytics.track('Read more than 30sec');
  }, 30000);
}

/**
 * Do stuff when analytics ready
 */
window.analytics.ready(() => {
  crossDomainTracking();
  bounceRate();
  // trackPageViewed();
});

export default {
  getReferral,
  trackUserCreated,
  trackUserCreatedSimple,
  trackCustomEvent
};
