function formv3(email, formID){

    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

    // Create the new request
    var xhr = new XMLHttpRequest();
    var url = `https://api.hsforms.com/submissions/v3/integration/submit/4999173/${formID}`

    // Example request JSON:
    var data = {
      "submittedAt": Date.now(),
      "fields": [
        {
          "name": "email",
          "value": email,
        }
      ],
	"context": {
        "hutk": getCookie('hubspotutk'), // include this parameter and set it to the hubspotutk cookie value to enable cookie tracking on your submission
        "pageUri": window.location.href,
        "pageName": document.title
      },
      "legalConsentOptions":{ // Include this object when GDPR options are enabled
        "consent":{
          "consentToProcess":true,
          "text": "Ich bin damit einverstanden, dass meine persönlichen Daten gespeichert und für den Erhalt des Newsletters verwendet werden",
          "communications":[
            {
              "value":true,
              "subscriptionTypeId":999,
              "text":"I agree to receive marketing communications from sevDesk."
            }
          ]
        }
      }
    }

    var final_data = JSON.stringify(data)

    xhr.open('POST', url);
    // Sets the value of the 'Content-Type' HTTP request headers to 'application/json'
    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && xhr.status == 200) {
            //alert(xhr.responseText); // Returns a 200 response if the submission is successful.
            console.log(xhr.responseText)
        } else if (xhr.readyState == 4 && xhr.status == 400){
            //alert(xhr.responseText); // Returns a 400 error the submission is rejected.
        } else if (xhr.readyState == 4 && xhr.status == 403){
            //alert(xhr.responseText); // Returns a 403 error if the portal isn't allowed to post submissions.
        } else if (xhr.readyState == 4 && xhr.status == 404){
            //alert(xhr.responseText); //Returns a 404 error if the formGuid isn't found
        }
    }
    // Sends the request

    xhr.send(final_data)
 }

 export default { formv3 }
