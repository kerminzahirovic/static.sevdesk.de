import axios from 'axios';
import sevDesk from './sevDesk';
import $ from 'jquery';

// publicApiKey sandbox key
const BILL_WERK_API_KEY = '55c88ecd1d8dd20c206a93c6';

// publicApiKey for BillwerkJS
// const BILL_WERK_API_KEY = '564f18267965090c1cc2b8d3';

// Auth dev
const authorization =
  'iFt-3d0HviMTyuDmfWK30Ie_-80rW9_7ejycqkQPlb5qb8jBZfdirvydB66HOpNuh4WU9z6C4LA98P1W_R0yLQ==';
// Auth production
// const authorization = 'iFt-3d0HviMTyuDmfWK30Ie_-80rW9_7ejycqkQPlb5qb8jBZfdirvydB66HOpNuh4WU9z6C4LA98P1W_R0yLQ==';

const baseURL = 'https://sandbox.billwerk.com/api/v1';

const axiosConfig = {
  header: {
    Authorization: `Bearer ${authorization}`
  }
};

const http = axios.create({ baseURL });

var instance = axios.create({
  baseURL,
  headers: {'Authorization': 'Bearer iFt-3d0HviMTyuDmfWK30Ie_-80rW9_7ejycqkQPlb5qb8jBZfdirvydB66HOpNuh4WU9z6C4LA98P1W_R0yLQ=='}
})

/**
 * Check if BillwerkJS is available
 */
function checkBillWerk() {
  if (!window.BillwerkJS != null) {
    return false;
  }
  return true;
}

/**
 * initialize
 *
 * @param {string} ott one time token
 * @param {string} publicApiKey
 */
function initialize(ott, publicApiKey, onSuccess, onError) {
  if (!checkBillWerk()) {
    return false;
  }
  const signupService = new window.BillwerkJS.Signup();
  const paymentService = new window.BillwerkJS.Payment(
    {
      publicApiKey,
      providerReturnUrl: `${
        sevDesk.baseURL
      }/finalizeBooking.html?hash_login=${ott}`
    },
    () => {
      onSuccess(signupService, paymentService);
    },
    () => {
      onError();
    }
  );
  return true;
}

/**
 * createPortalService
 *
 * @param {any} CustomerToken one time token for login
 * @returns
 */
function createPortalService(CustomerToken) {
  if (!checkBillWerk()) {
    return false;
  }
  const portalService = new window.BillwerkJS.Portal(CustomerToken);
  return portalService;
}


export default {
  initialize,
  createPortalService,
  BILL_WERK_API_KEY
};
