import Vue from 'vue';
import Vuelidate from 'vuelidate';
import VueI18n from 'vue-i18n';

import { CURRENT_LANGUAGE } from '../utils';
import Autocomplete from './search-lexikon.vue';
import AutocompleteBranch from './search-branch.vue';
import SignUpForm from './signup-form.vue';
import SignUpFormDefault from './signup-form-default.vue';
import SignUpModal from './signup-modal.vue';

import SignUpAccounting from './signup-accounting.vue';
import BlogNewsletter from './blog-newsletter.vue';

Vue.use(Vuelidate);
Vue.use(VueI18n);

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});

// Lexicon search
if (document.getElementById('search-autocomplete')) {
  // eslint-disable-next-line no-unused-vars
  const autocompleteInstance = new Vue({
    i18n,
    el: '#search-autocomplete',
    components: { 'c-autocomplete': Autocomplete }
  });
}

// Lexicon branch
if (document.getElementById('search-autocomplete-branch')) {
  // eslint-disable-next-line no-unused-vars
  const autocompleteInstance = new Vue({
    i18n,
    el: '#search-autocomplete-branch',
    components: { 'c-autocomplete': AutocompleteBranch }
  });
}

// Sign Up form
if (document.getElementById('hero-signup')) {
  // eslint-disable-next-line no-unused-vars
  const signUpFormInstance = new Vue({
    i18n,
    el: '#hero-signup',
    components: { 'c-hero-signup': SignUpForm }
  });
}

// Sign Up form default
if (document.getElementById('signup-form-default')) {
  // eslint-disable-next-line no-unused-vars
  const signUpFormDefaultInstance = new Vue({
    i18n,
    el: '#signup-form-default',
    components: { 'c-signup-default': SignUpFormDefault }
  });
}

// CTA form
if (document.getElementById('c-cta-form')) {
  // eslint-disable-next-line no-unused-vars
  const signUpFormDefaultInstance = new Vue({
    i18n,
    el: '#c-cta-form',
    components: { 'c-cta-signup': SignUpForm }
  });
}

// SignUp modal
if (document.getElementById('signup-modal')) {
  // eslint-disable-next-line no-unused-vars
  const signUpModalInstance = new Vue({
    i18n,
    el: '#signup-modal',
    components: { 'c-signup-modal': SignUpModal }
  });
}

// SignUp accounting
if (document.getElementById('signup-accounting')) {
  // eslint-disable-next-line no-unused-vars
  const signUpAccountingInstance = new Vue({
    i18n,
    el: '#signup-accounting',
    components: { 'c-signup-accounting': SignUpAccounting }
  });
}

// Newsletter form
if (document.getElementById('blog-newsletter')) {
  // eslint-disable-next-line no-unused-vars
  const autocompleteInstance = new Vue({
    i18n,
    el: '#blog-newsletter',
    components: { 'c-blog-newsletter': BlogNewsletter }
  });
}
