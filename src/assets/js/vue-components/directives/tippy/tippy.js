import tippy from 'tippy.js/dist/tippy.standalone.js';

const hasBindingChanged = (value, oldValue) => {
  return Object.keys(value).some(key => {
    return value[key] !== oldValue[key];
  })
}

export default (opts = {}) => {
  const init = (el, { value = {}, oldValue = {} }) => {
    // Tippy creates an instance on the element under el._tippy
    // https://atomiks.github.io/tippyjs/objects#instances
    if (el._tippy) {
      // Check if update is needed
      if (hasBindingChanged(value, oldValue)) {
        el._tippy.set({
          ...opts,
          ...value
        });
      }
    } else {
      // Init element
      tippy(el, {
        lazy: true,
        ...opts,
        ...value
      });
    }
  }

  const unbind = el => {
    if (el._tippy) {
      el._tippy.destroy();
    }
  }

  return {
    inserted: init,
    componentUpdated: init,
    unbind
  }
}