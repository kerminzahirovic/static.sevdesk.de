const infoWindow = {
  getContentHtml(marker) {
    const info = marker.item.info || [];
    let infoString = '';
    for (let i = 0; i < info.length; i++) {
      infoString += `<span class="phone">${info[i].text}</span>`;
    }

    return `
    <div class="c-map-info-window">
        <div class="c-map-info-window__content">
          <figure><img src="${marker.item.logo}" /></figure>
          <span class="name">${marker.item.name}</span>
          <span class="address">${marker.item.address}</span>
          ${infoString}
          <span class="website"><a href="${marker.item.website}" target="_blank">Webseite</span>
        </div>
      </div>`;
  },
  removeWhiteSpace() {
    const iwOuters = document.querySelectorAll('.gm-style-iw');
    for (const iwOuter of iwOuters) {
      const iwBackground = iwOuter.previousElementSibling;
      for (const child of iwBackground.children) {
        child.style.display = 'none';
      }
    }
  }
};

export default infoWindow;
