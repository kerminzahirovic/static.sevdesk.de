// Get current language
export const CURRENT_LANGUAGE = document.body.getAttribute('data-lang') || 'de';

/**
 * Credit card validator
 *
 * @export
 * @param {string} value
 * @returns {boolean}
 */
export function validCreditCard(value) {
  if (/[^0-9-\s]+/.test(value)) return false;
  var nCheck = 0,
    nDigit = 0,
    bEven = false;
  value = value.replace(/\D/g, '');
  for (var n = value.length - 1; n >= 0; n--) {
    var cDigit = value.charAt(n),
      nDigit = parseInt(cDigit, 10);
    if (bEven) {
      if ((nDigit *= 2) > 9) nDigit -= 9;
    }
    nCheck += nDigit;
    bEven = !bEven;
  }
  return nCheck % 10 == 0;
}

/**
 * getUrlParameter
 *
 * https://davidwalsh.name/query-string-javascript
 * @param {string} name
 * @returns
 */
export function getUrlParameter(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
