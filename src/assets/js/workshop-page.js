import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Vuelidate from 'vuelidate';

import { CURRENT_LANGUAGE } from './utils';

import WorkshopForm from './vue-components/workshop-form.vue';

Vue.use(Vuelidate);
Vue.use(VueI18n);

// Create VueI18n instance
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});

const workshopFormEl = document.getElementById('vue-workshop-form');
if (workshopFormEl) {
  const workshops = document.querySelector('[data-workshops-json]');
  // eslint-disable-next-line no-unused-vars
  const workshopFormInstance = new Vue({
    i18n,
    el: workshopFormEl,
    data() {
      return {
        workshops: Object.freeze(JSON.parse(workshops.innerHTML)) || {}
      };
    },
    components: { WorkshopForm }
  });
}
