import $ from 'jquery';

class ModalSignUp {
  constructor(element) {
    this.$element = $(element);
    this.$modal = $('#modalSignUp');

    this.initEvents();
  }

  initEvents() {
    $('body').on('click', '.js-open-signup-modal2', event => {
      event.preventDefault();
      this.$modal.modal('show');
    });
  }

}

export default new ModalSignUp();
