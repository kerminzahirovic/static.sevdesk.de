import $ from 'jquery';

$('body').on('click', '[data-ga-event]', function() {
  const $this = $(this);
  const eventName = $this.data('ga-event');
  const actionName = $this.data('ga-action');

  if (eventName && actionName) {
    window.analytics.track(eventName, {
      action: actionName
    });
  }
});
