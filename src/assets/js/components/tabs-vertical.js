import $ from 'jquery';

class Tabs {
  constructor(element) {
    this.$element = $(element);
    this.$nav = this.$element.find('.c-tabs-vertical__nav');
    if (!this.$nav.length > 0) return;
    this.loaded = false;

    this.$navItems = this.$nav.find('li');
    this.$main = this.$element.find('.c-tabs-vertical__main');
    this.$contentItems = this.$element.find('.c-tabs-vertical__content');
    this.initEvents();
  }

  initEvents() {
    this.initNav();
    // this.maxContentHeight();
    this.$element.addClass('js-init');
    $(window).on('resize', () => {
      this.maxContentHeight();
    });

    $(window).on('lazyloaded', () => {
      if (!this.loaded) {
        this.maxContentHeight();
        this.loaded = true;
      }
    });
  }

  initNav() {
    this.$navItems.on('click mouseenter', event => {
      event.preventDefault();
      const contentLink = event.currentTarget.getAttribute('data-href');
      // link starts with #, and we need to remove it
      this.goToTab(contentLink.substring(1));
      event.currentTarget.className = 'is-active';
    });
    this.$navItems[0].click();
  }

  maxContentHeight() {
    let maxHeight = 0;
    this.$contentItems.each((index, item) => {
      if (item.clientHeight > maxHeight) {
        maxHeight = item.clientHeight;
      }
    });
    this.$main.css('height', maxHeight);
  }

  goToTab(id) {
    this.$navItems.removeClass('is-active');
    this.showContent(id);
  }

  showContent(id) {
    this.$contentItems.each((index, item) => {
      if (item.id === id) {
        // eslint-disable-next-line no-param-reassign
        item.style.display = 'block';
      } else {
        // eslint-disable-next-line no-param-reassign
        item.style.display = 'none';
      }
    });
  }
}

$('.c-tabs-vertical').each(function init() {
  // eslint-disable-next-line no-new
  new Tabs(this);
});
