import forEach from 'lodash/forEach';

class LinksBox {
  constructor(element = '.c-links-box') {
    this.element = element;
    this.init();
  }

  init() {
    const boxes = document.querySelectorAll(this.element);

    forEach(boxes, box => {
      const title = box.querySelector('.c-links-box__title');

      if (title) {
        title.addEventListener('click', event => {
          event.preventDefault();
          box.classList.toggle('is-active');
        });
      }
    });
  }
}

export default new LinksBox();
