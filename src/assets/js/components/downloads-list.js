import $ from 'jquery';

import analytics from '../api/analytics';
import Hubspot from '../api/hubspot';

class DownloadList {
  constructor(element = '.c-downloads-list') {
    this.$element = $(element);
    if (this.$element.length > 0) {
      this.init();
    }
  }

  init() {
    const self = this;
    const $downloadLinkContainer = this.$element.find('.is-download');
    const $downloadLinks = $downloadLinkContainer.find('a');

    $downloadLinks.on('click', function downloadTrigger(event) {
      event.preventDefault();
      const $this = $(this);
      const $parent = $this.parent('.is-download');
      const url = $this.attr('href');
      const fileName = url.substring(url.lastIndexOf('/')+1);
      $parent.addClass('is-loading');

      analytics.trackCustomEvent('Downloaded Template');

      function startDownload() {
        self.forceDownload(fileName, url)
      }

      setTimeout(() => {
        self.showModal(startDownload, fileName);
        $parent.removeClass('is-loading');
      }, 1000);

      if (document.body.getAttribute('data-lang') !== 'de' && document.body.getAttribute('data-lang') !== 'at') {

        setTimeout(() => {
          self.forceDownload(fileName, url);
        }, 5000);

      }

    });
  }

  showModal(startDownload, fileName) {
    const $successModal = $('#download-sucess-modal');
    if ($successModal.length > 0) {
      $successModal.modal('show');
    }

    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

    var isChecked = false

    $successModal.find('.c-form-control__checkbox').on('change', function() {
      if (this.checked) {
        isChecked = true
      } else {
        isChecked = false
      }
    })

    $('#download-sucess-modal .c-signup-form-s__submit').on('click', function(e) {
      e.preventDefault()
      var email = $('#download-sucess-modal .c-signup-form-s__email').val()
      if (isEmail(email)) {
        $('.c-signup-form-s__control').removeClass('is-invalid')
        if (isChecked) {
          Hubspot.formv3(email, 'a398fa86-8768-4f27-990b-71186735a598')
          window.analytics.identify({
            email: email,
            tempDownload: fileName
          })
          startDownload()
          $('#download-sucess-modal #c-cta-form').replaceWith('<div class="p-3 mb-1 bg-success u-text-center u-text-white">Geschafft! Viel Spaß mit deiner kostenlosen Vorlage</div>')
        } else {
          $('.c-form-control--checkbox').addClass('is-invalid')
        }
      } else {
        $('.c-signup-form-s__control').addClass('is-invalid')
      }
    })


  }

  forceDownload(filename, url) {
    let element = document.createElement('a');
    element.setAttribute('href', url);
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();
    document.body.removeChild(element);
  }
}

export default new DownloadList();
