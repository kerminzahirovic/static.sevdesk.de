import $ from 'jquery';
import mqlBox from '../../../materials/components/mql-box.html'
import HubspotAPI from '../api/hubspot'
import validator from 'validator';

class MqlBox {
  constructor(element) {
    this.$element = $(element);
    this.init();
  }

  init() {
    var title = this.$element.attr('data-mql-title')
    var desc = this.$element.attr('data-mql-desc')
    var img = this.$element.attr('data-mql-image')
    var hubId = this.$element.attr('data-mql-content-id')

    this.$element.html(mqlBox)
    this.$element.find('.mql-box__header').text(title)
    this.$element.find('.mql-box__content').text(desc)
    this.$element.find('.mql-box__image').attr('src', img)

    
    var submit = this.$element.find('.c-signup-form-s__submit')
    var form = this.$element.find('.c-mql-box-form')

    submit.on('click', (e) => {
      e.preventDefault()
      var email = this.$element.find('.c-signup-form-s__email').val()
      var checked = this.$element.find('.c-form-control__checkbox')[0].checked
      if (validator.isEmail(email)) {
        this.$element.find('.c-signup-form-s__control').removeClass('is-invalid')
        this.$element.find('.c-sign-up-default__message').addClass('d-none')
        console.log('is email')
        if (checked) {
          HubspotAPI.formv3(email,hubId)
          this.$element.find('.mql-box__header').text('Fast geschafft! Prüfe dein E-Mail Postfach und lade dir deine Vorlage herunter.')
          this.$element.find('.c-signup-form-s__control').remove()
          this.$element.find('.c-blog-newsletter__form__terms').remove()
          this.$element.find('.mql-box__content').remove()
          this.$element.find('.c-signup-form-s__submit').remove()
        } else {
          // ErrorMsg: pls check
        }
      } else {
        this.$element.find('.c-signup-form-s__control').addClass('is-invalid')
        this.$element.find('.c-sign-up-default__message').removeClass('d-none')
        console.log('is not email')
      }
    })

  }

}

$('.mqlBox').each(function init() {
  new MqlBox(this);
});

