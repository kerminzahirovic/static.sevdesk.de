import forEach from 'lodash/forEach';

class Accordion {
  constructor(element = '.c-accordion') {
    this.element = element;
    this.init();
  }

  init() {
    const accordions = document.querySelectorAll(this.element);

    forEach(accordions, accordion => {
      const titles = accordion.querySelectorAll('.c-accordion__item__title');

      forEach(titles, title => {
        title.addEventListener('click', event => {
          event.preventDefault();
          title.classList.toggle('is-active');
        });
      });
    });
  }
}

export default new Accordion();
