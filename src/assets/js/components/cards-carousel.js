import Siema from 'siema';
import forEach from 'lodash/forEach';

class ImageCarousel {
  constructor(element = '.c-cards-carousel') {
    this.element = element;
    this.init();
  }

  init() {
    const slider = document.querySelectorAll(this.element);
    forEach(slider, item => {
      this.swiperInit(item);
    });
  }

  swiperInit(item) {
    const container = item.querySelector('.c-cards-carousel__slider');
    const prev = item.querySelector('.js-prev');
    const next = item.querySelector('.js-next');

    function addActiveClass(elements, perPage, current) {
      // console.log(current);
      // if (current < 0) {
      //   current = 0;
      // }
      const activeItems = elements.slice(current, current + perPage);
      activeItems.forEach((slide, i) => {
        activeItems[i].classList.add('is-active');
      });
    }

    function cleanActiveClass(elements) {
      elements.forEach((slide, i) => {
        elements[i].classList.remove('is-active');
      });
    }

    function printSlideIndex() {
      cleanActiveClass(this.innerElements);
      addActiveClass(this.innerElements, this.perPage, this.currentSlide);

      // this.innerElements.forEach((slide, i) => {
      //   const addOrRemove = i === this.currentSlide ? 'add' : 'remove';
      //   this.innerElements[i].classList[addOrRemove]('is-active');
      // });
    }

    const swiperInstance = new Siema({
      selector: container,
      loop: true,
      startIndex: 2,
      threshold: 0,
      perPage: {
        539: 2
      },
      onInit: printSlideIndex,
      onChange: printSlideIndex
    });

    prev.addEventListener('click', e => {
      e.preventDefault();
      swiperInstance.prev();
    });

    next.addEventListener('click', e => {
      e.preventDefault();
      swiperInstance.next();
    });
  }
}

export default new ImageCarousel();
