import Cookie from 'js-cookie';

class CookieNotifier {
  constructor() {
    if (!Cookie.get('cookie_banner')) {
      this.initElement();
    }
  }

  initElement() {
    const element = document.querySelector('.c-cookie-notification');

    if (element) {
      element.classList.add('is-visible');
      const closeBtn = element.querySelector('.js-close');

      closeBtn.addEventListener('click', event => {
        event.preventDefault();
        this.setCookie();
        element.classList.remove('is-visible');
      });
    }
  }

  setCookie() {
    Cookie.set('cookie_banner', 1);
  }
}

export default new CookieNotifier();
