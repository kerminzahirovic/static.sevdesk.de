import forEach from 'lodash/forEach';
import enquire from 'enquire.js';

class FeaturesTable {
  constructor(element) {
    if (!element) {
      return;
    }
    this.element = element;
    this.init();
  }

  init() {
    this.initMobileNav();
    const self = this;

    enquire.register("screen and (max-width: 767px)", {
      match() {
        self.toggleHeaderColspan(1);
      },
      unmatch() {
        self.toggleHeaderColspan(0);
      }
    });
  }

  toggleHeaderColspan(value) {
    const tableHeaders = this.element.querySelectorAll('thead th');
    forEach(tableHeaders, elem => {
      if(value) {
        elem.colSpan = 2;
      } else {
        elem.removeAttribute('colspan');
      }
    });
  }

  initMobileNav() {
    const buttons = this.getAllTabs();

    forEach(buttons, btn => {
      btn.addEventListener('click', event => {
        event.preventDefault();
        const id = event.target.getAttribute('data-target-index');
        this.goToPlan(id);
      });
    });
  }

  goToPlan(id) {
    this.element.className = `c-features-table js-active-p-${id}`;
    this.updateControls(id);
  }

  updateControls(id) {
    const buttons = this.getAllTabs();

    forEach(buttons, btn => {
      const index = btn.getAttribute('data-target-index');
      if (index === id) {
        btn.setAttribute('aria-pressed', 'true');
        btn.classList.add('js-is-active');
      } else {
        btn.setAttribute('aria-pressed', 'false');
        btn.classList.remove('js-is-active');
      }
    });
  }

  getAllTabs() {
    return document.querySelectorAll('.c-features-table-nav__tab');
  }
}

const featuresTable = document.querySelectorAll('.c-features-table');

forEach(featuresTable, elem => {
  /* eslint-disable no-new */
  new FeaturesTable(elem);
});
