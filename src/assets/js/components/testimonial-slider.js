// https://www.sitepoint.com/make-a-simple-javascript-slideshow-without-jquery/
class TestimonialSlider {
  constructor(selector = '.fc-testimonial-slider') {
    this.selector =
      typeof selector === 'string'
        ? document.querySelector(selector)
        : selector;
    if (this.selector === null) {
      return false;
    }

    this.currentSlide = 0;
    this.slides = this.selector.querySelectorAll('.slide');
    this.prevBtn = this.selector.querySelector('.fc-testimonial-slider__prev');
    this.nextBtn = this.selector.querySelector('.fc-testimonial-slider__next');

    if (this.slides.length > 0) {
      /**
       * Bind methods for callbacks to always reverence the correct "this"
       * http://reactkungfu.com/2015/07/why-and-how-to-bind-methods-in-your-react-component-classes/
       */
      ['touchstartHandler', 'touchendHandler', 'touchmoveHandler'].forEach(
        method => {
          this[method] = this[method].bind(this);
        }
      );
      this.init();
    } else {
      console.warn('please provide slides;');
    }
  }

  init() {
    this.attachEvents();
  }

  attachEvents() {
    // Check if mobile device
    this.TOUCH_DEVICE = 'ontouchstart' in document.documentElement;

    this.pointerDown = false;
    this.drag = {
      startX: 0,
      endX: 0,
      startY: 0,
      letItGo: null
    };

    if (this.TOUCH_DEVICE) {
      // Touch events
      // this.selector.addEventListener('touchstart', this.touchstartHandler);
      // this.selector.addEventListener('touchend', this.touchendHandler);
      // this.selector.addEventListener('touchmove', this.touchmoveHandler);
    }

    // Controls
    this.prevBtn.addEventListener('click', e => {
      e.preventDefault();
      this.prevSlide();
    });
    this.nextBtn.addEventListener('click', e => {
      e.preventDefault();
      this.nextSlide();
    });
  }

  touchstartHandler(e) {
    // Prevent dragging / swiping on inputs, selects and textareas
    const ignoreFields =
      ['TEXTAREA', 'OPTION', 'INPUT', 'SELECT'].indexOf(e.target.nodeName) !== -1;
    if (ignoreFields) {
      return;
    }

    e.stopPropagation();
    this.pointerDown = true;
    this.drag.startX = e.touches[0].pageX;
  }

  touchendHandler(e) {
    this.pointerDown = false;
    if (this.drag.endX) {
      e.stopPropagation();
      this.updateAfterDrag();
    }
    this.clearDrag();
  }

  clearDrag() {
    this.drag = {
      startX: 0,
      endX: 0
    };
  }

  touchmoveHandler(e) {
    if (this.pointerDown) {
      e.preventDefault();
      e.stopPropagation();
      this.drag.endX = e.touches[0].pageX;
    }
  }

  updateAfterDrag() {
    const movement = this.drag.endX - this.drag.startX;
    const movementDistance = Math.abs(movement);
    const threshold = 20;

    if (movement > 0 && movementDistance > threshold) {
      this.nextSlide();
    } else if (movement < 0 && movementDistance > threshold) {
      this.prevSlide();
    }
  }

  nextSlide() {
    this.goToSlide(this.currentSlide + 1);
  }
  prevSlide() {
    this.goToSlide(this.currentSlide - 1);
  }
  goToSlide(n) {
    this.slides[this.currentSlide].className = 'slide';
    this.currentSlide = (n + this.slides.length) % this.slides.length;
    this.slides[this.currentSlide].className = 'slide is-active';
  }
}

export default new TestimonialSlider();
