import forEach from 'lodash/forEach';

import Switch from './switch';
import ButtonGroup from './button-group';

class PricingTableOutlined {
  constructor(selector = '.c-pricing-table-outlined') {
    this.element = selector;
    this.init();
  }

  init() {
    const containers = document.querySelectorAll(this.element);
    forEach(containers, container => {
      const toogleSwitch = container.querySelector('.c-switch');
      const buttonGroup = container.querySelector('.c-button-group');
      /* eslint-disable no-new */
      new Switch(toogleSwitch, () => {
        this.toogglePaymentMode(container);
      });

      if (buttonGroup) {
        new ButtonGroup(buttonGroup, $elem => {
          this.toogglePaymentTabs(container, $elem);
        });
      }
    });
  }

  toogglePaymentMode(container) {
    container.classList.toggle('is-monthly');
  }

  toogglePaymentTabs(container, $elem) {
    const dataClass = $elem.data('class');
    /* eslint-disable no-param-reassign */
    container.className = `c-pricing-table-outlined ${dataClass}`;
  }
}

export default new PricingTableOutlined();
