function getFormData(t) {
  var e = [].slice.call(t.querySelectorAll('[name]')),
    i = {};
  return (
    e.forEach(function(t) {
      i[t.getAttribute('name')] = t.value;
    }),
    i
  );
}
function submitForm(t) {
  var e = document
      .querySelector('form [name=csrf-token]')
      .getAttribute('value'),
    i = getFormData(t),
    n = new XMLHttpRequest();
  n.addEventListener('load', submitDone),
    n.open('POST', '/atlas'),
    n.setRequestHeader('Content-Type', 'application/json;charset=UTF-8'),
    n.setRequestHeader('x-stripe-csrf-token', e),
    n.send(JSON.stringify(i));
}
function submitDone() {
  ga('send', 'event', 'Forms', 'Apply to Atlas', 'Request Access'),
    modal.classList.add('success');
}
!(function(t, e) {
  'object' == typeof exports && 'undefined' != typeof module
    ? e(exports)
    : 'function' == typeof define && define.amd
      ? define(['exports'], e)
      : e((t.THREE = t.THREE || {}));
})(this, function(t) {
  function e() {}
  function i(t, e) {
    (this.x = t || 0), (this.y = e || 0);
  }
  function n(e, r, a, o, s, c, h, l, u, d) {
    Object.defineProperty(this, 'id', { value: Bn++ }),
      (this.uuid = t.Math.generateUUID()),
      (this.sourceFile = this.name = ''),
      (this.image = void 0 !== e ? e : n.DEFAULT_IMAGE),
      (this.mipmaps = []),
      (this.mapping = void 0 !== r ? r : n.DEFAULT_MAPPING),
      (this.wrapS = void 0 !== a ? a : 1001),
      (this.wrapT = void 0 !== o ? o : 1001),
      (this.magFilter = void 0 !== s ? s : 1006),
      (this.minFilter = void 0 !== c ? c : 1008),
      (this.anisotropy = void 0 !== u ? u : 1),
      (this.format = void 0 !== h ? h : 1023),
      (this.type = void 0 !== l ? l : 1009),
      (this.offset = new i(0, 0)),
      (this.repeat = new i(1, 1)),
      (this.generateMipmaps = !0),
      (this.premultiplyAlpha = !1),
      (this.flipY = !0),
      (this.unpackAlignment = 4),
      (this.encoding = void 0 !== d ? d : 3e3),
      (this.version = 0),
      (this.onUpdate = null);
  }
  function r(t, e, i, n) {
    (this.x = t || 0),
      (this.y = e || 0),
      (this.z = i || 0),
      (this.w = void 0 !== n ? n : 1);
  }
  function a(e, i, a) {
    (this.uuid = t.Math.generateUUID()),
      (this.width = e),
      (this.height = i),
      (this.scissor = new r(0, 0, e, i)),
      (this.scissorTest = !1),
      (this.viewport = new r(0, 0, e, i)),
      (a = a || {}),
      void 0 === a.minFilter && (a.minFilter = 1006),
      (this.texture = new n(
        void 0,
        void 0,
        a.wrapS,
        a.wrapT,
        a.magFilter,
        a.minFilter,
        a.format,
        a.type,
        a.anisotropy,
        a.encoding
      )),
      (this.depthBuffer = void 0 === a.depthBuffer || a.depthBuffer),
      (this.stencilBuffer = void 0 === a.stencilBuffer || a.stencilBuffer),
      (this.depthTexture = void 0 !== a.depthTexture ? a.depthTexture : null);
  }
  function o(t, e, i) {
    a.call(this, t, e, i), (this.activeMipMapLevel = this.activeCubeFace = 0);
  }
  function s(t, e, i, n) {
    (this._x = t || 0),
      (this._y = e || 0),
      (this._z = i || 0),
      (this._w = void 0 !== n ? n : 1);
  }
  function c(t, e, i) {
    (this.x = t || 0), (this.y = e || 0), (this.z = i || 0);
  }
  function h() {
    (this.elements = new Float32Array([
      1,
      0,
      0,
      0,
      0,
      1,
      0,
      0,
      0,
      0,
      1,
      0,
      0,
      0,
      0,
      1
    ])),
      0 < arguments.length &&
        console.error(
          'THREE.Matrix4: the constructor no longer reads arguments. use .set() instead.'
        );
  }
  function l(t, e, i, r, a, o, s, c, h, l) {
    (t = void 0 !== t ? t : []),
      n.call(this, t, void 0 !== e ? e : 301, i, r, a, o, s, c, h, l),
      (this.flipY = !1);
  }
  function u(t, e, i) {
    var n = t[0];
    if (0 >= n || 0 < n) return t;
    var r = e * i,
      a = Vn[r];
    if ((void 0 === a && ((a = new Float32Array(r)), (Vn[r] = a)), 0 !== e))
      for (n.toArray(a, 0), n = 1, r = 0; n !== e; ++n)
        (r += i), t[n].toArray(a, r);
    return a;
  }
  function d(t, e) {
    var i = kn[e];
    void 0 === i && ((i = new Int32Array(e)), (kn[e] = i));
    for (var n = 0; n !== e; ++n) i[n] = t.allocTextureUnit();
    return i;
  }
  function p(t, e) {
    t.uniform1f(this.addr, e);
  }
  function f(t, e) {
    t.uniform1i(this.addr, e);
  }
  function m(t, e) {
    void 0 === e.x
      ? t.uniform2fv(this.addr, e)
      : t.uniform2f(this.addr, e.x, e.y);
  }
  function g(t, e) {
    void 0 !== e.x
      ? t.uniform3f(this.addr, e.x, e.y, e.z)
      : void 0 !== e.r
        ? t.uniform3f(this.addr, e.r, e.g, e.b)
        : t.uniform3fv(this.addr, e);
  }
  function v(t, e) {
    void 0 === e.x
      ? t.uniform4fv(this.addr, e)
      : t.uniform4f(this.addr, e.x, e.y, e.z, e.w);
  }
  function y(t, e) {
    t.uniformMatrix2fv(this.addr, !1, e.elements || e);
  }
  function x(t, e) {
    t.uniformMatrix3fv(this.addr, !1, e.elements || e);
  }
  function _(t, e) {
    t.uniformMatrix4fv(this.addr, !1, e.elements || e);
  }
  function b(t, e, i) {
    var n = i.allocTextureUnit();
    t.uniform1i(this.addr, n), i.setTexture2D(e || Gn, n);
  }
  function w(t, e, i) {
    var n = i.allocTextureUnit();
    t.uniform1i(this.addr, n), i.setTextureCube(e || Hn, n);
  }
  function M(t, e) {
    t.uniform2iv(this.addr, e);
  }
  function E(t, e) {
    t.uniform3iv(this.addr, e);
  }
  function S(t, e) {
    t.uniform4iv(this.addr, e);
  }
  function T(t) {
    switch (t) {
      case 5126:
        return p;
      case 35664:
        return m;
      case 35665:
        return g;
      case 35666:
        return v;
      case 35674:
        return y;
      case 35675:
        return x;
      case 35676:
        return _;
      case 35678:
        return b;
      case 35680:
        return w;
      case 5124:
      case 35670:
        return f;
      case 35667:
      case 35671:
        return M;
      case 35668:
      case 35672:
        return E;
      case 35669:
      case 35673:
        return S;
    }
  }
  function A(t, e) {
    t.uniform1fv(this.addr, e);
  }
  function L(t, e) {
    t.uniform1iv(this.addr, e);
  }
  function R(t, e) {
    t.uniform2fv(this.addr, u(e, this.size, 2));
  }
  function P(t, e) {
    t.uniform3fv(this.addr, u(e, this.size, 3));
  }
  function C(t, e) {
    t.uniform4fv(this.addr, u(e, this.size, 4));
  }
  function I(t, e) {
    t.uniformMatrix2fv(this.addr, !1, u(e, this.size, 4));
  }
  function U(t, e) {
    t.uniformMatrix3fv(this.addr, !1, u(e, this.size, 9));
  }
  function D(t, e) {
    t.uniformMatrix4fv(this.addr, !1, u(e, this.size, 16));
  }
  function N(t, e, i) {
    var n = e.length,
      r = d(i, n);
    for (t.uniform1iv(this.addr, r), t = 0; t !== n; ++t)
      i.setTexture2D(e[t] || Gn, r[t]);
  }
  function F(t, e, i) {
    var n = e.length,
      r = d(i, n);
    for (t.uniform1iv(this.addr, r), t = 0; t !== n; ++t)
      i.setTextureCube(e[t] || Hn, r[t]);
  }
  function O(t) {
    switch (t) {
      case 5126:
        return A;
      case 35664:
        return R;
      case 35665:
        return P;
      case 35666:
        return C;
      case 35674:
        return I;
      case 35675:
        return U;
      case 35676:
        return D;
      case 35678:
        return N;
      case 35680:
        return F;
      case 5124:
      case 35670:
        return L;
      case 35667:
      case 35671:
        return M;
      case 35668:
      case 35672:
        return E;
      case 35669:
      case 35673:
        return S;
    }
  }
  function z(t, e, i) {
    (this.id = t), (this.addr = i), (this.setValue = T(e.type));
  }
  function B(t, e, i) {
    (this.id = t),
      (this.addr = i),
      (this.size = e.size),
      (this.setValue = O(e.type));
  }
  function G(t) {
    (this.id = t), (this.seq = []), (this.map = {});
  }
  function H(t, e, i) {
    (this.seq = []),
      (this.map = {}),
      (this.renderer = i),
      (i = t.getProgramParameter(e, t.ACTIVE_UNIFORMS));
    for (var n = 0; n !== i; ++n) {
      var r = t.getActiveUniform(e, n),
        a = t.getUniformLocation(e, r.name),
        o = this,
        s = r.name,
        c = s.length;
      for (jn.lastIndex = 0; ; ) {
        var h = jn.exec(s),
          l = jn.lastIndex,
          u = h[1],
          d = h[3];
        if (
          (']' === h[2] && (u |= 0), void 0 === d || ('[' === d && l + 2 === c))
        ) {
          (s = o),
            (r = void 0 === d ? new z(u, r, a) : new B(u, r, a)),
            s.seq.push(r),
            (s.map[r.id] = r);
          break;
        }
        (d = o.map[u]),
          void 0 === d &&
            ((d = new G(u)),
            (u = o),
            (o = d),
            u.seq.push(o),
            (u.map[o.id] = o)),
          (o = d);
      }
    }
  }
  function V(t, e, i) {
    return void 0 === e && void 0 === i ? this.set(t) : this.setRGB(t, e, i);
  }
  function k(t, e) {
    (this.min = void 0 !== t ? t : new i(Infinity, Infinity)),
      (this.max = void 0 !== e ? e : new i(-Infinity, -Infinity));
  }
  function j(t, e) {
    var n,
      r,
      a,
      o,
      s,
      h,
      l,
      u,
      d,
      p,
      f,
      m,
      g,
      v,
      y,
      x,
      _ = t.context,
      b = t.state;
    this.render = function(w, M, E) {
      if (0 !== e.length) {
        w = new c();
        var S = E.w / E.z,
          T = 0.5 * E.z,
          A = 0.5 * E.w,
          L = 16 / E.w,
          R = new i(L * S, L),
          P = new c(1, 1, 0),
          C = new i(1, 1),
          I = new k();
        if ((I.min.set(0, 0), I.max.set(E.z - 16, E.w - 16), void 0 === v)) {
          var L = new Float32Array([
              -1,
              -1,
              0,
              0,
              1,
              -1,
              1,
              0,
              1,
              1,
              1,
              1,
              -1,
              1,
              0,
              1
            ]),
            U = new Uint16Array([0, 1, 2, 0, 2, 3]);
          (f = _.createBuffer()),
            (m = _.createBuffer()),
            _.bindBuffer(_.ARRAY_BUFFER, f),
            _.bufferData(_.ARRAY_BUFFER, L, _.STATIC_DRAW),
            _.bindBuffer(_.ELEMENT_ARRAY_BUFFER, m),
            _.bufferData(_.ELEMENT_ARRAY_BUFFER, U, _.STATIC_DRAW),
            (y = _.createTexture()),
            (x = _.createTexture()),
            b.bindTexture(_.TEXTURE_2D, y),
            _.texImage2D(
              _.TEXTURE_2D,
              0,
              _.RGB,
              16,
              16,
              0,
              _.RGB,
              _.UNSIGNED_BYTE,
              null
            ),
            _.texParameteri(_.TEXTURE_2D, _.TEXTURE_WRAP_S, _.CLAMP_TO_EDGE),
            _.texParameteri(_.TEXTURE_2D, _.TEXTURE_WRAP_T, _.CLAMP_TO_EDGE),
            _.texParameteri(_.TEXTURE_2D, _.TEXTURE_MAG_FILTER, _.NEAREST),
            _.texParameteri(_.TEXTURE_2D, _.TEXTURE_MIN_FILTER, _.NEAREST),
            b.bindTexture(_.TEXTURE_2D, x),
            _.texImage2D(
              _.TEXTURE_2D,
              0,
              _.RGBA,
              16,
              16,
              0,
              _.RGBA,
              _.UNSIGNED_BYTE,
              null
            ),
            _.texParameteri(_.TEXTURE_2D, _.TEXTURE_WRAP_S, _.CLAMP_TO_EDGE),
            _.texParameteri(_.TEXTURE_2D, _.TEXTURE_WRAP_T, _.CLAMP_TO_EDGE),
            _.texParameteri(_.TEXTURE_2D, _.TEXTURE_MAG_FILTER, _.NEAREST),
            _.texParameteri(_.TEXTURE_2D, _.TEXTURE_MIN_FILTER, _.NEAREST);
          var L = (g = {
              vertexShader:
                'uniform lowp int renderType;\nuniform vec3 screenPosition;\nuniform vec2 scale;\nuniform float rotation;\nuniform sampler2D occlusionMap;\nattribute vec2 position;\nattribute vec2 uv;\nvarying vec2 vUV;\nvarying float vVisibility;\nvoid main() {\nvUV = uv;\nvec2 pos = position;\nif ( renderType == 2 ) {\nvec4 visibility = texture2D( occlusionMap, vec2( 0.1, 0.1 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.5, 0.1 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.9, 0.1 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.9, 0.5 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.9, 0.9 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.5, 0.9 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.1, 0.9 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.1, 0.5 ) );\nvisibility += texture2D( occlusionMap, vec2( 0.5, 0.5 ) );\nvVisibility =        visibility.r / 9.0;\nvVisibility *= 1.0 - visibility.g / 9.0;\nvVisibility *=       visibility.b / 9.0;\nvVisibility *= 1.0 - visibility.a / 9.0;\npos.x = cos( rotation ) * position.x - sin( rotation ) * position.y;\npos.y = sin( rotation ) * position.x + cos( rotation ) * position.y;\n}\ngl_Position = vec4( ( pos * scale + screenPosition.xy ).xy, screenPosition.z, 1.0 );\n}',
              fragmentShader:
                'uniform lowp int renderType;\nuniform sampler2D map;\nuniform float opacity;\nuniform vec3 color;\nvarying vec2 vUV;\nvarying float vVisibility;\nvoid main() {\nif ( renderType == 0 ) {\ngl_FragColor = vec4( 1.0, 0.0, 1.0, 0.0 );\n} else if ( renderType == 1 ) {\ngl_FragColor = texture2D( map, vUV );\n} else {\nvec4 texture = texture2D( map, vUV );\ntexture.a *= opacity * vVisibility;\ngl_FragColor = texture;\ngl_FragColor.rgb *= color;\n}\n}'
            }),
            U = _.createProgram(),
            D = _.createShader(_.FRAGMENT_SHADER),
            N = _.createShader(_.VERTEX_SHADER),
            F = 'precision ' + t.getPrecision() + ' float;\n';
          _.shaderSource(D, F + L.fragmentShader),
            _.shaderSource(N, F + L.vertexShader),
            _.compileShader(D),
            _.compileShader(N),
            _.attachShader(U, D),
            _.attachShader(U, N),
            _.linkProgram(U),
            (v = U),
            (d = _.getAttribLocation(v, 'position')),
            (p = _.getAttribLocation(v, 'uv')),
            (n = _.getUniformLocation(v, 'renderType')),
            (r = _.getUniformLocation(v, 'map')),
            (a = _.getUniformLocation(v, 'occlusionMap')),
            (o = _.getUniformLocation(v, 'opacity')),
            (s = _.getUniformLocation(v, 'color')),
            (h = _.getUniformLocation(v, 'scale')),
            (l = _.getUniformLocation(v, 'rotation')),
            (u = _.getUniformLocation(v, 'screenPosition'));
        }
        for (
          _.useProgram(v),
            b.initAttributes(),
            b.enableAttribute(d),
            b.enableAttribute(p),
            b.disableUnusedAttributes(),
            _.uniform1i(a, 0),
            _.uniform1i(r, 1),
            _.bindBuffer(_.ARRAY_BUFFER, f),
            _.vertexAttribPointer(d, 2, _.FLOAT, !1, 16, 0),
            _.vertexAttribPointer(p, 2, _.FLOAT, !1, 16, 8),
            _.bindBuffer(_.ELEMENT_ARRAY_BUFFER, m),
            b.disable(_.CULL_FACE),
            b.setDepthWrite(!1),
            U = 0,
            D = e.length;
          U < D;
          U++
        )
          if (
            ((L = 16 / E.w),
            R.set(L * S, L),
            (N = e[U]),
            w.set(
              N.matrixWorld.elements[12],
              N.matrixWorld.elements[13],
              N.matrixWorld.elements[14]
            ),
            w.applyMatrix4(M.matrixWorldInverse),
            w.applyProjection(M.projectionMatrix),
            P.copy(w),
            (C.x = E.x + P.x * T + T - 8),
            (C.y = E.y + P.y * A + A - 8),
            !0 === I.containsPoint(C))
          ) {
            b.activeTexture(_.TEXTURE0),
              b.bindTexture(_.TEXTURE_2D, null),
              b.activeTexture(_.TEXTURE1),
              b.bindTexture(_.TEXTURE_2D, y),
              _.copyTexImage2D(_.TEXTURE_2D, 0, _.RGB, C.x, C.y, 16, 16, 0),
              _.uniform1i(n, 0),
              _.uniform2f(h, R.x, R.y),
              _.uniform3f(u, P.x, P.y, P.z),
              b.disable(_.BLEND),
              b.enable(_.DEPTH_TEST),
              _.drawElements(_.TRIANGLES, 6, _.UNSIGNED_SHORT, 0),
              b.activeTexture(_.TEXTURE0),
              b.bindTexture(_.TEXTURE_2D, x),
              _.copyTexImage2D(_.TEXTURE_2D, 0, _.RGBA, C.x, C.y, 16, 16, 0),
              _.uniform1i(n, 1),
              b.disable(_.DEPTH_TEST),
              b.activeTexture(_.TEXTURE1),
              b.bindTexture(_.TEXTURE_2D, y),
              _.drawElements(_.TRIANGLES, 6, _.UNSIGNED_SHORT, 0),
              N.positionScreen.copy(P),
              N.customUpdateCallback
                ? N.customUpdateCallback(N)
                : N.updateLensFlares(),
              _.uniform1i(n, 2),
              b.enable(_.BLEND);
            for (var F = 0, O = N.lensFlares.length; F < O; F++) {
              var z = N.lensFlares[F];
              0.001 < z.opacity &&
                0.001 < z.scale &&
                ((P.x = z.x),
                (P.y = z.y),
                (P.z = z.z),
                (L = z.size * z.scale / E.w),
                (R.x = L * S),
                (R.y = L),
                _.uniform3f(u, P.x, P.y, P.z),
                _.uniform2f(h, R.x, R.y),
                _.uniform1f(l, z.rotation),
                _.uniform1f(o, z.opacity),
                _.uniform3f(s, z.color.r, z.color.g, z.color.b),
                b.setBlending(
                  z.blending,
                  z.blendEquation,
                  z.blendSrc,
                  z.blendDst
                ),
                t.setTexture2D(z.texture, 1),
                _.drawElements(_.TRIANGLES, 6, _.UNSIGNED_SHORT, 0));
            }
          }
        b.enable(_.CULL_FACE),
          b.enable(_.DEPTH_TEST),
          b.setDepthWrite(!0),
          t.resetGLState();
      }
    };
  }
  function W(t, e) {
    function i(t, e) {
      return t.renderOrder !== e.renderOrder
        ? t.renderOrder - e.renderOrder
        : t.z !== e.z ? e.z - t.z : e.id - t.id;
    }
    var r,
      a,
      o,
      h,
      l,
      u,
      d,
      p,
      f,
      m,
      g,
      v,
      y,
      x,
      _,
      b,
      w,
      M,
      E,
      S,
      T,
      A = t.context,
      L = t.state,
      R = new c(),
      P = new s(),
      C = new c();
    this.render = function(s, c) {
      if (0 !== e.length) {
        if (void 0 === S) {
          var I = new Float32Array([
              -0.5,
              -0.5,
              0,
              0,
              0.5,
              -0.5,
              1,
              0,
              0.5,
              0.5,
              1,
              1,
              -0.5,
              0.5,
              0,
              1
            ]),
            U = new Uint16Array([0, 1, 2, 0, 2, 3]);
          (M = A.createBuffer()),
            (E = A.createBuffer()),
            A.bindBuffer(A.ARRAY_BUFFER, M),
            A.bufferData(A.ARRAY_BUFFER, I, A.STATIC_DRAW),
            A.bindBuffer(A.ELEMENT_ARRAY_BUFFER, E),
            A.bufferData(A.ELEMENT_ARRAY_BUFFER, U, A.STATIC_DRAW);
          var I = A.createProgram(),
            U = A.createShader(A.VERTEX_SHADER),
            D = A.createShader(A.FRAGMENT_SHADER);
          A.shaderSource(
            U,
            [
              'precision ' + t.getPrecision() + ' float;',
              'uniform mat4 modelViewMatrix;\nuniform mat4 projectionMatrix;\nuniform float rotation;\nuniform vec2 scale;\nuniform vec2 uvOffset;\nuniform vec2 uvScale;\nattribute vec2 position;\nattribute vec2 uv;\nvarying vec2 vUV;\nvoid main() {\nvUV = uvOffset + uv * uvScale;\nvec2 alignedPosition = position * scale;\nvec2 rotatedPosition;\nrotatedPosition.x = cos( rotation ) * alignedPosition.x - sin( rotation ) * alignedPosition.y;\nrotatedPosition.y = sin( rotation ) * alignedPosition.x + cos( rotation ) * alignedPosition.y;\nvec4 finalPosition;\nfinalPosition = modelViewMatrix * vec4( 0.0, 0.0, 0.0, 1.0 );\nfinalPosition.xy += rotatedPosition;\nfinalPosition = projectionMatrix * finalPosition;\ngl_Position = finalPosition;\n}'
            ].join('\n')
          ),
            A.shaderSource(
              D,
              [
                'precision ' + t.getPrecision() + ' float;',
                'uniform vec3 color;\nuniform sampler2D map;\nuniform float opacity;\nuniform int fogType;\nuniform vec3 fogColor;\nuniform float fogDensity;\nuniform float fogNear;\nuniform float fogFar;\nuniform float alphaTest;\nvarying vec2 vUV;\nvoid main() {\nvec4 texture = texture2D( map, vUV );\nif ( texture.a < alphaTest ) discard;\ngl_FragColor = vec4( color * texture.xyz, texture.a * opacity );\nif ( fogType > 0 ) {\nfloat depth = gl_FragCoord.z / gl_FragCoord.w;\nfloat fogFactor = 0.0;\nif ( fogType == 1 ) {\nfogFactor = smoothstep( fogNear, fogFar, depth );\n} else {\nconst float LOG2 = 1.442695;\nfogFactor = exp2( - fogDensity * fogDensity * depth * depth * LOG2 );\nfogFactor = 1.0 - clamp( fogFactor, 0.0, 1.0 );\n}\ngl_FragColor = mix( gl_FragColor, vec4( fogColor, gl_FragColor.w ), fogFactor );\n}\n}'
              ].join('\n')
            ),
            A.compileShader(U),
            A.compileShader(D),
            A.attachShader(I, U),
            A.attachShader(I, D),
            A.linkProgram(I),
            (S = I),
            (b = A.getAttribLocation(S, 'position')),
            (w = A.getAttribLocation(S, 'uv')),
            (r = A.getUniformLocation(S, 'uvOffset')),
            (a = A.getUniformLocation(S, 'uvScale')),
            (o = A.getUniformLocation(S, 'rotation')),
            (h = A.getUniformLocation(S, 'scale')),
            (l = A.getUniformLocation(S, 'color')),
            (u = A.getUniformLocation(S, 'map')),
            (d = A.getUniformLocation(S, 'opacity')),
            (p = A.getUniformLocation(S, 'modelViewMatrix')),
            (f = A.getUniformLocation(S, 'projectionMatrix')),
            (m = A.getUniformLocation(S, 'fogType')),
            (g = A.getUniformLocation(S, 'fogDensity')),
            (v = A.getUniformLocation(S, 'fogNear')),
            (y = A.getUniformLocation(S, 'fogFar')),
            (x = A.getUniformLocation(S, 'fogColor')),
            (_ = A.getUniformLocation(S, 'alphaTest')),
            (I = document.createElementNS(
              'http://www.w3.org/1999/xhtml',
              'canvas'
            )),
            (I.width = 8),
            (I.height = 8),
            (U = I.getContext('2d')),
            (U.fillStyle = 'white'),
            U.fillRect(0, 0, 8, 8),
            (T = new n(I)),
            (T.needsUpdate = !0);
        }
        A.useProgram(S),
          L.initAttributes(),
          L.enableAttribute(b),
          L.enableAttribute(w),
          L.disableUnusedAttributes(),
          L.disable(A.CULL_FACE),
          L.enable(A.BLEND),
          A.bindBuffer(A.ARRAY_BUFFER, M),
          A.vertexAttribPointer(b, 2, A.FLOAT, !1, 16, 0),
          A.vertexAttribPointer(w, 2, A.FLOAT, !1, 16, 8),
          A.bindBuffer(A.ELEMENT_ARRAY_BUFFER, E),
          A.uniformMatrix4fv(f, !1, c.projectionMatrix.elements),
          L.activeTexture(A.TEXTURE0),
          A.uniform1i(u, 0),
          (U = I = 0),
          (D = s.fog)
            ? (A.uniform3f(x, D.color.r, D.color.g, D.color.b),
              D && D.isFog
                ? (A.uniform1f(v, D.near),
                  A.uniform1f(y, D.far),
                  A.uniform1i(m, 1),
                  (U = I = 1))
                : D &&
                  D.isFogExp2 &&
                  (A.uniform1f(g, D.density), A.uniform1i(m, 2), (U = I = 2)))
            : (A.uniform1i(m, 0), (U = I = 0));
        for (var D = 0, N = e.length; D < N; D++) {
          var F = e[D];
          F.modelViewMatrix.multiplyMatrices(
            c.matrixWorldInverse,
            F.matrixWorld
          ),
            (F.z = -F.modelViewMatrix.elements[14]);
        }
        e.sort(i);
        for (var O = [], D = 0, N = e.length; D < N; D++) {
          var F = e[D],
            z = F.material;
          !1 !== z.visible &&
            (A.uniform1f(_, z.alphaTest),
            A.uniformMatrix4fv(p, !1, F.modelViewMatrix.elements),
            F.matrixWorld.decompose(R, P, C),
            (O[0] = C.x),
            (O[1] = C.y),
            (F = 0),
            s.fog && z.fog && (F = U),
            I !== F && (A.uniform1i(m, F), (I = F)),
            null !== z.map
              ? (A.uniform2f(r, z.map.offset.x, z.map.offset.y),
                A.uniform2f(a, z.map.repeat.x, z.map.repeat.y))
              : (A.uniform2f(r, 0, 0), A.uniform2f(a, 1, 1)),
            A.uniform1f(d, z.opacity),
            A.uniform3f(l, z.color.r, z.color.g, z.color.b),
            A.uniform1f(o, z.rotation),
            A.uniform2fv(h, O),
            L.setBlending(z.blending, z.blendEquation, z.blendSrc, z.blendDst),
            L.setDepthTest(z.depthTest),
            L.setDepthWrite(z.depthWrite),
            z.map ? t.setTexture2D(z.map, 0) : t.setTexture2D(T, 0),
            A.drawElements(A.TRIANGLES, 6, A.UNSIGNED_SHORT, 0));
        }
        L.enable(A.CULL_FACE), t.resetGLState();
      }
    };
  }
  function X() {
    Object.defineProperty(this, 'id', { value: Yn++ }),
      (this.uuid = t.Math.generateUUID()),
      (this.name = ''),
      (this.type = 'Material'),
      (this.lights = this.fog = !0),
      (this.blending = 1),
      (this.side = 0),
      (this.shading = 2),
      (this.vertexColors = 0),
      (this.opacity = 1),
      (this.transparent = !1),
      (this.blendSrc = 204),
      (this.blendDst = 205),
      (this.blendEquation = 100),
      (this.blendEquationAlpha = this.blendDstAlpha = this.blendSrcAlpha = null),
      (this.depthFunc = 3),
      (this.depthWrite = this.depthTest = !0),
      (this.clippingPlanes = null),
      (this.clipShadows = !1),
      (this.colorWrite = !0),
      (this.precision = null),
      (this.polygonOffset = !1),
      (this.alphaTest = this.polygonOffsetUnits = this.polygonOffsetFactor = 0),
      (this.premultipliedAlpha = !1),
      (this.overdraw = 0),
      (this._needsUpdate = this.visible = !0);
  }
  function q(t) {
    X.call(this),
      (this.type = 'ShaderMaterial'),
      (this.defines = {}),
      (this.uniforms = {}),
      (this.vertexShader =
        'void main() {\n\tgl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n}'),
      (this.fragmentShader =
        'void main() {\n\tgl_FragColor = vec4( 1.0, 0.0, 0.0, 1.0 );\n}'),
      (this.linewidth = 1),
      (this.wireframe = !1),
      (this.wireframeLinewidth = 1),
      (this.morphNormals = this.morphTargets = this.skinning = this.clipping = this.lights = this.fog = !1),
      (this.extensions = {
        derivatives: !1,
        fragDepth: !1,
        drawBuffers: !1,
        shaderTextureLOD: !1
      }),
      (this.defaultAttributeValues = {
        color: [1, 1, 1],
        uv: [0, 0],
        uv2: [0, 0]
      }),
      (this.index0AttributeName = void 0),
      void 0 !== t &&
        (void 0 !== t.attributes &&
          console.error(
            'THREE.ShaderMaterial: attributes should now be defined in THREE.BufferGeometry instead.'
          ),
        this.setValues(t));
  }
  function Y(t) {
    X.call(this),
      (this.type = 'MeshDepthMaterial'),
      (this.depthPacking = 3200),
      (this.morphTargets = this.skinning = !1),
      (this.displacementMap = this.alphaMap = this.map = null),
      (this.displacementScale = 1),
      (this.displacementBias = 0),
      (this.wireframe = !1),
      (this.wireframeLinewidth = 1),
      (this.lights = this.fog = !1),
      this.setValues(t);
  }
  function Z(t, e) {
    (this.min = void 0 !== t ? t : new c(Infinity, Infinity, Infinity)),
      (this.max = void 0 !== e ? e : new c(-Infinity, -Infinity, -Infinity));
  }
  function J(t, e) {
    (this.center = void 0 !== t ? t : new c()),
      (this.radius = void 0 !== e ? e : 0);
  }
  function Q() {
    (this.elements = new Float32Array([1, 0, 0, 0, 1, 0, 0, 0, 1])),
      0 < arguments.length &&
        console.error(
          'THREE.Matrix3: the constructor no longer reads arguments. use .set() instead.'
        );
  }
  function K(t, e) {
    (this.normal = void 0 !== t ? t : new c(1, 0, 0)),
      (this.constant = void 0 !== e ? e : 0);
  }
  function $(t, e, i, n, r, a) {
    this.planes = [
      void 0 !== t ? t : new K(),
      void 0 !== e ? e : new K(),
      void 0 !== i ? i : new K(),
      void 0 !== n ? n : new K(),
      void 0 !== r ? r : new K(),
      void 0 !== a ? a : new K()
    ];
  }
  function tt(e, n, o, s) {
    function l(t, i, n, r) {
      var a,
        o = t.geometry;
      a = w;
      var s = t.customDepthMaterial;
      return (
        n && ((a = M), (s = t.customDistanceMaterial)),
        s
          ? (a = s)
          : ((s = !1),
            i.morphTargets &&
              (o && o.isBufferGeometry
                ? (s =
                    o.morphAttributes &&
                    o.morphAttributes.position &&
                    0 < o.morphAttributes.position.length)
                : o &&
                  o.isGeometry &&
                  (s = o.morphTargets && 0 < o.morphTargets.length)),
            (t = t.isSkinnedMesh && i.skinning),
            (o = 0),
            s && (o |= 1),
            t && (o |= 2),
            (a = a[o])),
        e.localClippingEnabled &&
          !0 === i.clipShadows &&
          0 !== i.clippingPlanes.length &&
          ((o = a.uuid),
          (s = i.uuid),
          (t = E[o]),
          void 0 === t && ((t = {}), (E[o] = t)),
          (o = t[s]),
          void 0 === o && ((o = a.clone()), (t[s] = o)),
          (a = o)),
        (a.visible = i.visible),
        (a.wireframe = i.wireframe),
        (s = i.side),
        U.renderSingleSided && 2 == s && (s = 0),
        U.renderReverseSided && (0 === s ? (s = 1) : 1 === s && (s = 0)),
        (a.side = s),
        (a.clipShadows = i.clipShadows),
        (a.clippingPlanes = i.clippingPlanes),
        (a.wireframeLinewidth = i.wireframeLinewidth),
        (a.linewidth = i.linewidth),
        n &&
          void 0 !== a.uniforms.lightPos &&
          a.uniforms.lightPos.value.copy(r),
        a
      );
    }
    function u(t, e, i) {
      if (!1 !== t.visible) {
        0 != (t.layers.mask & e.layers.mask) &&
          (t.isMesh || t.isLine || t.isPoints) &&
          t.castShadow &&
          (!1 === t.frustumCulled || !0 === f.intersectsObject(t)) &&
          !0 === t.material.visible &&
          (t.modelViewMatrix.multiplyMatrices(
            i.matrixWorldInverse,
            t.matrixWorld
          ),
          b.push(t)),
          (t = t.children);
        for (var n = 0, r = t.length; n < r; n++) u(t[n], e, i);
      }
    }
    var d = e.context,
      p = e.state,
      f = new $(),
      m = new h(),
      g = n.shadows,
      v = new i(),
      y = new i(s.maxTextureSize, s.maxTextureSize),
      x = new c(),
      _ = new c(),
      b = [],
      w = Array(4),
      M = Array(4),
      E = {},
      S = [
        new c(1, 0, 0),
        new c(-1, 0, 0),
        new c(0, 0, 1),
        new c(0, 0, -1),
        new c(0, 1, 0),
        new c(0, -1, 0)
      ],
      T = [
        new c(0, 1, 0),
        new c(0, 1, 0),
        new c(0, 1, 0),
        new c(0, 1, 0),
        new c(0, 0, 1),
        new c(0, 0, -1)
      ],
      A = [new r(), new r(), new r(), new r(), new r(), new r()];
    (n = new Y()),
      (n.depthPacking = 3201),
      (n.clipping = !0),
      (s = qn.distanceRGBA);
    for (var L = t.UniformsUtils.clone(s.uniforms), R = 0; 4 !== R; ++R) {
      var P = 0 != (1 & R),
        C = 0 != (2 & R),
        I = n.clone();
      (I.morphTargets = P),
        (I.skinning = C),
        (w[R] = I),
        (P = new q({
          defines: { USE_SHADOWMAP: '' },
          uniforms: L,
          vertexShader: s.vertexShader,
          fragmentShader: s.fragmentShader,
          morphTargets: P,
          skinning: C,
          clipping: !0
        })),
        (M[R] = P);
    }
    var U = this;
    (this.enabled = !1),
      (this.autoUpdate = !0),
      (this.needsUpdate = !1),
      (this.type = 1),
      (this.renderSingleSided = this.renderReverseSided = !0),
      (this.render = function(t, i) {
        if (
          !1 !== U.enabled &&
          (!1 !== U.autoUpdate || !1 !== U.needsUpdate) &&
          0 !== g.length
        ) {
          p.clearColor(1, 1, 1, 1),
            p.disable(d.BLEND),
            p.setDepthTest(!0),
            p.setScissorTest(!1);
          for (var n, r, s = 0, c = g.length; s < c; s++) {
            var h = g[s],
              w = h.shadow;
            if (void 0 === w)
              console.warn('THREE.WebGLShadowMap:', h, 'has no shadow.');
            else {
              var M = w.camera;
              if ((v.copy(w.mapSize), v.min(y), h && h.isPointLight)) {
                (n = 6), (r = !0);
                var E = v.x,
                  L = v.y;
                A[0].set(2 * E, L, E, L),
                  A[1].set(0, L, E, L),
                  A[2].set(3 * E, L, E, L),
                  A[3].set(E, L, E, L),
                  A[4].set(3 * E, 0, E, L),
                  A[5].set(E, 0, E, L),
                  (v.x *= 4),
                  (v.y *= 2);
              } else (n = 1), (r = !1);
              for (
                null === w.map &&
                  ((w.map = new a(v.x, v.y, {
                    minFilter: 1003,
                    magFilter: 1003,
                    format: 1023
                  })),
                  M.updateProjectionMatrix()),
                  w && w.isSpotLightShadow && w.update(h),
                  E = w.map,
                  w = w.matrix,
                  _.setFromMatrixPosition(h.matrixWorld),
                  M.position.copy(_),
                  e.setRenderTarget(E),
                  e.clear(),
                  E = 0;
                E < n;
                E++
              ) {
                r
                  ? (x.copy(M.position),
                    x.add(S[E]),
                    M.up.copy(T[E]),
                    M.lookAt(x),
                    p.viewport(A[E]))
                  : (x.setFromMatrixPosition(h.target.matrixWorld),
                    M.lookAt(x)),
                  M.updateMatrixWorld(),
                  M.matrixWorldInverse.getInverse(M.matrixWorld),
                  w.set(
                    0.5,
                    0,
                    0,
                    0.5,
                    0,
                    0.5,
                    0,
                    0.5,
                    0,
                    0,
                    0.5,
                    0.5,
                    0,
                    0,
                    0,
                    1
                  ),
                  w.multiply(M.projectionMatrix),
                  w.multiply(M.matrixWorldInverse),
                  m.multiplyMatrices(M.projectionMatrix, M.matrixWorldInverse),
                  f.setFromMatrix(m),
                  (b.length = 0),
                  u(t, i, M);
                for (var L = 0, R = b.length; L < R; L++) {
                  var P = b[L],
                    C = o.update(P),
                    I = P.material;
                  if (I && I.isMultiMaterial)
                    for (
                      var D = C.groups, I = I.materials, N = 0, F = D.length;
                      N < F;
                      N++
                    ) {
                      var O = D[N],
                        z = I[O.materialIndex];
                      !0 === z.visible &&
                        ((z = l(P, z, r, _)),
                        e.renderBufferDirect(M, null, C, z, P, O));
                    }
                  else
                    (z = l(P, I, r, _)),
                      e.renderBufferDirect(M, null, C, z, P, null);
                }
              }
            }
          }
          (n = e.getClearColor()),
            (r = e.getClearAlpha()),
            e.setClearColor(n, r),
            (U.needsUpdate = !1);
        }
      });
  }
  function et(t, e) {
    (this.origin = void 0 !== t ? t : new c()),
      (this.direction = void 0 !== e ? e : new c());
  }
  function it(t, e, i, n) {
    (this._x = t || 0),
      (this._y = e || 0),
      (this._z = i || 0),
      (this._order = n || it.DefaultOrder);
  }
  function nt() {
    this.mask = 1;
  }
  function rt() {
    Object.defineProperty(this, 'id', { value: Zn++ }),
      (this.uuid = t.Math.generateUUID()),
      (this.name = ''),
      (this.type = 'Object3D'),
      (this.parent = null),
      (this.children = []),
      (this.up = rt.DefaultUp.clone());
    var e = new c(),
      i = new it(),
      n = new s(),
      r = new c(1, 1, 1);
    i.onChange(function() {
      n.setFromEuler(i, !1);
    }),
      n.onChange(function() {
        i.setFromQuaternion(n, void 0, !1);
      }),
      Object.defineProperties(this, {
        position: { enumerable: !0, value: e },
        rotation: { enumerable: !0, value: i },
        quaternion: { enumerable: !0, value: n },
        scale: { enumerable: !0, value: r },
        modelViewMatrix: { value: new h() },
        normalMatrix: { value: new Q() }
      }),
      (this.matrix = new h()),
      (this.matrixWorld = new h()),
      (this.matrixAutoUpdate = rt.DefaultMatrixAutoUpdate),
      (this.matrixWorldNeedsUpdate = !1),
      (this.layers = new nt()),
      (this.visible = !0),
      (this.receiveShadow = this.castShadow = !1),
      (this.frustumCulled = !0),
      (this.renderOrder = 0),
      (this.userData = {}),
      (this.onBeforeRender = null);
  }
  function at(t, e) {
    (this.start = void 0 !== t ? t : new c()),
      (this.end = void 0 !== e ? e : new c());
  }
  function ot(t, e, i) {
    (this.a = void 0 !== t ? t : new c()),
      (this.b = void 0 !== e ? e : new c()),
      (this.c = void 0 !== i ? i : new c());
  }
  function st(t, e, i, n, r, a) {
    (this.a = t),
      (this.b = e),
      (this.c = i),
      (this.normal = n && n.isVector3 ? n : new c()),
      (this.vertexNormals = Array.isArray(n) ? n : []),
      (this.color = r && r.isColor ? r : new V()),
      (this.vertexColors = Array.isArray(r) ? r : []),
      (this.materialIndex = void 0 !== a ? a : 0);
  }
  function ct(t) {
    X.call(this),
      (this.type = 'MeshBasicMaterial'),
      (this.color = new V(16777215)),
      (this.aoMap = this.map = null),
      (this.aoMapIntensity = 1),
      (this.envMap = this.alphaMap = this.specularMap = null),
      (this.combine = 0),
      (this.reflectivity = 1),
      (this.refractionRatio = 0.98),
      (this.wireframe = !1),
      (this.wireframeLinewidth = 1),
      (this.wireframeLinejoin = this.wireframeLinecap = 'round'),
      (this.lights = this.morphTargets = this.skinning = !1),
      this.setValues(t);
  }
  function ht(e, i, n) {
    if (Array.isArray(e))
      throw new TypeError(
        'THREE.BufferAttribute: array should be a Typed Array.'
      );
    (this.uuid = t.Math.generateUUID()),
      (this.array = e),
      (this.itemSize = i),
      (this.count = void 0 !== e ? e.length / i : 0),
      (this.normalized = !0 === n),
      (this.dynamic = !1),
      (this.updateRange = { offset: 0, count: -1 }),
      (this.version = 0);
  }
  function lt(t, e) {
    return new ht(new Uint16Array(t), e);
  }
  function ut(t, e) {
    return new ht(new Uint32Array(t), e);
  }
  function dt(t, e) {
    return new ht(new Float32Array(t), e);
  }
  function pt() {
    Object.defineProperty(this, 'id', { value: Jn++ }),
      (this.uuid = t.Math.generateUUID()),
      (this.name = ''),
      (this.type = 'Geometry'),
      (this.vertices = []),
      (this.colors = []),
      (this.faces = []),
      (this.faceVertexUvs = [[]]),
      (this.morphTargets = []),
      (this.morphNormals = []),
      (this.skinWeights = []),
      (this.skinIndices = []),
      (this.lineDistances = []),
      (this.boundingSphere = this.boundingBox = null),
      (this.groupsNeedUpdate = this.lineDistancesNeedUpdate = this.colorsNeedUpdate = this.normalsNeedUpdate = this.uvsNeedUpdate = this.verticesNeedUpdate = this.elementsNeedUpdate = !1);
  }
  function ft() {
    Object.defineProperty(this, 'id', { value: Jn++ }),
      (this.uuid = t.Math.generateUUID()),
      (this.name = ''),
      (this.type = 'DirectGeometry'),
      (this.indices = []),
      (this.vertices = []),
      (this.normals = []),
      (this.colors = []),
      (this.uvs = []),
      (this.uvs2 = []),
      (this.groups = []),
      (this.morphTargets = {}),
      (this.skinWeights = []),
      (this.skinIndices = []),
      (this.boundingSphere = this.boundingBox = null),
      (this.groupsNeedUpdate = this.uvsNeedUpdate = this.colorsNeedUpdate = this.normalsNeedUpdate = this.verticesNeedUpdate = !1);
  }
  function mt() {
    Object.defineProperty(this, 'id', { value: Jn++ }),
      (this.uuid = t.Math.generateUUID()),
      (this.name = ''),
      (this.type = 'BufferGeometry'),
      (this.index = null),
      (this.attributes = {}),
      (this.morphAttributes = {}),
      (this.groups = []),
      (this.boundingSphere = this.boundingBox = null),
      (this.drawRange = { start: 0, count: Infinity });
  }
  function gt(t, e) {
    rt.call(this),
      (this.type = 'Mesh'),
      (this.geometry = void 0 !== t ? t : new mt()),
      (this.material =
        void 0 !== e ? e : new ct({ color: 16777215 * Math.random() })),
      (this.drawMode = 0),
      this.updateMorphTargets();
  }
  function vt(t, e, i, n, r, a) {
    function o(t, e, i, n, r, a, o, h, l, _, b) {
      var w = a / l,
        M = o / _,
        E = a / 2,
        S = o / 2,
        T = h / 2;
      o = l + 1;
      for (var A = _ + 1, L = (a = 0), R = new c(), P = 0; P < A; P++)
        for (var C = P * M - S, I = 0; I < o; I++)
          (R[t] = (I * w - E) * n),
            (R[e] = C * r),
            (R[i] = T),
            (d[m] = R.x),
            (d[m + 1] = R.y),
            (d[m + 2] = R.z),
            (R[t] = 0),
            (R[e] = 0),
            (R[i] = 0 < h ? 1 : -1),
            (p[m] = R.x),
            (p[m + 1] = R.y),
            (p[m + 2] = R.z),
            (f[g] = I / l),
            (f[g + 1] = 1 - P / _),
            (m += 3),
            (g += 2),
            (a += 1);
      for (P = 0; P < _; P++)
        for (I = 0; I < l; I++)
          (t = y + I + o * (P + 1)),
            (e = y + (I + 1) + o * (P + 1)),
            (i = y + (I + 1) + o * P),
            (u[v] = y + I + o * P),
            (u[v + 1] = t),
            (u[v + 2] = i),
            (u[v + 3] = t),
            (u[v + 4] = e),
            (u[v + 5] = i),
            (v += 6),
            (L += 6);
      s.addGroup(x, L, b), (x += L), (y += a);
    }
    mt.call(this),
      (this.type = 'BoxBufferGeometry'),
      (this.parameters = {
        width: t,
        height: e,
        depth: i,
        widthSegments: n,
        heightSegments: r,
        depthSegments: a
      });
    var s = this;
    (n = Math.floor(n) || 1),
      (r = Math.floor(r) || 1),
      (a = Math.floor(a) || 1);
    var h = (function(t, e, i) {
        return (t =
          0 +
          (t + 1) * (e + 1) * 2 +
          (t + 1) * (i + 1) * 2 +
          (i + 1) * (e + 1) * 2);
      })(n, r, a),
      l = (function(t, e, i) {
        return 6 * (t = 0 + t * e * 2 + t * i * 2 + i * e * 2);
      })(n, r, a),
      u = new (65535 < l ? Uint32Array : Uint16Array)(l),
      d = new Float32Array(3 * h),
      p = new Float32Array(3 * h),
      f = new Float32Array(2 * h),
      m = 0,
      g = 0,
      v = 0,
      y = 0,
      x = 0;
    o('z', 'y', 'x', -1, -1, i, e, t, a, r, 0),
      o('z', 'y', 'x', 1, -1, i, e, -t, a, r, 1),
      o('x', 'z', 'y', 1, 1, t, i, e, n, a, 2),
      o('x', 'z', 'y', 1, -1, t, i, -e, n, a, 3),
      o('x', 'y', 'z', 1, -1, t, e, i, n, r, 4),
      o('x', 'y', 'z', -1, -1, t, e, -i, n, r, 5),
      this.setIndex(new ht(u, 1)),
      this.addAttribute('position', new ht(d, 3)),
      this.addAttribute('normal', new ht(p, 3)),
      this.addAttribute('uv', new ht(f, 2));
  }
  function yt(t, e, i, n) {
    mt.call(this),
      (this.type = 'PlaneBufferGeometry'),
      (this.parameters = {
        width: t,
        height: e,
        widthSegments: i,
        heightSegments: n
      });
    var r = t / 2,
      a = e / 2;
    (i = Math.floor(i) || 1), (n = Math.floor(n) || 1);
    var o = i + 1,
      s = n + 1,
      c = t / i,
      h = e / n;
    (e = new Float32Array(o * s * 3)), (t = new Float32Array(o * s * 3));
    for (var l = new Float32Array(o * s * 2), u = 0, d = 0, p = 0; p < s; p++)
      for (var f = p * h - a, m = 0; m < o; m++)
        (e[u] = m * c - r),
          (e[u + 1] = -f),
          (t[u + 2] = 1),
          (l[d] = m / i),
          (l[d + 1] = 1 - p / n),
          (u += 3),
          (d += 2);
    for (
      u = 0,
        r = new (65535 < e.length / 3 ? Uint32Array : Uint16Array)(i * n * 6),
        p = 0;
      p < n;
      p++
    )
      for (m = 0; m < i; m++)
        (a = m + o * (p + 1)),
          (s = m + 1 + o * (p + 1)),
          (c = m + 1 + o * p),
          (r[u] = m + o * p),
          (r[u + 1] = a),
          (r[u + 2] = c),
          (r[u + 3] = a),
          (r[u + 4] = s),
          (r[u + 5] = c),
          (u += 6);
    this.setIndex(new ht(r, 1)),
      this.addAttribute('position', new ht(e, 3)),
      this.addAttribute('normal', new ht(t, 3)),
      this.addAttribute('uv', new ht(l, 2));
  }
  function xt() {
    rt.call(this),
      (this.type = 'Camera'),
      (this.matrixWorldInverse = new h()),
      (this.projectionMatrix = new h());
  }
  function _t(t, e, i, n) {
    xt.call(this),
      (this.type = 'PerspectiveCamera'),
      (this.fov = void 0 !== t ? t : 50),
      (this.zoom = 1),
      (this.near = void 0 !== i ? i : 0.1),
      (this.far = void 0 !== n ? n : 2e3),
      (this.focus = 10),
      (this.aspect = void 0 !== e ? e : 1),
      (this.view = null),
      (this.filmGauge = 35),
      (this.filmOffset = 0),
      this.updateProjectionMatrix();
  }
  function bt(t, e, i, n, r, a) {
    xt.call(this),
      (this.type = 'OrthographicCamera'),
      (this.zoom = 1),
      (this.view = null),
      (this.left = t),
      (this.right = e),
      (this.top = i),
      (this.bottom = n),
      (this.near = void 0 !== r ? r : 0.1),
      (this.far = void 0 !== a ? a : 2e3),
      this.updateProjectionMatrix();
  }
  function wt(t, e, i) {
    var n, r, a;
    return {
      setMode: function(t) {
        n = t;
      },
      setIndex: function(i) {
        i.array instanceof Uint32Array && e.get('OES_element_index_uint')
          ? ((r = t.UNSIGNED_INT), (a = 4))
          : ((r = t.UNSIGNED_SHORT), (a = 2));
      },
      render: function(e, o) {
        t.drawElements(n, o, r, e * a),
          i.calls++,
          (i.vertices += o),
          n === t.TRIANGLES && (i.faces += o / 3);
      },
      renderInstances: function(o, s, c) {
        var h = e.get('ANGLE_instanced_arrays');
        null === h
          ? console.error(
              'THREE.WebGLBufferRenderer: using THREE.InstancedBufferGeometry but hardware does not support extension ANGLE_instanced_arrays.'
            )
          : (h.drawElementsInstancedANGLE(n, c, r, s * a, o.maxInstancedCount),
            i.calls++,
            (i.vertices += c * o.maxInstancedCount),
            n === t.TRIANGLES && (i.faces += o.maxInstancedCount * c / 3));
      }
    };
  }
  function Mt(t, e, i) {
    var n;
    return {
      setMode: function(t) {
        n = t;
      },
      render: function(e, r) {
        t.drawArrays(n, e, r),
          i.calls++,
          (i.vertices += r),
          n === t.TRIANGLES && (i.faces += r / 3);
      },
      renderInstances: function(r) {
        var a = e.get('ANGLE_instanced_arrays');
        if (null === a)
          console.error(
            'THREE.WebGLBufferRenderer: using THREE.InstancedBufferGeometry but hardware does not support extension ANGLE_instanced_arrays.'
          );
        else {
          var o = r.attributes.position,
            o = o && o.isInterleavedBufferAttribute ? o.data.count : o.count;
          a.drawArraysInstancedANGLE(n, 0, o, r.maxInstancedCount),
            i.calls++,
            (i.vertices += o * r.maxInstancedCount),
            n === t.TRIANGLES && (i.faces += r.maxInstancedCount * o / 3);
        }
      }
    };
  }
  function Et() {
    var t = {};
    return {
      get: function(e) {
        if (void 0 !== t[e.id]) return t[e.id];
        var n;
        switch (e.type) {
          case 'DirectionalLight':
            n = {
              direction: new c(),
              color: new V(),
              shadow: !1,
              shadowBias: 0,
              shadowRadius: 1,
              shadowMapSize: new i()
            };
            break;
          case 'SpotLight':
            n = {
              position: new c(),
              direction: new c(),
              color: new V(),
              distance: 0,
              coneCos: 0,
              penumbraCos: 0,
              decay: 0,
              shadow: !1,
              shadowBias: 0,
              shadowRadius: 1,
              shadowMapSize: new i()
            };
            break;
          case 'PointLight':
            n = {
              position: new c(),
              color: new V(),
              distance: 0,
              decay: 0,
              shadow: !1,
              shadowBias: 0,
              shadowRadius: 1,
              shadowMapSize: new i()
            };
            break;
          case 'HemisphereLight':
            n = { direction: new c(), skyColor: new V(), groundColor: new V() };
        }
        return (t[e.id] = n);
      }
    };
  }
  function St(t) {
    t = t.split('\n');
    for (var e = 0; e < t.length; e++) t[e] = e + 1 + ': ' + t[e];
    return t.join('\n');
  }
  function Tt(t, e, i) {
    var n = t.createShader(e);
    return (
      t.shaderSource(n, i),
      t.compileShader(n),
      !1 === t.getShaderParameter(n, t.COMPILE_STATUS) &&
        console.error("THREE.WebGLShader: Shader couldn't compile."),
      '' !== t.getShaderInfoLog(n) &&
        console.warn(
          'THREE.WebGLShader: gl.getShaderInfoLog()',
          e === t.VERTEX_SHADER ? 'vertex' : 'fragment',
          t.getShaderInfoLog(n),
          St(i)
        ),
      n
    );
  }
  function At(t) {
    switch (t) {
      case 3e3:
        return ['Linear', '( value )'];
      case 3001:
        return ['sRGB', '( value )'];
      case 3002:
        return ['RGBE', '( value )'];
      case 3004:
        return ['RGBM', '( value, 7.0 )'];
      case 3005:
        return ['RGBM', '( value, 16.0 )'];
      case 3006:
        return ['RGBD', '( value, 256.0 )'];
      case 3007:
        return ['Gamma', '( value, float( GAMMA_FACTOR ) )'];
      default:
        throw Error('unsupported encoding: ' + t);
    }
  }
  function Lt(t, e) {
    var i = At(e);
    return (
      'vec4 ' +
      t +
      '( vec4 value ) { return ' +
      i[0] +
      'ToLinear' +
      i[1] +
      '; }'
    );
  }
  function Rt(t, e) {
    var i = At(e);
    return (
      'vec4 ' + t + '( vec4 value ) { return LinearTo' + i[0] + i[1] + '; }'
    );
  }
  function Pt(t, e) {
    var i;
    switch (e) {
      case 1:
        i = 'Linear';
        break;
      case 2:
        i = 'Reinhard';
        break;
      case 3:
        i = 'Uncharted2';
        break;
      case 4:
        i = 'OptimizedCineon';
        break;
      default:
        throw Error('unsupported toneMapping: ' + e);
    }
    return (
      'vec3 ' + t + '( vec3 color ) { return ' + i + 'ToneMapping( color ); }'
    );
  }
  function Ct(t, e, i) {
    return (
      (t = t || {}),
      [
        t.derivatives ||
        e.envMapCubeUV ||
        e.bumpMap ||
        e.normalMap ||
        e.flatShading
          ? '#extension GL_OES_standard_derivatives : enable'
          : '',
        (t.fragDepth || e.logarithmicDepthBuffer) && i.get('EXT_frag_depth')
          ? '#extension GL_EXT_frag_depth : enable'
          : '',
        t.drawBuffers && i.get('WEBGL_draw_buffers')
          ? '#extension GL_EXT_draw_buffers : require'
          : '',
        (t.shaderTextureLOD || e.envMap) && i.get('EXT_shader_texture_lod')
          ? '#extension GL_EXT_shader_texture_lod : enable'
          : ''
      ]
        .filter(Ut)
        .join('\n')
    );
  }
  function It(t) {
    var e,
      i = [];
    for (e in t) {
      var n = t[e];
      !1 !== n && i.push('#define ' + e + ' ' + n);
    }
    return i.join('\n');
  }
  function Ut(t) {
    return '' !== t;
  }
  function Dt(t, e) {
    return t
      .replace(/NUM_DIR_LIGHTS/g, e.numDirLights)
      .replace(/NUM_SPOT_LIGHTS/g, e.numSpotLights)
      .replace(/NUM_POINT_LIGHTS/g, e.numPointLights)
      .replace(/NUM_HEMI_LIGHTS/g, e.numHemiLights);
  }
  function Nt(t) {
    return t.replace(/#include +<([\w\d.]+)>/g, function(t, e) {
      var i = Wn[e];
      if (void 0 === i) throw Error('Can not resolve #include <' + e + '>');
      return Nt(i);
    });
  }
  function Ft(t) {
    return t.replace(
      /for \( int i \= (\d+)\; i < (\d+)\; i \+\+ \) \{([\s\S]+?)(?=\})\}/g,
      function(t, e, i, n) {
        for (t = '', e = parseInt(e); e < parseInt(i); e++)
          t += n.replace(/\[ i \]/g, '[ ' + e + ' ]');
        return t;
      }
    );
  }
  function Ot(t, e, i, n) {
    var r = t.context,
      a = i.extensions,
      o = i.defines,
      s = i.__webglShader.vertexShader,
      c = i.__webglShader.fragmentShader,
      h = 'SHADOWMAP_TYPE_BASIC';
    1 === n.shadowMapType
      ? (h = 'SHADOWMAP_TYPE_PCF')
      : 2 === n.shadowMapType && (h = 'SHADOWMAP_TYPE_PCF_SOFT');
    var l = 'ENVMAP_TYPE_CUBE',
      u = 'ENVMAP_MODE_REFLECTION',
      d = 'ENVMAP_BLENDING_MULTIPLY';
    if (n.envMap) {
      switch (i.envMap.mapping) {
        case 301:
        case 302:
          l = 'ENVMAP_TYPE_CUBE';
          break;
        case 306:
        case 307:
          l = 'ENVMAP_TYPE_CUBE_UV';
          break;
        case 303:
        case 304:
          l = 'ENVMAP_TYPE_EQUIREC';
          break;
        case 305:
          l = 'ENVMAP_TYPE_SPHERE';
      }
      switch (i.envMap.mapping) {
        case 302:
        case 304:
          u = 'ENVMAP_MODE_REFRACTION';
      }
      switch (i.combine) {
        case 0:
          d = 'ENVMAP_BLENDING_MULTIPLY';
          break;
        case 1:
          d = 'ENVMAP_BLENDING_MIX';
          break;
        case 2:
          d = 'ENVMAP_BLENDING_ADD';
      }
    }
    var p = 0 < t.gammaFactor ? t.gammaFactor : 1,
      a = Ct(a, n, t.extensions),
      f = It(o),
      m = r.createProgram();
    i.isRawShaderMaterial
      ? ((o = [f, '\n'].filter(Ut).join('\n')),
        (h = [a, f, '\n'].filter(Ut).join('\n')))
      : ((o = [
          'precision ' + n.precision + ' float;',
          'precision ' + n.precision + ' int;',
          '#define SHADER_NAME ' + i.__webglShader.name,
          f,
          n.supportsVertexTextures ? '#define VERTEX_TEXTURES' : '',
          '#define GAMMA_FACTOR ' + p,
          '#define MAX_BONES ' + n.maxBones,
          n.map ? '#define USE_MAP' : '',
          n.envMap ? '#define USE_ENVMAP' : '',
          n.envMap ? '#define ' + u : '',
          n.lightMap ? '#define USE_LIGHTMAP' : '',
          n.aoMap ? '#define USE_AOMAP' : '',
          n.emissiveMap ? '#define USE_EMISSIVEMAP' : '',
          n.bumpMap ? '#define USE_BUMPMAP' : '',
          n.normalMap ? '#define USE_NORMALMAP' : '',
          n.displacementMap && n.supportsVertexTextures
            ? '#define USE_DISPLACEMENTMAP'
            : '',
          n.specularMap ? '#define USE_SPECULARMAP' : '',
          n.roughnessMap ? '#define USE_ROUGHNESSMAP' : '',
          n.metalnessMap ? '#define USE_METALNESSMAP' : '',
          n.alphaMap ? '#define USE_ALPHAMAP' : '',
          n.vertexColors ? '#define USE_COLOR' : '',
          n.flatShading ? '#define FLAT_SHADED' : '',
          n.skinning ? '#define USE_SKINNING' : '',
          n.useVertexTexture ? '#define BONE_TEXTURE' : '',
          n.morphTargets ? '#define USE_MORPHTARGETS' : '',
          n.morphNormals && !1 === n.flatShading
            ? '#define USE_MORPHNORMALS'
            : '',
          n.doubleSided ? '#define DOUBLE_SIDED' : '',
          n.flipSided ? '#define FLIP_SIDED' : '',
          '#define NUM_CLIPPING_PLANES ' + n.numClippingPlanes,
          n.shadowMapEnabled ? '#define USE_SHADOWMAP' : '',
          n.shadowMapEnabled ? '#define ' + h : '',
          n.sizeAttenuation ? '#define USE_SIZEATTENUATION' : '',
          n.logarithmicDepthBuffer ? '#define USE_LOGDEPTHBUF' : '',
          n.logarithmicDepthBuffer && t.extensions.get('EXT_frag_depth')
            ? '#define USE_LOGDEPTHBUF_EXT'
            : '',
          'uniform mat4 modelMatrix;',
          'uniform mat4 modelViewMatrix;',
          'uniform mat4 projectionMatrix;',
          'uniform mat4 viewMatrix;',
          'uniform mat3 normalMatrix;',
          'uniform vec3 cameraPosition;',
          'attribute vec3 position;',
          'attribute vec3 normal;',
          'attribute vec2 uv;',
          '#ifdef USE_COLOR',
          '\tattribute vec3 color;',
          '#endif',
          '#ifdef USE_MORPHTARGETS',
          '\tattribute vec3 morphTarget0;',
          '\tattribute vec3 morphTarget1;',
          '\tattribute vec3 morphTarget2;',
          '\tattribute vec3 morphTarget3;',
          '\t#ifdef USE_MORPHNORMALS',
          '\t\tattribute vec3 morphNormal0;',
          '\t\tattribute vec3 morphNormal1;',
          '\t\tattribute vec3 morphNormal2;',
          '\t\tattribute vec3 morphNormal3;',
          '\t#else',
          '\t\tattribute vec3 morphTarget4;',
          '\t\tattribute vec3 morphTarget5;',
          '\t\tattribute vec3 morphTarget6;',
          '\t\tattribute vec3 morphTarget7;',
          '\t#endif',
          '#endif',
          '#ifdef USE_SKINNING',
          '\tattribute vec4 skinIndex;',
          '\tattribute vec4 skinWeight;',
          '#endif',
          '\n'
        ]
          .filter(Ut)
          .join('\n')),
        (h = [
          a,
          'precision ' + n.precision + ' float;',
          'precision ' + n.precision + ' int;',
          '#define SHADER_NAME ' + i.__webglShader.name,
          f,
          n.alphaTest ? '#define ALPHATEST ' + n.alphaTest : '',
          '#define GAMMA_FACTOR ' + p,
          n.useFog && n.fog ? '#define USE_FOG' : '',
          n.useFog && n.fogExp ? '#define FOG_EXP2' : '',
          n.map ? '#define USE_MAP' : '',
          n.envMap ? '#define USE_ENVMAP' : '',
          n.envMap ? '#define ' + l : '',
          n.envMap ? '#define ' + u : '',
          n.envMap ? '#define ' + d : '',
          n.lightMap ? '#define USE_LIGHTMAP' : '',
          n.aoMap ? '#define USE_AOMAP' : '',
          n.emissiveMap ? '#define USE_EMISSIVEMAP' : '',
          n.bumpMap ? '#define USE_BUMPMAP' : '',
          n.normalMap ? '#define USE_NORMALMAP' : '',
          n.specularMap ? '#define USE_SPECULARMAP' : '',
          n.roughnessMap ? '#define USE_ROUGHNESSMAP' : '',
          n.metalnessMap ? '#define USE_METALNESSMAP' : '',
          n.alphaMap ? '#define USE_ALPHAMAP' : '',
          n.vertexColors ? '#define USE_COLOR' : '',
          n.flatShading ? '#define FLAT_SHADED' : '',
          n.doubleSided ? '#define DOUBLE_SIDED' : '',
          n.flipSided ? '#define FLIP_SIDED' : '',
          '#define NUM_CLIPPING_PLANES ' + n.numClippingPlanes,
          n.shadowMapEnabled ? '#define USE_SHADOWMAP' : '',
          n.shadowMapEnabled ? '#define ' + h : '',
          n.premultipliedAlpha ? '#define PREMULTIPLIED_ALPHA' : '',
          n.physicallyCorrectLights ? '#define PHYSICALLY_CORRECT_LIGHTS' : '',
          n.logarithmicDepthBuffer ? '#define USE_LOGDEPTHBUF' : '',
          n.logarithmicDepthBuffer && t.extensions.get('EXT_frag_depth')
            ? '#define USE_LOGDEPTHBUF_EXT'
            : '',
          n.envMap && t.extensions.get('EXT_shader_texture_lod')
            ? '#define TEXTURE_LOD_EXT'
            : '',
          'uniform mat4 viewMatrix;',
          'uniform vec3 cameraPosition;',
          0 !== n.toneMapping ? '#define TONE_MAPPING' : '',
          0 !== n.toneMapping ? Wn.tonemapping_pars_fragment : '',
          0 !== n.toneMapping ? Pt('toneMapping', n.toneMapping) : '',
          n.outputEncoding ||
          n.mapEncoding ||
          n.envMapEncoding ||
          n.emissiveMapEncoding
            ? Wn.encodings_pars_fragment
            : '',
          n.mapEncoding ? Lt('mapTexelToLinear', n.mapEncoding) : '',
          n.envMapEncoding ? Lt('envMapTexelToLinear', n.envMapEncoding) : '',
          n.emissiveMapEncoding
            ? Lt('emissiveMapTexelToLinear', n.emissiveMapEncoding)
            : '',
          n.outputEncoding ? Rt('linearToOutputTexel', n.outputEncoding) : '',
          n.depthPacking ? '#define DEPTH_PACKING ' + i.depthPacking : '',
          '\n'
        ]
          .filter(Ut)
          .join('\n'))),
      (s = Nt(s, n)),
      (s = Dt(s, n)),
      (c = Nt(c, n)),
      (c = Dt(c, n)),
      i.isShaderMaterial || ((s = Ft(s)), (c = Ft(c))),
      (c = h + c),
      (s = Tt(r, r.VERTEX_SHADER, o + s)),
      (c = Tt(r, r.FRAGMENT_SHADER, c)),
      r.attachShader(m, s),
      r.attachShader(m, c),
      void 0 !== i.index0AttributeName
        ? r.bindAttribLocation(m, 0, i.index0AttributeName)
        : !0 === n.morphTargets && r.bindAttribLocation(m, 0, 'position'),
      r.linkProgram(m),
      (n = r.getProgramInfoLog(m)),
      (l = r.getShaderInfoLog(s)),
      (u = r.getShaderInfoLog(c)),
      (p = d = !0),
      !1 === r.getProgramParameter(m, r.LINK_STATUS)
        ? ((d = !1),
          console.error(
            'THREE.WebGLProgram: shader error: ',
            r.getError(),
            'gl.VALIDATE_STATUS',
            r.getProgramParameter(m, r.VALIDATE_STATUS),
            'gl.getProgramInfoLog',
            n,
            l,
            u
          ))
        : '' !== n
          ? console.warn('THREE.WebGLProgram: gl.getProgramInfoLog()', n)
          : ('' !== l && '' !== u) || (p = !1),
      p &&
        (this.diagnostics = {
          runnable: d,
          material: i,
          programLog: n,
          vertexShader: { log: l, prefix: o },
          fragmentShader: { log: u, prefix: h }
        }),
      r.deleteShader(s),
      r.deleteShader(c);
    var g;
    this.getUniforms = function() {
      return void 0 === g && (g = new H(r, m, t)), g;
    };
    var v;
    return (
      (this.getAttributes = function() {
        if (void 0 === v) {
          for (
            var t = {},
              e = r.getProgramParameter(m, r.ACTIVE_ATTRIBUTES),
              i = 0;
            i < e;
            i++
          ) {
            var n = r.getActiveAttrib(m, i).name;
            t[n] = r.getAttribLocation(m, n);
          }
          v = t;
        }
        return v;
      }),
      (this.destroy = function() {
        r.deleteProgram(m), (this.program = void 0);
      }),
      Object.defineProperties(this, {
        uniforms: {
          get: function() {
            return (
              console.warn(
                'THREE.WebGLProgram: .uniforms is now .getUniforms().'
              ),
              this.getUniforms()
            );
          }
        },
        attributes: {
          get: function() {
            return (
              console.warn(
                'THREE.WebGLProgram: .attributes is now .getAttributes().'
              ),
              this.getAttributes()
            );
          }
        }
      }),
      (this.id = Qn++),
      (this.code = e),
      (this.usedTimes = 1),
      (this.program = m),
      (this.vertexShader = s),
      (this.fragmentShader = c),
      this
    );
  }
  function zt(t, e) {
    function i(t, e) {
      var i;
      return (
        t
          ? t && t.isTexture
            ? (i = t.encoding)
            : t &&
              t.isWebGLRenderTarget &&
              (console.warn(
                "THREE.WebGLPrograms.getTextureEncodingFromMap: don't use render targets as textures. Use their .texture property instead."
              ),
              (i = t.texture.encoding))
          : (i = 3e3),
        3e3 === i && e && (i = 3007),
        i
      );
    }
    var n = [],
      r = {
        MeshDepthMaterial: 'depth',
        MeshNormalMaterial: 'normal',
        MeshBasicMaterial: 'basic',
        MeshLambertMaterial: 'lambert',
        MeshPhongMaterial: 'phong',
        MeshStandardMaterial: 'physical',
        MeshPhysicalMaterial: 'physical',
        LineBasicMaterial: 'basic',
        LineDashedMaterial: 'dashed',
        PointsMaterial: 'points'
      },
      a = 'precision supportsVertexTextures map mapEncoding envMap envMapMode envMapEncoding lightMap aoMap emissiveMap emissiveMapEncoding bumpMap normalMap displacementMap specularMap roughnessMap metalnessMap alphaMap combine vertexColors fog useFog fogExp flatShading sizeAttenuation logarithmicDepthBuffer skinning maxBones useVertexTexture morphTargets morphNormals maxMorphTargets maxMorphNormals premultipliedAlpha numDirLights numPointLights numSpotLights numHemiLights shadowMapEnabled shadowMapType toneMapping physicallyCorrectLights alphaTest doubleSided flipSided numClippingPlanes depthPacking'.split(
        ' '
      );
    (this.getParameters = function(n, a, o, s, c) {
      var h,
        l = r[n.type];
      e.floatVertexTextures && c && c.skeleton && c.skeleton.useVertexTexture
        ? (h = 1024)
        : ((h = Math.floor((e.maxVertexUniforms - 20) / 4)),
          void 0 !== c &&
            c &&
            c.isSkinnedMesh &&
            (h = Math.min(c.skeleton.bones.length, h)) <
              c.skeleton.bones.length &&
            console.warn(
              'WebGLRenderer: too many bones - ' +
                c.skeleton.bones.length +
                ', this GPU supports just ' +
                h +
                ' (try OpenGL instead of ANGLE)'
            ));
      var u = t.getPrecision();
      null !== n.precision &&
        (u = e.getMaxPrecision(n.precision)) !== n.precision &&
        console.warn(
          'THREE.WebGLProgram.getParameters:',
          n.precision,
          'not supported, using',
          u,
          'instead.'
        );
      var d = t.getCurrentRenderTarget();
      return {
        shaderID: l,
        precision: u,
        supportsVertexTextures: e.vertexTextures,
        outputEncoding: i(d ? d.texture : null, t.gammaOutput),
        map: !!n.map,
        mapEncoding: i(n.map, t.gammaInput),
        envMap: !!n.envMap,
        envMapMode: n.envMap && n.envMap.mapping,
        envMapEncoding: i(n.envMap, t.gammaInput),
        envMapCubeUV:
          !!n.envMap && (306 === n.envMap.mapping || 307 === n.envMap.mapping),
        lightMap: !!n.lightMap,
        aoMap: !!n.aoMap,
        emissiveMap: !!n.emissiveMap,
        emissiveMapEncoding: i(n.emissiveMap, t.gammaInput),
        bumpMap: !!n.bumpMap,
        normalMap: !!n.normalMap,
        displacementMap: !!n.displacementMap,
        roughnessMap: !!n.roughnessMap,
        metalnessMap: !!n.metalnessMap,
        specularMap: !!n.specularMap,
        alphaMap: !!n.alphaMap,
        combine: n.combine,
        vertexColors: n.vertexColors,
        fog: !!o,
        useFog: n.fog,
        fogExp: o && o.isFogExp2,
        flatShading: 1 === n.shading,
        sizeAttenuation: n.sizeAttenuation,
        logarithmicDepthBuffer: e.logarithmicDepthBuffer,
        skinning: n.skinning,
        maxBones: h,
        useVertexTexture:
          e.floatVertexTextures &&
          c &&
          c.skeleton &&
          c.skeleton.useVertexTexture,
        morphTargets: n.morphTargets,
        morphNormals: n.morphNormals,
        maxMorphTargets: t.maxMorphTargets,
        maxMorphNormals: t.maxMorphNormals,
        numDirLights: a.directional.length,
        numPointLights: a.point.length,
        numSpotLights: a.spot.length,
        numHemiLights: a.hemi.length,
        numClippingPlanes: s,
        shadowMapEnabled:
          t.shadowMap.enabled && c.receiveShadow && 0 < a.shadows.length,
        shadowMapType: t.shadowMap.type,
        toneMapping: t.toneMapping,
        physicallyCorrectLights: t.physicallyCorrectLights,
        premultipliedAlpha: n.premultipliedAlpha,
        alphaTest: n.alphaTest,
        doubleSided: 2 === n.side,
        flipSided: 1 === n.side,
        depthPacking: void 0 !== n.depthPacking && n.depthPacking
      };
    }),
      (this.getProgramCode = function(t, e) {
        var i = [];
        if (
          (e.shaderID
            ? i.push(e.shaderID)
            : (i.push(t.fragmentShader), i.push(t.vertexShader)),
          void 0 !== t.defines)
        )
          for (var n in t.defines) i.push(n), i.push(t.defines[n]);
        for (n = 0; n < a.length; n++) i.push(e[a[n]]);
        return i.join();
      }),
      (this.acquireProgram = function(e, i, r) {
        for (var a, o = 0, s = n.length; o < s; o++) {
          var c = n[o];
          if (c.code === r) {
            (a = c), ++a.usedTimes;
            break;
          }
        }
        return void 0 === a && ((a = new Ot(t, r, e, i)), n.push(a)), a;
      }),
      (this.releaseProgram = function(t) {
        if (0 == --t.usedTimes) {
          var e = n.indexOf(t);
          (n[e] = n[n.length - 1]), n.pop(), t.destroy();
        }
      }),
      (this.programs = n);
  }
  function Bt(t, e, i) {
    function n(t) {
      var o = t.target;
      (t = a[o.id]), null !== t.index && r(t.index);
      var s,
        c = t.attributes;
      for (s in c) r(c[s]);
      o.removeEventListener('dispose', n),
        delete a[o.id],
        (s = e.get(o)),
        s.wireframe && r(s.wireframe),
        e['delete'](o),
        (o = e.get(t)),
        o.wireframe && r(o.wireframe),
        e['delete'](t),
        i.memory.geometries--;
    }
    function r(i) {
      var n;
      void 0 !==
        (n = i.isInterleavedBufferAttribute
          ? e.get(i.data).__webglBuffer
          : e.get(i).__webglBuffer) &&
        (t.deleteBuffer(n),
        i.isInterleavedBufferAttribute ? e['delete'](i.data) : e['delete'](i));
    }
    var a = {};
    return {
      get: function(t) {
        var e = t.geometry;
        if (void 0 !== a[e.id]) return a[e.id];
        e.addEventListener('dispose', n);
        var r;
        return (
          e.isBufferGeometry
            ? (r = e)
            : e.isGeometry &&
              (void 0 === e._bufferGeometry &&
                (e._bufferGeometry = new mt().setFromObject(t)),
              (r = e._bufferGeometry)),
          (a[e.id] = r),
          i.memory.geometries++,
          r
        );
      }
    };
  }
  function Gt(t, e, i) {
    function n(i, n) {
      var r = i.isInterleavedBufferAttribute ? i.data : i,
        a = e.get(r);
      void 0 === a.__webglBuffer
        ? ((a.__webglBuffer = t.createBuffer()),
          t.bindBuffer(n, a.__webglBuffer),
          t.bufferData(n, r.array, r.dynamic ? t.DYNAMIC_DRAW : t.STATIC_DRAW),
          (a.version = r.version))
        : a.version !== r.version &&
          (t.bindBuffer(n, a.__webglBuffer),
          !1 === r.dynamic || -1 === r.updateRange.count
            ? t.bufferSubData(n, 0, r.array)
            : 0 === r.updateRange.count
              ? console.error(
                  'THREE.WebGLObjects.updateBuffer: dynamic THREE.BufferAttribute marked as needsUpdate but updateRange.count is 0, ensure you are using set methods or updating manually.'
                )
              : (t.bufferSubData(
                  n,
                  r.updateRange.offset * r.array.BYTES_PER_ELEMENT,
                  r.array.subarray(
                    r.updateRange.offset,
                    r.updateRange.offset + r.updateRange.count
                  )
                ),
                (r.updateRange.count = 0)),
          (a.version = r.version));
    }
    var r = new Bt(t, e, i);
    return {
      getAttributeBuffer: function(t) {
        return t.isInterleavedBufferAttribute
          ? e.get(t.data).__webglBuffer
          : e.get(t).__webglBuffer;
      },
      getWireframeAttribute: function(i) {
        var r = e.get(i);
        if (void 0 !== r.wireframe) return r.wireframe;
        var a = [],
          o = i.index,
          s = i.attributes;
        if (((i = s.position), null !== o))
          for (var o = o.array, s = 0, c = o.length; s < c; s += 3) {
            var h = o[s + 0],
              l = o[s + 1],
              u = o[s + 2];
            a.push(h, l, l, u, u, h);
          }
        else
          for (o = s.position.array, s = 0, c = o.length / 3 - 1; s < c; s += 3)
            (h = s + 0), (l = s + 1), (u = s + 2), a.push(h, l, l, u, u, h);
        return (
          (a = new ht(new (65535 < i.count ? Uint32Array : Uint16Array)(a), 1)),
          n(a, t.ELEMENT_ARRAY_BUFFER),
          (r.wireframe = a)
        );
      },
      update: function(e) {
        var i = r.get(e);
        e.geometry.isGeometry && i.updateFromObject(e), (e = i.index);
        var a = i.attributes;
        null !== e && n(e, t.ELEMENT_ARRAY_BUFFER);
        for (var o in a) n(a[o], t.ARRAY_BUFFER);
        e = i.morphAttributes;
        for (o in e)
          for (var a = e[o], s = 0, c = a.length; s < c; s++)
            n(a[s], t.ARRAY_BUFFER);
        return i;
      }
    };
  }
  function Ht(e, i, n, r, a, o, s) {
    function c(t, e) {
      if (t.width > e || t.height > e) {
        var i = e / Math.max(t.width, t.height),
          n = document.createElementNS(
            'http://www.w3.org/1999/xhtml',
            'canvas'
          );
        return (
          (n.width = Math.floor(t.width * i)),
          (n.height = Math.floor(t.height * i)),
          n
            .getContext('2d')
            .drawImage(t, 0, 0, t.width, t.height, 0, 0, n.width, n.height),
          console.warn(
            'THREE.WebGLRenderer: image is too big (' +
              t.width +
              'x' +
              t.height +
              '). Resized to ' +
              n.width +
              'x' +
              n.height,
            t
          ),
          n
        );
      }
      return t;
    }
    function h(e) {
      return t.Math.isPowerOfTwo(e.width) && t.Math.isPowerOfTwo(e.height);
    }
    function l(t) {
      return 1003 === t || 1004 === t || 1005 === t ? e.NEAREST : e.LINEAR;
    }
    function u(t) {
      (t = t.target), t.removeEventListener('dispose', u);
      t: {
        var i = r.get(t);
        if (t.image && i.__image__webglTextureCube)
          e.deleteTexture(i.__image__webglTextureCube);
        else {
          if (void 0 === i.__webglInit) break t;
          e.deleteTexture(i.__webglTexture);
        }
        r['delete'](t);
      }
      v.textures--;
    }
    function d(t) {
      (t = t.target), t.removeEventListener('dispose', d);
      var i = r.get(t),
        n = r.get(t.texture);
      if (t) {
        if (
          (void 0 !== n.__webglTexture && e.deleteTexture(n.__webglTexture),
          t.depthTexture && t.depthTexture.dispose(),
          t && t.isWebGLRenderTargetCube)
        )
          for (n = 0; 6 > n; n++)
            e.deleteFramebuffer(i.__webglFramebuffer[n]),
              i.__webglDepthbuffer &&
                e.deleteRenderbuffer(i.__webglDepthbuffer[n]);
        else
          e.deleteFramebuffer(i.__webglFramebuffer),
            i.__webglDepthbuffer && e.deleteRenderbuffer(i.__webglDepthbuffer);
        r['delete'](t.texture), r['delete'](t);
      }
      v.textures--;
    }
    function p(i, s) {
      var l = r.get(i);
      if (0 < i.version && l.__version !== i.version) {
        var d = i.image;
        if (void 0 === d)
          console.warn(
            'THREE.WebGLRenderer: Texture marked for update but image is undefined',
            i
          );
        else {
          if (!1 !== d.complete) {
            void 0 === l.__webglInit &&
              ((l.__webglInit = !0),
              i.addEventListener('dispose', u),
              (l.__webglTexture = e.createTexture()),
              v.textures++),
              n.activeTexture(e.TEXTURE0 + s),
              n.bindTexture(e.TEXTURE_2D, l.__webglTexture),
              e.pixelStorei(e.UNPACK_FLIP_Y_WEBGL, i.flipY),
              e.pixelStorei(
                e.UNPACK_PREMULTIPLY_ALPHA_WEBGL,
                i.premultiplyAlpha
              ),
              e.pixelStorei(e.UNPACK_ALIGNMENT, i.unpackAlignment);
            var p = c(i.image, a.maxTextureSize);
            if (
              (1001 !== i.wrapS ||
                1001 !== i.wrapT ||
                (1003 !== i.minFilter && 1006 !== i.minFilter)) &&
              !1 === h(p)
            )
              if (
                (d = p) instanceof HTMLImageElement ||
                d instanceof HTMLCanvasElement
              ) {
                var m = document.createElementNS(
                  'http://www.w3.org/1999/xhtml',
                  'canvas'
                );
                (m.width = t.Math.nearestPowerOfTwo(d.width)),
                  (m.height = t.Math.nearestPowerOfTwo(d.height)),
                  m.getContext('2d').drawImage(d, 0, 0, m.width, m.height),
                  console.warn(
                    'THREE.WebGLRenderer: image is not power of two (' +
                      d.width +
                      'x' +
                      d.height +
                      '). Resized to ' +
                      m.width +
                      'x' +
                      m.height,
                    d
                  ),
                  (p = m);
              } else p = d;
            var d = h(p),
              m = o(i.format),
              g = o(i.type);
            f(e.TEXTURE_2D, i, d);
            var x = i.mipmaps;
            if (i && i.isDepthTexture) {
              if (((x = e.DEPTH_COMPONENT), 1015 === i.type)) {
                if (!y)
                  throw Error('Float Depth Texture only supported in WebGL2.0');
                x = e.DEPTH_COMPONENT32F;
              } else y && (x = e.DEPTH_COMPONENT16);
              1027 === i.format && (x = e.DEPTH_STENCIL),
                n.texImage2D(
                  e.TEXTURE_2D,
                  0,
                  x,
                  p.width,
                  p.height,
                  0,
                  m,
                  g,
                  null
                );
            } else if (i && i.isDataTexture)
              if (0 < x.length && d) {
                for (var _ = 0, b = x.length; _ < b; _++)
                  (p = x[_]),
                    n.texImage2D(
                      e.TEXTURE_2D,
                      _,
                      m,
                      p.width,
                      p.height,
                      0,
                      m,
                      g,
                      p.data
                    );
                i.generateMipmaps = !1;
              } else
                n.texImage2D(
                  e.TEXTURE_2D,
                  0,
                  m,
                  p.width,
                  p.height,
                  0,
                  m,
                  g,
                  p.data
                );
            else if (i && i.isCompressedTexture)
              for (_ = 0, b = x.length; _ < b; _++)
                (p = x[_]),
                  1023 !== i.format && 1022 !== i.format
                    ? -1 < n.getCompressedTextureFormats().indexOf(m)
                      ? n.compressedTexImage2D(
                          e.TEXTURE_2D,
                          _,
                          m,
                          p.width,
                          p.height,
                          0,
                          p.data
                        )
                      : console.warn(
                          'THREE.WebGLRenderer: Attempt to load unsupported compressed texture format in .uploadTexture()'
                        )
                    : n.texImage2D(
                        e.TEXTURE_2D,
                        _,
                        m,
                        p.width,
                        p.height,
                        0,
                        m,
                        g,
                        p.data
                      );
            else if (0 < x.length && d) {
              for (_ = 0, b = x.length; _ < b; _++)
                (p = x[_]), n.texImage2D(e.TEXTURE_2D, _, m, m, g, p);
              i.generateMipmaps = !1;
            } else n.texImage2D(e.TEXTURE_2D, 0, m, m, g, p);
            return (
              i.generateMipmaps && d && e.generateMipmap(e.TEXTURE_2D),
              (l.__version = i.version),
              void (i.onUpdate && i.onUpdate(i))
            );
          }
          console.warn(
            'THREE.WebGLRenderer: Texture marked for update but image is incomplete',
            i
          );
        }
      }
      n.activeTexture(e.TEXTURE0 + s),
        n.bindTexture(e.TEXTURE_2D, l.__webglTexture);
    }
    function f(t, n, s) {
      s
        ? (e.texParameteri(t, e.TEXTURE_WRAP_S, o(n.wrapS)),
          e.texParameteri(t, e.TEXTURE_WRAP_T, o(n.wrapT)),
          e.texParameteri(t, e.TEXTURE_MAG_FILTER, o(n.magFilter)),
          e.texParameteri(t, e.TEXTURE_MIN_FILTER, o(n.minFilter)))
        : (e.texParameteri(t, e.TEXTURE_WRAP_S, e.CLAMP_TO_EDGE),
          e.texParameteri(t, e.TEXTURE_WRAP_T, e.CLAMP_TO_EDGE),
          (1001 === n.wrapS && 1001 === n.wrapT) ||
            console.warn(
              'THREE.WebGLRenderer: Texture is not power of two. Texture.wrapS and Texture.wrapT should be set to THREE.ClampToEdgeWrapping.',
              n
            ),
          e.texParameteri(t, e.TEXTURE_MAG_FILTER, l(n.magFilter)),
          e.texParameteri(t, e.TEXTURE_MIN_FILTER, l(n.minFilter)),
          1003 !== n.minFilter &&
            1006 !== n.minFilter &&
            console.warn(
              'THREE.WebGLRenderer: Texture is not power of two. Texture.minFilter should be set to THREE.NearestFilter or THREE.LinearFilter.',
              n
            )),
        !(s = i.get('EXT_texture_filter_anisotropic')) ||
          (1015 === n.type && null === i.get('OES_texture_float_linear')) ||
          (1016 === n.type &&
            null === i.get('OES_texture_half_float_linear')) ||
          !(1 < n.anisotropy || r.get(n).__currentAnisotropy) ||
          (e.texParameterf(
            t,
            s.TEXTURE_MAX_ANISOTROPY_EXT,
            Math.min(n.anisotropy, a.getMaxAnisotropy())
          ),
          (r.get(n).__currentAnisotropy = n.anisotropy));
    }
    function m(t, i, a, s) {
      var c = o(i.texture.format),
        h = o(i.texture.type);
      n.texImage2D(s, 0, c, i.width, i.height, 0, c, h, null),
        e.bindFramebuffer(e.FRAMEBUFFER, t),
        e.framebufferTexture2D(
          e.FRAMEBUFFER,
          a,
          s,
          r.get(i.texture).__webglTexture,
          0
        ),
        e.bindFramebuffer(e.FRAMEBUFFER, null);
    }
    function g(t, i) {
      e.bindRenderbuffer(e.RENDERBUFFER, t),
        i.depthBuffer && !i.stencilBuffer
          ? (e.renderbufferStorage(
              e.RENDERBUFFER,
              e.DEPTH_COMPONENT16,
              i.width,
              i.height
            ),
            e.framebufferRenderbuffer(
              e.FRAMEBUFFER,
              e.DEPTH_ATTACHMENT,
              e.RENDERBUFFER,
              t
            ))
          : i.depthBuffer && i.stencilBuffer
            ? (e.renderbufferStorage(
                e.RENDERBUFFER,
                e.DEPTH_STENCIL,
                i.width,
                i.height
              ),
              e.framebufferRenderbuffer(
                e.FRAMEBUFFER,
                e.DEPTH_STENCIL_ATTACHMENT,
                e.RENDERBUFFER,
                t
              ))
            : e.renderbufferStorage(e.RENDERBUFFER, e.RGBA4, i.width, i.height),
        e.bindRenderbuffer(e.RENDERBUFFER, null);
    }
    var v = s.memory,
      y =
        'undefined' != typeof WebGL2RenderingContext &&
        e instanceof WebGL2RenderingContext;
    (this.setTexture2D = p),
      (this.setTextureCube = function(t, i) {
        var s = r.get(t);
        if (6 === t.image.length)
          if (0 < t.version && s.__version !== t.version) {
            s.__image__webglTextureCube ||
              (t.addEventListener('dispose', u),
              (s.__image__webglTextureCube = e.createTexture()),
              v.textures++),
              n.activeTexture(e.TEXTURE0 + i),
              n.bindTexture(e.TEXTURE_CUBE_MAP, s.__image__webglTextureCube),
              e.pixelStorei(e.UNPACK_FLIP_Y_WEBGL, t.flipY);
            for (
              var l = t && t.isCompressedTexture,
                d = t.image[0] && t.image[0].isDataTexture,
                p = [],
                m = 0;
              6 > m;
              m++
            )
              p[m] =
                l || d
                  ? d ? t.image[m].image : t.image[m]
                  : c(t.image[m], a.maxCubemapSize);
            var g = h(p[0]),
              y = o(t.format),
              x = o(t.type);
            for (f(e.TEXTURE_CUBE_MAP, t, g), m = 0; 6 > m; m++)
              if (l)
                for (var _, b = p[m].mipmaps, w = 0, M = b.length; w < M; w++)
                  (_ = b[w]),
                    1023 !== t.format && 1022 !== t.format
                      ? -1 < n.getCompressedTextureFormats().indexOf(y)
                        ? n.compressedTexImage2D(
                            e.TEXTURE_CUBE_MAP_POSITIVE_X + m,
                            w,
                            y,
                            _.width,
                            _.height,
                            0,
                            _.data
                          )
                        : console.warn(
                            'THREE.WebGLRenderer: Attempt to load unsupported compressed texture format in .setTextureCube()'
                          )
                      : n.texImage2D(
                          e.TEXTURE_CUBE_MAP_POSITIVE_X + m,
                          w,
                          y,
                          _.width,
                          _.height,
                          0,
                          y,
                          x,
                          _.data
                        );
              else
                d
                  ? n.texImage2D(
                      e.TEXTURE_CUBE_MAP_POSITIVE_X + m,
                      0,
                      y,
                      p[m].width,
                      p[m].height,
                      0,
                      y,
                      x,
                      p[m].data
                    )
                  : n.texImage2D(
                      e.TEXTURE_CUBE_MAP_POSITIVE_X + m,
                      0,
                      y,
                      y,
                      x,
                      p[m]
                    );
            t.generateMipmaps && g && e.generateMipmap(e.TEXTURE_CUBE_MAP),
              (s.__version = t.version),
              t.onUpdate && t.onUpdate(t);
          } else
            n.activeTexture(e.TEXTURE0 + i),
              n.bindTexture(e.TEXTURE_CUBE_MAP, s.__image__webglTextureCube);
      }),
      (this.setTextureCubeDynamic = function(t, i) {
        n.activeTexture(e.TEXTURE0 + i),
          n.bindTexture(e.TEXTURE_CUBE_MAP, r.get(t).__webglTexture);
      }),
      (this.setupRenderTarget = function(t) {
        var i = r.get(t),
          a = r.get(t.texture);
        t.addEventListener('dispose', d),
          (a.__webglTexture = e.createTexture()),
          v.textures++;
        var o = t && t.isWebGLRenderTargetCube,
          s = h(t);
        if (o) {
          i.__webglFramebuffer = [];
          for (var c = 0; 6 > c; c++)
            i.__webglFramebuffer[c] = e.createFramebuffer();
        } else i.__webglFramebuffer = e.createFramebuffer();
        if (o) {
          for (
            n.bindTexture(e.TEXTURE_CUBE_MAP, a.__webglTexture),
              f(e.TEXTURE_CUBE_MAP, t.texture, s),
              c = 0;
            6 > c;
            c++
          )
            m(
              i.__webglFramebuffer[c],
              t,
              e.COLOR_ATTACHMENT0,
              e.TEXTURE_CUBE_MAP_POSITIVE_X + c
            );
          t.texture.generateMipmaps &&
            s &&
            e.generateMipmap(e.TEXTURE_CUBE_MAP),
            n.bindTexture(e.TEXTURE_CUBE_MAP, null);
        } else
          n.bindTexture(e.TEXTURE_2D, a.__webglTexture),
            f(e.TEXTURE_2D, t.texture, s),
            m(i.__webglFramebuffer, t, e.COLOR_ATTACHMENT0, e.TEXTURE_2D),
            t.texture.generateMipmaps && s && e.generateMipmap(e.TEXTURE_2D),
            n.bindTexture(e.TEXTURE_2D, null);
        if (t.depthBuffer) {
          if (
            ((i = r.get(t)),
            (a = t && t.isWebGLRenderTargetCube),
            t.depthTexture)
          ) {
            if (a)
              throw Error(
                'target.depthTexture not supported in Cube render targets'
              );
            if (t && t.isWebGLRenderTargetCube)
              throw Error(
                'Depth Texture with cube render targets is not supported!'
              );
            if (
              (e.bindFramebuffer(e.FRAMEBUFFER, i.__webglFramebuffer),
              !t.depthTexture || !t.depthTexture.isDepthTexture)
            )
              throw Error(
                'renderTarget.depthTexture must be an instance of THREE.DepthTexture'
              );
            if (
              ((r.get(t.depthTexture).__webglTexture &&
                t.depthTexture.image.width === t.width &&
                t.depthTexture.image.height === t.height) ||
                ((t.depthTexture.image.width = t.width),
                (t.depthTexture.image.height = t.height),
                (t.depthTexture.needsUpdate = !0)),
              p(t.depthTexture, 0),
              (i = r.get(t.depthTexture).__webglTexture),
              1026 === t.depthTexture.format)
            )
              e.framebufferTexture2D(
                e.FRAMEBUFFER,
                e.DEPTH_ATTACHMENT,
                e.TEXTURE_2D,
                i,
                0
              );
            else {
              if (1027 !== t.depthTexture.format)
                throw Error('Unknown depthTexture format');
              e.framebufferTexture2D(
                e.FRAMEBUFFER,
                e.DEPTH_STENCIL_ATTACHMENT,
                e.TEXTURE_2D,
                i,
                0
              );
            }
          } else if (a)
            for (i.__webglDepthbuffer = [], a = 0; 6 > a; a++)
              e.bindFramebuffer(e.FRAMEBUFFER, i.__webglFramebuffer[a]),
                (i.__webglDepthbuffer[a] = e.createRenderbuffer()),
                g(i.__webglDepthbuffer[a], t);
          else
            e.bindFramebuffer(e.FRAMEBUFFER, i.__webglFramebuffer),
              (i.__webglDepthbuffer = e.createRenderbuffer()),
              g(i.__webglDepthbuffer, t);
          e.bindFramebuffer(e.FRAMEBUFFER, null);
        }
      }),
      (this.updateRenderTargetMipmap = function(t) {
        var i = t.texture;
        i.generateMipmaps &&
          h(t) &&
          1003 !== i.minFilter &&
          1006 !== i.minFilter &&
          ((t =
            t && t.isWebGLRenderTargetCube ? e.TEXTURE_CUBE_MAP : e.TEXTURE_2D),
          (i = r.get(i).__webglTexture),
          n.bindTexture(t, i),
          e.generateMipmap(t),
          n.bindTexture(t, null));
      });
  }
  function Vt() {
    var t = {};
    return {
      get: function(e) {
        e = e.uuid;
        var i = t[e];
        return void 0 === i && ((i = {}), (t[e] = i)), i;
      },
      delete: function(e) {
        delete t[e.uuid];
      },
      clear: function() {
        t = {};
      }
    };
  }
  function kt(t, e, i) {
    function n(e, i, n) {
      var r = new Uint8Array(4),
        a = t.createTexture();
      for (
        t.bindTexture(e, a),
          t.texParameteri(e, t.TEXTURE_MIN_FILTER, t.NEAREST),
          t.texParameteri(e, t.TEXTURE_MAG_FILTER, t.NEAREST),
          e = 0;
        e < n;
        e++
      )
        t.texImage2D(i + e, 0, t.RGBA, 1, 1, 0, t.RGBA, t.UNSIGNED_BYTE, r);
      return a;
    }
    function a(e) {
      !0 !== w[e] && (t.enable(e), (w[e] = !0));
    }
    function o(e) {
      !1 !== w[e] && (t.disable(e), (w[e] = !1));
    }
    function s(e, n, r, s, c, h, l, u) {
      0 !== e
        ? (a(t.BLEND),
          (e === E && u === C) ||
            (2 === e
              ? u
                ? (t.blendEquationSeparate(t.FUNC_ADD, t.FUNC_ADD),
                  t.blendFuncSeparate(t.ONE, t.ONE, t.ONE, t.ONE))
                : (t.blendEquation(t.FUNC_ADD), t.blendFunc(t.SRC_ALPHA, t.ONE))
              : 3 === e
                ? u
                  ? (t.blendEquationSeparate(t.FUNC_ADD, t.FUNC_ADD),
                    t.blendFuncSeparate(
                      t.ZERO,
                      t.ZERO,
                      t.ONE_MINUS_SRC_COLOR,
                      t.ONE_MINUS_SRC_ALPHA
                    ))
                  : (t.blendEquation(t.FUNC_ADD),
                    t.blendFunc(t.ZERO, t.ONE_MINUS_SRC_COLOR))
                : 4 === e
                  ? u
                    ? (t.blendEquationSeparate(t.FUNC_ADD, t.FUNC_ADD),
                      t.blendFuncSeparate(
                        t.ZERO,
                        t.SRC_COLOR,
                        t.ZERO,
                        t.SRC_ALPHA
                      ))
                    : (t.blendEquation(t.FUNC_ADD),
                      t.blendFunc(t.ZERO, t.SRC_COLOR))
                  : u
                    ? (t.blendEquationSeparate(t.FUNC_ADD, t.FUNC_ADD),
                      t.blendFuncSeparate(
                        t.ONE,
                        t.ONE_MINUS_SRC_ALPHA,
                        t.ONE,
                        t.ONE_MINUS_SRC_ALPHA
                      ))
                    : (t.blendEquationSeparate(t.FUNC_ADD, t.FUNC_ADD),
                      t.blendFuncSeparate(
                        t.SRC_ALPHA,
                        t.ONE_MINUS_SRC_ALPHA,
                        t.ONE,
                        t.ONE_MINUS_SRC_ALPHA
                      )),
            (E = e),
            (C = u)),
          5 === e
            ? ((c = c || n),
              (h = h || r),
              (l = l || s),
              (n === S && c === L) ||
                (t.blendEquationSeparate(i(n), i(c)), (S = n), (L = c)),
              (r === T && s === A && h === R && l === P) ||
                (t.blendFuncSeparate(i(r), i(s), i(h), i(l)),
                (T = r),
                (A = s),
                (R = h),
                (P = l)))
            : (P = R = L = A = T = S = null))
        : (o(t.BLEND), (E = e));
    }
    function c(t) {
      g.setFunc(t);
    }
    function h(e) {
      I !== e && (e ? t.frontFace(t.CW) : t.frontFace(t.CCW), (I = e));
    }
    function l(e) {
      0 !== e
        ? (a(t.CULL_FACE),
          e !== U &&
            (1 === e
              ? t.cullFace(t.BACK)
              : 2 === e ? t.cullFace(t.FRONT) : t.cullFace(t.FRONT_AND_BACK)))
        : o(t.CULL_FACE),
        (U = e);
    }
    function u(e) {
      void 0 === e && (e = t.TEXTURE0 + z - 1),
        B !== e && (t.activeTexture(e), (B = e));
    }
    function d(t, e, i, n) {
      m.setClear(t, e, i, n);
    }
    function p(t) {
      g.setClear(t);
    }
    function f(t) {
      v.setClear(t);
    }
    var m = new function() {
        var e = !1,
          i = new r(),
          n = null,
          a = new r();
        return {
          setMask: function(i) {
            n === i || e || (t.colorMask(i, i, i, i), (n = i));
          },
          setLocked: function(t) {
            e = t;
          },
          setClear: function(e, n, r, o) {
            i.set(e, n, r, o),
              !1 === a.equals(i) && (t.clearColor(e, n, r, o), a.copy(i));
          },
          reset: function() {
            (e = !1), (n = null), a.set(0, 0, 0, 1);
          }
        };
      }(),
      g = new function() {
        var e = !1,
          i = null,
          n = null,
          r = null;
        return {
          setTest: function(e) {
            e ? a(t.DEPTH_TEST) : o(t.DEPTH_TEST);
          },
          setMask: function(n) {
            i === n || e || (t.depthMask(n), (i = n));
          },
          setFunc: function(e) {
            if (n !== e) {
              if (e)
                switch (e) {
                  case 0:
                    t.depthFunc(t.NEVER);
                    break;
                  case 1:
                    t.depthFunc(t.ALWAYS);
                    break;
                  case 2:
                    t.depthFunc(t.LESS);
                    break;
                  case 3:
                    t.depthFunc(t.LEQUAL);
                    break;
                  case 4:
                    t.depthFunc(t.EQUAL);
                    break;
                  case 5:
                    t.depthFunc(t.GEQUAL);
                    break;
                  case 6:
                    t.depthFunc(t.GREATER);
                    break;
                  case 7:
                    t.depthFunc(t.NOTEQUAL);
                    break;
                  default:
                    t.depthFunc(t.LEQUAL);
                }
              else t.depthFunc(t.LEQUAL);
              n = e;
            }
          },
          setLocked: function(t) {
            e = t;
          },
          setClear: function(e) {
            r !== e && (t.clearDepth(e), (r = e));
          },
          reset: function() {
            (e = !1), (r = n = i = null);
          }
        };
      }(),
      v = new function() {
        var e = !1,
          i = null,
          n = null,
          r = null,
          s = null,
          c = null,
          h = null,
          l = null,
          u = null;
        return {
          setTest: function(e) {
            e ? a(t.STENCIL_TEST) : o(t.STENCIL_TEST);
          },
          setMask: function(n) {
            i === n || e || (t.stencilMask(n), (i = n));
          },
          setFunc: function(e, i, a) {
            (n === e && r === i && s === a) ||
              (t.stencilFunc(e, i, a), (n = e), (r = i), (s = a));
          },
          setOp: function(e, i, n) {
            (c === e && h === i && l === n) ||
              (t.stencilOp(e, i, n), (c = e), (h = i), (l = n));
          },
          setLocked: function(t) {
            e = t;
          },
          setClear: function(e) {
            u !== e && (t.clearStencil(e), (u = e));
          },
          reset: function() {
            (e = !1), (u = l = h = c = s = r = n = i = null);
          }
        };
      }(),
      y = t.getParameter(t.MAX_VERTEX_ATTRIBS),
      x = new Uint8Array(y),
      _ = new Uint8Array(y),
      b = new Uint8Array(y),
      w = {},
      M = null,
      E = null,
      S = null,
      T = null,
      A = null,
      L = null,
      R = null,
      P = null,
      C = !1,
      I = null,
      U = null,
      D = null,
      N = null,
      F = null,
      O = null,
      z = t.getParameter(t.MAX_TEXTURE_IMAGE_UNITS),
      B = null,
      G = {},
      H = new r(),
      V = new r(),
      k = {};
    return (
      (k[t.TEXTURE_2D] = n(t.TEXTURE_2D, t.TEXTURE_2D, 1)),
      (k[t.TEXTURE_CUBE_MAP] = n(
        t.TEXTURE_CUBE_MAP,
        t.TEXTURE_CUBE_MAP_POSITIVE_X,
        6
      )),
      {
        buffers: { color: m, depth: g, stencil: v },
        init: function() {
          d(0, 0, 0, 1),
            p(1),
            f(0),
            a(t.DEPTH_TEST),
            c(3),
            h(!1),
            l(1),
            a(t.CULL_FACE),
            a(t.BLEND),
            s(1);
        },
        initAttributes: function() {
          for (var t = 0, e = x.length; t < e; t++) x[t] = 0;
        },
        enableAttribute: function(i) {
          (x[i] = 1),
            0 === _[i] && (t.enableVertexAttribArray(i), (_[i] = 1)),
            0 !== b[i] &&
              (e.get('ANGLE_instanced_arrays').vertexAttribDivisorANGLE(i, 0),
              (b[i] = 0));
        },
        enableAttributeAndDivisor: function(e, i, n) {
          (x[e] = 1),
            0 === _[e] && (t.enableVertexAttribArray(e), (_[e] = 1)),
            b[e] !== i && (n.vertexAttribDivisorANGLE(e, i), (b[e] = i));
        },
        disableUnusedAttributes: function() {
          for (var e = 0, i = _.length; e !== i; ++e)
            _[e] !== x[e] && (t.disableVertexAttribArray(e), (_[e] = 0));
        },
        enable: a,
        disable: o,
        getCompressedTextureFormats: function() {
          if (
            null === M &&
            ((M = []),
            e.get('WEBGL_compressed_texture_pvrtc') ||
              e.get('WEBGL_compressed_texture_s3tc') ||
              e.get('WEBGL_compressed_texture_etc1'))
          )
            for (
              var i = t.getParameter(t.COMPRESSED_TEXTURE_FORMATS), n = 0;
              n < i.length;
              n++
            )
              M.push(i[n]);
          return M;
        },
        setBlending: s,
        setColorWrite: function(t) {
          m.setMask(t);
        },
        setDepthTest: function(t) {
          g.setTest(t);
        },
        setDepthWrite: function(t) {
          g.setMask(t);
        },
        setDepthFunc: c,
        setStencilTest: function(t) {
          v.setTest(t);
        },
        setStencilWrite: function(t) {
          v.setMask(t);
        },
        setStencilFunc: function(t, e, i) {
          v.setFunc(t, e, i);
        },
        setStencilOp: function(t, e, i) {
          v.setOp(t, e, i);
        },
        setFlipSided: h,
        setCullFace: l,
        setLineWidth: function(e) {
          e !== D && (t.lineWidth(e), (D = e));
        },
        setPolygonOffset: function(e, i, n) {
          e
            ? (a(t.POLYGON_OFFSET_FILL),
              (N !== i || F !== n) && (t.polygonOffset(i, n), (N = i), (F = n)))
            : o(t.POLYGON_OFFSET_FILL);
        },
        getScissorTest: function() {
          return O;
        },
        setScissorTest: function(e) {
          (O = e) ? a(t.SCISSOR_TEST) : o(t.SCISSOR_TEST);
        },
        activeTexture: u,
        bindTexture: function(e, i) {
          null === B && u();
          var n = G[B];
          void 0 === n && ((n = { type: void 0, texture: void 0 }), (G[B] = n)),
            (n.type === e && n.texture === i) ||
              (t.bindTexture(e, i || k[e]), (n.type = e), (n.texture = i));
        },
        compressedTexImage2D: function() {
          try {
            t.compressedTexImage2D.apply(t, arguments);
          } catch (t) {
            console.error(t);
          }
        },
        texImage2D: function() {
          try {
            t.texImage2D.apply(t, arguments);
          } catch (t) {
            console.error(t);
          }
        },
        clearColor: d,
        clearDepth: p,
        clearStencil: f,
        scissor: function(e) {
          !1 === H.equals(e) && (t.scissor(e.x, e.y, e.z, e.w), H.copy(e));
        },
        viewport: function(e) {
          !1 === V.equals(e) && (t.viewport(e.x, e.y, e.z, e.w), V.copy(e));
        },
        reset: function() {
          for (var e = 0; e < _.length; e++)
            1 === _[e] && (t.disableVertexAttribArray(e), (_[e] = 0));
          (w = {}),
            (B = M = null),
            (G = {}),
            (U = I = E = null),
            m.reset(),
            g.reset(),
            v.reset();
        }
      }
    );
  }
  function jt(t, e, i) {
    function n(e) {
      if ('highp' === e) {
        if (
          0 <
            t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.HIGH_FLOAT)
              .precision &&
          0 <
            t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.HIGH_FLOAT)
              .precision
        )
          return 'highp';
        e = 'mediump';
      }
      return 'mediump' === e &&
        0 <
          t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.MEDIUM_FLOAT)
            .precision &&
        0 <
          t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.MEDIUM_FLOAT)
            .precision
        ? 'mediump'
        : 'lowp';
    }
    var r,
      a = void 0 !== i.precision ? i.precision : 'highp',
      o = n(a);
    o !== a &&
      (console.warn(
        'THREE.WebGLRenderer:',
        a,
        'not supported, using',
        o,
        'instead.'
      ),
      (a = o)),
      (i = !0 === i.logarithmicDepthBuffer && !!e.get('EXT_frag_depth'));
    var o = t.getParameter(t.MAX_TEXTURE_IMAGE_UNITS),
      s = t.getParameter(t.MAX_VERTEX_TEXTURE_IMAGE_UNITS),
      c = t.getParameter(t.MAX_TEXTURE_SIZE),
      h = t.getParameter(t.MAX_CUBE_MAP_TEXTURE_SIZE),
      l = t.getParameter(t.MAX_VERTEX_ATTRIBS),
      u = t.getParameter(t.MAX_VERTEX_UNIFORM_VECTORS),
      d = t.getParameter(t.MAX_VARYING_VECTORS),
      p = t.getParameter(t.MAX_FRAGMENT_UNIFORM_VECTORS),
      f = 0 < s,
      m = !!e.get('OES_texture_float');
    return {
      getMaxAnisotropy: function() {
        if (void 0 !== r) return r;
        var i = e.get('EXT_texture_filter_anisotropic');
        return (r =
          null !== i ? t.getParameter(i.MAX_TEXTURE_MAX_ANISOTROPY_EXT) : 0);
      },
      getMaxPrecision: n,
      precision: a,
      logarithmicDepthBuffer: i,
      maxTextures: o,
      maxVertexTextures: s,
      maxTextureSize: c,
      maxCubemapSize: h,
      maxAttributes: l,
      maxVertexUniforms: u,
      maxVaryings: d,
      maxFragmentUniforms: p,
      vertexTextures: f,
      floatFragmentTextures: m,
      floatVertexTextures: f && m
    };
  }
  function Wt(t) {
    var e = {};
    return {
      get: function(i) {
        if (void 0 !== e[i]) return e[i];
        var n;
        switch (i) {
          case 'WEBGL_depth_texture':
            n =
              t.getExtension('WEBGL_depth_texture') ||
              t.getExtension('MOZ_WEBGL_depth_texture') ||
              t.getExtension('WEBKIT_WEBGL_depth_texture');
            break;
          case 'EXT_texture_filter_anisotropic':
            n =
              t.getExtension('EXT_texture_filter_anisotropic') ||
              t.getExtension('MOZ_EXT_texture_filter_anisotropic') ||
              t.getExtension('WEBKIT_EXT_texture_filter_anisotropic');
            break;
          case 'WEBGL_compressed_texture_s3tc':
            n =
              t.getExtension('WEBGL_compressed_texture_s3tc') ||
              t.getExtension('MOZ_WEBGL_compressed_texture_s3tc') ||
              t.getExtension('WEBKIT_WEBGL_compressed_texture_s3tc');
            break;
          case 'WEBGL_compressed_texture_pvrtc':
            n =
              t.getExtension('WEBGL_compressed_texture_pvrtc') ||
              t.getExtension('WEBKIT_WEBGL_compressed_texture_pvrtc');
            break;
          case 'WEBGL_compressed_texture_etc1':
            n = t.getExtension('WEBGL_compressed_texture_etc1');
            break;
          default:
            n = t.getExtension(i);
        }
        return (
          null === n &&
            console.warn(
              'THREE.WebGLRenderer: ' + i + ' extension not supported.'
            ),
          (e[i] = n)
        );
      }
    };
  }
  function Xt() {
    function t() {
      h.value !== n && ((h.value = n), (h.needsUpdate = 0 < r)),
        (i.numPlanes = r);
    }
    function e(t, e, n, r) {
      var a = null !== t ? t.length : 0,
        o = null;
      if (0 !== a) {
        if (((o = h.value), !0 !== r || null === o))
          for (
            r = n + 4 * a,
              e = e.matrixWorldInverse,
              c.getNormalMatrix(e),
              (null === o || o.length < r) && (o = new Float32Array(r)),
              r = 0;
            r !== a;
            ++r, n += 4
          )
            s.copy(t[r]).applyMatrix4(e, c),
              s.normal.toArray(o, n),
              (o[n + 3] = s.constant);
        (h.value = o), (h.needsUpdate = !0);
      }
      return (i.numPlanes = a), o;
    }
    var i = this,
      n = null,
      r = 0,
      a = !1,
      o = !1,
      s = new K(),
      c = new Q(),
      h = { value: null, needsUpdate: !1 };
    (this.uniform = h),
      (this.numPlanes = 0),
      (this.init = function(t, i, o) {
        var s = 0 !== t.length || i || 0 !== r || a;
        return (a = i), (n = e(t, o, 0)), (r = t.length), s;
      }),
      (this.beginShadows = function() {
        (o = !0), e(null);
      }),
      (this.endShadows = function() {
        (o = !1), t();
      }),
      (this.setState = function(i, s, c, l, u) {
        if (!a || null === i || 0 === i.length || (o && !s)) o ? e(null) : t();
        else {
          s = o ? 0 : r;
          var d = 4 * s,
            p = l.clippingState || null;
          for (h.value = p, p = e(i, c, d, u), i = 0; i !== d; ++i) p[i] = n[i];
          (l.clippingState = p), (this.numPlanes += s);
        }
      });
  }
  function qt(e) {
    function i(t, e, i, n) {
      !0 === L && ((t *= n), (e *= n), (i *= n)), Ut.clearColor(t, e, i, n);
    }
    function n() {
      Ut.init(),
        Ut.scissor(Q.copy(ht).multiplyScalar(st)),
        Ut.viewport(et.copy(ut).multiplyScalar(st)),
        i(nt.r, nt.g, nt.b, rt);
    }
    function a() {
      (Z = B = null), (Y = ''), (X = -1), Ut.reset();
    }
    function o(t) {
      t.preventDefault(), a(), n(), Dt.clear();
    }
    function s(t) {
      (t = t.target),
        t.removeEventListener('dispose', s),
        l(t),
        Dt['delete'](t);
    }
    function l(t) {
      var e = Dt.get(t).program;
      (t.program = void 0), void 0 !== e && Ot.releaseProgram(e);
    }
    function u(t, e) {
      return Math.abs(e[0]) - Math.abs(t[0]);
    }
    function d(t, e) {
      return t.object.renderOrder !== e.object.renderOrder
        ? t.object.renderOrder - e.object.renderOrder
        : t.material.program &&
          e.material.program &&
          t.material.program !== e.material.program
          ? t.material.program.id - e.material.program.id
          : t.material.id !== e.material.id
            ? t.material.id - e.material.id
            : t.z !== e.z ? t.z - e.z : t.id - e.id;
    }
    function p(t, e) {
      return t.object.renderOrder !== e.object.renderOrder
        ? t.object.renderOrder - e.object.renderOrder
        : t.z !== e.z ? e.z - t.z : t.id - e.id;
    }
    function f(t, e, i, n, r) {
      var a;
      i.transparent ? ((n = U), (a = ++D)) : ((n = C), (a = ++I)),
        (a = n[a]),
        void 0 !== a
          ? ((a.id = t.id),
            (a.object = t),
            (a.geometry = e),
            (a.material = i),
            (a.z = At.z),
            (a.group = r))
          : ((a = {
              id: t.id,
              object: t,
              geometry: e,
              material: i,
              z: At.z,
              group: r
            }),
            n.push(a));
    }
    function m(t) {
      if (!dt.intersectsSphere(t)) return !1;
      var e = pt.numPlanes;
      if (0 === e) return !0;
      var i = z.clippingPlanes,
        n = t.center;
      t = -t.radius;
      var r = 0;
      do {
        if (i[r].distanceToPoint(n) < t) return !1;
      } while (++r !== e);
      return !0;
    }
    function g(t, e) {
      if (!1 !== t.visible) {
        if (0 != (t.layers.mask & e.layers.mask))
          if (t.isLight) P.push(t);
          else if (t.isSprite) {
            var i;
            (i = !1 === t.frustumCulled) ||
              (St.center.set(0, 0, 0),
              (St.radius = 0.7071067811865476),
              St.applyMatrix4(t.matrixWorld),
              (i = !0 === m(St))),
              i && F.push(t);
          } else if (t.isLensFlare) O.push(t);
          else if (t.isImmediateRenderObject)
            !0 === z.sortObjects &&
              (At.setFromMatrixPosition(t.matrixWorld), At.applyProjection(Tt)),
              f(t, null, t.material, At.z, null);
          else if (
            (t.isMesh || t.isLine || t.isPoints) &&
            (t.isSkinnedMesh && t.skeleton.update(),
            (i = !1 === t.frustumCulled) ||
              ((i = t.geometry),
              null === i.boundingSphere && i.computeBoundingSphere(),
              St.copy(i.boundingSphere).applyMatrix4(t.matrixWorld),
              (i = !0 === m(St))),
            i)
          ) {
            var n = t.material;
            if (!0 === n.visible)
              if (
                (!0 === z.sortObjects &&
                  (At.setFromMatrixPosition(t.matrixWorld),
                  At.applyProjection(Tt)),
                (i = Ft.update(t)),
                n.isMultiMaterial)
              )
                for (
                  var r = i.groups, a = n.materials, n = 0, o = r.length;
                  n < o;
                  n++
                ) {
                  var s = r[n],
                    c = a[s.materialIndex];
                  !0 === c.visible && f(t, i, c, At.z, s);
                }
              else f(t, i, n, At.z, null);
          }
        for (i = t.children, n = 0, o = i.length; n < o; n++) g(i[n], e);
      }
    }
    function v(t, e, i, n) {
      for (var r = 0, a = t.length; r < a; r++) {
        var o = t[r],
          s = o.object,
          c = o.geometry,
          h = void 0 === n ? o.material : n,
          o = o.group;
        if (
          (s.modelViewMatrix.multiplyMatrices(
            e.matrixWorldInverse,
            s.matrixWorld
          ),
          s.normalMatrix.getNormalMatrix(s.modelViewMatrix),
          s.isImmediateRenderObject)
        ) {
          y(h);
          var l = x(e, i, h, s);
          (Y = ''),
            s.render(function(t) {
              z.renderBufferImmediate(t, l, h);
            });
        } else
          null !== s.onBeforeRender && s.onBeforeRender(),
            z.renderBufferDirect(e, i, c, h, s, o);
      }
    }
    function y(t) {
      2 === t.side ? Ut.disable(Pt.CULL_FACE) : Ut.enable(Pt.CULL_FACE),
        Ut.setFlipSided(1 === t.side),
        !0 === t.transparent
          ? Ut.setBlending(
              t.blending,
              t.blendEquation,
              t.blendSrc,
              t.blendDst,
              t.blendEquationAlpha,
              t.blendSrcAlpha,
              t.blendDstAlpha,
              t.premultipliedAlpha
            )
          : Ut.setBlending(0),
        Ut.setDepthFunc(t.depthFunc),
        Ut.setDepthTest(t.depthTest),
        Ut.setDepthWrite(t.depthWrite),
        Ut.setColorWrite(t.colorWrite),
        Ut.setPolygonOffset(
          t.polygonOffset,
          t.polygonOffsetFactor,
          t.polygonOffsetUnits
        );
    }
    function x(e, i, n, r) {
      it = 0;
      var a = Dt.get(n);
      if (
        (ft &&
          (xt || e !== Z) &&
          pt.setState(
            n.clippingPlanes,
            n.clipShadows,
            e,
            a,
            e === Z && n.id === X
          ),
        !1 === n.needsUpdate &&
          (void 0 === a.program
            ? (n.needsUpdate = !0)
            : n.fog && a.fog !== i
              ? (n.needsUpdate = !0)
              : n.lights && a.lightsHash !== Lt.hash
                ? (n.needsUpdate = !0)
                : void 0 !== a.numClippingPlanes &&
                  a.numClippingPlanes !== pt.numPlanes &&
                  (n.needsUpdate = !0)),
        n.needsUpdate)
      ) {
        t: {
          var o = Dt.get(n),
            c = Ot.getParameters(n, Lt, i, pt.numPlanes, r),
            h = Ot.getProgramCode(n, c),
            u = o.program,
            d = !0;
          if (void 0 === u) n.addEventListener('dispose', s);
          else if (u.code !== h) l(n);
          else {
            if (void 0 !== c.shaderID) break t;
            d = !1;
          }
          if (
            (d &&
              (c.shaderID
                ? ((u = qn[c.shaderID]),
                  (o.__webglShader = {
                    name: n.type,
                    uniforms: t.UniformsUtils.clone(u.uniforms),
                    vertexShader: u.vertexShader,
                    fragmentShader: u.fragmentShader
                  }))
                : (o.__webglShader = {
                    name: n.type,
                    uniforms: n.uniforms,
                    vertexShader: n.vertexShader,
                    fragmentShader: n.fragmentShader
                  }),
              (n.__webglShader = o.__webglShader),
              (u = Ot.acquireProgram(n, c, h)),
              (o.program = u),
              (n.program = u)),
            (c = u.getAttributes()),
            n.morphTargets)
          )
            for (h = n.numSupportedMorphTargets = 0; h < z.maxMorphTargets; h++)
              0 <= c['morphTarget' + h] && n.numSupportedMorphTargets++;
          if (n.morphNormals)
            for (h = n.numSupportedMorphNormals = 0; h < z.maxMorphNormals; h++)
              0 <= c['morphNormal' + h] && n.numSupportedMorphNormals++;
          (c = o.__webglShader.uniforms),
            ((n.isShaderMaterial || n.isRawShaderMaterial) &&
              !0 !== n.clipping) ||
              ((o.numClippingPlanes = pt.numPlanes),
              (c.clippingPlanes = pt.uniform)),
            (o.fog = i),
            (o.lightsHash = Lt.hash),
            n.lights &&
              ((c.ambientLightColor.value = Lt.ambient),
              (c.directionalLights.value = Lt.directional),
              (c.spotLights.value = Lt.spot),
              (c.pointLights.value = Lt.point),
              (c.hemisphereLights.value = Lt.hemi),
              (c.directionalShadowMap.value = Lt.directionalShadowMap),
              (c.directionalShadowMatrix.value = Lt.directionalShadowMatrix),
              (c.spotShadowMap.value = Lt.spotShadowMap),
              (c.spotShadowMatrix.value = Lt.spotShadowMatrix),
              (c.pointShadowMap.value = Lt.pointShadowMap),
              (c.pointShadowMatrix.value = Lt.pointShadowMatrix)),
            (h = o.program.getUniforms()),
            (h = H.seqWithValue(h.seq, c)),
            (o.uniformsList = h),
            (o.dynamicUniforms = H.splitDynamic(h, c));
        }
        n.needsUpdate = !1;
      }
      var p = !1,
        d = (u = !1),
        o = a.program,
        h = o.getUniforms(),
        c = a.__webglShader.uniforms;
      if (
        (o.id !== B && (Pt.useProgram(o.program), (B = o.id), (d = u = p = !0)),
        n.id !== X && ((X = n.id), (u = !0)),
        (p || e !== Z) &&
          (h.set(Pt, e, 'projectionMatrix'),
          It.logarithmicDepthBuffer &&
            h.setValue(
              Pt,
              'logDepthBufFC',
              2 / (Math.log(e.far + 1) / Math.LN2)
            ),
          e !== Z && ((Z = e), (d = u = !0)),
          (n.isShaderMaterial ||
            n.isMeshPhongMaterial ||
            n.isMeshStandardMaterial ||
            n.envMap) &&
            void 0 !== (p = h.map.cameraPosition) &&
            p.setValue(Pt, At.setFromMatrixPosition(e.matrixWorld)),
          (n.isMeshPhongMaterial ||
            n.isMeshLambertMaterial ||
            n.isMeshBasicMaterial ||
            n.isMeshStandardMaterial ||
            n.isShaderMaterial ||
            n.skinning) &&
            h.setValue(Pt, 'viewMatrix', e.matrixWorldInverse),
          h.set(Pt, z, 'toneMappingExposure'),
          h.set(Pt, z, 'toneMappingWhitePoint')),
        n.skinning &&
          (h.setOptional(Pt, r, 'bindMatrix'),
          h.setOptional(Pt, r, 'bindMatrixInverse'),
          (p = r.skeleton)) &&
          (It.floatVertexTextures && p.useVertexTexture
            ? (h.set(Pt, p, 'boneTexture'),
              h.set(Pt, p, 'boneTextureWidth'),
              h.set(Pt, p, 'boneTextureHeight'))
            : h.setOptional(Pt, p, 'boneMatrices')),
        u)
      ) {
        if (
          (n.lights &&
            ((u = d),
            (c.ambientLightColor.needsUpdate = u),
            (c.directionalLights.needsUpdate = u),
            (c.pointLights.needsUpdate = u),
            (c.spotLights.needsUpdate = u),
            (c.hemisphereLights.needsUpdate = u)),
          i &&
            n.fog &&
            ((c.fogColor.value = i.color),
            i.isFog
              ? ((c.fogNear.value = i.near), (c.fogFar.value = i.far))
              : i.isFogExp2 && (c.fogDensity.value = i.density)),
          n.isMeshBasicMaterial ||
            n.isMeshLambertMaterial ||
            n.isMeshPhongMaterial ||
            n.isMeshStandardMaterial ||
            n.isMeshDepthMaterial)
        ) {
          (c.opacity.value = n.opacity),
            (c.diffuse.value = n.color),
            n.emissive &&
              c.emissive.value
                .copy(n.emissive)
                .multiplyScalar(n.emissiveIntensity),
            (c.map.value = n.map),
            (c.specularMap.value = n.specularMap),
            (c.alphaMap.value = n.alphaMap),
            n.aoMap &&
              ((c.aoMap.value = n.aoMap),
              (c.aoMapIntensity.value = n.aoMapIntensity));
          var f;
          n.map
            ? (f = n.map)
            : n.specularMap
              ? (f = n.specularMap)
              : n.displacementMap
                ? (f = n.displacementMap)
                : n.normalMap
                  ? (f = n.normalMap)
                  : n.bumpMap
                    ? (f = n.bumpMap)
                    : n.roughnessMap
                      ? (f = n.roughnessMap)
                      : n.metalnessMap
                        ? (f = n.metalnessMap)
                        : n.alphaMap
                          ? (f = n.alphaMap)
                          : n.emissiveMap && (f = n.emissiveMap),
            void 0 !== f &&
              (f.isWebGLRenderTarget && (f = f.texture),
              (i = f.offset),
              (f = f.repeat),
              c.offsetRepeat.value.set(i.x, i.y, f.x, f.y)),
            (c.envMap.value = n.envMap),
            (c.flipEnvMap.value = n.envMap && n.envMap.isCubeTexture ? -1 : 1),
            (c.reflectivity.value = n.reflectivity),
            (c.refractionRatio.value = n.refractionRatio);
        }
        n.isLineBasicMaterial
          ? ((c.diffuse.value = n.color), (c.opacity.value = n.opacity))
          : n.isLineDashedMaterial
            ? ((c.diffuse.value = n.color),
              (c.opacity.value = n.opacity),
              (c.dashSize.value = n.dashSize),
              (c.totalSize.value = n.dashSize + n.gapSize),
              (c.scale.value = n.scale))
            : n.isPointsMaterial
              ? ((c.diffuse.value = n.color),
                (c.opacity.value = n.opacity),
                (c.size.value = n.size * st),
                (c.scale.value = 0.5 * w.clientHeight),
                (c.map.value = n.map),
                null !== n.map &&
                  ((f = n.map.offset),
                  (i = n.map.repeat),
                  c.offsetRepeat.value.set(f.x, f.y, i.x, i.y)))
              : n.isMeshLambertMaterial
                ? (n.lightMap &&
                    ((c.lightMap.value = n.lightMap),
                    (c.lightMapIntensity.value = n.lightMapIntensity)),
                  n.emissiveMap && (c.emissiveMap.value = n.emissiveMap))
                : n.isMeshPhongMaterial
                  ? ((c.specular.value = n.specular),
                    (c.shininess.value = Math.max(n.shininess, 1e-4)),
                    n.lightMap &&
                      ((c.lightMap.value = n.lightMap),
                      (c.lightMapIntensity.value = n.lightMapIntensity)),
                    n.emissiveMap && (c.emissiveMap.value = n.emissiveMap),
                    n.bumpMap &&
                      ((c.bumpMap.value = n.bumpMap),
                      (c.bumpScale.value = n.bumpScale)),
                    n.normalMap &&
                      ((c.normalMap.value = n.normalMap),
                      c.normalScale.value.copy(n.normalScale)),
                    n.displacementMap &&
                      ((c.displacementMap.value = n.displacementMap),
                      (c.displacementScale.value = n.displacementScale),
                      (c.displacementBias.value = n.displacementBias)))
                  : n.isMeshPhysicalMaterial
                    ? ((c.clearCoat.value = n.clearCoat),
                      (c.clearCoatRoughness.value = n.clearCoatRoughness),
                      _(c, n))
                    : n.isMeshStandardMaterial
                      ? _(c, n)
                      : n.isMeshDepthMaterial
                        ? n.displacementMap &&
                          ((c.displacementMap.value = n.displacementMap),
                          (c.displacementScale.value = n.displacementScale),
                          (c.displacementBias.value = n.displacementBias))
                        : n.isMeshNormalMaterial &&
                          (c.opacity.value = n.opacity),
          H.upload(Pt, a.uniformsList, c, z);
      }
      return (
        h.set(Pt, r, 'modelViewMatrix'),
        h.set(Pt, r, 'normalMatrix'),
        h.setValue(Pt, 'modelMatrix', r.matrixWorld),
        (a = a.dynamicUniforms),
        null !== a && (H.evalDynamic(a, c, r, n, e), H.upload(Pt, a, c, z)),
        o
      );
    }
    function _(t, e) {
      (t.roughness.value = e.roughness),
        (t.metalness.value = e.metalness),
        e.roughnessMap && (t.roughnessMap.value = e.roughnessMap),
        e.metalnessMap && (t.metalnessMap.value = e.metalnessMap),
        e.lightMap &&
          ((t.lightMap.value = e.lightMap),
          (t.lightMapIntensity.value = e.lightMapIntensity)),
        e.emissiveMap && (t.emissiveMap.value = e.emissiveMap),
        e.bumpMap &&
          ((t.bumpMap.value = e.bumpMap), (t.bumpScale.value = e.bumpScale)),
        e.normalMap &&
          ((t.normalMap.value = e.normalMap),
          t.normalScale.value.copy(e.normalScale)),
        e.displacementMap &&
          ((t.displacementMap.value = e.displacementMap),
          (t.displacementScale.value = e.displacementScale),
          (t.displacementBias.value = e.displacementBias)),
        e.envMap && (t.envMapIntensity.value = e.envMapIntensity);
    }
    function b(t) {
      var e;
      if (1e3 === t) return Pt.REPEAT;
      if (1001 === t) return Pt.CLAMP_TO_EDGE;
      if (1002 === t) return Pt.MIRRORED_REPEAT;
      if (1003 === t) return Pt.NEAREST;
      if (1004 === t) return Pt.NEAREST_MIPMAP_NEAREST;
      if (1005 === t) return Pt.NEAREST_MIPMAP_LINEAR;
      if (1006 === t) return Pt.LINEAR;
      if (1007 === t) return Pt.LINEAR_MIPMAP_NEAREST;
      if (1008 === t) return Pt.LINEAR_MIPMAP_LINEAR;
      if (1009 === t) return Pt.UNSIGNED_BYTE;
      if (1017 === t) return Pt.UNSIGNED_SHORT_4_4_4_4;
      if (1018 === t) return Pt.UNSIGNED_SHORT_5_5_5_1;
      if (1019 === t) return Pt.UNSIGNED_SHORT_5_6_5;
      if (1010 === t) return Pt.BYTE;
      if (1011 === t) return Pt.SHORT;
      if (1012 === t) return Pt.UNSIGNED_SHORT;
      if (1013 === t) return Pt.INT;
      if (1014 === t) return Pt.UNSIGNED_INT;
      if (1015 === t) return Pt.FLOAT;
      if (null !== (e = Ct.get('OES_texture_half_float')) && 1016 === t)
        return e.HALF_FLOAT_OES;
      if (1021 === t) return Pt.ALPHA;
      if (1022 === t) return Pt.RGB;
      if (1023 === t) return Pt.RGBA;
      if (1024 === t) return Pt.LUMINANCE;
      if (1025 === t) return Pt.LUMINANCE_ALPHA;
      if (1026 === t) return Pt.DEPTH_COMPONENT;
      if (1027 === t) return Pt.DEPTH_STENCIL;
      if (100 === t) return Pt.FUNC_ADD;
      if (101 === t) return Pt.FUNC_SUBTRACT;
      if (102 === t) return Pt.FUNC_REVERSE_SUBTRACT;
      if (200 === t) return Pt.ZERO;
      if (201 === t) return Pt.ONE;
      if (202 === t) return Pt.SRC_COLOR;
      if (203 === t) return Pt.ONE_MINUS_SRC_COLOR;
      if (204 === t) return Pt.SRC_ALPHA;
      if (205 === t) return Pt.ONE_MINUS_SRC_ALPHA;
      if (206 === t) return Pt.DST_ALPHA;
      if (207 === t) return Pt.ONE_MINUS_DST_ALPHA;
      if (208 === t) return Pt.DST_COLOR;
      if (209 === t) return Pt.ONE_MINUS_DST_COLOR;
      if (210 === t) return Pt.SRC_ALPHA_SATURATE;
      if (null !== (e = Ct.get('WEBGL_compressed_texture_s3tc'))) {
        if (2001 === t) return e.COMPRESSED_RGB_S3TC_DXT1_EXT;
        if (2002 === t) return e.COMPRESSED_RGBA_S3TC_DXT1_EXT;
        if (2003 === t) return e.COMPRESSED_RGBA_S3TC_DXT3_EXT;
        if (2004 === t) return e.COMPRESSED_RGBA_S3TC_DXT5_EXT;
      }
      if (null !== (e = Ct.get('WEBGL_compressed_texture_pvrtc'))) {
        if (2100 === t) return e.COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
        if (2101 === t) return e.COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
        if (2102 === t) return e.COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;
        if (2103 === t) return e.COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;
      }
      if (null !== (e = Ct.get('WEBGL_compressed_texture_etc1')) && 2151 === t)
        return e.COMPRESSED_RGB_ETC1_WEBGL;
      if (null !== (e = Ct.get('EXT_blend_minmax'))) {
        if (103 === t) return e.MIN_EXT;
        if (104 === t) return e.MAX_EXT;
      }
      return (
        (e = Ct.get('WEBGL_depth_texture')),
        null !== e && 1020 === t ? e.UNSIGNED_INT_24_8_WEBGL : 0
      );
    }
    console.log('THREE.WebGLRenderer', '81'), (e = e || {});
    var w =
        void 0 !== e.canvas
          ? e.canvas
          : document.createElementNS('http://www.w3.org/1999/xhtml', 'canvas'),
      M = void 0 !== e.context ? e.context : null,
      E = void 0 !== e.alpha && e.alpha,
      S = void 0 === e.depth || e.depth,
      T = void 0 === e.stencil || e.stencil,
      A = void 0 !== e.antialias && e.antialias,
      L = void 0 === e.premultipliedAlpha || e.premultipliedAlpha,
      R = void 0 !== e.preserveDrawingBuffer && e.preserveDrawingBuffer,
      P = [],
      C = [],
      I = -1,
      U = [],
      D = -1,
      N = new Float32Array(8),
      F = [],
      O = [];
    (this.domElement = w),
      (this.context = null),
      (this.sortObjects = this.autoClearStencil = this.autoClearDepth = this.autoClearColor = this.autoClear = !0),
      (this.clippingPlanes = []),
      (this.localClippingEnabled = !1),
      (this.gammaFactor = 2),
      (this.physicallyCorrectLights = this.gammaOutput = this.gammaInput = !1),
      (this.toneMappingWhitePoint = this.toneMappingExposure = this.toneMapping = 1),
      (this.maxMorphTargets = 8),
      (this.maxMorphNormals = 4);
    var z = this,
      B = null,
      G = null,
      k = null,
      X = -1,
      Y = '',
      Z = null,
      Q = new r(),
      K = null,
      et = new r(),
      it = 0,
      nt = new V(0),
      rt = 0,
      at = w.width,
      ot = w.height,
      st = 1,
      ht = new r(0, 0, at, ot),
      lt = !1,
      ut = new r(0, 0, at, ot),
      dt = new $(),
      pt = new Xt(),
      ft = !1,
      xt = !1,
      St = new J(),
      Tt = new h(),
      At = new c(),
      Lt = {
        hash: '',
        ambient: [0, 0, 0],
        directional: [],
        directionalShadowMap: [],
        directionalShadowMatrix: [],
        spot: [],
        spotShadowMap: [],
        spotShadowMatrix: [],
        point: [],
        pointShadowMap: [],
        pointShadowMatrix: [],
        hemi: [],
        shadows: []
      },
      Rt = { calls: 0, vertices: 0, faces: 0, points: 0 };
    this.info = {
      render: Rt,
      memory: { geometries: 0, textures: 0 },
      programs: null
    };
    var Pt;
    try {
      if (
        ((E = {
          alpha: E,
          depth: S,
          stencil: T,
          antialias: A,
          premultipliedAlpha: L,
          preserveDrawingBuffer: R
        }),
        null ===
          (Pt =
            M ||
            w.getContext('webgl', E) ||
            w.getContext('experimental-webgl', E)))
      ) {
        if (null !== w.getContext('webgl'))
          throw 'Error creating WebGL context with your selected attributes.';
        throw 'Error creating WebGL context.';
      }
      void 0 === Pt.getShaderPrecisionFormat &&
        (Pt.getShaderPrecisionFormat = function() {
          return { rangeMin: 1, rangeMax: 1, precision: 1 };
        }),
        w.addEventListener('webglcontextlost', o, !1);
    } catch (t) {
      console.error('THREE.WebGLRenderer: ' + t);
    }
    var Ct = new Wt(Pt);
    Ct.get('WEBGL_depth_texture'),
      Ct.get('OES_texture_float'),
      Ct.get('OES_texture_float_linear'),
      Ct.get('OES_texture_half_float'),
      Ct.get('OES_texture_half_float_linear'),
      Ct.get('OES_standard_derivatives'),
      Ct.get('ANGLE_instanced_arrays'),
      Ct.get('OES_element_index_uint') && (mt.MaxIndex = 4294967296);
    var It = new jt(Pt, Ct, e),
      Ut = new kt(Pt, Ct, b),
      Dt = new Vt(),
      Nt = new Ht(Pt, Ct, Ut, Dt, It, b, this.info),
      Ft = new Gt(Pt, Dt, this.info),
      Ot = new zt(this, It),
      Bt = new Et();
    this.info.programs = Ot.programs;
    var qt = new Mt(Pt, Ct, Rt),
      Yt = new wt(Pt, Ct, Rt),
      Zt = new bt(-1, 1, 1, -1, 0, 1),
      Jt = new _t(),
      Qt = new gt(
        new yt(2, 2),
        new ct({ depthTest: !1, depthWrite: !1, fog: !1 })
      );
    e = qn.cube;
    var Kt = new gt(
      new vt(5, 5, 5),
      new q({
        uniforms: e.uniforms,
        vertexShader: e.vertexShader,
        fragmentShader: e.fragmentShader,
        side: 1,
        depthTest: !1,
        depthWrite: !1,
        fog: !1
      })
    );
    n(),
      (this.context = Pt),
      (this.capabilities = It),
      (this.extensions = Ct),
      (this.properties = Dt),
      (this.state = Ut);
    var $t = new tt(this, Lt, Ft, It);
    this.shadowMap = $t;
    var te = new W(this, F),
      ee = new j(this, O);
    (this.getContext = function() {
      return Pt;
    }),
      (this.getContextAttributes = function() {
        return Pt.getContextAttributes();
      }),
      (this.forceContextLoss = function() {
        Ct.get('WEBGL_lose_context').loseContext();
      }),
      (this.getMaxAnisotropy = function() {
        return It.getMaxAnisotropy();
      }),
      (this.getPrecision = function() {
        return It.precision;
      }),
      (this.getPixelRatio = function() {
        return st;
      }),
      (this.setPixelRatio = function(t) {
        void 0 !== t && ((st = t), this.setSize(ut.z, ut.w, !1));
      }),
      (this.getSize = function() {
        return { width: at, height: ot };
      }),
      (this.setSize = function(t, e, i) {
        (at = t),
          (ot = e),
          (w.width = t * st),
          (w.height = e * st),
          !1 !== i && ((w.style.width = t + 'px'), (w.style.height = e + 'px')),
          this.setViewport(0, 0, t, e);
      }),
      (this.setViewport = function(t, e, i, n) {
        Ut.viewport(ut.set(t, e, i, n));
      }),
      (this.setScissor = function(t, e, i, n) {
        Ut.scissor(ht.set(t, e, i, n));
      }),
      (this.setScissorTest = function(t) {
        Ut.setScissorTest((lt = t));
      }),
      (this.getClearColor = function() {
        return nt;
      }),
      (this.setClearColor = function(t, e) {
        nt.set(t), (rt = void 0 !== e ? e : 1), i(nt.r, nt.g, nt.b, rt);
      }),
      (this.getClearAlpha = function() {
        return rt;
      }),
      (this.setClearAlpha = function(t) {
        (rt = t), i(nt.r, nt.g, nt.b, rt);
      }),
      (this.clear = function(t, e, i) {
        var n = 0;
        (void 0 === t || t) && (n |= Pt.COLOR_BUFFER_BIT),
          (void 0 === e || e) && (n |= Pt.DEPTH_BUFFER_BIT),
          (void 0 === i || i) && (n |= Pt.STENCIL_BUFFER_BIT),
          Pt.clear(n);
      }),
      (this.clearColor = function() {
        this.clear(!0, !1, !1);
      }),
      (this.clearDepth = function() {
        this.clear(!1, !0, !1);
      }),
      (this.clearStencil = function() {
        this.clear(!1, !1, !0);
      }),
      (this.clearTarget = function(t, e, i, n) {
        this.setRenderTarget(t), this.clear(e, i, n);
      }),
      (this.resetGLState = a),
      (this.dispose = function() {
        (U = []),
          (D = -1),
          (C = []),
          (I = -1),
          w.removeEventListener('webglcontextlost', o, !1);
      }),
      (this.renderBufferImmediate = function(t, e, i) {
        Ut.initAttributes();
        var n = Dt.get(t);
        if (
          (t.hasPositions && !n.position && (n.position = Pt.createBuffer()),
          t.hasNormals && !n.normal && (n.normal = Pt.createBuffer()),
          t.hasUvs && !n.uv && (n.uv = Pt.createBuffer()),
          t.hasColors && !n.color && (n.color = Pt.createBuffer()),
          (e = e.getAttributes()),
          t.hasPositions &&
            (Pt.bindBuffer(Pt.ARRAY_BUFFER, n.position),
            Pt.bufferData(Pt.ARRAY_BUFFER, t.positionArray, Pt.DYNAMIC_DRAW),
            Ut.enableAttribute(e.position),
            Pt.vertexAttribPointer(e.position, 3, Pt.FLOAT, !1, 0, 0)),
          t.hasNormals)
        ) {
          if (
            (Pt.bindBuffer(Pt.ARRAY_BUFFER, n.normal),
            !i.isMeshPhongMaterial &&
              !i.isMeshStandardMaterial &&
              1 === i.shading)
          )
            for (var r = 0, a = 3 * t.count; r < a; r += 9) {
              var o = t.normalArray,
                s = (o[r + 0] + o[r + 3] + o[r + 6]) / 3,
                c = (o[r + 1] + o[r + 4] + o[r + 7]) / 3,
                h = (o[r + 2] + o[r + 5] + o[r + 8]) / 3;
              (o[r + 0] = s),
                (o[r + 1] = c),
                (o[r + 2] = h),
                (o[r + 3] = s),
                (o[r + 4] = c),
                (o[r + 5] = h),
                (o[r + 6] = s),
                (o[r + 7] = c),
                (o[r + 8] = h);
            }
          Pt.bufferData(Pt.ARRAY_BUFFER, t.normalArray, Pt.DYNAMIC_DRAW),
            Ut.enableAttribute(e.normal),
            Pt.vertexAttribPointer(e.normal, 3, Pt.FLOAT, !1, 0, 0);
        }
        t.hasUvs &&
          i.map &&
          (Pt.bindBuffer(Pt.ARRAY_BUFFER, n.uv),
          Pt.bufferData(Pt.ARRAY_BUFFER, t.uvArray, Pt.DYNAMIC_DRAW),
          Ut.enableAttribute(e.uv),
          Pt.vertexAttribPointer(e.uv, 2, Pt.FLOAT, !1, 0, 0)),
          t.hasColors &&
            0 !== i.vertexColors &&
            (Pt.bindBuffer(Pt.ARRAY_BUFFER, n.color),
            Pt.bufferData(Pt.ARRAY_BUFFER, t.colorArray, Pt.DYNAMIC_DRAW),
            Ut.enableAttribute(e.color),
            Pt.vertexAttribPointer(e.color, 3, Pt.FLOAT, !1, 0, 0)),
          Ut.disableUnusedAttributes(),
          Pt.drawArrays(Pt.TRIANGLES, 0, t.count),
          (t.count = 0);
      }),
      (this.renderBufferDirect = function(t, e, i, n, r, a) {
        y(n);
        var o = x(t, e, n, r),
          s = !1;
        if (
          ((t = i.id + '_' + o.id + '_' + n.wireframe),
          t !== Y && ((Y = t), (s = !0)),
          void 0 !== (e = r.morphTargetInfluences))
        ) {
          var c = [];
          t = 0;
          for (var h = e.length; t < h; t++) (s = e[t]), c.push([s, t]);
          c.sort(u), 8 < c.length && (c.length = 8);
          var l = i.morphAttributes;
          for (t = 0, h = c.length; t < h; t++)
            (s = c[t]),
              (N[t] = s[0]),
              0 !== s[0]
                ? ((e = s[1]),
                  !0 === n.morphTargets &&
                    l.position &&
                    i.addAttribute('morphTarget' + t, l.position[e]),
                  !0 === n.morphNormals &&
                    l.normal &&
                    i.addAttribute('morphNormal' + t, l.normal[e]))
                : (!0 === n.morphTargets &&
                    i.removeAttribute('morphTarget' + t),
                  !0 === n.morphNormals &&
                    i.removeAttribute('morphNormal' + t));
          for (t = c.length, e = N.length; t < e; t++) N[t] = 0;
          o.getUniforms().setValue(Pt, 'morphTargetInfluences', N), (s = !0);
        }
        if (
          ((e = i.index),
          (h = i.attributes.position),
          (c = 1),
          !0 === n.wireframe && ((e = Ft.getWireframeAttribute(i)), (c = 2)),
          null !== e ? ((t = Yt), t.setIndex(e)) : (t = qt),
          s)
        ) {
          var d,
            s = void 0;
          if (
            i &&
            i.isInstancedBufferGeometry &&
            null === (d = Ct.get('ANGLE_instanced_arrays'))
          )
            console.error(
              'THREE.WebGLRenderer.setupVertexAttributes: using THREE.InstancedBufferGeometry but hardware does not support extension ANGLE_instanced_arrays.'
            );
          else {
            void 0 === s && (s = 0), Ut.initAttributes();
            var p,
              l = i.attributes,
              o = o.getAttributes(),
              f = n.defaultAttributeValues;
            for (p in o) {
              var m = o[p];
              if (0 <= m) {
                var g = l[p];
                if (void 0 !== g) {
                  var v = Pt.FLOAT,
                    _ = g.array,
                    b = g.normalized;
                  _ instanceof Float32Array
                    ? (v = Pt.FLOAT)
                    : _ instanceof Float64Array
                      ? console.warn(
                          'Unsupported data buffer format: Float64Array'
                        )
                      : _ instanceof Uint16Array
                        ? (v = Pt.UNSIGNED_SHORT)
                        : _ instanceof Int16Array
                          ? (v = Pt.SHORT)
                          : _ instanceof Uint32Array
                            ? (v = Pt.UNSIGNED_INT)
                            : _ instanceof Int32Array
                              ? (v = Pt.INT)
                              : _ instanceof Int8Array
                                ? (v = Pt.BYTE)
                                : _ instanceof Uint8Array &&
                                  (v = Pt.UNSIGNED_BYTE);
                  var _ = g.itemSize,
                    w = Ft.getAttributeBuffer(g);
                  if (g && g.isInterleavedBufferAttribute) {
                    var M = g.data,
                      E = M.stride,
                      g = g.offset;
                    M && M.isInstancedInterleavedBuffer
                      ? (Ut.enableAttributeAndDivisor(m, M.meshPerAttribute, d),
                        void 0 === i.maxInstancedCount &&
                          (i.maxInstancedCount = M.meshPerAttribute * M.count))
                      : Ut.enableAttribute(m),
                      Pt.bindBuffer(Pt.ARRAY_BUFFER, w),
                      Pt.vertexAttribPointer(
                        m,
                        _,
                        v,
                        b,
                        E * M.array.BYTES_PER_ELEMENT,
                        (s * E + g) * M.array.BYTES_PER_ELEMENT
                      );
                  } else
                    g && g.isInstancedBufferAttribute
                      ? (Ut.enableAttributeAndDivisor(m, g.meshPerAttribute, d),
                        void 0 === i.maxInstancedCount &&
                          (i.maxInstancedCount = g.meshPerAttribute * g.count))
                      : Ut.enableAttribute(m),
                      Pt.bindBuffer(Pt.ARRAY_BUFFER, w),
                      Pt.vertexAttribPointer(
                        m,
                        _,
                        v,
                        b,
                        0,
                        s * _ * g.array.BYTES_PER_ELEMENT
                      );
                } else if (void 0 !== f && void 0 !== (v = f[p]))
                  switch (v.length) {
                    case 2:
                      Pt.vertexAttrib2fv(m, v);
                      break;
                    case 3:
                      Pt.vertexAttrib3fv(m, v);
                      break;
                    case 4:
                      Pt.vertexAttrib4fv(m, v);
                      break;
                    default:
                      Pt.vertexAttrib1fv(m, v);
                  }
              }
            }
            Ut.disableUnusedAttributes();
          }
          null !== e &&
            Pt.bindBuffer(Pt.ELEMENT_ARRAY_BUFFER, Ft.getAttributeBuffer(e));
        }
        if (
          ((d = 0),
          null !== e ? (d = e.count) : void 0 !== h && (d = h.count),
          (e = i.drawRange.start * c),
          (h = null !== a ? a.start * c : 0),
          (p = Math.max(e, h)),
          0 !==
            (a = Math.max(
              0,
              Math.min(
                d,
                e + i.drawRange.count * c,
                h + (null !== a ? a.count * c : Infinity)
              ) -
                1 -
                p +
                1
            )))
        ) {
          if (r.isMesh)
            if (!0 === n.wireframe)
              Ut.setLineWidth(n.wireframeLinewidth * (null === G ? st : 1)),
                t.setMode(Pt.LINES);
            else
              switch (r.drawMode) {
                case 0:
                  t.setMode(Pt.TRIANGLES);
                  break;
                case 1:
                  t.setMode(Pt.TRIANGLE_STRIP);
                  break;
                case 2:
                  t.setMode(Pt.TRIANGLE_FAN);
              }
          else
            r.isLine
              ? ((n = n.linewidth),
                void 0 === n && (n = 1),
                Ut.setLineWidth(n * (null === G ? st : 1)),
                r.isLineSegments
                  ? t.setMode(Pt.LINES)
                  : t.setMode(Pt.LINE_STRIP))
              : r.isPoints && t.setMode(Pt.POINTS);
          i && i.isInstancedBufferGeometry
            ? 0 < i.maxInstancedCount && t.renderInstances(i, p, a)
            : t.render(p, a);
        }
      }),
      (this.render = function(t, e, n, r) {
        if (void 0 !== e && !0 !== e.isCamera)
          console.error(
            'THREE.WebGLRenderer.render: camera is not an instance of THREE.Camera.'
          );
        else {
          var a = t.fog;
          (Y = ''),
            (X = -1),
            (Z = null),
            !0 === t.autoUpdate && t.updateMatrixWorld(),
            null === e.parent && e.updateMatrixWorld(),
            e.matrixWorldInverse.getInverse(e.matrixWorld),
            Tt.multiplyMatrices(e.projectionMatrix, e.matrixWorldInverse),
            dt.setFromMatrix(Tt),
            (P.length = 0),
            (D = I = -1),
            (F.length = 0),
            (O.length = 0),
            (xt = this.localClippingEnabled),
            (ft = pt.init(this.clippingPlanes, xt, e)),
            g(t, e),
            (C.length = I + 1),
            (U.length = D + 1),
            !0 === z.sortObjects && (C.sort(d), U.sort(p)),
            ft && pt.beginShadows();
          for (var o = P, s = 0, c = 0, l = o.length; c < l; c++) {
            var u = o[c];
            u.castShadow && (Lt.shadows[s++] = u);
          }
          (Lt.shadows.length = s), $t.render(t, e);
          for (
            var f,
              m,
              y,
              x,
              o = P,
              _ = (u = 0),
              b = 0,
              w = e.matrixWorldInverse,
              M = 0,
              E = 0,
              S = 0,
              T = 0,
              s = 0,
              c = o.length;
            s < c;
            s++
          )
            if (
              ((l = o[s]),
              (f = l.color),
              (m = l.intensity),
              (y = l.distance),
              (x = l.shadow && l.shadow.map ? l.shadow.map.texture : null),
              l.isAmbientLight)
            )
              (u += f.r * m), (_ += f.g * m), (b += f.b * m);
            else if (l.isDirectionalLight) {
              var A = Bt.get(l);
              A.color.copy(l.color).multiplyScalar(l.intensity),
                A.direction.setFromMatrixPosition(l.matrixWorld),
                At.setFromMatrixPosition(l.target.matrixWorld),
                A.direction.sub(At),
                A.direction.transformDirection(w),
                (A.shadow = l.castShadow) &&
                  ((A.shadowBias = l.shadow.bias),
                  (A.shadowRadius = l.shadow.radius),
                  (A.shadowMapSize = l.shadow.mapSize)),
                (Lt.directionalShadowMap[M] = x),
                (Lt.directionalShadowMatrix[M] = l.shadow.matrix),
                (Lt.directional[M++] = A);
            } else
              l.isSpotLight
                ? ((A = Bt.get(l)),
                  A.position.setFromMatrixPosition(l.matrixWorld),
                  A.position.applyMatrix4(w),
                  A.color.copy(f).multiplyScalar(m),
                  (A.distance = y),
                  A.direction.setFromMatrixPosition(l.matrixWorld),
                  At.setFromMatrixPosition(l.target.matrixWorld),
                  A.direction.sub(At),
                  A.direction.transformDirection(w),
                  (A.coneCos = Math.cos(l.angle)),
                  (A.penumbraCos = Math.cos(l.angle * (1 - l.penumbra))),
                  (A.decay = 0 === l.distance ? 0 : l.decay),
                  (A.shadow = l.castShadow) &&
                    ((A.shadowBias = l.shadow.bias),
                    (A.shadowRadius = l.shadow.radius),
                    (A.shadowMapSize = l.shadow.mapSize)),
                  (Lt.spotShadowMap[S] = x),
                  (Lt.spotShadowMatrix[S] = l.shadow.matrix),
                  (Lt.spot[S++] = A))
                : l.isPointLight
                  ? ((A = Bt.get(l)),
                    A.position.setFromMatrixPosition(l.matrixWorld),
                    A.position.applyMatrix4(w),
                    A.color.copy(l.color).multiplyScalar(l.intensity),
                    (A.distance = l.distance),
                    (A.decay = 0 === l.distance ? 0 : l.decay),
                    (A.shadow = l.castShadow) &&
                      ((A.shadowBias = l.shadow.bias),
                      (A.shadowRadius = l.shadow.radius),
                      (A.shadowMapSize = l.shadow.mapSize)),
                    (Lt.pointShadowMap[E] = x),
                    void 0 === Lt.pointShadowMatrix[E] &&
                      (Lt.pointShadowMatrix[E] = new h()),
                    At.setFromMatrixPosition(l.matrixWorld).negate(),
                    Lt.pointShadowMatrix[E].identity().setPosition(At),
                    (Lt.point[E++] = A))
                  : l.isHemisphereLight &&
                    ((A = Bt.get(l)),
                    A.direction.setFromMatrixPosition(l.matrixWorld),
                    A.direction.transformDirection(w),
                    A.direction.normalize(),
                    A.skyColor.copy(l.color).multiplyScalar(m),
                    A.groundColor.copy(l.groundColor).multiplyScalar(m),
                    (Lt.hemi[T++] = A));
          (Lt.ambient[0] = u),
            (Lt.ambient[1] = _),
            (Lt.ambient[2] = b),
            (Lt.directional.length = M),
            (Lt.spot.length = S),
            (Lt.point.length = E),
            (Lt.hemi.length = T),
            (Lt.hash =
              M + ',' + E + ',' + S + ',' + T + ',' + Lt.shadows.length),
            ft && pt.endShadows(),
            (Rt.calls = 0),
            (Rt.vertices = 0),
            (Rt.faces = 0),
            (Rt.points = 0),
            void 0 === n && (n = null),
            this.setRenderTarget(n),
            (o = t.background),
            null === o
              ? i(nt.r, nt.g, nt.b, rt)
              : o && o.isColor && (i(o.r, o.g, o.b, 1), (r = !0)),
            (this.autoClear || r) &&
              this.clear(
                this.autoClearColor,
                this.autoClearDepth,
                this.autoClearStencil
              ),
            o && o.isCubeTexture
              ? (Jt.projectionMatrix.copy(e.projectionMatrix),
                Jt.matrixWorld.extractRotation(e.matrixWorld),
                Jt.matrixWorldInverse.getInverse(Jt.matrixWorld),
                (Kt.material.uniforms.tCube.value = o),
                Kt.modelViewMatrix.multiplyMatrices(
                  Jt.matrixWorldInverse,
                  Kt.matrixWorld
                ),
                Ft.update(Kt),
                z.renderBufferDirect(
                  Jt,
                  null,
                  Kt.geometry,
                  Kt.material,
                  Kt,
                  null
                ))
              : o &&
                o.isTexture &&
                ((Qt.material.map = o),
                Ft.update(Qt),
                z.renderBufferDirect(
                  Zt,
                  null,
                  Qt.geometry,
                  Qt.material,
                  Qt,
                  null
                )),
            t.overrideMaterial
              ? ((r = t.overrideMaterial), v(C, e, a, r), v(U, e, a, r))
              : (Ut.setBlending(0), v(C, e, a), v(U, e, a)),
            te.render(t, e),
            ee.render(t, e, et),
            n && Nt.updateRenderTargetMipmap(n),
            Ut.setDepthTest(!0),
            Ut.setDepthWrite(!0),
            Ut.setColorWrite(!0);
        }
      }),
      (this.setFaceCulling = function(t, e) {
        Ut.setCullFace(t), Ut.setFlipSided(0 === e);
      }),
      (this.allocTextureUnit = function() {
        var t = it;
        return (
          t >= It.maxTextures &&
            console.warn(
              'WebGLRenderer: trying to use ' +
                t +
                ' texture units while this GPU supports only ' +
                It.maxTextures
            ),
          (it += 1),
          t
        );
      }),
      (this.setTexture2D = (function() {
        var t = !1;
        return function(e, i) {
          e &&
            e.isWebGLRenderTarget &&
            (t ||
              (console.warn(
                "THREE.WebGLRenderer.setTexture2D: don't use render targets as textures. Use their .texture property instead."
              ),
              (t = !0)),
            (e = e.texture)),
            Nt.setTexture2D(e, i);
        };
      })()),
      (this.setTexture = (function() {
        var t = !1;
        return function(e, i) {
          t ||
            (console.warn(
              'THREE.WebGLRenderer: .setTexture is deprecated, use setTexture2D instead.'
            ),
            (t = !0)),
            Nt.setTexture2D(e, i);
        };
      })()),
      (this.setTextureCube = (function() {
        var t = !1;
        return function(e, i) {
          e &&
            e.isWebGLRenderTargetCube &&
            (t ||
              (console.warn(
                "THREE.WebGLRenderer.setTextureCube: don't use cube render targets as textures. Use their .texture property instead."
              ),
              (t = !0)),
            (e = e.texture)),
            (e && e.isCubeTexture) ||
            (Array.isArray(e.image) && 6 === e.image.length)
              ? Nt.setTextureCube(e, i)
              : Nt.setTextureCubeDynamic(e, i);
        };
      })()),
      (this.getCurrentRenderTarget = function() {
        return G;
      }),
      (this.setRenderTarget = function(t) {
        (G = t) &&
          void 0 === Dt.get(t).__webglFramebuffer &&
          Nt.setupRenderTarget(t);
        var e,
          i = t && t.isWebGLRenderTargetCube;
        t
          ? ((e = Dt.get(t)),
            (e = i
              ? e.__webglFramebuffer[t.activeCubeFace]
              : e.__webglFramebuffer),
            Q.copy(t.scissor),
            (K = t.scissorTest),
            et.copy(t.viewport))
          : ((e = null),
            Q.copy(ht).multiplyScalar(st),
            (K = lt),
            et.copy(ut).multiplyScalar(st)),
          k !== e && (Pt.bindFramebuffer(Pt.FRAMEBUFFER, e), (k = e)),
          Ut.scissor(Q),
          Ut.setScissorTest(K),
          Ut.viewport(et),
          i &&
            ((i = Dt.get(t.texture)),
            Pt.framebufferTexture2D(
              Pt.FRAMEBUFFER,
              Pt.COLOR_ATTACHMENT0,
              Pt.TEXTURE_CUBE_MAP_POSITIVE_X + t.activeCubeFace,
              i.__webglTexture,
              t.activeMipMapLevel
            ));
      }),
      (this.readRenderTargetPixels = function(t, e, i, n, r, a) {
        if (!1 === (t && t.isWebGLRenderTarget))
          console.error(
            'THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not THREE.WebGLRenderTarget.'
          );
        else {
          var o = Dt.get(t).__webglFramebuffer;
          if (o) {
            var s = !1;
            o !== k && (Pt.bindFramebuffer(Pt.FRAMEBUFFER, o), (s = !0));
            try {
              var c = t.texture,
                h = c.format,
                l = c.type;
              1023 !== h &&
              b(h) !== Pt.getParameter(Pt.IMPLEMENTATION_COLOR_READ_FORMAT)
                ? console.error(
                    'THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not in RGBA or implementation defined format.'
                  )
                : 1009 === l ||
                  b(l) === Pt.getParameter(Pt.IMPLEMENTATION_COLOR_READ_TYPE) ||
                  (1015 === l &&
                    (Ct.get('OES_texture_float') ||
                      Ct.get('WEBGL_color_buffer_float'))) ||
                  (1016 === l && Ct.get('EXT_color_buffer_half_float'))
                  ? Pt.checkFramebufferStatus(Pt.FRAMEBUFFER) ===
                    Pt.FRAMEBUFFER_COMPLETE
                    ? 0 <= e &&
                      e <= t.width - n &&
                      0 <= i &&
                      i <= t.height - r &&
                      Pt.readPixels(e, i, n, r, b(h), b(l), a)
                    : console.error(
                        'THREE.WebGLRenderer.readRenderTargetPixels: readPixels from renderTarget failed. Framebuffer not complete.'
                      )
                  : console.error(
                      'THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not in UnsignedByteType or implementation defined type.'
                    );
            } finally {
              s && Pt.bindFramebuffer(Pt.FRAMEBUFFER, k);
            }
          }
        }
      });
  }
  function Yt(t, e) {
    (this.name = ''),
      (this.color = new V(t)),
      (this.density = void 0 !== e ? e : 25e-5);
  }
  function Zt(t, e, i) {
    (this.name = ''),
      (this.color = new V(t)),
      (this.near = void 0 !== e ? e : 1),
      (this.far = void 0 !== i ? i : 1e3);
  }
  function Jt() {
    rt.call(this),
      (this.type = 'Scene'),
      (this.overrideMaterial = this.fog = this.background = null),
      (this.autoUpdate = !0);
  }
  function Qt(t, e, i, n, r) {
    rt.call(this),
      (this.lensFlares = []),
      (this.positionScreen = new c()),
      (this.customUpdateCallback = void 0),
      void 0 !== t && this.add(t, e, i, n, r);
  }
  function Kt(t) {
    X.call(this),
      (this.type = 'SpriteMaterial'),
      (this.color = new V(16777215)),
      (this.map = null),
      (this.rotation = 0),
      (this.lights = this.fog = !1),
      this.setValues(t);
  }
  function $t(t) {
    rt.call(this),
      (this.type = 'Sprite'),
      (this.material = void 0 !== t ? t : new Kt());
  }
  function te() {
    rt.call(this),
      (this.type = 'LOD'),
      Object.defineProperties(this, { levels: { enumerable: !0, value: [] } });
  }
  function ee(t, e, i, r, a, o, s, c, h, l, u, d) {
    n.call(this, null, o, s, c, h, l, r, a, u, d),
      (this.image = { data: t, width: e, height: i }),
      (this.magFilter = void 0 !== h ? h : 1003),
      (this.minFilter = void 0 !== l ? l : 1003),
      (this.generateMipmaps = this.flipY = !1);
  }
  function ie(e, i, n) {
    if (
      ((this.useVertexTexture = void 0 === n || n),
      (this.identityMatrix = new h()),
      (e = e || []),
      (this.bones = e.slice(0)),
      this.useVertexTexture
        ? ((e = Math.sqrt(4 * this.bones.length)),
          (e = t.Math.nextPowerOfTwo(Math.ceil(e))),
          (this.boneTextureHeight = this.boneTextureWidth = e = Math.max(e, 4)),
          (this.boneMatrices = new Float32Array(
            this.boneTextureWidth * this.boneTextureHeight * 4
          )),
          (this.boneTexture = new ee(
            this.boneMatrices,
            this.boneTextureWidth,
            this.boneTextureHeight,
            1023,
            1015
          )))
        : (this.boneMatrices = new Float32Array(16 * this.bones.length)),
      void 0 === i)
    )
      this.calculateInverses();
    else if (this.bones.length === i.length) this.boneInverses = i.slice(0);
    else
      for (
        console.warn('THREE.Skeleton bonInverses is the wrong length.'),
          this.boneInverses = [],
          i = 0,
          e = this.bones.length;
        i < e;
        i++
      )
        this.boneInverses.push(new h());
  }
  function ne(t) {
    rt.call(this), (this.type = 'Bone'), (this.skin = t);
  }
  function re(t, e, i) {
    if (
      (gt.call(this, t, e),
      (this.type = 'SkinnedMesh'),
      (this.bindMode = 'attached'),
      (this.bindMatrix = new h()),
      (this.bindMatrixInverse = new h()),
      (t = []),
      this.geometry && void 0 !== this.geometry.bones)
    ) {
      for (var n, r = 0, a = this.geometry.bones.length; r < a; ++r)
        (n = this.geometry.bones[r]),
          (e = new ne(this)),
          t.push(e),
          (e.name = n.name),
          e.position.fromArray(n.pos),
          e.quaternion.fromArray(n.rotq),
          void 0 !== n.scl && e.scale.fromArray(n.scl);
      for (r = 0, a = this.geometry.bones.length; r < a; ++r)
        (n = this.geometry.bones[r]),
          -1 !== n.parent && null !== n.parent && void 0 !== t[n.parent]
            ? t[n.parent].add(t[r])
            : this.add(t[r]);
    }
    this.normalizeSkinWeights(),
      this.updateMatrixWorld(!0),
      this.bind(new ie(t, void 0, i), this.matrixWorld);
  }
  function ae(t) {
    X.call(this),
      (this.type = 'LineBasicMaterial'),
      (this.color = new V(16777215)),
      (this.linewidth = 1),
      (this.linejoin = this.linecap = 'round'),
      (this.lights = !1),
      this.setValues(t);
  }
  function oe(t, e, i) {
    if (1 === i)
      return (
        console.warn(
          'THREE.Line: parameter THREE.LinePieces no longer supported. Created THREE.LineSegments instead.'
        ),
        new se(t, e)
      );
    rt.call(this),
      (this.type = 'Line'),
      (this.geometry = void 0 !== t ? t : new mt()),
      (this.material =
        void 0 !== e ? e : new ae({ color: 16777215 * Math.random() }));
  }
  function se(t, e) {
    oe.call(this, t, e), (this.type = 'LineSegments');
  }
  function ce(t) {
    X.call(this),
      (this.type = 'PointsMaterial'),
      (this.color = new V(16777215)),
      (this.map = null),
      (this.size = 1),
      (this.sizeAttenuation = !0),
      (this.lights = !1),
      this.setValues(t);
  }
  function he(t, e) {
    rt.call(this),
      (this.type = 'Points'),
      (this.geometry = void 0 !== t ? t : new mt()),
      (this.material =
        void 0 !== e ? e : new ce({ color: 16777215 * Math.random() }));
  }
  function le() {
    rt.call(this), (this.type = 'Group');
  }
  function ue(t, e, i, r, a, o, s, c, h) {
    function l() {
      requestAnimationFrame(l),
        t.readyState >= t.HAVE_CURRENT_DATA && (u.needsUpdate = !0);
    }
    n.call(this, t, e, i, r, a, o, s, c, h), (this.generateMipmaps = !1);
    var u = this;
    l();
  }
  function de(t, e, i, r, a, o, s, c, h, l, u, d) {
    n.call(this, null, o, s, c, h, l, r, a, u, d),
      (this.image = { width: e, height: i }),
      (this.mipmaps = t),
      (this.generateMipmaps = this.flipY = !1);
  }
  function pe(t, e, i, r, a, o, s, c, h) {
    n.call(this, t, e, i, r, a, o, s, c, h), (this.needsUpdate = !0);
  }
  function fe(t, e, i, r, a, o, s, c, h, l) {
    if (1026 !== (l = void 0 !== l ? l : 1026) && 1027 !== l)
      throw Error(
        'DepthTexture format must be either THREE.DepthFormat or THREE.DepthStencilFormat'
      );
    n.call(this, null, r, a, o, s, c, l, i, h),
      (this.image = { width: t, height: e }),
      (this.type = void 0 !== i ? i : 1012),
      (this.magFilter = void 0 !== s ? s : 1003),
      (this.minFilter = void 0 !== c ? c : 1003),
      (this.generateMipmaps = this.flipY = !1);
  }
  function me(t) {
    function e(t, e) {
      return t - e;
    }
    mt.call(this);
    var i = [0, 0],
      n = {},
      r = ['a', 'b', 'c'];
    if (t && t.isGeometry) {
      var a = t.vertices,
        o = t.faces,
        s = 0,
        c = new Uint32Array(6 * o.length);
      t = 0;
      for (var h = o.length; t < h; t++)
        for (var l = o[t], u = 0; 3 > u; u++) {
          (i[0] = l[r[u]]), (i[1] = l[r[(u + 1) % 3]]), i.sort(e);
          var d = i.toString();
          void 0 === n[d] &&
            ((c[2 * s] = i[0]), (c[2 * s + 1] = i[1]), (n[d] = !0), s++);
        }
      for (i = new Float32Array(6 * s), t = 0, h = s; t < h; t++)
        for (u = 0; 2 > u; u++)
          (n = a[c[2 * t + u]]),
            (s = 6 * t + 3 * u),
            (i[s + 0] = n.x),
            (i[s + 1] = n.y),
            (i[s + 2] = n.z);
      this.addAttribute('position', new ht(i, 3));
    } else if (t && t.isBufferGeometry) {
      if (null !== t.index) {
        for (
          h = t.index.array,
            a = t.attributes.position,
            r = t.groups,
            s = 0,
            0 === r.length && t.addGroup(0, h.length),
            c = new Uint32Array(2 * h.length),
            o = 0,
            l = r.length;
          o < l;
          ++o
        ) {
          (t = r[o]), (u = t.start), (d = t.count), (t = u);
          for (var p = u + d; t < p; t += 3)
            for (u = 0; 3 > u; u++)
              (i[0] = h[t + u]),
                (i[1] = h[t + (u + 1) % 3]),
                i.sort(e),
                (d = i.toString()),
                void 0 === n[d] &&
                  ((c[2 * s] = i[0]), (c[2 * s + 1] = i[1]), (n[d] = !0), s++);
        }
        for (i = new Float32Array(6 * s), t = 0, h = s; t < h; t++)
          for (u = 0; 2 > u; u++)
            (s = 6 * t + 3 * u),
              (n = c[2 * t + u]),
              (i[s + 0] = a.getX(n)),
              (i[s + 1] = a.getY(n)),
              (i[s + 2] = a.getZ(n));
      } else
        for (
          a = t.attributes.position.array,
            s = a.length / 3,
            c = s / 3,
            i = new Float32Array(6 * s),
            t = 0,
            h = c;
          t < h;
          t++
        )
          for (u = 0; 3 > u; u++)
            (s = 18 * t + 6 * u),
              (c = 9 * t + 3 * u),
              (i[s + 0] = a[c]),
              (i[s + 1] = a[c + 1]),
              (i[s + 2] = a[c + 2]),
              (n = 9 * t + ((u + 1) % 3) * 3),
              (i[s + 3] = a[n]),
              (i[s + 4] = a[n + 1]),
              (i[s + 5] = a[n + 2]);
      this.addAttribute('position', new ht(i, 3));
    }
  }
  function ge(t, e, n) {
    pt.call(this),
      (this.type = 'ParametricGeometry'),
      (this.parameters = { func: t, slices: e, stacks: n });
    var r,
      a,
      o,
      s,
      c = this.vertices,
      h = this.faces,
      l = this.faceVertexUvs[0],
      u = e + 1;
    for (r = 0; r <= n; r++)
      for (s = r / n, a = 0; a <= e; a++) (o = a / e), (o = t(o, s)), c.push(o);
    var d, p, f, m;
    for (r = 0; r < n; r++)
      for (a = 0; a < e; a++)
        (t = r * u + a),
          (c = r * u + a + 1),
          (s = (r + 1) * u + a + 1),
          (o = (r + 1) * u + a),
          (d = new i(a / e, r / n)),
          (p = new i((a + 1) / e, r / n)),
          (f = new i((a + 1) / e, (r + 1) / n)),
          (m = new i(a / e, (r + 1) / n)),
          h.push(new st(t, c, o)),
          l.push([d, p, m]),
          h.push(new st(c, s, o)),
          l.push([p.clone(), f, m.clone()]);
    this.computeFaceNormals(), this.computeVertexNormals();
  }
  function ve(t, e, n, r) {
    function a(t) {
      var e = t.normalize().clone();
      return (
        (e.index = l.vertices.push(e) - 1),
        (e.uv = new i(
          Math.atan2(t.z, -t.x) / 2 / Math.PI + 0.5,
          1 -
            (Math.atan2(-t.y, Math.sqrt(t.x * t.x + t.z * t.z)) / Math.PI + 0.5)
        )),
        e
      );
    }
    function o(t, e, i) {
      var n = new st(t.index, e.index, i.index, [
        t.clone(),
        e.clone(),
        i.clone()
      ]);
      l.faces.push(n),
        y
          .copy(t)
          .add(e)
          .add(i)
          .divideScalar(3),
        (n = Math.atan2(y.z, -y.x)),
        l.faceVertexUvs[0].push([h(t.uv, t, n), h(e.uv, e, n), h(i.uv, i, n)]);
    }
    function s(t, e) {
      for (
        var i = Math.pow(2, e),
          n = a(l.vertices[t.a]),
          r = a(l.vertices[t.b]),
          s = a(l.vertices[t.c]),
          c = [],
          h = 0;
        h <= i;
        h++
      ) {
        c[h] = [];
        for (
          var u = a(n.clone().lerp(s, h / i)),
            d = a(r.clone().lerp(s, h / i)),
            p = i - h,
            f = 0;
          f <= p;
          f++
        )
          c[h][f] = 0 === f && h === i ? u : a(u.clone().lerp(d, f / p));
      }
      for (h = 0; h < i; h++)
        for (f = 0; f < 2 * (i - h) - 1; f++)
          (n = Math.floor(f / 2)),
            0 == f % 2
              ? o(c[h][n + 1], c[h + 1][n], c[h][n])
              : o(c[h][n + 1], c[h + 1][n + 1], c[h + 1][n]);
    }
    function h(t, e, n) {
      return (
        0 > n && 1 === t.x && (t = new i(t.x - 1, t.y)),
        0 === e.x && 0 === e.z && (t = new i(n / 2 / Math.PI + 0.5, t.y)),
        t.clone()
      );
    }
    pt.call(this),
      (this.type = 'PolyhedronGeometry'),
      (this.parameters = { vertices: t, indices: e, radius: n, detail: r }),
      (n = n || 1),
      (r = r || 0);
    for (var l = this, u = 0, d = t.length; u < d; u += 3)
      a(new c(t[u], t[u + 1], t[u + 2]));
    t = this.vertices;
    for (var p = [], f = (u = 0), d = e.length; u < d; u += 3, f++) {
      var m = t[e[u]],
        g = t[e[u + 1]],
        v = t[e[u + 2]];
      p[f] = new st(m.index, g.index, v.index, [
        m.clone(),
        g.clone(),
        v.clone()
      ]);
    }
    for (var y = new c(), u = 0, d = p.length; u < d; u++) s(p[u], r);
    for (u = 0, d = this.faceVertexUvs[0].length; u < d; u++)
      (e = this.faceVertexUvs[0][u]),
        (r = e[0].x),
        (t = e[1].x),
        (p = e[2].x),
        (f = Math.min(r, t, p)),
        0.9 < Math.max(r, t, p) &&
          0.1 > f &&
          (0.2 > r && (e[0].x += 1),
          0.2 > t && (e[1].x += 1),
          0.2 > p && (e[2].x += 1));
    for (u = 0, d = this.vertices.length; u < d; u++)
      this.vertices[u].multiplyScalar(n);
    this.mergeVertices(),
      this.computeFaceNormals(),
      (this.boundingSphere = new J(new c(), n));
  }
  function ye(t, e) {
    ve.call(
      this,
      [1, 1, 1, -1, -1, 1, -1, 1, -1, 1, -1, -1],
      [2, 1, 0, 0, 3, 2, 1, 3, 0, 2, 3, 1],
      t,
      e
    ),
      (this.type = 'TetrahedronGeometry'),
      (this.parameters = { radius: t, detail: e });
  }
  function xe(t, e) {
    ve.call(
      this,
      [1, 0, 0, -1, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 1, 0, 0, -1],
      [0, 2, 4, 0, 4, 3, 0, 3, 5, 0, 5, 2, 1, 2, 5, 1, 5, 3, 1, 3, 4, 1, 4, 2],
      t,
      e
    ),
      (this.type = 'OctahedronGeometry'),
      (this.parameters = { radius: t, detail: e });
  }
  function _e(t, e) {
    var i = (1 + Math.sqrt(5)) / 2;
    ve.call(
      this,
      [
        -1,
        i,
        0,
        1,
        i,
        0,
        -1,
        -i,
        0,
        1,
        -i,
        0,
        0,
        -1,
        i,
        0,
        1,
        i,
        0,
        -1,
        -i,
        0,
        1,
        -i,
        i,
        0,
        -1,
        i,
        0,
        1,
        -i,
        0,
        -1,
        -i,
        0,
        1
      ],
      [
        0,
        11,
        5,
        0,
        5,
        1,
        0,
        1,
        7,
        0,
        7,
        10,
        0,
        10,
        11,
        1,
        5,
        9,
        5,
        11,
        4,
        11,
        10,
        2,
        10,
        7,
        6,
        7,
        1,
        8,
        3,
        9,
        4,
        3,
        4,
        2,
        3,
        2,
        6,
        3,
        6,
        8,
        3,
        8,
        9,
        4,
        9,
        5,
        2,
        4,
        11,
        6,
        2,
        10,
        8,
        6,
        7,
        9,
        8,
        1
      ],
      t,
      e
    ),
      (this.type = 'IcosahedronGeometry'),
      (this.parameters = { radius: t, detail: e });
  }
  function be(t, e) {
    var i = (1 + Math.sqrt(5)) / 2,
      n = 1 / i;
    ve.call(
      this,
      [
        -1,
        -1,
        -1,
        -1,
        -1,
        1,
        -1,
        1,
        -1,
        -1,
        1,
        1,
        1,
        -1,
        -1,
        1,
        -1,
        1,
        1,
        1,
        -1,
        1,
        1,
        1,
        0,
        -n,
        -i,
        0,
        -n,
        i,
        0,
        n,
        -i,
        0,
        n,
        i,
        -n,
        -i,
        0,
        -n,
        i,
        0,
        n,
        -i,
        0,
        n,
        i,
        0,
        -i,
        0,
        -n,
        i,
        0,
        -n,
        -i,
        0,
        n,
        i,
        0,
        n
      ],
      [
        3,
        11,
        7,
        3,
        7,
        15,
        3,
        15,
        13,
        7,
        19,
        17,
        7,
        17,
        6,
        7,
        6,
        15,
        17,
        4,
        8,
        17,
        8,
        10,
        17,
        10,
        6,
        8,
        0,
        16,
        8,
        16,
        2,
        8,
        2,
        10,
        0,
        12,
        1,
        0,
        1,
        18,
        0,
        18,
        16,
        6,
        10,
        2,
        6,
        2,
        13,
        6,
        13,
        15,
        2,
        16,
        18,
        2,
        18,
        3,
        2,
        3,
        13,
        18,
        1,
        9,
        18,
        9,
        11,
        18,
        11,
        3,
        4,
        14,
        12,
        4,
        12,
        0,
        4,
        0,
        8,
        11,
        9,
        5,
        11,
        5,
        19,
        11,
        19,
        7,
        19,
        5,
        14,
        19,
        14,
        4,
        19,
        4,
        17,
        1,
        12,
        14,
        1,
        14,
        5,
        1,
        5,
        9
      ],
      t,
      e
    ),
      (this.type = 'DodecahedronGeometry'),
      (this.parameters = { radius: t, detail: e });
  }
  function we(t, e, n, r, a, o) {
    pt.call(this),
      (this.type = 'TubeGeometry'),
      (this.parameters = {
        path: t,
        segments: e,
        radius: n,
        radialSegments: r,
        closed: a,
        taper: o
      }),
      (e = e || 64),
      (n = n || 1),
      (r = r || 8),
      (a = a || !1),
      (o = o || we.NoTaper);
    var s,
      h,
      l,
      u,
      d,
      p,
      f,
      m,
      g,
      v,
      y = [],
      x = e + 1,
      _ = new c();
    for (
      m = new we.FrenetFrames(t, e, a),
        g = m.normals,
        v = m.binormals,
        this.tangents = m.tangents,
        this.normals = g,
        this.binormals = v,
        m = 0;
      m < x;
      m++
    )
      for (
        y[m] = [],
          l = m / (x - 1),
          f = t.getPointAt(l),
          s = g[m],
          h = v[m],
          d = n * o(l),
          l = 0;
        l < r;
        l++
      )
        (u = l / r * 2 * Math.PI),
          (p = -d * Math.cos(u)),
          (u = d * Math.sin(u)),
          _.copy(f),
          (_.x += p * s.x + u * h.x),
          (_.y += p * s.y + u * h.y),
          (_.z += p * s.z + u * h.z),
          (y[m][l] = this.vertices.push(new c(_.x, _.y, _.z)) - 1);
    for (m = 0; m < e; m++)
      for (l = 0; l < r; l++)
        (o = a ? (m + 1) % e : m + 1),
          (x = (l + 1) % r),
          (t = y[m][l]),
          (n = y[o][l]),
          (o = y[o][x]),
          (x = y[m][x]),
          (_ = new i(m / e, l / r)),
          (g = new i((m + 1) / e, l / r)),
          (v = new i((m + 1) / e, (l + 1) / r)),
          (s = new i(m / e, (l + 1) / r)),
          this.faces.push(new st(t, n, x)),
          this.faceVertexUvs[0].push([_, g, s]),
          this.faces.push(new st(n, o, x)),
          this.faceVertexUvs[0].push([g.clone(), v, s.clone()]);
    this.computeFaceNormals(), this.computeVertexNormals();
  }
  function Me(t, e, n, r, a, o) {
    function s(t, e, i, n, r) {
      var a = Math.sin(t);
      (e = i / e * t),
        (i = Math.cos(e)),
        (r.x = n * (2 + i) * 0.5 * Math.cos(t)),
        (r.y = n * (2 + i) * a * 0.5),
        (r.z = n * Math.sin(e) * 0.5);
    }
    mt.call(this),
      (this.type = 'TorusKnotBufferGeometry'),
      (this.parameters = {
        radius: t,
        tube: e,
        tubularSegments: n,
        radialSegments: r,
        p: a,
        q: o
      }),
      (t = t || 100),
      (e = e || 40),
      (n = Math.floor(n) || 64),
      (r = Math.floor(r) || 8),
      (a = a || 2),
      (o = o || 3);
    var h,
      l,
      u = (r + 1) * (n + 1),
      d = r * n * 6,
      d = new ht(new (65535 < d ? Uint32Array : Uint16Array)(d), 1),
      p = new ht(new Float32Array(3 * u), 3),
      f = new ht(new Float32Array(3 * u), 3),
      u = new ht(new Float32Array(2 * u), 2),
      m = 0,
      g = 0,
      v = new c(),
      y = new c(),
      x = new i(),
      _ = new c(),
      b = new c(),
      w = new c(),
      M = new c(),
      E = new c();
    for (h = 0; h <= n; ++h)
      for (
        l = h / n * a * Math.PI * 2,
          s(l, a, o, t, _),
          s(l + 0.01, a, o, t, b),
          M.subVectors(b, _),
          E.addVectors(b, _),
          w.crossVectors(M, E),
          E.crossVectors(w, M),
          w.normalize(),
          E.normalize(),
          l = 0;
        l <= r;
        ++l
      ) {
        var S = l / r * Math.PI * 2,
          T = -e * Math.cos(S),
          S = e * Math.sin(S);
        (v.x = _.x + (T * E.x + S * w.x)),
          (v.y = _.y + (T * E.y + S * w.y)),
          (v.z = _.z + (T * E.z + S * w.z)),
          p.setXYZ(m, v.x, v.y, v.z),
          y.subVectors(v, _).normalize(),
          f.setXYZ(m, y.x, y.y, y.z),
          (x.x = h / n),
          (x.y = l / r),
          u.setXY(m, x.x, x.y),
          m++;
      }
    for (l = 1; l <= n; l++)
      for (h = 1; h <= r; h++)
        (t = (r + 1) * l + (h - 1)),
          (e = (r + 1) * l + h),
          (a = (r + 1) * (l - 1) + h),
          d.setX(g, (r + 1) * (l - 1) + (h - 1)),
          g++,
          d.setX(g, t),
          g++,
          d.setX(g, a),
          g++,
          d.setX(g, t),
          g++,
          d.setX(g, e),
          g++,
          d.setX(g, a),
          g++;
    this.setIndex(d),
      this.addAttribute('position', p),
      this.addAttribute('normal', f),
      this.addAttribute('uv', u);
  }
  function Ee(t, e, i, n, r, a, o) {
    pt.call(this),
      (this.type = 'TorusKnotGeometry'),
      (this.parameters = {
        radius: t,
        tube: e,
        tubularSegments: i,
        radialSegments: n,
        p: r,
        q: a
      }),
      void 0 !== o &&
        console.warn(
          'THREE.TorusKnotGeometry: heightScale has been deprecated. Use .scale( x, y, z ) instead.'
        ),
      this.fromBufferGeometry(new Me(t, e, i, n, r, a)),
      this.mergeVertices();
  }
  function Se(t, e, i, n, r) {
    mt.call(this),
      (this.type = 'TorusBufferGeometry'),
      (this.parameters = {
        radius: t,
        tube: e,
        radialSegments: i,
        tubularSegments: n,
        arc: r
      }),
      (t = t || 100),
      (e = e || 40),
      (i = Math.floor(i) || 8),
      (n = Math.floor(n) || 6),
      (r = r || 2 * Math.PI);
    var a,
      o,
      s = (i + 1) * (n + 1),
      h = i * n * 6,
      h = new (65535 < h ? Uint32Array : Uint16Array)(h),
      l = new Float32Array(3 * s),
      u = new Float32Array(3 * s),
      s = new Float32Array(2 * s),
      d = 0,
      p = 0,
      f = 0,
      m = new c(),
      g = new c(),
      v = new c();
    for (a = 0; a <= i; a++)
      for (o = 0; o <= n; o++) {
        var y = o / n * r,
          x = a / i * Math.PI * 2;
        (g.x = (t + e * Math.cos(x)) * Math.cos(y)),
          (g.y = (t + e * Math.cos(x)) * Math.sin(y)),
          (g.z = e * Math.sin(x)),
          (l[d] = g.x),
          (l[d + 1] = g.y),
          (l[d + 2] = g.z),
          (m.x = t * Math.cos(y)),
          (m.y = t * Math.sin(y)),
          v.subVectors(g, m).normalize(),
          (u[d] = v.x),
          (u[d + 1] = v.y),
          (u[d + 2] = v.z),
          (s[p] = o / n),
          (s[p + 1] = a / i),
          (d += 3),
          (p += 2);
      }
    for (a = 1; a <= i; a++)
      for (o = 1; o <= n; o++)
        (t = (n + 1) * (a - 1) + o - 1),
          (e = (n + 1) * (a - 1) + o),
          (r = (n + 1) * a + o),
          (h[f] = (n + 1) * a + o - 1),
          (h[f + 1] = t),
          (h[f + 2] = r),
          (h[f + 3] = t),
          (h[f + 4] = e),
          (h[f + 5] = r),
          (f += 6);
    this.setIndex(new ht(h, 1)),
      this.addAttribute('position', new ht(l, 3)),
      this.addAttribute('normal', new ht(u, 3)),
      this.addAttribute('uv', new ht(s, 2));
  }
  function Te(t, e, i, n, r) {
    pt.call(this),
      (this.type = 'TorusGeometry'),
      (this.parameters = {
        radius: t,
        tube: e,
        radialSegments: i,
        tubularSegments: n,
        arc: r
      }),
      this.fromBufferGeometry(new Se(t, e, i, n, r));
  }
  function Ae(t, e) {
    void 0 !== t &&
      (pt.call(this),
      (this.type = 'ExtrudeGeometry'),
      (t = Array.isArray(t) ? t : [t]),
      this.addShapeList(t, e),
      this.computeFaceNormals());
  }
  function Le(t, e) {
    e = e || {};
    var i = e.font;
    if (!1 === (i && i.isFont))
      return (
        console.error(
          'THREE.TextGeometry: font parameter is not an instance of THREE.Font.'
        ),
        new pt()
      );
    (i = i.generateShapes(t, e.size, e.curveSegments)),
      (e.amount = void 0 !== e.height ? e.height : 50),
      void 0 === e.bevelThickness && (e.bevelThickness = 10),
      void 0 === e.bevelSize && (e.bevelSize = 8),
      void 0 === e.bevelEnabled && (e.bevelEnabled = !1),
      Ae.call(this, i, e),
      (this.type = 'TextGeometry');
  }
  function Re(t, e, i, n, r, a, o) {
    mt.call(this),
      (this.type = 'SphereBufferGeometry'),
      (this.parameters = {
        radius: t,
        widthSegments: e,
        heightSegments: i,
        phiStart: n,
        phiLength: r,
        thetaStart: a,
        thetaLength: o
      }),
      (t = t || 50),
      (e = Math.max(3, Math.floor(e) || 8)),
      (i = Math.max(2, Math.floor(i) || 6)),
      (n = void 0 !== n ? n : 0),
      (r = void 0 !== r ? r : 2 * Math.PI),
      (a = void 0 !== a ? a : 0),
      (o = void 0 !== o ? o : Math.PI);
    for (
      var s = a + o,
        h = (e + 1) * (i + 1),
        l = new ht(new Float32Array(3 * h), 3),
        u = new ht(new Float32Array(3 * h), 3),
        h = new ht(new Float32Array(2 * h), 2),
        d = 0,
        p = [],
        f = new c(),
        m = 0;
      m <= i;
      m++
    ) {
      for (var g = [], v = m / i, y = 0; y <= e; y++) {
        var x = y / e,
          _ = -t * Math.cos(n + x * r) * Math.sin(a + v * o),
          b = t * Math.cos(a + v * o),
          w = t * Math.sin(n + x * r) * Math.sin(a + v * o);
        f.set(_, b, w).normalize(),
          l.setXYZ(d, _, b, w),
          u.setXYZ(d, f.x, f.y, f.z),
          h.setXY(d, x, 1 - v),
          g.push(d),
          d++;
      }
      p.push(g);
    }
    for (n = [], m = 0; m < i; m++)
      for (y = 0; y < e; y++)
        (r = p[m][y + 1]),
          (o = p[m][y]),
          (d = p[m + 1][y]),
          (f = p[m + 1][y + 1]),
          (0 !== m || 0 < a) && n.push(r, o, f),
          (m !== i - 1 || s < Math.PI) && n.push(o, d, f);
    this.setIndex(new (65535 < l.count ? ut : lt)(n, 1)),
      this.addAttribute('position', l),
      this.addAttribute('normal', u),
      this.addAttribute('uv', h),
      (this.boundingSphere = new J(new c(), t));
  }
  function Pe(t, e, i, n, r, a, o) {
    pt.call(this),
      (this.type = 'SphereGeometry'),
      (this.parameters = {
        radius: t,
        widthSegments: e,
        heightSegments: i,
        phiStart: n,
        phiLength: r,
        thetaStart: a,
        thetaLength: o
      }),
      this.fromBufferGeometry(new Re(t, e, i, n, r, a, o));
  }
  function Ce(t, e, n, r, a, o) {
    mt.call(this),
      (this.type = 'RingBufferGeometry'),
      (this.parameters = {
        innerRadius: t,
        outerRadius: e,
        thetaSegments: n,
        phiSegments: r,
        thetaStart: a,
        thetaLength: o
      }),
      (t = t || 20),
      (e = e || 50),
      (a = void 0 !== a ? a : 0),
      (o = void 0 !== o ? o : 2 * Math.PI),
      (n = void 0 !== n ? Math.max(3, n) : 8),
      (r = void 0 !== r ? Math.max(1, r) : 1);
    var s,
      h,
      l = (n + 1) * (r + 1),
      u = n * r * 6,
      u = new ht(new (65535 < u ? Uint32Array : Uint16Array)(u), 1),
      d = new ht(new Float32Array(3 * l), 3),
      p = new ht(new Float32Array(3 * l), 3),
      l = new ht(new Float32Array(2 * l), 2),
      f = 0,
      m = 0,
      g = t,
      v = (e - t) / r,
      y = new c(),
      x = new i();
    for (t = 0; t <= r; t++) {
      for (h = 0; h <= n; h++)
        (s = a + h / n * o),
          (y.x = g * Math.cos(s)),
          (y.y = g * Math.sin(s)),
          d.setXYZ(f, y.x, y.y, y.z),
          p.setXYZ(f, 0, 0, 1),
          (x.x = (y.x / e + 1) / 2),
          (x.y = (y.y / e + 1) / 2),
          l.setXY(f, x.x, x.y),
          f++;
      g += v;
    }
    for (t = 0; t < r; t++)
      for (e = t * (n + 1), h = 0; h < n; h++)
        (a = s = h + e),
          (o = s + n + 1),
          (f = s + n + 2),
          (s += 1),
          u.setX(m, a),
          m++,
          u.setX(m, o),
          m++,
          u.setX(m, f),
          m++,
          u.setX(m, a),
          m++,
          u.setX(m, f),
          m++,
          u.setX(m, s),
          m++;
    this.setIndex(u),
      this.addAttribute('position', d),
      this.addAttribute('normal', p),
      this.addAttribute('uv', l);
  }
  function Ie(t, e, i, n, r, a) {
    pt.call(this),
      (this.type = 'RingGeometry'),
      (this.parameters = {
        innerRadius: t,
        outerRadius: e,
        thetaSegments: i,
        phiSegments: n,
        thetaStart: r,
        thetaLength: a
      }),
      this.fromBufferGeometry(new Ce(t, e, i, n, r, a));
  }
  function Ue(t, e, i, n) {
    pt.call(this),
      (this.type = 'PlaneGeometry'),
      (this.parameters = {
        width: t,
        height: e,
        widthSegments: i,
        heightSegments: n
      }),
      this.fromBufferGeometry(new yt(t, e, i, n));
  }
  function De(e, n, r, a) {
    mt.call(this),
      (this.type = 'LatheBufferGeometry'),
      (this.parameters = { points: e, segments: n, phiStart: r, phiLength: a }),
      (n = Math.floor(n) || 12),
      (r = r || 0),
      (a = a || 2 * Math.PI),
      (a = t.Math.clamp(a, 0, 2 * Math.PI));
    for (
      var o = (n + 1) * e.length,
        s = n * e.length * 6,
        h = new ht(new (65535 < s ? Uint32Array : Uint16Array)(s), 1),
        l = new ht(new Float32Array(3 * o), 3),
        u = new ht(new Float32Array(2 * o), 2),
        d = 0,
        p = 0,
        f = 1 / n,
        m = new c(),
        g = new i(),
        o = 0;
      o <= n;
      o++
    )
      for (
        var s = r + o * f * a, v = Math.sin(s), y = Math.cos(s), s = 0;
        s <= e.length - 1;
        s++
      )
        (m.x = e[s].x * v),
          (m.y = e[s].y),
          (m.z = e[s].x * y),
          l.setXYZ(d, m.x, m.y, m.z),
          (g.x = o / n),
          (g.y = s / (e.length - 1)),
          u.setXY(d, g.x, g.y),
          d++;
    for (o = 0; o < n; o++)
      for (s = 0; s < e.length - 1; s++)
        (r = s + o * e.length),
          (d = r + e.length),
          (f = r + e.length + 1),
          (m = r + 1),
          h.setX(p, r),
          p++,
          h.setX(p, d),
          p++,
          h.setX(p, m),
          p++,
          h.setX(p, d),
          p++,
          h.setX(p, f),
          p++,
          h.setX(p, m),
          p++;
    if (
      (this.setIndex(h),
      this.addAttribute('position', l),
      this.addAttribute('uv', u),
      this.computeVertexNormals(),
      a === 2 * Math.PI)
    )
      for (
        a = this.attributes.normal.array,
          h = new c(),
          l = new c(),
          u = new c(),
          r = n * e.length * 3,
          s = o = 0;
        o < e.length;
        o++, s += 3
      )
        (h.x = a[s + 0]),
          (h.y = a[s + 1]),
          (h.z = a[s + 2]),
          (l.x = a[r + s + 0]),
          (l.y = a[r + s + 1]),
          (l.z = a[r + s + 2]),
          u.addVectors(h, l).normalize(),
          (a[s + 0] = a[r + s + 0] = u.x),
          (a[s + 1] = a[r + s + 1] = u.y),
          (a[s + 2] = a[r + s + 2] = u.z);
  }
  function Ne(t, e, i, n) {
    pt.call(this),
      (this.type = 'LatheGeometry'),
      (this.parameters = { points: t, segments: e, phiStart: i, phiLength: n }),
      this.fromBufferGeometry(new De(t, e, i, n)),
      this.mergeVertices();
  }
  function Fe(t, e) {
    pt.call(this),
      (this.type = 'ShapeGeometry'),
      !1 === Array.isArray(t) && (t = [t]),
      this.addShapeList(t, e),
      this.computeFaceNormals();
  }
  function Oe(e, i) {
    function n(t, e) {
      return t - e;
    }
    mt.call(this);
    var r,
      a = Math.cos(t.Math.DEG2RAD * (void 0 !== i ? i : 1)),
      o = [0, 0],
      s = {},
      c = ['a', 'b', 'c'];
    e && e.isBufferGeometry
      ? ((r = new pt()), r.fromBufferGeometry(e))
      : (r = e.clone()),
      r.mergeVertices(),
      r.computeFaceNormals();
    var h = r.vertices;
    r = r.faces;
    for (var l = 0, u = r.length; l < u; l++)
      for (var d = r[l], p = 0; 3 > p; p++) {
        (o[0] = d[c[p]]), (o[1] = d[c[(p + 1) % 3]]), o.sort(n);
        var f = o.toString();
        void 0 === s[f]
          ? (s[f] = { vert1: o[0], vert2: o[1], face1: l, face2: void 0 })
          : (s[f].face2 = l);
      }
    o = [];
    for (f in s)
      (c = s[f]),
        (void 0 === c.face2 || r[c.face1].normal.dot(r[c.face2].normal) <= a) &&
          ((l = h[c.vert1]),
          o.push(l.x),
          o.push(l.y),
          o.push(l.z),
          (l = h[c.vert2]),
          o.push(l.x),
          o.push(l.y),
          o.push(l.z));
    this.addAttribute('position', new ht(new Float32Array(o), 3));
  }
  function ze(t, e, n, r, a, o, s, h) {
    function l(n) {
      var a,
        o,
        l,
        d = new i(),
        p = new c(),
        f = 0,
        b = !0 === n ? t : e,
        E = !0 === n ? 1 : -1;
      for (o = x, a = 1; a <= r; a++)
        g.setXYZ(x, 0, w * E, 0),
          v.setXYZ(x, 0, E, 0),
          (d.x = 0.5),
          (d.y = 0.5),
          y.setXY(x, d.x, d.y),
          x++;
      for (l = x, a = 0; a <= r; a++) {
        var S = a / r * h + s,
          T = Math.cos(S),
          S = Math.sin(S);
        (p.x = b * S),
          (p.y = w * E),
          (p.z = b * T),
          g.setXYZ(x, p.x, p.y, p.z),
          v.setXYZ(x, 0, E, 0),
          (d.x = 0.5 * T + 0.5),
          (d.y = 0.5 * S * E + 0.5),
          y.setXY(x, d.x, d.y),
          x++;
      }
      for (a = 0; a < r; a++)
        (d = o + a),
          (p = l + a),
          !0 === n
            ? (m.setX(_, p), _++, m.setX(_, p + 1))
            : (m.setX(_, p + 1), _++, m.setX(_, p)),
          _++,
          m.setX(_, d),
          _++,
          (f += 3);
      u.addGroup(M, f, !0 === n ? 1 : 2), (M += f);
    }
    mt.call(this),
      (this.type = 'CylinderBufferGeometry'),
      (this.parameters = {
        radiusTop: t,
        radiusBottom: e,
        height: n,
        radialSegments: r,
        heightSegments: a,
        openEnded: o,
        thetaStart: s,
        thetaLength: h
      });
    var u = this;
    (t = void 0 !== t ? t : 20),
      (e = void 0 !== e ? e : 20),
      (n = void 0 !== n ? n : 100),
      (r = Math.floor(r) || 8),
      (a = Math.floor(a) || 1),
      (o = void 0 !== o && o),
      (s = void 0 !== s ? s : 0),
      (h = void 0 !== h ? h : 2 * Math.PI);
    var d = 0;
    !1 === o && (0 < t && d++, 0 < e && d++);
    var p = (function() {
        var t = (r + 1) * (a + 1);
        return !1 === o && (t += (r + 1) * d + r * d), t;
      })(),
      f = (function() {
        var t = r * a * 6;
        return !1 === o && (t += r * d * 3), t;
      })(),
      m = new ht(new (65535 < f ? Uint32Array : Uint16Array)(f), 1),
      g = new ht(new Float32Array(3 * p), 3),
      v = new ht(new Float32Array(3 * p), 3),
      y = new ht(new Float32Array(2 * p), 2),
      x = 0,
      _ = 0,
      b = [],
      w = n / 2,
      M = 0;
    !(function() {
      var i,
        o,
        l = new c(),
        d = new c(),
        p = 0,
        f = (e - t) / n;
      for (o = 0; o <= a; o++) {
        var E = [],
          S = o / a,
          T = S * (e - t) + t;
        for (i = 0; i <= r; i++) {
          var A = i / r,
            L = A * h + s,
            R = Math.sin(L),
            L = Math.cos(L);
          (d.x = T * R),
            (d.y = -S * n + w),
            (d.z = T * L),
            g.setXYZ(x, d.x, d.y, d.z),
            l.set(R, f, L).normalize(),
            v.setXYZ(x, l.x, l.y, l.z),
            y.setXY(x, A, 1 - S),
            E.push(x),
            x++;
        }
        b.push(E);
      }
      for (i = 0; i < r; i++)
        for (o = 0; o < a; o++)
          (l = b[o + 1][i]),
            (d = b[o + 1][i + 1]),
            (f = b[o][i + 1]),
            m.setX(_, b[o][i]),
            _++,
            m.setX(_, l),
            _++,
            m.setX(_, f),
            _++,
            m.setX(_, l),
            _++,
            m.setX(_, d),
            _++,
            m.setX(_, f),
            _++,
            (p += 6);
      u.addGroup(M, p, 0), (M += p);
    })(),
      !1 === o && (0 < t && l(!0), 0 < e && l(!1)),
      this.setIndex(m),
      this.addAttribute('position', g),
      this.addAttribute('normal', v),
      this.addAttribute('uv', y);
  }
  function Be(t, e, i, n, r, a, o, s) {
    pt.call(this),
      (this.type = 'CylinderGeometry'),
      (this.parameters = {
        radiusTop: t,
        radiusBottom: e,
        height: i,
        radialSegments: n,
        heightSegments: r,
        openEnded: a,
        thetaStart: o,
        thetaLength: s
      }),
      this.fromBufferGeometry(new ze(t, e, i, n, r, a, o, s)),
      this.mergeVertices();
  }
  function Ge(t, e, i, n, r, a, o) {
    Be.call(this, 0, t, e, i, n, r, a, o),
      (this.type = 'ConeGeometry'),
      (this.parameters = {
        radius: t,
        height: e,
        radialSegments: i,
        heightSegments: n,
        openEnded: r,
        thetaStart: a,
        thetaLength: o
      });
  }
  function He(t, e, i, n, r, a, o) {
    ze.call(this, 0, t, e, i, n, r, a, o),
      (this.type = 'ConeBufferGeometry'),
      (this.parameters = {
        radius: t,
        height: e,
        radialSegments: i,
        heightSegments: n,
        thetaStart: a,
        thetaLength: o
      });
  }
  function Ve(t, e, i, n) {
    mt.call(this),
      (this.type = 'CircleBufferGeometry'),
      (this.parameters = {
        radius: t,
        segments: e,
        thetaStart: i,
        thetaLength: n
      }),
      (t = t || 50),
      (e = void 0 !== e ? Math.max(3, e) : 8),
      (i = void 0 !== i ? i : 0),
      (n = void 0 !== n ? n : 2 * Math.PI);
    var r = e + 2,
      a = new Float32Array(3 * r),
      o = new Float32Array(3 * r),
      r = new Float32Array(2 * r);
    (o[2] = 1), (r[0] = 0.5), (r[1] = 0.5);
    for (var s = 0, h = 3, l = 2; s <= e; s++, h += 3, l += 2) {
      var u = i + s / e * n;
      (a[h] = t * Math.cos(u)),
        (a[h + 1] = t * Math.sin(u)),
        (o[h + 2] = 1),
        (r[l] = (a[h] / t + 1) / 2),
        (r[l + 1] = (a[h + 1] / t + 1) / 2);
    }
    for (i = [], h = 1; h <= e; h++) i.push(h, h + 1, 0);
    this.setIndex(new ht(new Uint16Array(i), 1)),
      this.addAttribute('position', new ht(a, 3)),
      this.addAttribute('normal', new ht(o, 3)),
      this.addAttribute('uv', new ht(r, 2)),
      (this.boundingSphere = new J(new c(), t));
  }
  function ke(t, e, i, n) {
    pt.call(this),
      (this.type = 'CircleGeometry'),
      (this.parameters = {
        radius: t,
        segments: e,
        thetaStart: i,
        thetaLength: n
      }),
      this.fromBufferGeometry(new Ve(t, e, i, n));
  }
  function je(t, e, i, n, r, a) {
    pt.call(this),
      (this.type = 'BoxGeometry'),
      (this.parameters = {
        width: t,
        height: e,
        depth: i,
        widthSegments: n,
        heightSegments: r,
        depthSegments: a
      }),
      this.fromBufferGeometry(new vt(t, e, i, n, r, a)),
      this.mergeVertices();
  }
  function We() {
    q.call(this, {
      uniforms: t.UniformsUtils.merge([Xn.lights, { opacity: { value: 1 } }]),
      vertexShader: Wn.shadow_vert,
      fragmentShader: Wn.shadow_frag
    }),
      (this.transparent = this.lights = !0),
      Object.defineProperties(this, {
        opacity: {
          enumerable: !0,
          get: function() {
            return this.uniforms.opacity.value;
          },
          set: function(t) {
            this.uniforms.opacity.value = t;
          }
        }
      });
  }
  function Xe(t) {
    q.call(this, t), (this.type = 'RawShaderMaterial');
  }
  function qe(e) {
    (this.uuid = t.Math.generateUUID()),
      (this.type = 'MultiMaterial'),
      (this.materials = e instanceof Array ? e : []),
      (this.visible = !0);
  }
  function Ye(t) {
    X.call(this),
      (this.defines = { STANDARD: '' }),
      (this.type = 'MeshStandardMaterial'),
      (this.color = new V(16777215)),
      (this.metalness = this.roughness = 0.5),
      (this.lightMap = this.map = null),
      (this.lightMapIntensity = 1),
      (this.aoMap = null),
      (this.aoMapIntensity = 1),
      (this.emissive = new V(0)),
      (this.emissiveIntensity = 1),
      (this.bumpMap = this.emissiveMap = null),
      (this.bumpScale = 1),
      (this.normalMap = null),
      (this.normalScale = new i(1, 1)),
      (this.displacementMap = null),
      (this.displacementScale = 1),
      (this.displacementBias = 0),
      (this.envMap = this.alphaMap = this.metalnessMap = this.roughnessMap = null),
      (this.envMapIntensity = 1),
      (this.refractionRatio = 0.98),
      (this.wireframe = !1),
      (this.wireframeLinewidth = 1),
      (this.wireframeLinejoin = this.wireframeLinecap = 'round'),
      (this.morphNormals = this.morphTargets = this.skinning = !1),
      this.setValues(t);
  }
  function Ze(t) {
    Ye.call(this),
      (this.defines = { PHYSICAL: '' }),
      (this.type = 'MeshPhysicalMaterial'),
      (this.reflectivity = 0.5),
      (this.clearCoatRoughness = this.clearCoat = 0),
      this.setValues(t);
  }
  function Je(t) {
    X.call(this),
      (this.type = 'MeshPhongMaterial'),
      (this.color = new V(16777215)),
      (this.specular = new V(1118481)),
      (this.shininess = 30),
      (this.lightMap = this.map = null),
      (this.lightMapIntensity = 1),
      (this.aoMap = null),
      (this.aoMapIntensity = 1),
      (this.emissive = new V(0)),
      (this.emissiveIntensity = 1),
      (this.bumpMap = this.emissiveMap = null),
      (this.bumpScale = 1),
      (this.normalMap = null),
      (this.normalScale = new i(1, 1)),
      (this.displacementMap = null),
      (this.displacementScale = 1),
      (this.displacementBias = 0),
      (this.envMap = this.alphaMap = this.specularMap = null),
      (this.combine = 0),
      (this.reflectivity = 1),
      (this.refractionRatio = 0.98),
      (this.wireframe = !1),
      (this.wireframeLinewidth = 1),
      (this.wireframeLinejoin = this.wireframeLinecap = 'round'),
      (this.morphNormals = this.morphTargets = this.skinning = !1),
      this.setValues(t);
  }
  function Qe(t) {
    X.call(this, t),
      (this.type = 'MeshNormalMaterial'),
      (this.wireframe = !1),
      (this.wireframeLinewidth = 1),
      (this.morphTargets = this.lights = this.fog = !1),
      this.setValues(t);
  }
  function Ke(t) {
    X.call(this),
      (this.type = 'MeshLambertMaterial'),
      (this.color = new V(16777215)),
      (this.lightMap = this.map = null),
      (this.lightMapIntensity = 1),
      (this.aoMap = null),
      (this.aoMapIntensity = 1),
      (this.emissive = new V(0)),
      (this.emissiveIntensity = 1),
      (this.envMap = this.alphaMap = this.specularMap = this.emissiveMap = null),
      (this.combine = 0),
      (this.reflectivity = 1),
      (this.refractionRatio = 0.98),
      (this.wireframe = !1),
      (this.wireframeLinewidth = 1),
      (this.wireframeLinejoin = this.wireframeLinecap = 'round'),
      (this.morphNormals = this.morphTargets = this.skinning = !1),
      this.setValues(t);
  }
  function $e(t) {
    X.call(this),
      (this.type = 'LineDashedMaterial'),
      (this.color = new V(16777215)),
      (this.scale = this.linewidth = 1),
      (this.dashSize = 3),
      (this.gapSize = 1),
      (this.lights = !1),
      this.setValues(t);
  }
  function ti(t, e, i) {
    var n = this,
      r = !1,
      a = 0,
      o = 0;
    (this.onStart = void 0),
      (this.onLoad = t),
      (this.onProgress = e),
      (this.onError = i),
      (this.itemStart = function(t) {
        o++, !1 === r && void 0 !== n.onStart && n.onStart(t, a, o), (r = !0);
      }),
      (this.itemEnd = function(t) {
        a++,
          void 0 !== n.onProgress && n.onProgress(t, a, o),
          a === o && ((r = !1), void 0 !== n.onLoad) && n.onLoad();
      }),
      (this.itemError = function(t) {
        void 0 !== n.onError && n.onError(t);
      });
  }
  function ei(e) {
    this.manager = void 0 !== e ? e : t.DefaultLoadingManager;
  }
  function ii(e) {
    (this.manager = void 0 !== e ? e : t.DefaultLoadingManager),
      (this._parser = null);
  }
  function ni(e) {
    (this.manager = void 0 !== e ? e : t.DefaultLoadingManager),
      (this._parser = null);
  }
  function ri(e) {
    this.manager = void 0 !== e ? e : t.DefaultLoadingManager;
  }
  function ai(e) {
    this.manager = void 0 !== e ? e : t.DefaultLoadingManager;
  }
  function oi(e) {
    this.manager = void 0 !== e ? e : t.DefaultLoadingManager;
  }
  function si(t, e) {
    rt.call(this),
      (this.type = 'Light'),
      (this.color = new V(t)),
      (this.intensity = void 0 !== e ? e : 1),
      (this.receiveShadow = void 0);
  }
  function ci(t, e, i) {
    si.call(this, t, i),
      (this.type = 'HemisphereLight'),
      (this.castShadow = void 0),
      this.position.copy(rt.DefaultUp),
      this.updateMatrix(),
      (this.groundColor = new V(e));
  }
  function hi(t) {
    (this.camera = t),
      (this.bias = 0),
      (this.radius = 1),
      (this.mapSize = new i(512, 512)),
      (this.map = null),
      (this.matrix = new h());
  }
  function li() {
    hi.call(this, new _t(50, 1, 0.5, 500));
  }
  function ui(t, e, i, n, r, a) {
    si.call(this, t, e),
      (this.type = 'SpotLight'),
      this.position.copy(rt.DefaultUp),
      this.updateMatrix(),
      (this.target = new rt()),
      Object.defineProperty(this, 'power', {
        get: function() {
          return this.intensity * Math.PI;
        },
        set: function(t) {
          this.intensity = t / Math.PI;
        }
      }),
      (this.distance = void 0 !== i ? i : 0),
      (this.angle = void 0 !== n ? n : Math.PI / 3),
      (this.penumbra = void 0 !== r ? r : 0),
      (this.decay = void 0 !== a ? a : 1),
      (this.shadow = new li());
  }
  function di(t, e, i, n) {
    si.call(this, t, e),
      (this.type = 'PointLight'),
      Object.defineProperty(this, 'power', {
        get: function() {
          return 4 * this.intensity * Math.PI;
        },
        set: function(t) {
          this.intensity = t / (4 * Math.PI);
        }
      }),
      (this.distance = void 0 !== i ? i : 0),
      (this.decay = void 0 !== n ? n : 1),
      (this.shadow = new hi(new _t(90, 1, 0.5, 500)));
  }
  function pi() {
    hi.call(this, new bt(-5, 5, 5, -5, 0.5, 500));
  }
  function fi(t, e) {
    si.call(this, t, e),
      (this.type = 'DirectionalLight'),
      this.position.copy(rt.DefaultUp),
      this.updateMatrix(),
      (this.target = new rt()),
      (this.shadow = new pi());
  }
  function mi(t, e) {
    si.call(this, t, e),
      (this.type = 'AmbientLight'),
      (this.castShadow = void 0);
  }
  function gi(t, e, i, n) {
    (this.parameterPositions = t),
      (this._cachedIndex = 0),
      (this.resultBuffer = void 0 !== n ? n : new e.constructor(i)),
      (this.sampleValues = e),
      (this.valueSize = i);
  }
  function vi(t, e, i, n) {
    gi.call(this, t, e, i, n),
      (this._offsetNext = this._weightNext = this._offsetPrev = this._weightPrev = -0);
  }
  function yi(t, e, i, n) {
    gi.call(this, t, e, i, n);
  }
  function xi(t, e, i, n) {
    gi.call(this, t, e, i, n);
  }
  function _i(e, i, n, r) {
    if (void 0 === e) throw Error('track name is undefined');
    if (void 0 === i || 0 === i.length)
      throw Error('no keyframes in track named ' + e);
    (this.name = e),
      (this.times = t.AnimationUtils.convertArray(i, this.TimeBufferType)),
      (this.values = t.AnimationUtils.convertArray(n, this.ValueBufferType)),
      this.setInterpolation(r || this.DefaultInterpolation),
      this.validate(),
      this.optimize();
  }
  function bi(t, e, i, n) {
    _i.call(this, t, e, i, n);
  }
  function wi(t, e, i, n) {
    gi.call(this, t, e, i, n);
  }
  function Mi(t, e, i, n) {
    _i.call(this, t, e, i, n);
  }
  function Ei(t, e, i, n) {
    _i.call(this, t, e, i, n);
  }
  function Si(t, e, i, n) {
    _i.call(this, t, e, i, n);
  }
  function Ti(t, e, i) {
    _i.call(this, t, e, i);
  }
  function Ai(t, e, i, n) {
    _i.call(this, t, e, i, n);
  }
  function Li() {
    _i.apply(this, arguments);
  }
  function Ri(e, i, n) {
    (this.name = e),
      (this.tracks = n),
      (this.duration = void 0 !== i ? i : -1),
      (this.uuid = t.Math.generateUUID()),
      0 > this.duration && this.resetDuration(),
      this.optimize();
  }
  function Pi(e) {
    (this.manager = void 0 !== e ? e : t.DefaultLoadingManager),
      (this.textures = {});
  }
  function Ci(e) {
    this.manager = void 0 !== e ? e : t.DefaultLoadingManager;
  }
  function Ii() {
    (this.onLoadStart = function() {}),
      (this.onLoadProgress = function() {}),
      (this.onLoadComplete = function() {});
  }
  function Ui(e) {
    'boolean' == typeof e &&
      (console.warn(
        'THREE.JSONLoader: showStatus parameter has been removed from constructor.'
      ),
      (e = void 0)),
      (this.manager = void 0 !== e ? e : t.DefaultLoadingManager),
      (this.withCredentials = !1);
  }
  function Di(e) {
    (this.manager = void 0 !== e ? e : t.DefaultLoadingManager),
      (this.texturePath = '');
  }
  function Ni() {}
  function Fi(t, e) {
    (this.v1 = t), (this.v2 = e);
  }
  function Oi() {
    (this.curves = []), (this.autoClose = !1);
  }
  function zi(t, e, i, n, r, a, o, s) {
    (this.aX = t),
      (this.aY = e),
      (this.xRadius = i),
      (this.yRadius = n),
      (this.aStartAngle = r),
      (this.aEndAngle = a),
      (this.aClockwise = o),
      (this.aRotation = s || 0);
  }
  function Bi(t) {
    this.points = void 0 === t ? [] : t;
  }
  function Gi(t, e, i, n) {
    (this.v0 = t), (this.v1 = e), (this.v2 = i), (this.v3 = n);
  }
  function Hi(t, e, i) {
    (this.v0 = t), (this.v1 = e), (this.v2 = i);
  }
  function Vi() {
    ki.apply(this, arguments), (this.holes = []);
  }
  function ki(t) {
    Oi.call(this), (this.currentPoint = new i()), t && this.fromPoints(t);
  }
  function ji() {
    (this.subPaths = []), (this.currentPath = null);
  }
  function Wi(t) {
    this.data = t;
  }
  function Xi(e) {
    this.manager = void 0 !== e ? e : t.DefaultLoadingManager;
  }
  function qi() {
    return (
      void 0 === ir &&
        (ir = new (window.AudioContext || window.webkitAudioContext)()),
      ir
    );
  }
  function Yi(e) {
    this.manager = void 0 !== e ? e : t.DefaultLoadingManager;
  }
  function Zi() {
    (this.type = 'StereoCamera'),
      (this.aspect = 1),
      (this.eyeSep = 0.064),
      (this.cameraL = new _t()),
      this.cameraL.layers.enable(1),
      (this.cameraL.matrixAutoUpdate = !1),
      (this.cameraR = new _t()),
      this.cameraR.layers.enable(2),
      (this.cameraR.matrixAutoUpdate = !1);
  }
  function Ji(t, e, i) {
    rt.call(this), (this.type = 'CubeCamera');
    var n = new _t(90, 1, t, e);
    n.up.set(0, -1, 0), n.lookAt(new c(1, 0, 0)), this.add(n);
    var r = new _t(90, 1, t, e);
    r.up.set(0, -1, 0), r.lookAt(new c(-1, 0, 0)), this.add(r);
    var a = new _t(90, 1, t, e);
    a.up.set(0, 0, 1), a.lookAt(new c(0, 1, 0)), this.add(a);
    var s = new _t(90, 1, t, e);
    s.up.set(0, 0, -1), s.lookAt(new c(0, -1, 0)), this.add(s);
    var h = new _t(90, 1, t, e);
    h.up.set(0, -1, 0), h.lookAt(new c(0, 0, 1)), this.add(h);
    var l = new _t(90, 1, t, e);
    l.up.set(0, -1, 0),
      l.lookAt(new c(0, 0, -1)),
      this.add(l),
      (this.renderTarget = new o(i, i, {
        format: 1022,
        magFilter: 1006,
        minFilter: 1006
      })),
      (this.updateCubeMap = function(t, e) {
        null === this.parent && this.updateMatrixWorld();
        var i = this.renderTarget,
          o = i.texture.generateMipmaps;
        (i.texture.generateMipmaps = !1),
          (i.activeCubeFace = 0),
          t.render(e, n, i),
          (i.activeCubeFace = 1),
          t.render(e, r, i),
          (i.activeCubeFace = 2),
          t.render(e, a, i),
          (i.activeCubeFace = 3),
          t.render(e, s, i),
          (i.activeCubeFace = 4),
          t.render(e, h, i),
          (i.texture.generateMipmaps = o),
          (i.activeCubeFace = 5),
          t.render(e, l, i),
          t.setRenderTarget(null);
      });
  }
  function Qi() {
    rt.call(this),
      (this.type = 'AudioListener'),
      (this.context = qi()),
      (this.gain = this.context.createGain()),
      this.gain.connect(this.context.destination),
      (this.filter = null);
  }
  function Ki(t) {
    rt.call(this),
      (this.type = 'Audio'),
      (this.context = t.context),
      (this.source = this.context.createBufferSource()),
      (this.source.onended = this.onEnded.bind(this)),
      (this.gain = this.context.createGain()),
      this.gain.connect(t.getInput()),
      (this.autoplay = !1),
      (this.startTime = 0),
      (this.playbackRate = 1),
      (this.isPlaying = !1),
      (this.hasPlaybackControl = !0),
      (this.sourceType = 'empty'),
      (this.filters = []);
  }
  function $i(t) {
    Ki.call(this, t),
      (this.panner = this.context.createPanner()),
      this.panner.connect(this.gain);
  }
  function tn(t, e) {
    (this.analyser = t.context.createAnalyser()),
      (this.analyser.fftSize = void 0 !== e ? e : 2048),
      (this.data = new Uint8Array(this.analyser.frequencyBinCount)),
      t.getOutput().connect(this.analyser);
  }
  function en(t, e, i) {
    switch (((this.binding = t), (this.valueSize = i), (t = Float64Array), e)) {
      case 'quaternion':
        e = this._slerp;
        break;
      case 'string':
      case 'bool':
        (t = Array), (e = this._select);
        break;
      default:
        e = this._lerp;
    }
    (this.buffer = new t(4 * i)),
      (this._mixBufferRegion = e),
      (this.referenceCount = this.useCount = this.cumulativeWeight = 0);
  }
  function nn(t, e, i) {
    (this.path = e),
      (this.parsedPath = i || nn.parseTrackName(e)),
      (this.node = nn.findNode(t, this.parsedPath.nodeName) || t),
      (this.rootNode = t);
  }
  function rn() {
    (this.uuid = t.Math.generateUUID()),
      (this._objects = Array.prototype.slice.call(arguments)),
      (this.nCachedObjects_ = 0);
    var e = {};
    this._indicesByUUID = e;
    for (var i = 0, n = arguments.length; i !== n; ++i)
      e[arguments[i].uuid] = i;
    (this._paths = []),
      (this._parsedPaths = []),
      (this._bindings = []),
      (this._bindingsIndicesByPath = {});
    var r = this;
    this.stats = {
      objects: {
        get total() {
          return r._objects.length;
        },
        get inUse() {
          return this.total - r.nCachedObjects_;
        }
      },
      get bindingsPerObject() {
        return r._bindings.length;
      }
    };
  }
  function an(t, e, i) {
    (this._mixer = t),
      (this._clip = e),
      (this._localRoot = i || null),
      (t = e.tracks),
      (e = t.length),
      (i = Array(e));
    for (var n = { endingStart: 2400, endingEnd: 2400 }, r = 0; r !== e; ++r) {
      var a = t[r].createInterpolant(null);
      (i[r] = a), (a.settings = n);
    }
    (this._interpolantSettings = n),
      (this._interpolants = i),
      (this._propertyBindings = Array(e)),
      (this._weightInterpolant = this._timeScaleInterpolant = this._byClipCacheIndex = this._cacheIndex = null),
      (this.loop = 2201),
      (this._loopCount = -1),
      (this._startTime = null),
      (this.time = 0),
      (this._effectiveWeight = this.weight = this._effectiveTimeScale = this.timeScale = 1),
      (this.repetitions = Infinity),
      (this.paused = !1),
      (this.enabled = !0),
      (this.clampWhenFinished = !1),
      (this.zeroSlopeAtEnd = this.zeroSlopeAtStart = !0);
  }
  function on(t) {
    (this._root = t),
      this._initMemoryManager(),
      (this.time = this._accuIndex = 0),
      (this.timeScale = 1);
  }
  function sn(t, e) {
    'string' == typeof t &&
      (console.warn('THREE.Uniform: Type parameter is no longer needed.'),
      (t = e)),
      (this.value = t),
      (this.dynamic = !1);
  }
  function cn() {
    mt.call(this),
      (this.type = 'InstancedBufferGeometry'),
      (this.maxInstancedCount = void 0);
  }
  function hn(e, i, n, r) {
    (this.uuid = t.Math.generateUUID()),
      (this.data = e),
      (this.itemSize = i),
      (this.offset = n),
      (this.normalized = !0 === r);
  }
  function ln(e, i) {
    (this.uuid = t.Math.generateUUID()),
      (this.array = e),
      (this.stride = i),
      (this.count = void 0 !== e ? e.length / i : 0),
      (this.dynamic = !1),
      (this.updateRange = { offset: 0, count: -1 }),
      (this.version = 0);
  }
  function un(t, e, i) {
    ln.call(this, t, e), (this.meshPerAttribute = i || 1);
  }
  function dn(t, e, i) {
    ht.call(this, t, e), (this.meshPerAttribute = i || 1);
  }
  function pn(t, e, i, n) {
    (this.ray = new et(t, e)),
      (this.near = i || 0),
      (this.far = n || Infinity),
      (this.params = {
        Mesh: {},
        Line: {},
        LOD: {},
        Points: { threshold: 1 },
        Sprite: {}
      }),
      Object.defineProperties(this.params, {
        PointCloud: {
          get: function() {
            return (
              console.warn(
                'THREE.Raycaster: params.PointCloud has been renamed to params.Points.'
              ),
              this.Points
            );
          }
        }
      });
  }
  function fn(t, e) {
    return t.distance - e.distance;
  }
  function mn(t, e, i, n) {
    if (!1 !== t.visible && (t.raycast(e, i), !0 === n)) {
      (t = t.children), (n = 0);
      for (var r = t.length; n < r; n++) mn(t[n], e, i, !0);
    }
  }
  function gn(t) {
    (this.autoStart = void 0 === t || t),
      (this.elapsedTime = this.oldTime = this.startTime = 0),
      (this.running = !1);
  }
  function vn(t, e, i) {
    return (
      (this.radius = void 0 !== t ? t : 1),
      (this.phi = void 0 !== e ? e : 0),
      (this.theta = void 0 !== i ? i : 0),
      this
    );
  }
  function yn(t, e) {
    gt.call(this, t, e), (this.animationsMap = {}), (this.animationsList = []);
    var i = this.geometry.morphTargets.length;
    this.createAnimation('__default', 0, i - 1, i / 1),
      this.setAnimationWeight('__default', 1);
  }
  function xn(t) {
    rt.call(this), (this.material = t), (this.render = function() {});
  }
  function _n(t, e, i, n) {
    (this.object = t),
      (this.size = void 0 !== e ? e : 1),
      (t = void 0 !== i ? i : 16711680),
      (n = void 0 !== n ? n : 1),
      (e = 0),
      (i = this.object.geometry) && i.isGeometry
        ? (e = 3 * i.faces.length)
        : i && i.isBufferGeometry && (e = i.attributes.normal.count),
      (i = new mt()),
      (e = new dt(6 * e, 3)),
      i.addAttribute('position', e),
      se.call(this, i, new ae({ color: t, linewidth: n })),
      (this.matrixAutoUpdate = !1),
      this.update();
  }
  function bn(t) {
    rt.call(this),
      (this.light = t),
      this.light.updateMatrixWorld(),
      (this.matrix = t.matrixWorld),
      (this.matrixAutoUpdate = !1),
      (t = new mt());
    for (
      var e = [
          0,
          0,
          0,
          0,
          0,
          1,
          0,
          0,
          0,
          1,
          0,
          1,
          0,
          0,
          0,
          -1,
          0,
          1,
          0,
          0,
          0,
          0,
          1,
          1,
          0,
          0,
          0,
          0,
          -1,
          1
        ],
        i = 0,
        n = 1;
      32 > i;
      i++, n++
    ) {
      var r = i / 32 * Math.PI * 2,
        a = n / 32 * Math.PI * 2;
      e.push(Math.cos(r), Math.sin(r), 1, Math.cos(a), Math.sin(a), 1);
    }
    t.addAttribute('position', new dt(e, 3)),
      (e = new ae({ fog: !1 })),
      (this.cone = new se(t, e)),
      this.add(this.cone),
      this.update();
  }
  function wn(t) {
    this.bones = this.getBoneList(t);
    for (var e = new pt(), i = 0; i < this.bones.length; i++) {
      var n = this.bones[i];
      n.parent &&
        n.parent.isBone &&
        (e.vertices.push(new c()),
        e.vertices.push(new c()),
        e.colors.push(new V(0, 0, 1)),
        e.colors.push(new V(0, 1, 0)));
    }
    (e.dynamic = !0),
      (i = new ae({
        vertexColors: 2,
        depthTest: !1,
        depthWrite: !1,
        transparent: !0
      })),
      se.call(this, e, i),
      (this.root = t),
      (this.matrix = t.matrixWorld),
      (this.matrixAutoUpdate = !1),
      this.update();
  }
  function Mn(t, e) {
    (this.light = t), this.light.updateMatrixWorld();
    var i = new Re(e, 4, 2),
      n = new ct({ wireframe: !0, fog: !1 });
    n.color.copy(this.light.color).multiplyScalar(this.light.intensity),
      gt.call(this, i, n),
      (this.matrix = this.light.matrixWorld),
      (this.matrixAutoUpdate = !1);
  }
  function En(t, e) {
    rt.call(this),
      (this.light = t),
      this.light.updateMatrixWorld(),
      (this.matrix = t.matrixWorld),
      (this.matrixAutoUpdate = !1),
      (this.colors = [new V(), new V()]);
    var i = new Pe(e, 4, 2);
    i.rotateX(-Math.PI / 2);
    for (var n = 0; 8 > n; n++) i.faces[n].color = this.colors[4 > n ? 0 : 1];
    (n = new ct({ vertexColors: 1, wireframe: !0 })),
      (this.lightSphere = new gt(i, n)),
      this.add(this.lightSphere),
      this.update();
  }
  function Sn(t, e, i, n) {
    (e = e || 1),
      (i = new V(void 0 !== i ? i : 4473924)),
      (n = new V(void 0 !== n ? n : 8947848));
    for (
      var r = e / 2, a = 2 * t / e, o = [], s = [], c = 0, h = 0, l = -t;
      c <= e;
      c++, l += a
    ) {
      o.push(-t, 0, l, t, 0, l), o.push(l, 0, -t, l, 0, t);
      var u = c === r ? i : n;
      u.toArray(s, h),
        (h += 3),
        u.toArray(s, h),
        (h += 3),
        u.toArray(s, h),
        (h += 3),
        u.toArray(s, h),
        (h += 3);
    }
    (t = new mt()),
      t.addAttribute('position', new dt(o, 3)),
      t.addAttribute('color', new dt(s, 3)),
      (o = new ae({ vertexColors: 2 })),
      se.call(this, t, o);
  }
  function Tn(t, e, i, n) {
    (this.object = t),
      (this.size = void 0 !== e ? e : 1),
      (t = void 0 !== i ? i : 16776960),
      (n = void 0 !== n ? n : 1),
      (e = 0),
      (i = this.object.geometry) && i.isGeometry
        ? (e = i.faces.length)
        : console.warn(
            'THREE.FaceNormalsHelper: only THREE.Geometry is supported. Use THREE.VertexNormalsHelper, instead.'
          ),
      (i = new mt()),
      (e = new dt(6 * e, 3)),
      i.addAttribute('position', e),
      se.call(this, i, new ae({ color: t, linewidth: n })),
      (this.matrixAutoUpdate = !1),
      this.update();
  }
  function An(t, e) {
    rt.call(this),
      (this.light = t),
      this.light.updateMatrixWorld(),
      (this.matrix = t.matrixWorld),
      (this.matrixAutoUpdate = !1),
      void 0 === e && (e = 1);
    var i = new mt();
    i.addAttribute(
      'position',
      new dt([-e, e, 0, e, e, 0, e, -e, 0, -e, -e, 0, -e, e, 0], 3)
    );
    var n = new ae({ fog: !1 });
    this.add(new oe(i, n)),
      (i = new mt()),
      i.addAttribute('position', new dt([0, 0, 0, 0, 0, 1], 3)),
      this.add(new oe(i, n)),
      this.update();
  }
  function Ln(t) {
    function e(t, e, n) {
      i(t, n), i(e, n);
    }
    function i(t, e) {
      n.vertices.push(new c()),
        n.colors.push(new V(e)),
        void 0 === a[t] && (a[t] = []),
        a[t].push(n.vertices.length - 1);
    }
    var n = new pt(),
      r = new ae({ color: 16777215, vertexColors: 1 }),
      a = {};
    e('n1', 'n2', 16755200),
      e('n2', 'n4', 16755200),
      e('n4', 'n3', 16755200),
      e('n3', 'n1', 16755200),
      e('f1', 'f2', 16755200),
      e('f2', 'f4', 16755200),
      e('f4', 'f3', 16755200),
      e('f3', 'f1', 16755200),
      e('n1', 'f1', 16755200),
      e('n2', 'f2', 16755200),
      e('n3', 'f3', 16755200),
      e('n4', 'f4', 16755200),
      e('p', 'n1', 16711680),
      e('p', 'n2', 16711680),
      e('p', 'n3', 16711680),
      e('p', 'n4', 16711680),
      e('u1', 'u2', 43775),
      e('u2', 'u3', 43775),
      e('u3', 'u1', 43775),
      e('c', 't', 16777215),
      e('p', 'c', 3355443),
      e('cn1', 'cn2', 3355443),
      e('cn3', 'cn4', 3355443),
      e('cf1', 'cf2', 3355443),
      e('cf3', 'cf4', 3355443),
      se.call(this, n, r),
      (this.camera = t),
      this.camera.updateProjectionMatrix &&
        this.camera.updateProjectionMatrix(),
      (this.matrix = t.matrixWorld),
      (this.matrixAutoUpdate = !1),
      (this.pointMap = a),
      this.update();
  }
  function Rn(t, e) {
    var i = void 0 !== e ? e : 8947848;
    (this.object = t),
      (this.box = new Z()),
      gt.call(this, new je(1, 1, 1), new ct({ color: i, wireframe: !0 }));
  }
  function Pn(t, e) {
    void 0 === e && (e = 16776960);
    var i = new Uint16Array([
        0,
        1,
        1,
        2,
        2,
        3,
        3,
        0,
        4,
        5,
        5,
        6,
        6,
        7,
        7,
        4,
        0,
        4,
        1,
        5,
        2,
        6,
        3,
        7
      ]),
      n = new Float32Array(24),
      r = new mt();
    r.setIndex(new ht(i, 1)),
      r.addAttribute('position', new ht(n, 3)),
      se.call(this, r, new ae({ color: e })),
      void 0 !== t && this.update(t);
  }
  function Cn(t, e, i, n, r, a) {
    rt.call(this),
      void 0 === n && (n = 16776960),
      void 0 === i && (i = 1),
      void 0 === r && (r = 0.2 * i),
      void 0 === a && (a = 0.2 * r),
      this.position.copy(e),
      (this.line = new oe(nr, new ae({ color: n }))),
      (this.line.matrixAutoUpdate = !1),
      this.add(this.line),
      (this.cone = new gt(rr, new ct({ color: n }))),
      (this.cone.matrixAutoUpdate = !1),
      this.add(this.cone),
      this.setDirection(t),
      this.setLength(i, r, a);
  }
  function In(t) {
    t = t || 1;
    var e = new Float32Array([
        0,
        0,
        0,
        t,
        0,
        0,
        0,
        0,
        0,
        0,
        t,
        0,
        0,
        0,
        0,
        0,
        0,
        t
      ]),
      i = new Float32Array([
        1,
        0,
        0,
        1,
        0.6,
        0,
        0,
        1,
        0,
        0.6,
        1,
        0,
        0,
        0,
        1,
        0,
        0.6,
        1
      ]);
    (t = new mt()),
      t.addAttribute('position', new ht(e, 3)),
      t.addAttribute('color', new ht(i, 3)),
      (e = new ae({ vertexColors: 2 })),
      se.call(this, t, e);
  }
  function Un(e) {
    console.warn(
      'THREE.ClosedSplineCurve3 has been deprecated. Please use THREE.CatmullRomCurve3.'
    ),
      t.CatmullRomCurve3.call(this, e),
      (this.type = 'catmullrom'),
      (this.closed = !0);
  }
  function Dn(t, e, i, n, r, a) {
    zi.call(this, t, e, i, i, n, r, a);
  }
  void 0 === Number.EPSILON && (Number.EPSILON = Math.pow(2, -52)),
    void 0 === Math.sign &&
      (Math.sign = function(t) {
        return 0 > t ? -1 : 0 < t ? 1 : +t;
      }),
    void 0 === Function.prototype.name &&
      Object.defineProperty(Function.prototype, 'name', {
        get: function() {
          return this.toString().match(/^\s*function\s*(\S*)\s*\(/)[1];
        }
      }),
    void 0 === Object.assign &&
      (function() {
        Object.assign = function(t) {
          if (void 0 === t || null === t)
            throw new TypeError('Cannot convert undefined or null to object');
          for (var e = Object(t), i = 1; i < arguments.length; i++) {
            var n = arguments[i];
            if (void 0 !== n && null !== n)
              for (var r in n)
                Object.prototype.hasOwnProperty.call(n, r) && (e[r] = n[r]);
          }
          return e;
        };
      })(),
    Object.assign(e.prototype, {
      addEventListener: function(t, e) {
        void 0 === this._listeners && (this._listeners = {});
        var i = this._listeners;
        void 0 === i[t] && (i[t] = []), -1 === i[t].indexOf(e) && i[t].push(e);
      },
      hasEventListener: function(t, e) {
        if (void 0 === this._listeners) return !1;
        var i = this._listeners;
        return void 0 !== i[t] && -1 !== i[t].indexOf(e);
      },
      removeEventListener: function(t, e) {
        if (void 0 !== this._listeners) {
          var i = this._listeners[t];
          if (void 0 !== i) {
            var n = i.indexOf(e);
            -1 !== n && i.splice(n, 1);
          }
        }
      },
      dispatchEvent: function(t) {
        if (void 0 !== this._listeners) {
          var e = this._listeners[t.type];
          if (void 0 !== e) {
            t.target = this;
            var i,
              n = [],
              r = e.length;
            for (i = 0; i < r; i++) n[i] = e[i];
            for (i = 0; i < r; i++) n[i].call(this, t);
          }
        }
      }
    });
  var Nn = {
      NoBlending: 0,
      NormalBlending: 1,
      AdditiveBlending: 2,
      SubtractiveBlending: 3,
      MultiplyBlending: 4,
      CustomBlending: 5
    },
    Fn = {
      UVMapping: 300,
      CubeReflectionMapping: 301,
      CubeRefractionMapping: 302,
      EquirectangularReflectionMapping: 303,
      EquirectangularRefractionMapping: 304,
      SphericalReflectionMapping: 305,
      CubeUVReflectionMapping: 306,
      CubeUVRefractionMapping: 307
    },
    On = {
      RepeatWrapping: 1e3,
      ClampToEdgeWrapping: 1001,
      MirroredRepeatWrapping: 1002
    },
    zn = {
      NearestFilter: 1003,
      NearestMipMapNearestFilter: 1004,
      NearestMipMapLinearFilter: 1005,
      LinearFilter: 1006,
      LinearMipMapNearestFilter: 1007,
      LinearMipMapLinearFilter: 1008
    };
  (t.Math = {
    DEG2RAD: Math.PI / 180,
    RAD2DEG: 180 / Math.PI,
    generateUUID: (function() {
      var t,
        e = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split(
          ''
        ),
        i = Array(36),
        n = 0;
      return function() {
        for (var r = 0; 36 > r; r++)
          8 === r || 13 === r || 18 === r || 23 === r
            ? (i[r] = '-')
            : 14 === r
              ? (i[r] = '4')
              : (2 >= n && (n = (33554432 + 16777216 * Math.random()) | 0),
                (t = 15 & n),
                (n >>= 4),
                (i[r] = e[19 === r ? (3 & t) | 8 : t]));
        return i.join('');
      };
    })(),
    clamp: function(t, e, i) {
      return Math.max(e, Math.min(i, t));
    },
    euclideanModulo: function(t, e) {
      return (t % e + e) % e;
    },
    mapLinear: function(t, e, i, n, r) {
      return n + (t - e) * (r - n) / (i - e);
    },
    smoothstep: function(t, e, i) {
      return t <= e
        ? 0
        : t >= i ? 1 : (t = (t - e) / (i - e)) * t * (3 - 2 * t);
    },
    smootherstep: function(t, e, i) {
      return t <= e
        ? 0
        : t >= i
          ? 1
          : (t = (t - e) / (i - e)) * t * t * (t * (6 * t - 15) + 10);
    },
    random16: function() {
      return (
        console.warn(
          'THREE.Math.random16() has been deprecated. Use Math.random() instead.'
        ),
        Math.random()
      );
    },
    randInt: function(t, e) {
      return t + Math.floor(Math.random() * (e - t + 1));
    },
    randFloat: function(t, e) {
      return t + Math.random() * (e - t);
    },
    randFloatSpread: function(t) {
      return t * (0.5 - Math.random());
    },
    degToRad: function(e) {
      return e * t.Math.DEG2RAD;
    },
    radToDeg: function(e) {
      return e * t.Math.RAD2DEG;
    },
    isPowerOfTwo: function(t) {
      return 0 == (t & (t - 1)) && 0 !== t;
    },
    nearestPowerOfTwo: function(t) {
      return Math.pow(2, Math.round(Math.log(t) / Math.LN2));
    },
    nextPowerOfTwo: function(t) {
      return (
        t--,
        (t |= t >> 1),
        (t |= t >> 2),
        (t |= t >> 4),
        (t |= t >> 8),
        (t |= t >> 16),
        ++t
      );
    }
  }),
    (i.prototype = {
      constructor: i,
      isVector2: !0,
      get width() {
        return this.x;
      },
      set width(t) {
        this.x = t;
      },
      get height() {
        return this.y;
      },
      set height(t) {
        this.y = t;
      },
      set: function(t, e) {
        return (this.x = t), (this.y = e), this;
      },
      setScalar: function(t) {
        return (this.y = this.x = t), this;
      },
      setX: function(t) {
        return (this.x = t), this;
      },
      setY: function(t) {
        return (this.y = t), this;
      },
      setComponent: function(t, e) {
        switch (t) {
          case 0:
            this.x = e;
            break;
          case 1:
            this.y = e;
            break;
          default:
            throw Error('index is out of range: ' + t);
        }
      },
      getComponent: function(t) {
        switch (t) {
          case 0:
            return this.x;
          case 1:
            return this.y;
          default:
            throw Error('index is out of range: ' + t);
        }
      },
      clone: function() {
        return new this.constructor(this.x, this.y);
      },
      copy: function(t) {
        return (this.x = t.x), (this.y = t.y), this;
      },
      add: function(t, e) {
        return void 0 !== e
          ? (console.warn(
              'THREE.Vector2: .add() now only accepts one argument. Use .addVectors( a, b ) instead.'
            ),
            this.addVectors(t, e))
          : ((this.x += t.x), (this.y += t.y), this);
      },
      addScalar: function(t) {
        return (this.x += t), (this.y += t), this;
      },
      addVectors: function(t, e) {
        return (this.x = t.x + e.x), (this.y = t.y + e.y), this;
      },
      addScaledVector: function(t, e) {
        return (this.x += t.x * e), (this.y += t.y * e), this;
      },
      sub: function(t, e) {
        return void 0 !== e
          ? (console.warn(
              'THREE.Vector2: .sub() now only accepts one argument. Use .subVectors( a, b ) instead.'
            ),
            this.subVectors(t, e))
          : ((this.x -= t.x), (this.y -= t.y), this);
      },
      subScalar: function(t) {
        return (this.x -= t), (this.y -= t), this;
      },
      subVectors: function(t, e) {
        return (this.x = t.x - e.x), (this.y = t.y - e.y), this;
      },
      multiply: function(t) {
        return (this.x *= t.x), (this.y *= t.y), this;
      },
      multiplyScalar: function(t) {
        return (
          isFinite(t) ? ((this.x *= t), (this.y *= t)) : (this.y = this.x = 0),
          this
        );
      },
      divide: function(t) {
        return (this.x /= t.x), (this.y /= t.y), this;
      },
      divideScalar: function(t) {
        return this.multiplyScalar(1 / t);
      },
      min: function(t) {
        return (
          (this.x = Math.min(this.x, t.x)),
          (this.y = Math.min(this.y, t.y)),
          this
        );
      },
      max: function(t) {
        return (
          (this.x = Math.max(this.x, t.x)),
          (this.y = Math.max(this.y, t.y)),
          this
        );
      },
      clamp: function(t, e) {
        return (
          (this.x = Math.max(t.x, Math.min(e.x, this.x))),
          (this.y = Math.max(t.y, Math.min(e.y, this.y))),
          this
        );
      },
      clampScalar: (function() {
        var t, e;
        return function(n, r) {
          return (
            void 0 === t && ((t = new i()), (e = new i())),
            t.set(n, n),
            e.set(r, r),
            this.clamp(t, e)
          );
        };
      })(),
      clampLength: function(t, e) {
        var i = this.length();
        return this.multiplyScalar(Math.max(t, Math.min(e, i)) / i);
      },
      floor: function() {
        return (
          (this.x = Math.floor(this.x)), (this.y = Math.floor(this.y)), this
        );
      },
      ceil: function() {
        return (this.x = Math.ceil(this.x)), (this.y = Math.ceil(this.y)), this;
      },
      round: function() {
        return (
          (this.x = Math.round(this.x)), (this.y = Math.round(this.y)), this
        );
      },
      roundToZero: function() {
        return (
          (this.x = 0 > this.x ? Math.ceil(this.x) : Math.floor(this.x)),
          (this.y = 0 > this.y ? Math.ceil(this.y) : Math.floor(this.y)),
          this
        );
      },
      negate: function() {
        return (this.x = -this.x), (this.y = -this.y), this;
      },
      dot: function(t) {
        return this.x * t.x + this.y * t.y;
      },
      lengthSq: function() {
        return this.x * this.x + this.y * this.y;
      },
      length: function() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
      },
      lengthManhattan: function() {
        return Math.abs(this.x) + Math.abs(this.y);
      },
      normalize: function() {
        return this.divideScalar(this.length());
      },
      angle: function() {
        var t = Math.atan2(this.y, this.x);
        return 0 > t && (t += 2 * Math.PI), t;
      },
      distanceTo: function(t) {
        return Math.sqrt(this.distanceToSquared(t));
      },
      distanceToSquared: function(t) {
        var e = this.x - t.x;
        return (t = this.y - t.y), e * e + t * t;
      },
      distanceToManhattan: function(t) {
        return Math.abs(this.x - t.x) + Math.abs(this.y - t.y);
      },
      setLength: function(t) {
        return this.multiplyScalar(t / this.length());
      },
      lerp: function(t, e) {
        return (
          (this.x += (t.x - this.x) * e), (this.y += (t.y - this.y) * e), this
        );
      },
      lerpVectors: function(t, e, i) {
        return this.subVectors(e, t)
          .multiplyScalar(i)
          .add(t);
      },
      equals: function(t) {
        return t.x === this.x && t.y === this.y;
      },
      fromArray: function(t, e) {
        return (
          void 0 === e && (e = 0), (this.x = t[e]), (this.y = t[e + 1]), this
        );
      },
      toArray: function(t, e) {
        return (
          void 0 === t && (t = []),
          void 0 === e && (e = 0),
          (t[e] = this.x),
          (t[e + 1] = this.y),
          t
        );
      },
      fromAttribute: function(t, e, i) {
        return (
          void 0 === i && (i = 0),
          (e = e * t.itemSize + i),
          (this.x = t.array[e]),
          (this.y = t.array[e + 1]),
          this
        );
      },
      rotateAround: function(t, e) {
        var i = Math.cos(e),
          n = Math.sin(e),
          r = this.x - t.x,
          a = this.y - t.y;
        return (
          (this.x = r * i - a * n + t.x), (this.y = r * n + a * i + t.y), this
        );
      }
    }),
    (n.DEFAULT_IMAGE = void 0),
    (n.DEFAULT_MAPPING = 300),
    (n.prototype = {
      constructor: n,
      isTexture: !0,
      set needsUpdate(t) {
        !0 === t && this.version++;
      },
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        return (
          (this.image = t.image),
          (this.mipmaps = t.mipmaps.slice(0)),
          (this.mapping = t.mapping),
          (this.wrapS = t.wrapS),
          (this.wrapT = t.wrapT),
          (this.magFilter = t.magFilter),
          (this.minFilter = t.minFilter),
          (this.anisotropy = t.anisotropy),
          (this.format = t.format),
          (this.type = t.type),
          this.offset.copy(t.offset),
          this.repeat.copy(t.repeat),
          (this.generateMipmaps = t.generateMipmaps),
          (this.premultiplyAlpha = t.premultiplyAlpha),
          (this.flipY = t.flipY),
          (this.unpackAlignment = t.unpackAlignment),
          (this.encoding = t.encoding),
          this
        );
      },
      toJSON: function(e) {
        if (void 0 !== e.textures[this.uuid]) return e.textures[this.uuid];
        var i = {
          metadata: {
            version: 4.4,
            type: 'Texture',
            generator: 'Texture.toJSON'
          },
          uuid: this.uuid,
          name: this.name,
          mapping: this.mapping,
          repeat: [this.repeat.x, this.repeat.y],
          offset: [this.offset.x, this.offset.y],
          wrap: [this.wrapS, this.wrapT],
          minFilter: this.minFilter,
          magFilter: this.magFilter,
          anisotropy: this.anisotropy,
          flipY: this.flipY
        };
        if (void 0 !== this.image) {
          var n = this.image;
          if (
            (void 0 === n.uuid && (n.uuid = t.Math.generateUUID()),
            void 0 === e.images[n.uuid])
          ) {
            var r,
              a = e.images,
              o = n.uuid,
              s = n.uuid;
            void 0 !== n.toDataURL
              ? (r = n)
              : ((r = document.createElementNS(
                  'http://www.w3.org/1999/xhtml',
                  'canvas'
                )),
                (r.width = n.width),
                (r.height = n.height),
                r.getContext('2d').drawImage(n, 0, 0, n.width, n.height)),
              (r =
                2048 < r.width || 2048 < r.height
                  ? r.toDataURL('image/jpeg', 0.6)
                  : r.toDataURL('image/png')),
              (a[o] = { uuid: s, url: r });
          }
          i.image = n.uuid;
        }
        return (e.textures[this.uuid] = i);
      },
      dispose: function() {
        this.dispatchEvent({ type: 'dispose' });
      },
      transformUv: function(t) {
        if (300 === this.mapping) {
          if ((t.multiply(this.repeat), t.add(this.offset), 0 > t.x || 1 < t.x))
            switch (this.wrapS) {
              case 1e3:
                t.x -= Math.floor(t.x);
                break;
              case 1001:
                t.x = 0 > t.x ? 0 : 1;
                break;
              case 1002:
                t.x =
                  1 === Math.abs(Math.floor(t.x) % 2)
                    ? Math.ceil(t.x) - t.x
                    : t.x - Math.floor(t.x);
            }
          if (0 > t.y || 1 < t.y)
            switch (this.wrapT) {
              case 1e3:
                t.y -= Math.floor(t.y);
                break;
              case 1001:
                t.y = 0 > t.y ? 0 : 1;
                break;
              case 1002:
                t.y =
                  1 === Math.abs(Math.floor(t.y) % 2)
                    ? Math.ceil(t.y) - t.y
                    : t.y - Math.floor(t.y);
            }
          this.flipY && (t.y = 1 - t.y);
        }
      }
    }),
    Object.assign(n.prototype, e.prototype);
  var Bn = 0;
  (r.prototype = {
    constructor: r,
    isVector4: !0,
    set: function(t, e, i, n) {
      return (this.x = t), (this.y = e), (this.z = i), (this.w = n), this;
    },
    setScalar: function(t) {
      return (this.w = this.z = this.y = this.x = t), this;
    },
    setX: function(t) {
      return (this.x = t), this;
    },
    setY: function(t) {
      return (this.y = t), this;
    },
    setZ: function(t) {
      return (this.z = t), this;
    },
    setW: function(t) {
      return (this.w = t), this;
    },
    setComponent: function(t, e) {
      switch (t) {
        case 0:
          this.x = e;
          break;
        case 1:
          this.y = e;
          break;
        case 2:
          this.z = e;
          break;
        case 3:
          this.w = e;
          break;
        default:
          throw Error('index is out of range: ' + t);
      }
    },
    getComponent: function(t) {
      switch (t) {
        case 0:
          return this.x;
        case 1:
          return this.y;
        case 2:
          return this.z;
        case 3:
          return this.w;
        default:
          throw Error('index is out of range: ' + t);
      }
    },
    clone: function() {
      return new this.constructor(this.x, this.y, this.z, this.w);
    },
    copy: function(t) {
      return (
        (this.x = t.x),
        (this.y = t.y),
        (this.z = t.z),
        (this.w = void 0 !== t.w ? t.w : 1),
        this
      );
    },
    add: function(t, e) {
      return void 0 !== e
        ? (console.warn(
            'THREE.Vector4: .add() now only accepts one argument. Use .addVectors( a, b ) instead.'
          ),
          this.addVectors(t, e))
        : ((this.x += t.x),
          (this.y += t.y),
          (this.z += t.z),
          (this.w += t.w),
          this);
    },
    addScalar: function(t) {
      return (this.x += t), (this.y += t), (this.z += t), (this.w += t), this;
    },
    addVectors: function(t, e) {
      return (
        (this.x = t.x + e.x),
        (this.y = t.y + e.y),
        (this.z = t.z + e.z),
        (this.w = t.w + e.w),
        this
      );
    },
    addScaledVector: function(t, e) {
      return (
        (this.x += t.x * e),
        (this.y += t.y * e),
        (this.z += t.z * e),
        (this.w += t.w * e),
        this
      );
    },
    sub: function(t, e) {
      return void 0 !== e
        ? (console.warn(
            'THREE.Vector4: .sub() now only accepts one argument. Use .subVectors( a, b ) instead.'
          ),
          this.subVectors(t, e))
        : ((this.x -= t.x),
          (this.y -= t.y),
          (this.z -= t.z),
          (this.w -= t.w),
          this);
    },
    subScalar: function(t) {
      return (this.x -= t), (this.y -= t), (this.z -= t), (this.w -= t), this;
    },
    subVectors: function(t, e) {
      return (
        (this.x = t.x - e.x),
        (this.y = t.y - e.y),
        (this.z = t.z - e.z),
        (this.w = t.w - e.w),
        this
      );
    },
    multiplyScalar: function(t) {
      return (
        isFinite(t)
          ? ((this.x *= t), (this.y *= t), (this.z *= t), (this.w *= t))
          : (this.w = this.z = this.y = this.x = 0),
        this
      );
    },
    applyMatrix4: function(t) {
      var e = this.x,
        i = this.y,
        n = this.z,
        r = this.w;
      return (
        (t = t.elements),
        (this.x = t[0] * e + t[4] * i + t[8] * n + t[12] * r),
        (this.y = t[1] * e + t[5] * i + t[9] * n + t[13] * r),
        (this.z = t[2] * e + t[6] * i + t[10] * n + t[14] * r),
        (this.w = t[3] * e + t[7] * i + t[11] * n + t[15] * r),
        this
      );
    },
    divideScalar: function(t) {
      return this.multiplyScalar(1 / t);
    },
    setAxisAngleFromQuaternion: function(t) {
      this.w = 2 * Math.acos(t.w);
      var e = Math.sqrt(1 - t.w * t.w);
      return (
        1e-4 > e
          ? ((this.x = 1), (this.z = this.y = 0))
          : ((this.x = t.x / e), (this.y = t.y / e), (this.z = t.z / e)),
        this
      );
    },
    setAxisAngleFromRotationMatrix: function(t) {
      var e, i, n;
      t = t.elements;
      var r = t[0];
      n = t[4];
      var a = t[8],
        o = t[1],
        s = t[5],
        c = t[9];
      (i = t[2]), (e = t[6]);
      var h = t[10];
      return 0.01 > Math.abs(n - o) &&
        0.01 > Math.abs(a - i) &&
        0.01 > Math.abs(c - e)
        ? 0.1 > Math.abs(n + o) &&
          0.1 > Math.abs(a + i) &&
          0.1 > Math.abs(c + e) &&
          0.1 > Math.abs(r + s + h - 3)
          ? (this.set(1, 0, 0, 0), this)
          : ((t = Math.PI),
            (r = (r + 1) / 2),
            (s = (s + 1) / 2),
            (h = (h + 1) / 2),
            (n = (n + o) / 4),
            (a = (a + i) / 4),
            (c = (c + e) / 4),
            r > s && r > h
              ? 0.01 > r
                ? ((e = 0), (n = i = 0.707106781))
                : ((e = Math.sqrt(r)), (i = n / e), (n = a / e))
              : s > h
                ? 0.01 > s
                  ? ((e = 0.707106781), (i = 0), (n = 0.707106781))
                  : ((i = Math.sqrt(s)), (e = n / i), (n = c / i))
                : 0.01 > h
                  ? ((i = e = 0.707106781), (n = 0))
                  : ((n = Math.sqrt(h)), (e = a / n), (i = c / n)),
            this.set(e, i, n, t),
            this)
        : ((t = Math.sqrt(
            (e - c) * (e - c) + (a - i) * (a - i) + (o - n) * (o - n)
          )),
          0.001 > Math.abs(t) && (t = 1),
          (this.x = (e - c) / t),
          (this.y = (a - i) / t),
          (this.z = (o - n) / t),
          (this.w = Math.acos((r + s + h - 1) / 2)),
          this);
    },
    min: function(t) {
      return (
        (this.x = Math.min(this.x, t.x)),
        (this.y = Math.min(this.y, t.y)),
        (this.z = Math.min(this.z, t.z)),
        (this.w = Math.min(this.w, t.w)),
        this
      );
    },
    max: function(t) {
      return (
        (this.x = Math.max(this.x, t.x)),
        (this.y = Math.max(this.y, t.y)),
        (this.z = Math.max(this.z, t.z)),
        (this.w = Math.max(this.w, t.w)),
        this
      );
    },
    clamp: function(t, e) {
      return (
        (this.x = Math.max(t.x, Math.min(e.x, this.x))),
        (this.y = Math.max(t.y, Math.min(e.y, this.y))),
        (this.z = Math.max(t.z, Math.min(e.z, this.z))),
        (this.w = Math.max(t.w, Math.min(e.w, this.w))),
        this
      );
    },
    clampScalar: (function() {
      var t, e;
      return function(i, n) {
        return (
          void 0 === t && ((t = new r()), (e = new r())),
          t.set(i, i, i, i),
          e.set(n, n, n, n),
          this.clamp(t, e)
        );
      };
    })(),
    floor: function() {
      return (
        (this.x = Math.floor(this.x)),
        (this.y = Math.floor(this.y)),
        (this.z = Math.floor(this.z)),
        (this.w = Math.floor(this.w)),
        this
      );
    },
    ceil: function() {
      return (
        (this.x = Math.ceil(this.x)),
        (this.y = Math.ceil(this.y)),
        (this.z = Math.ceil(this.z)),
        (this.w = Math.ceil(this.w)),
        this
      );
    },
    round: function() {
      return (
        (this.x = Math.round(this.x)),
        (this.y = Math.round(this.y)),
        (this.z = Math.round(this.z)),
        (this.w = Math.round(this.w)),
        this
      );
    },
    roundToZero: function() {
      return (
        (this.x = 0 > this.x ? Math.ceil(this.x) : Math.floor(this.x)),
        (this.y = 0 > this.y ? Math.ceil(this.y) : Math.floor(this.y)),
        (this.z = 0 > this.z ? Math.ceil(this.z) : Math.floor(this.z)),
        (this.w = 0 > this.w ? Math.ceil(this.w) : Math.floor(this.w)),
        this
      );
    },
    negate: function() {
      return (
        (this.x = -this.x),
        (this.y = -this.y),
        (this.z = -this.z),
        (this.w = -this.w),
        this
      );
    },
    dot: function(t) {
      return this.x * t.x + this.y * t.y + this.z * t.z + this.w * t.w;
    },
    lengthSq: function() {
      return (
        this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w
      );
    },
    length: function() {
      return Math.sqrt(
        this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w
      );
    },
    lengthManhattan: function() {
      return (
        Math.abs(this.x) +
        Math.abs(this.y) +
        Math.abs(this.z) +
        Math.abs(this.w)
      );
    },
    normalize: function() {
      return this.divideScalar(this.length());
    },
    setLength: function(t) {
      return this.multiplyScalar(t / this.length());
    },
    lerp: function(t, e) {
      return (
        (this.x += (t.x - this.x) * e),
        (this.y += (t.y - this.y) * e),
        (this.z += (t.z - this.z) * e),
        (this.w += (t.w - this.w) * e),
        this
      );
    },
    lerpVectors: function(t, e, i) {
      return this.subVectors(e, t)
        .multiplyScalar(i)
        .add(t);
    },
    equals: function(t) {
      return (
        t.x === this.x && t.y === this.y && t.z === this.z && t.w === this.w
      );
    },
    fromArray: function(t, e) {
      return (
        void 0 === e && (e = 0),
        (this.x = t[e]),
        (this.y = t[e + 1]),
        (this.z = t[e + 2]),
        (this.w = t[e + 3]),
        this
      );
    },
    toArray: function(t, e) {
      return (
        void 0 === t && (t = []),
        void 0 === e && (e = 0),
        (t[e] = this.x),
        (t[e + 1] = this.y),
        (t[e + 2] = this.z),
        (t[e + 3] = this.w),
        t
      );
    },
    fromAttribute: function(t, e, i) {
      return (
        void 0 === i && (i = 0),
        (e = e * t.itemSize + i),
        (this.x = t.array[e]),
        (this.y = t.array[e + 1]),
        (this.z = t.array[e + 2]),
        (this.w = t.array[e + 3]),
        this
      );
    }
  }),
    Object.assign(a.prototype, e.prototype, {
      isWebGLRenderTarget: !0,
      setSize: function(t, e) {
        (this.width === t && this.height === e) ||
          ((this.width = t), (this.height = e), this.dispose()),
          this.viewport.set(0, 0, t, e),
          this.scissor.set(0, 0, t, e);
      },
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        return (
          (this.width = t.width),
          (this.height = t.height),
          this.viewport.copy(t.viewport),
          (this.texture = t.texture.clone()),
          (this.depthBuffer = t.depthBuffer),
          (this.stencilBuffer = t.stencilBuffer),
          (this.depthTexture = t.depthTexture),
          this
        );
      },
      dispose: function() {
        this.dispatchEvent({ type: 'dispose' });
      }
    }),
    (o.prototype = Object.create(a.prototype)),
    (o.prototype.constructor = o),
    (o.prototype.isWebGLRenderTargetCube = !0),
    (s.prototype = {
      constructor: s,
      get x() {
        return this._x;
      },
      set x(t) {
        (this._x = t), this.onChangeCallback();
      },
      get y() {
        return this._y;
      },
      set y(t) {
        (this._y = t), this.onChangeCallback();
      },
      get z() {
        return this._z;
      },
      set z(t) {
        (this._z = t), this.onChangeCallback();
      },
      get w() {
        return this._w;
      },
      set w(t) {
        (this._w = t), this.onChangeCallback();
      },
      set: function(t, e, i, n) {
        return (
          (this._x = t),
          (this._y = e),
          (this._z = i),
          (this._w = n),
          this.onChangeCallback(),
          this
        );
      },
      clone: function() {
        return new this.constructor(this._x, this._y, this._z, this._w);
      },
      copy: function(t) {
        return (
          (this._x = t.x),
          (this._y = t.y),
          (this._z = t.z),
          (this._w = t.w),
          this.onChangeCallback(),
          this
        );
      },
      setFromEuler: function(t, e) {
        if (!1 === (t && t.isEuler))
          throw Error(
            'THREE.Quaternion: .setFromEuler() now expects a Euler rotation rather than a Vector3 and order.'
          );
        var i = Math.cos(t._x / 2),
          n = Math.cos(t._y / 2),
          r = Math.cos(t._z / 2),
          a = Math.sin(t._x / 2),
          o = Math.sin(t._y / 2),
          s = Math.sin(t._z / 2),
          c = t.order;
        return (
          'XYZ' === c
            ? ((this._x = a * n * r + i * o * s),
              (this._y = i * o * r - a * n * s),
              (this._z = i * n * s + a * o * r),
              (this._w = i * n * r - a * o * s))
            : 'YXZ' === c
              ? ((this._x = a * n * r + i * o * s),
                (this._y = i * o * r - a * n * s),
                (this._z = i * n * s - a * o * r),
                (this._w = i * n * r + a * o * s))
              : 'ZXY' === c
                ? ((this._x = a * n * r - i * o * s),
                  (this._y = i * o * r + a * n * s),
                  (this._z = i * n * s + a * o * r),
                  (this._w = i * n * r - a * o * s))
                : 'ZYX' === c
                  ? ((this._x = a * n * r - i * o * s),
                    (this._y = i * o * r + a * n * s),
                    (this._z = i * n * s - a * o * r),
                    (this._w = i * n * r + a * o * s))
                  : 'YZX' === c
                    ? ((this._x = a * n * r + i * o * s),
                      (this._y = i * o * r + a * n * s),
                      (this._z = i * n * s - a * o * r),
                      (this._w = i * n * r - a * o * s))
                    : 'XZY' === c &&
                      ((this._x = a * n * r - i * o * s),
                      (this._y = i * o * r - a * n * s),
                      (this._z = i * n * s + a * o * r),
                      (this._w = i * n * r + a * o * s)),
          !1 !== e && this.onChangeCallback(),
          this
        );
      },
      setFromAxisAngle: function(t, e) {
        var i = e / 2,
          n = Math.sin(i);
        return (
          (this._x = t.x * n),
          (this._y = t.y * n),
          (this._z = t.z * n),
          (this._w = Math.cos(i)),
          this.onChangeCallback(),
          this
        );
      },
      setFromRotationMatrix: function(t) {
        var e = t.elements,
          i = e[0];
        t = e[4];
        var n = e[8],
          r = e[1],
          a = e[5],
          o = e[9],
          s = e[2],
          c = e[6],
          e = e[10],
          h = i + a + e;
        return (
          0 < h
            ? ((i = 0.5 / Math.sqrt(h + 1)),
              (this._w = 0.25 / i),
              (this._x = (c - o) * i),
              (this._y = (n - s) * i),
              (this._z = (r - t) * i))
            : i > a && i > e
              ? ((i = 2 * Math.sqrt(1 + i - a - e)),
                (this._w = (c - o) / i),
                (this._x = 0.25 * i),
                (this._y = (t + r) / i),
                (this._z = (n + s) / i))
              : a > e
                ? ((i = 2 * Math.sqrt(1 + a - i - e)),
                  (this._w = (n - s) / i),
                  (this._x = (t + r) / i),
                  (this._y = 0.25 * i),
                  (this._z = (o + c) / i))
                : ((i = 2 * Math.sqrt(1 + e - i - a)),
                  (this._w = (r - t) / i),
                  (this._x = (n + s) / i),
                  (this._y = (o + c) / i),
                  (this._z = 0.25 * i)),
          this.onChangeCallback(),
          this
        );
      },
      setFromUnitVectors: (function() {
        var t, e;
        return function(i, n) {
          return (
            void 0 === t && (t = new c()),
            (e = i.dot(n) + 1),
            1e-6 > e
              ? ((e = 0),
                Math.abs(i.x) > Math.abs(i.z)
                  ? t.set(-i.y, i.x, 0)
                  : t.set(0, -i.z, i.y))
              : t.crossVectors(i, n),
            (this._x = t.x),
            (this._y = t.y),
            (this._z = t.z),
            (this._w = e),
            this.normalize()
          );
        };
      })(),
      inverse: function() {
        return this.conjugate().normalize();
      },
      conjugate: function() {
        return (
          (this._x *= -1),
          (this._y *= -1),
          (this._z *= -1),
          this.onChangeCallback(),
          this
        );
      },
      dot: function(t) {
        return (
          this._x * t._x + this._y * t._y + this._z * t._z + this._w * t._w
        );
      },
      lengthSq: function() {
        return (
          this._x * this._x +
          this._y * this._y +
          this._z * this._z +
          this._w * this._w
        );
      },
      length: function() {
        return Math.sqrt(
          this._x * this._x +
            this._y * this._y +
            this._z * this._z +
            this._w * this._w
        );
      },
      normalize: function() {
        var t = this.length();
        return (
          0 === t
            ? ((this._z = this._y = this._x = 0), (this._w = 1))
            : ((t = 1 / t),
              (this._x *= t),
              (this._y *= t),
              (this._z *= t),
              (this._w *= t)),
          this.onChangeCallback(),
          this
        );
      },
      multiply: function(t, e) {
        return void 0 !== e
          ? (console.warn(
              'THREE.Quaternion: .multiply() now only accepts one argument. Use .multiplyQuaternions( a, b ) instead.'
            ),
            this.multiplyQuaternions(t, e))
          : this.multiplyQuaternions(this, t);
      },
      premultiply: function(t) {
        return this.multiplyQuaternions(t, this);
      },
      multiplyQuaternions: function(t, e) {
        var i = t._x,
          n = t._y,
          r = t._z,
          a = t._w,
          o = e._x,
          s = e._y,
          c = e._z,
          h = e._w;
        return (
          (this._x = i * h + a * o + n * c - r * s),
          (this._y = n * h + a * s + r * o - i * c),
          (this._z = r * h + a * c + i * s - n * o),
          (this._w = a * h - i * o - n * s - r * c),
          this.onChangeCallback(),
          this
        );
      },
      slerp: function(t, e) {
        if (0 === e) return this;
        if (1 === e) return this.copy(t);
        var i = this._x,
          n = this._y,
          r = this._z,
          a = this._w,
          o = a * t._w + i * t._x + n * t._y + r * t._z;
        if (
          (0 > o
            ? ((this._w = -t._w),
              (this._x = -t._x),
              (this._y = -t._y),
              (this._z = -t._z),
              (o = -o))
            : this.copy(t),
          1 <= o)
        )
          return (
            (this._w = a), (this._x = i), (this._y = n), (this._z = r), this
          );
        var s = Math.sqrt(1 - o * o);
        if (0.001 > Math.abs(s))
          return (
            (this._w = 0.5 * (a + this._w)),
            (this._x = 0.5 * (i + this._x)),
            (this._y = 0.5 * (n + this._y)),
            (this._z = 0.5 * (r + this._z)),
            this
          );
        var c = Math.atan2(s, o),
          o = Math.sin((1 - e) * c) / s,
          s = Math.sin(e * c) / s;
        return (
          (this._w = a * o + this._w * s),
          (this._x = i * o + this._x * s),
          (this._y = n * o + this._y * s),
          (this._z = r * o + this._z * s),
          this.onChangeCallback(),
          this
        );
      },
      equals: function(t) {
        return (
          t._x === this._x &&
          t._y === this._y &&
          t._z === this._z &&
          t._w === this._w
        );
      },
      fromArray: function(t, e) {
        return (
          void 0 === e && (e = 0),
          (this._x = t[e]),
          (this._y = t[e + 1]),
          (this._z = t[e + 2]),
          (this._w = t[e + 3]),
          this.onChangeCallback(),
          this
        );
      },
      toArray: function(t, e) {
        return (
          void 0 === t && (t = []),
          void 0 === e && (e = 0),
          (t[e] = this._x),
          (t[e + 1] = this._y),
          (t[e + 2] = this._z),
          (t[e + 3] = this._w),
          t
        );
      },
      onChange: function(t) {
        return (this.onChangeCallback = t), this;
      },
      onChangeCallback: function() {}
    }),
    Object.assign(s, {
      slerp: function(t, e, i, n) {
        return i.copy(t).slerp(e, n);
      },
      slerpFlat: function(t, e, i, n, r, a, o) {
        var s = i[n + 0],
          c = i[n + 1],
          h = i[n + 2];
        (i = i[n + 3]), (n = r[a + 0]);
        var l = r[a + 1],
          u = r[a + 2];
        if (((r = r[a + 3]), i !== r || s !== n || c !== l || h !== u)) {
          a = 1 - o;
          var d = s * n + c * l + h * u + i * r,
            p = 0 <= d ? 1 : -1,
            f = 1 - d * d;
          f > Number.EPSILON &&
            ((f = Math.sqrt(f)),
            (d = Math.atan2(f, d * p)),
            (a = Math.sin(a * d) / f),
            (o = Math.sin(o * d) / f)),
            (p *= o),
            (s = s * a + n * p),
            (c = c * a + l * p),
            (h = h * a + u * p),
            (i = i * a + r * p),
            a === 1 - o &&
              ((o = 1 / Math.sqrt(s * s + c * c + h * h + i * i)),
              (s *= o),
              (c *= o),
              (h *= o),
              (i *= o));
        }
        (t[e] = s), (t[e + 1] = c), (t[e + 2] = h), (t[e + 3] = i);
      }
    }),
    (c.prototype = {
      constructor: c,
      isVector3: !0,
      set: function(t, e, i) {
        return (this.x = t), (this.y = e), (this.z = i), this;
      },
      setScalar: function(t) {
        return (this.z = this.y = this.x = t), this;
      },
      setX: function(t) {
        return (this.x = t), this;
      },
      setY: function(t) {
        return (this.y = t), this;
      },
      setZ: function(t) {
        return (this.z = t), this;
      },
      setComponent: function(t, e) {
        switch (t) {
          case 0:
            this.x = e;
            break;
          case 1:
            this.y = e;
            break;
          case 2:
            this.z = e;
            break;
          default:
            throw Error('index is out of range: ' + t);
        }
      },
      getComponent: function(t) {
        switch (t) {
          case 0:
            return this.x;
          case 1:
            return this.y;
          case 2:
            return this.z;
          default:
            throw Error('index is out of range: ' + t);
        }
      },
      clone: function() {
        return new this.constructor(this.x, this.y, this.z);
      },
      copy: function(t) {
        return (this.x = t.x), (this.y = t.y), (this.z = t.z), this;
      },
      add: function(t, e) {
        return void 0 !== e
          ? (console.warn(
              'THREE.Vector3: .add() now only accepts one argument. Use .addVectors( a, b ) instead.'
            ),
            this.addVectors(t, e))
          : ((this.x += t.x), (this.y += t.y), (this.z += t.z), this);
      },
      addScalar: function(t) {
        return (this.x += t), (this.y += t), (this.z += t), this;
      },
      addVectors: function(t, e) {
        return (
          (this.x = t.x + e.x), (this.y = t.y + e.y), (this.z = t.z + e.z), this
        );
      },
      addScaledVector: function(t, e) {
        return (
          (this.x += t.x * e), (this.y += t.y * e), (this.z += t.z * e), this
        );
      },
      sub: function(t, e) {
        return void 0 !== e
          ? (console.warn(
              'THREE.Vector3: .sub() now only accepts one argument. Use .subVectors( a, b ) instead.'
            ),
            this.subVectors(t, e))
          : ((this.x -= t.x), (this.y -= t.y), (this.z -= t.z), this);
      },
      subScalar: function(t) {
        return (this.x -= t), (this.y -= t), (this.z -= t), this;
      },
      subVectors: function(t, e) {
        return (
          (this.x = t.x - e.x), (this.y = t.y - e.y), (this.z = t.z - e.z), this
        );
      },
      multiply: function(t, e) {
        return void 0 !== e
          ? (console.warn(
              'THREE.Vector3: .multiply() now only accepts one argument. Use .multiplyVectors( a, b ) instead.'
            ),
            this.multiplyVectors(t, e))
          : ((this.x *= t.x), (this.y *= t.y), (this.z *= t.z), this);
      },
      multiplyScalar: function(t) {
        return (
          isFinite(t)
            ? ((this.x *= t), (this.y *= t), (this.z *= t))
            : (this.z = this.y = this.x = 0),
          this
        );
      },
      multiplyVectors: function(t, e) {
        return (
          (this.x = t.x * e.x), (this.y = t.y * e.y), (this.z = t.z * e.z), this
        );
      },
      applyEuler: (function() {
        var t;
        return function(e) {
          return (
            !1 === (e && e.isEuler) &&
              console.error(
                'THREE.Vector3: .applyEuler() now expects an Euler rotation rather than a Vector3 and order.'
              ),
            void 0 === t && (t = new s()),
            this.applyQuaternion(t.setFromEuler(e))
          );
        };
      })(),
      applyAxisAngle: (function() {
        var t;
        return function(e, i) {
          return (
            void 0 === t && (t = new s()),
            this.applyQuaternion(t.setFromAxisAngle(e, i))
          );
        };
      })(),
      applyMatrix3: function(t) {
        var e = this.x,
          i = this.y,
          n = this.z;
        return (
          (t = t.elements),
          (this.x = t[0] * e + t[3] * i + t[6] * n),
          (this.y = t[1] * e + t[4] * i + t[7] * n),
          (this.z = t[2] * e + t[5] * i + t[8] * n),
          this
        );
      },
      applyMatrix4: function(t) {
        var e = this.x,
          i = this.y,
          n = this.z;
        return (
          (t = t.elements),
          (this.x = t[0] * e + t[4] * i + t[8] * n + t[12]),
          (this.y = t[1] * e + t[5] * i + t[9] * n + t[13]),
          (this.z = t[2] * e + t[6] * i + t[10] * n + t[14]),
          this
        );
      },
      applyProjection: function(t) {
        var e = this.x,
          i = this.y,
          n = this.z;
        t = t.elements;
        var r = 1 / (t[3] * e + t[7] * i + t[11] * n + t[15]);
        return (
          (this.x = (t[0] * e + t[4] * i + t[8] * n + t[12]) * r),
          (this.y = (t[1] * e + t[5] * i + t[9] * n + t[13]) * r),
          (this.z = (t[2] * e + t[6] * i + t[10] * n + t[14]) * r),
          this
        );
      },
      applyQuaternion: function(t) {
        var e = this.x,
          i = this.y,
          n = this.z,
          r = t.x,
          a = t.y,
          o = t.z;
        t = t.w;
        var s = t * e + a * n - o * i,
          c = t * i + o * e - r * n,
          h = t * n + r * i - a * e,
          e = -r * e - a * i - o * n;
        return (
          (this.x = s * t + e * -r + c * -o - h * -a),
          (this.y = c * t + e * -a + h * -r - s * -o),
          (this.z = h * t + e * -o + s * -a - c * -r),
          this
        );
      },
      project: (function() {
        var t;
        return function(e) {
          return (
            void 0 === t && (t = new h()),
            t.multiplyMatrices(e.projectionMatrix, t.getInverse(e.matrixWorld)),
            this.applyProjection(t)
          );
        };
      })(),
      unproject: (function() {
        var t;
        return function(e) {
          return (
            void 0 === t && (t = new h()),
            t.multiplyMatrices(e.matrixWorld, t.getInverse(e.projectionMatrix)),
            this.applyProjection(t)
          );
        };
      })(),
      transformDirection: function(t) {
        var e = this.x,
          i = this.y,
          n = this.z;
        return (
          (t = t.elements),
          (this.x = t[0] * e + t[4] * i + t[8] * n),
          (this.y = t[1] * e + t[5] * i + t[9] * n),
          (this.z = t[2] * e + t[6] * i + t[10] * n),
          this.normalize()
        );
      },
      divide: function(t) {
        return (this.x /= t.x), (this.y /= t.y), (this.z /= t.z), this;
      },
      divideScalar: function(t) {
        return this.multiplyScalar(1 / t);
      },
      min: function(t) {
        return (
          (this.x = Math.min(this.x, t.x)),
          (this.y = Math.min(this.y, t.y)),
          (this.z = Math.min(this.z, t.z)),
          this
        );
      },
      max: function(t) {
        return (
          (this.x = Math.max(this.x, t.x)),
          (this.y = Math.max(this.y, t.y)),
          (this.z = Math.max(this.z, t.z)),
          this
        );
      },
      clamp: function(t, e) {
        return (
          (this.x = Math.max(t.x, Math.min(e.x, this.x))),
          (this.y = Math.max(t.y, Math.min(e.y, this.y))),
          (this.z = Math.max(t.z, Math.min(e.z, this.z))),
          this
        );
      },
      clampScalar: (function() {
        var t, e;
        return function(i, n) {
          return (
            void 0 === t && ((t = new c()), (e = new c())),
            t.set(i, i, i),
            e.set(n, n, n),
            this.clamp(t, e)
          );
        };
      })(),
      clampLength: function(t, e) {
        var i = this.length();
        return this.multiplyScalar(Math.max(t, Math.min(e, i)) / i);
      },
      floor: function() {
        return (
          (this.x = Math.floor(this.x)),
          (this.y = Math.floor(this.y)),
          (this.z = Math.floor(this.z)),
          this
        );
      },
      ceil: function() {
        return (
          (this.x = Math.ceil(this.x)),
          (this.y = Math.ceil(this.y)),
          (this.z = Math.ceil(this.z)),
          this
        );
      },
      round: function() {
        return (
          (this.x = Math.round(this.x)),
          (this.y = Math.round(this.y)),
          (this.z = Math.round(this.z)),
          this
        );
      },
      roundToZero: function() {
        return (
          (this.x = 0 > this.x ? Math.ceil(this.x) : Math.floor(this.x)),
          (this.y = 0 > this.y ? Math.ceil(this.y) : Math.floor(this.y)),
          (this.z = 0 > this.z ? Math.ceil(this.z) : Math.floor(this.z)),
          this
        );
      },
      negate: function() {
        return (this.x = -this.x), (this.y = -this.y), (this.z = -this.z), this;
      },
      dot: function(t) {
        return this.x * t.x + this.y * t.y + this.z * t.z;
      },
      lengthSq: function() {
        return this.x * this.x + this.y * this.y + this.z * this.z;
      },
      length: function() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
      },
      lengthManhattan: function() {
        return Math.abs(this.x) + Math.abs(this.y) + Math.abs(this.z);
      },
      normalize: function() {
        return this.divideScalar(this.length());
      },
      setLength: function(t) {
        return this.multiplyScalar(t / this.length());
      },
      lerp: function(t, e) {
        return (
          (this.x += (t.x - this.x) * e),
          (this.y += (t.y - this.y) * e),
          (this.z += (t.z - this.z) * e),
          this
        );
      },
      lerpVectors: function(t, e, i) {
        return this.subVectors(e, t)
          .multiplyScalar(i)
          .add(t);
      },
      cross: function(t, e) {
        if (void 0 !== e)
          return (
            console.warn(
              'THREE.Vector3: .cross() now only accepts one argument. Use .crossVectors( a, b ) instead.'
            ),
            this.crossVectors(t, e)
          );
        var i = this.x,
          n = this.y,
          r = this.z;
        return (
          (this.x = n * t.z - r * t.y),
          (this.y = r * t.x - i * t.z),
          (this.z = i * t.y - n * t.x),
          this
        );
      },
      crossVectors: function(t, e) {
        var i = t.x,
          n = t.y,
          r = t.z,
          a = e.x,
          o = e.y,
          s = e.z;
        return (
          (this.x = n * s - r * o),
          (this.y = r * a - i * s),
          (this.z = i * o - n * a),
          this
        );
      },
      projectOnVector: function(t) {
        var e = t.dot(this) / t.lengthSq();
        return this.copy(t).multiplyScalar(e);
      },
      projectOnPlane: (function() {
        var t;
        return function(e) {
          return (
            void 0 === t && (t = new c()),
            t.copy(this).projectOnVector(e),
            this.sub(t)
          );
        };
      })(),
      reflect: (function() {
        var t;
        return function(e) {
          return (
            void 0 === t && (t = new c()),
            this.sub(t.copy(e).multiplyScalar(2 * this.dot(e)))
          );
        };
      })(),
      angleTo: function(e) {
        return (
          (e = this.dot(e) / Math.sqrt(this.lengthSq() * e.lengthSq())),
          Math.acos(t.Math.clamp(e, -1, 1))
        );
      },
      distanceTo: function(t) {
        return Math.sqrt(this.distanceToSquared(t));
      },
      distanceToSquared: function(t) {
        var e = this.x - t.x,
          i = this.y - t.y;
        return (t = this.z - t.z), e * e + i * i + t * t;
      },
      distanceToManhattan: function(t) {
        return (
          Math.abs(this.x - t.x) +
          Math.abs(this.y - t.y) +
          Math.abs(this.z - t.z)
        );
      },
      setFromSpherical: function(t) {
        var e = Math.sin(t.phi) * t.radius;
        return (
          (this.x = e * Math.sin(t.theta)),
          (this.y = Math.cos(t.phi) * t.radius),
          (this.z = e * Math.cos(t.theta)),
          this
        );
      },
      setFromMatrixPosition: function(t) {
        return this.setFromMatrixColumn(t, 3);
      },
      setFromMatrixScale: function(t) {
        var e = this.setFromMatrixColumn(t, 0).length(),
          i = this.setFromMatrixColumn(t, 1).length();
        return (
          (t = this.setFromMatrixColumn(t, 2).length()),
          (this.x = e),
          (this.y = i),
          (this.z = t),
          this
        );
      },
      setFromMatrixColumn: function(t, e) {
        if ('number' == typeof t) {
          console.warn(
            'THREE.Vector3: setFromMatrixColumn now expects ( matrix, index ).'
          );
          var i = t;
          (t = e), (e = i);
        }
        return this.fromArray(t.elements, 4 * e);
      },
      equals: function(t) {
        return t.x === this.x && t.y === this.y && t.z === this.z;
      },
      fromArray: function(t, e) {
        return (
          void 0 === e && (e = 0),
          (this.x = t[e]),
          (this.y = t[e + 1]),
          (this.z = t[e + 2]),
          this
        );
      },
      toArray: function(t, e) {
        return (
          void 0 === t && (t = []),
          void 0 === e && (e = 0),
          (t[e] = this.x),
          (t[e + 1] = this.y),
          (t[e + 2] = this.z),
          t
        );
      },
      fromAttribute: function(t, e, i) {
        return (
          void 0 === i && (i = 0),
          (e = e * t.itemSize + i),
          (this.x = t.array[e]),
          (this.y = t.array[e + 1]),
          (this.z = t.array[e + 2]),
          this
        );
      }
    }),
    (h.prototype = {
      constructor: h,
      isMatrix4: !0,
      set: function(t, e, i, n, r, a, o, s, c, h, l, u, d, p, f, m) {
        var g = this.elements;
        return (
          (g[0] = t),
          (g[4] = e),
          (g[8] = i),
          (g[12] = n),
          (g[1] = r),
          (g[5] = a),
          (g[9] = o),
          (g[13] = s),
          (g[2] = c),
          (g[6] = h),
          (g[10] = l),
          (g[14] = u),
          (g[3] = d),
          (g[7] = p),
          (g[11] = f),
          (g[15] = m),
          this
        );
      },
      identity: function() {
        return this.set(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1), this;
      },
      clone: function() {
        return new h().fromArray(this.elements);
      },
      copy: function(t) {
        return this.elements.set(t.elements), this;
      },
      copyPosition: function(t) {
        var e = this.elements;
        return (
          (t = t.elements),
          (e[12] = t[12]),
          (e[13] = t[13]),
          (e[14] = t[14]),
          this
        );
      },
      extractBasis: function(t, e, i) {
        return (
          t.setFromMatrixColumn(this, 0),
          e.setFromMatrixColumn(this, 1),
          i.setFromMatrixColumn(this, 2),
          this
        );
      },
      makeBasis: function(t, e, i) {
        return (
          this.set(
            t.x,
            e.x,
            i.x,
            0,
            t.y,
            e.y,
            i.y,
            0,
            t.z,
            e.z,
            i.z,
            0,
            0,
            0,
            0,
            1
          ),
          this
        );
      },
      extractRotation: (function() {
        var t;
        return function(e) {
          void 0 === t && (t = new c());
          var i = this.elements,
            n = e.elements,
            r = 1 / t.setFromMatrixColumn(e, 0).length(),
            a = 1 / t.setFromMatrixColumn(e, 1).length();
          return (
            (e = 1 / t.setFromMatrixColumn(e, 2).length()),
            (i[0] = n[0] * r),
            (i[1] = n[1] * r),
            (i[2] = n[2] * r),
            (i[4] = n[4] * a),
            (i[5] = n[5] * a),
            (i[6] = n[6] * a),
            (i[8] = n[8] * e),
            (i[9] = n[9] * e),
            (i[10] = n[10] * e),
            this
          );
        };
      })(),
      makeRotationFromEuler: function(t) {
        !1 === (t && t.isEuler) &&
          console.error(
            'THREE.Matrix: .makeRotationFromEuler() now expects a Euler rotation rather than a Vector3 and order.'
          );
        var e = this.elements,
          i = t.x,
          n = t.y,
          r = t.z,
          a = Math.cos(i),
          i = Math.sin(i),
          o = Math.cos(n),
          n = Math.sin(n),
          s = Math.cos(r),
          r = Math.sin(r);
        if ('XYZ' === t.order) {
          t = a * s;
          var c = a * r,
            h = i * s,
            l = i * r;
          (e[0] = o * s),
            (e[4] = -o * r),
            (e[8] = n),
            (e[1] = c + h * n),
            (e[5] = t - l * n),
            (e[9] = -i * o),
            (e[2] = l - t * n),
            (e[6] = h + c * n),
            (e[10] = a * o);
        } else
          'YXZ' === t.order
            ? ((t = o * s),
              (c = o * r),
              (h = n * s),
              (l = n * r),
              (e[0] = t + l * i),
              (e[4] = h * i - c),
              (e[8] = a * n),
              (e[1] = a * r),
              (e[5] = a * s),
              (e[9] = -i),
              (e[2] = c * i - h),
              (e[6] = l + t * i),
              (e[10] = a * o))
            : 'ZXY' === t.order
              ? ((t = o * s),
                (c = o * r),
                (h = n * s),
                (l = n * r),
                (e[0] = t - l * i),
                (e[4] = -a * r),
                (e[8] = h + c * i),
                (e[1] = c + h * i),
                (e[5] = a * s),
                (e[9] = l - t * i),
                (e[2] = -a * n),
                (e[6] = i),
                (e[10] = a * o))
              : 'ZYX' === t.order
                ? ((t = a * s),
                  (c = a * r),
                  (h = i * s),
                  (l = i * r),
                  (e[0] = o * s),
                  (e[4] = h * n - c),
                  (e[8] = t * n + l),
                  (e[1] = o * r),
                  (e[5] = l * n + t),
                  (e[9] = c * n - h),
                  (e[2] = -n),
                  (e[6] = i * o),
                  (e[10] = a * o))
                : 'YZX' === t.order
                  ? ((t = a * o),
                    (c = a * n),
                    (h = i * o),
                    (l = i * n),
                    (e[0] = o * s),
                    (e[4] = l - t * r),
                    (e[8] = h * r + c),
                    (e[1] = r),
                    (e[5] = a * s),
                    (e[9] = -i * s),
                    (e[2] = -n * s),
                    (e[6] = c * r + h),
                    (e[10] = t - l * r))
                  : 'XZY' === t.order &&
                    ((t = a * o),
                    (c = a * n),
                    (h = i * o),
                    (l = i * n),
                    (e[0] = o * s),
                    (e[4] = -r),
                    (e[8] = n * s),
                    (e[1] = t * r + l),
                    (e[5] = a * s),
                    (e[9] = c * r - h),
                    (e[2] = h * r - c),
                    (e[6] = i * s),
                    (e[10] = l * r + t));
        return (
          (e[3] = 0),
          (e[7] = 0),
          (e[11] = 0),
          (e[12] = 0),
          (e[13] = 0),
          (e[14] = 0),
          (e[15] = 1),
          this
        );
      },
      makeRotationFromQuaternion: function(t) {
        var e = this.elements,
          i = t.x,
          n = t.y,
          r = t.z,
          a = t.w,
          o = i + i,
          s = n + n,
          c = r + r;
        t = i * o;
        var h = i * s,
          i = i * c,
          l = n * s,
          n = n * c,
          r = r * c,
          o = a * o,
          s = a * s,
          a = a * c;
        return (
          (e[0] = 1 - (l + r)),
          (e[4] = h - a),
          (e[8] = i + s),
          (e[1] = h + a),
          (e[5] = 1 - (t + r)),
          (e[9] = n - o),
          (e[2] = i - s),
          (e[6] = n + o),
          (e[10] = 1 - (t + l)),
          (e[3] = 0),
          (e[7] = 0),
          (e[11] = 0),
          (e[12] = 0),
          (e[13] = 0),
          (e[14] = 0),
          (e[15] = 1),
          this
        );
      },
      lookAt: (function() {
        var t, e, i;
        return function(n, r, a) {
          void 0 === t && ((t = new c()), (e = new c()), (i = new c()));
          var o = this.elements;
          return (
            i.subVectors(n, r).normalize(),
            0 === i.lengthSq() && (i.z = 1),
            t.crossVectors(a, i).normalize(),
            0 === t.lengthSq() &&
              ((i.z += 1e-4), t.crossVectors(a, i).normalize()),
            e.crossVectors(i, t),
            (o[0] = t.x),
            (o[4] = e.x),
            (o[8] = i.x),
            (o[1] = t.y),
            (o[5] = e.y),
            (o[9] = i.y),
            (o[2] = t.z),
            (o[6] = e.z),
            (o[10] = i.z),
            this
          );
        };
      })(),
      multiply: function(t, e) {
        return void 0 !== e
          ? (console.warn(
              'THREE.Matrix4: .multiply() now only accepts one argument. Use .multiplyMatrices( a, b ) instead.'
            ),
            this.multiplyMatrices(t, e))
          : this.multiplyMatrices(this, t);
      },
      premultiply: function(t) {
        return this.multiplyMatrices(t, this);
      },
      multiplyMatrices: function(t, e) {
        var i = t.elements,
          n = e.elements,
          r = this.elements,
          a = i[0],
          o = i[4],
          s = i[8],
          c = i[12],
          h = i[1],
          l = i[5],
          u = i[9],
          d = i[13],
          p = i[2],
          f = i[6],
          m = i[10],
          g = i[14],
          v = i[3],
          y = i[7],
          x = i[11],
          i = i[15],
          _ = n[0],
          b = n[4],
          w = n[8],
          M = n[12],
          E = n[1],
          S = n[5],
          T = n[9],
          A = n[13],
          L = n[2],
          R = n[6],
          P = n[10],
          C = n[14],
          I = n[3],
          U = n[7],
          D = n[11],
          n = n[15];
        return (
          (r[0] = a * _ + o * E + s * L + c * I),
          (r[4] = a * b + o * S + s * R + c * U),
          (r[8] = a * w + o * T + s * P + c * D),
          (r[12] = a * M + o * A + s * C + c * n),
          (r[1] = h * _ + l * E + u * L + d * I),
          (r[5] = h * b + l * S + u * R + d * U),
          (r[9] = h * w + l * T + u * P + d * D),
          (r[13] = h * M + l * A + u * C + d * n),
          (r[2] = p * _ + f * E + m * L + g * I),
          (r[6] = p * b + f * S + m * R + g * U),
          (r[10] = p * w + f * T + m * P + g * D),
          (r[14] = p * M + f * A + m * C + g * n),
          (r[3] = v * _ + y * E + x * L + i * I),
          (r[7] = v * b + y * S + x * R + i * U),
          (r[11] = v * w + y * T + x * P + i * D),
          (r[15] = v * M + y * A + x * C + i * n),
          this
        );
      },
      multiplyToArray: function(t, e, i) {
        var n = this.elements;
        return (
          this.multiplyMatrices(t, e),
          (i[0] = n[0]),
          (i[1] = n[1]),
          (i[2] = n[2]),
          (i[3] = n[3]),
          (i[4] = n[4]),
          (i[5] = n[5]),
          (i[6] = n[6]),
          (i[7] = n[7]),
          (i[8] = n[8]),
          (i[9] = n[9]),
          (i[10] = n[10]),
          (i[11] = n[11]),
          (i[12] = n[12]),
          (i[13] = n[13]),
          (i[14] = n[14]),
          (i[15] = n[15]),
          this
        );
      },
      multiplyScalar: function(t) {
        var e = this.elements;
        return (
          (e[0] *= t),
          (e[4] *= t),
          (e[8] *= t),
          (e[12] *= t),
          (e[1] *= t),
          (e[5] *= t),
          (e[9] *= t),
          (e[13] *= t),
          (e[2] *= t),
          (e[6] *= t),
          (e[10] *= t),
          (e[14] *= t),
          (e[3] *= t),
          (e[7] *= t),
          (e[11] *= t),
          (e[15] *= t),
          this
        );
      },
      applyToVector3Array: (function() {
        var t;
        return function(e, i, n) {
          void 0 === t && (t = new c()),
            void 0 === i && (i = 0),
            void 0 === n && (n = e.length);
          for (var r = 0; r < n; r += 3, i += 3)
            t.fromArray(e, i), t.applyMatrix4(this), t.toArray(e, i);
          return e;
        };
      })(),
      applyToBuffer: (function() {
        var t;
        return function(e, i, n) {
          void 0 === t && (t = new c()),
            void 0 === i && (i = 0),
            void 0 === n && (n = e.length / e.itemSize);
          for (var r = 0; r < n; r++, i++)
            (t.x = e.getX(i)),
              (t.y = e.getY(i)),
              (t.z = e.getZ(i)),
              t.applyMatrix4(this),
              e.setXYZ(t.x, t.y, t.z);
          return e;
        };
      })(),
      determinant: function() {
        var t = this.elements,
          e = t[0],
          i = t[4],
          n = t[8],
          r = t[12],
          a = t[1],
          o = t[5],
          s = t[9],
          c = t[13],
          h = t[2],
          l = t[6],
          u = t[10],
          d = t[14];
        return (
          t[3] *
            (+r * s * l -
              n * c * l -
              r * o * u +
              i * c * u +
              n * o * d -
              i * s * d) +
          t[7] *
            (+e * s * d -
              e * c * u +
              r * a * u -
              n * a * d +
              n * c * h -
              r * s * h) +
          t[11] *
            (+e * c * l -
              e * o * d -
              r * a * l +
              i * a * d +
              r * o * h -
              i * c * h) +
          t[15] *
            (-n * o * h -
              e * s * l +
              e * o * u +
              n * a * l -
              i * a * u +
              i * s * h)
        );
      },
      transpose: function() {
        var t,
          e = this.elements;
        return (
          (t = e[1]),
          (e[1] = e[4]),
          (e[4] = t),
          (t = e[2]),
          (e[2] = e[8]),
          (e[8] = t),
          (t = e[6]),
          (e[6] = e[9]),
          (e[9] = t),
          (t = e[3]),
          (e[3] = e[12]),
          (e[12] = t),
          (t = e[7]),
          (e[7] = e[13]),
          (e[13] = t),
          (t = e[11]),
          (e[11] = e[14]),
          (e[14] = t),
          this
        );
      },
      flattenToArrayOffset: function(t, e) {
        return (
          console.warn(
            'THREE.Matrix3: .flattenToArrayOffset is deprecated - just use .toArray instead.'
          ),
          this.toArray(t, e)
        );
      },
      getPosition: (function() {
        var t;
        return function() {
          return (
            void 0 === t && (t = new c()),
            console.warn(
              'THREE.Matrix4: .getPosition() has been removed. Use Vector3.setFromMatrixPosition( matrix ) instead.'
            ),
            t.setFromMatrixColumn(this, 3)
          );
        };
      })(),
      setPosition: function(t) {
        var e = this.elements;
        return (e[12] = t.x), (e[13] = t.y), (e[14] = t.z), this;
      },
      getInverse: function(t, e) {
        var i = this.elements,
          n = t.elements,
          r = n[0],
          a = n[1],
          o = n[2],
          s = n[3],
          c = n[4],
          h = n[5],
          l = n[6],
          u = n[7],
          d = n[8],
          p = n[9],
          f = n[10],
          m = n[11],
          g = n[12],
          v = n[13],
          y = n[14],
          n = n[15],
          x =
            p * y * u -
            v * f * u +
            v * l * m -
            h * y * m -
            p * l * n +
            h * f * n,
          _ =
            g * f * u -
            d * y * u -
            g * l * m +
            c * y * m +
            d * l * n -
            c * f * n,
          b =
            d * v * u -
            g * p * u +
            g * h * m -
            c * v * m -
            d * h * n +
            c * p * n,
          w =
            g * p * l -
            d * v * l -
            g * h * f +
            c * v * f +
            d * h * y -
            c * p * y,
          M = r * x + a * _ + o * b + s * w;
        if (0 === M) {
          if (!0 === e)
            throw Error(
              "THREE.Matrix4.getInverse(): can't invert matrix, determinant is 0"
            );
          return (
            console.warn(
              "THREE.Matrix4.getInverse(): can't invert matrix, determinant is 0"
            ),
            this.identity()
          );
        }
        return (
          (M = 1 / M),
          (i[0] = x * M),
          (i[1] =
            (v * f * s -
              p * y * s -
              v * o * m +
              a * y * m +
              p * o * n -
              a * f * n) *
            M),
          (i[2] =
            (h * y * s -
              v * l * s +
              v * o * u -
              a * y * u -
              h * o * n +
              a * l * n) *
            M),
          (i[3] =
            (p * l * s -
              h * f * s -
              p * o * u +
              a * f * u +
              h * o * m -
              a * l * m) *
            M),
          (i[4] = _ * M),
          (i[5] =
            (d * y * s -
              g * f * s +
              g * o * m -
              r * y * m -
              d * o * n +
              r * f * n) *
            M),
          (i[6] =
            (g * l * s -
              c * y * s -
              g * o * u +
              r * y * u +
              c * o * n -
              r * l * n) *
            M),
          (i[7] =
            (c * f * s -
              d * l * s +
              d * o * u -
              r * f * u -
              c * o * m +
              r * l * m) *
            M),
          (i[8] = b * M),
          (i[9] =
            (g * p * s -
              d * v * s -
              g * a * m +
              r * v * m +
              d * a * n -
              r * p * n) *
            M),
          (i[10] =
            (c * v * s -
              g * h * s +
              g * a * u -
              r * v * u -
              c * a * n +
              r * h * n) *
            M),
          (i[11] =
            (d * h * s -
              c * p * s -
              d * a * u +
              r * p * u +
              c * a * m -
              r * h * m) *
            M),
          (i[12] = w * M),
          (i[13] =
            (d * v * o -
              g * p * o +
              g * a * f -
              r * v * f -
              d * a * y +
              r * p * y) *
            M),
          (i[14] =
            (g * h * o -
              c * v * o -
              g * a * l +
              r * v * l +
              c * a * y -
              r * h * y) *
            M),
          (i[15] =
            (c * p * o -
              d * h * o +
              d * a * l -
              r * p * l -
              c * a * f +
              r * h * f) *
            M),
          this
        );
      },
      scale: function(t) {
        var e = this.elements,
          i = t.x,
          n = t.y;
        return (
          (t = t.z),
          (e[0] *= i),
          (e[4] *= n),
          (e[8] *= t),
          (e[1] *= i),
          (e[5] *= n),
          (e[9] *= t),
          (e[2] *= i),
          (e[6] *= n),
          (e[10] *= t),
          (e[3] *= i),
          (e[7] *= n),
          (e[11] *= t),
          this
        );
      },
      getMaxScaleOnAxis: function() {
        var t = this.elements;
        return Math.sqrt(
          Math.max(
            t[0] * t[0] + t[1] * t[1] + t[2] * t[2],
            t[4] * t[4] + t[5] * t[5] + t[6] * t[6],
            t[8] * t[8] + t[9] * t[9] + t[10] * t[10]
          )
        );
      },
      makeTranslation: function(t, e, i) {
        return this.set(1, 0, 0, t, 0, 1, 0, e, 0, 0, 1, i, 0, 0, 0, 1), this;
      },
      makeRotationX: function(t) {
        var e = Math.cos(t);
        return (
          (t = Math.sin(t)),
          this.set(1, 0, 0, 0, 0, e, -t, 0, 0, t, e, 0, 0, 0, 0, 1),
          this
        );
      },
      makeRotationY: function(t) {
        var e = Math.cos(t);
        return (
          (t = Math.sin(t)),
          this.set(e, 0, t, 0, 0, 1, 0, 0, -t, 0, e, 0, 0, 0, 0, 1),
          this
        );
      },
      makeRotationZ: function(t) {
        var e = Math.cos(t);
        return (
          (t = Math.sin(t)),
          this.set(e, -t, 0, 0, t, e, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
          this
        );
      },
      makeRotationAxis: function(t, e) {
        var i = Math.cos(e),
          n = Math.sin(e),
          r = 1 - i,
          a = t.x,
          o = t.y,
          s = t.z,
          c = r * a,
          h = r * o;
        return (
          this.set(
            c * a + i,
            c * o - n * s,
            c * s + n * o,
            0,
            c * o + n * s,
            h * o + i,
            h * s - n * a,
            0,
            c * s - n * o,
            h * s + n * a,
            r * s * s + i,
            0,
            0,
            0,
            0,
            1
          ),
          this
        );
      },
      makeScale: function(t, e, i) {
        return this.set(t, 0, 0, 0, 0, e, 0, 0, 0, 0, i, 0, 0, 0, 0, 1), this;
      },
      compose: function(t, e, i) {
        return (
          this.makeRotationFromQuaternion(e),
          this.scale(i),
          this.setPosition(t),
          this
        );
      },
      decompose: (function() {
        var t, e;
        return function(i, n, r) {
          void 0 === t && ((t = new c()), (e = new h()));
          var a = this.elements,
            o = t.set(a[0], a[1], a[2]).length(),
            s = t.set(a[4], a[5], a[6]).length(),
            l = t.set(a[8], a[9], a[10]).length();
          0 > this.determinant() && (o = -o),
            (i.x = a[12]),
            (i.y = a[13]),
            (i.z = a[14]),
            e.elements.set(this.elements),
            (i = 1 / o);
          var a = 1 / s,
            u = 1 / l;
          return (
            (e.elements[0] *= i),
            (e.elements[1] *= i),
            (e.elements[2] *= i),
            (e.elements[4] *= a),
            (e.elements[5] *= a),
            (e.elements[6] *= a),
            (e.elements[8] *= u),
            (e.elements[9] *= u),
            (e.elements[10] *= u),
            n.setFromRotationMatrix(e),
            (r.x = o),
            (r.y = s),
            (r.z = l),
            this
          );
        };
      })(),
      makeFrustum: function(t, e, i, n, r, a) {
        var o = this.elements;
        return (
          (o[0] = 2 * r / (e - t)),
          (o[4] = 0),
          (o[8] = (e + t) / (e - t)),
          (o[12] = 0),
          (o[1] = 0),
          (o[5] = 2 * r / (n - i)),
          (o[9] = (n + i) / (n - i)),
          (o[13] = 0),
          (o[2] = 0),
          (o[6] = 0),
          (o[10] = -(a + r) / (a - r)),
          (o[14] = -2 * a * r / (a - r)),
          (o[3] = 0),
          (o[7] = 0),
          (o[11] = -1),
          (o[15] = 0),
          this
        );
      },
      makePerspective: function(e, i, n, r) {
        e = n * Math.tan(t.Math.DEG2RAD * e * 0.5);
        var a = -e;
        return this.makeFrustum(a * i, e * i, a, e, n, r);
      },
      makeOrthographic: function(t, e, i, n, r, a) {
        var o = this.elements,
          s = 1 / (e - t),
          c = 1 / (i - n),
          h = 1 / (a - r);
        return (
          (o[0] = 2 * s),
          (o[4] = 0),
          (o[8] = 0),
          (o[12] = -(e + t) * s),
          (o[1] = 0),
          (o[5] = 2 * c),
          (o[9] = 0),
          (o[13] = -(i + n) * c),
          (o[2] = 0),
          (o[6] = 0),
          (o[10] = -2 * h),
          (o[14] = -(a + r) * h),
          (o[3] = 0),
          (o[7] = 0),
          (o[11] = 0),
          (o[15] = 1),
          this
        );
      },
      equals: function(t) {
        var e = this.elements;
        t = t.elements;
        for (var i = 0; 16 > i; i++) if (e[i] !== t[i]) return !1;
        return !0;
      },
      fromArray: function(t, e) {
        void 0 === e && (e = 0);
        for (var i = 0; 16 > i; i++) this.elements[i] = t[i + e];
        return this;
      },
      toArray: function(t, e) {
        void 0 === t && (t = []), void 0 === e && (e = 0);
        var i = this.elements;
        return (
          (t[e] = i[0]),
          (t[e + 1] = i[1]),
          (t[e + 2] = i[2]),
          (t[e + 3] = i[3]),
          (t[e + 4] = i[4]),
          (t[e + 5] = i[5]),
          (t[e + 6] = i[6]),
          (t[e + 7] = i[7]),
          (t[e + 8] = i[8]),
          (t[e + 9] = i[9]),
          (t[e + 10] = i[10]),
          (t[e + 11] = i[11]),
          (t[e + 12] = i[12]),
          (t[e + 13] = i[13]),
          (t[e + 14] = i[14]),
          (t[e + 15] = i[15]),
          t
        );
      }
    }),
    (l.prototype = Object.create(n.prototype)),
    (l.prototype.constructor = l),
    (l.prototype.isCubeTexture = !0),
    Object.defineProperty(l.prototype, 'images', {
      get: function() {
        return this.image;
      },
      set: function(t) {
        this.image = t;
      }
    });
  var Gn = new n(),
    Hn = new l(),
    Vn = [],
    kn = [];
  G.prototype.setValue = function(t, e) {
    for (var i = this.seq, n = 0, r = i.length; n !== r; ++n) {
      var a = i[n];
      a.setValue(t, e[a.id]);
    }
  };
  var jn = /([\w\d_]+)(\])?(\[|\.)?/g;
  (H.prototype.setValue = function(t, e, i) {
    void 0 !== (e = this.map[e]) && e.setValue(t, i, this.renderer);
  }),
    (H.prototype.set = function(t, e, i) {
      var n = this.map[i];
      void 0 !== n && n.setValue(t, e[i], this.renderer);
    }),
    (H.prototype.setOptional = function(t, e, i) {
      void 0 !== (e = e[i]) && this.setValue(t, i, e);
    }),
    (H.upload = function(t, e, i, n) {
      for (var r = 0, a = e.length; r !== a; ++r) {
        var o = e[r],
          s = i[o.id];
        !1 !== s.needsUpdate && o.setValue(t, s.value, n);
      }
    }),
    (H.seqWithValue = function(t, e) {
      for (var i = [], n = 0, r = t.length; n !== r; ++n) {
        var a = t[n];
        a.id in e && i.push(a);
      }
      return i;
    }),
    (H.splitDynamic = function(t, e) {
      for (var i = null, n = t.length, r = 0, a = 0; a !== n; ++a) {
        var o = t[a],
          s = e[o.id];
        s && !0 === s.dynamic
          ? (null === i && (i = []), i.push(o))
          : (r < a && (t[r] = o), ++r);
      }
      return r < n && (t.length = r), i;
    }),
    (H.evalDynamic = function(t, e, i, n, r) {
      for (var a = 0, o = t.length; a !== o; ++a) {
        var s = e[t[a].id],
          c = s.onUpdateCallback;
        void 0 !== c && c.call(s, i, n, r);
      }
    }),
    (t.UniformsUtils = {
      merge: function(t) {
        for (var e = {}, i = 0; i < t.length; i++) {
          var n,
            r = this.clone(t[i]);
          for (n in r) e[n] = r[n];
        }
        return e;
      },
      clone: function(t) {
        var e,
          i = {};
        for (e in t) {
          i[e] = {};
          for (var n in t[e]) {
            var r = t[e][n];
            (r && r.isColor) ||
            (r && r.isVector2) ||
            (r && r.isVector3) ||
            (r && r.isVector4) ||
            (r && r.isMatrix3) ||
            (r && r.isMatrix4) ||
            (r && r.isTexture)
              ? (i[e][n] = r.clone())
              : Array.isArray(r) ? (i[e][n] = r.slice()) : (i[e][n] = r);
          }
        }
        return i;
      }
    });
  var Wn = {
    alphamap_fragment:
      '#ifdef USE_ALPHAMAP\n\tdiffuseColor.a *= texture2D( alphaMap, vUv ).g;\n#endif\n',
    alphamap_pars_fragment:
      '#ifdef USE_ALPHAMAP\n\tuniform sampler2D alphaMap;\n#endif\n',
    alphatest_fragment:
      '#ifdef ALPHATEST\n\tif ( diffuseColor.a < ALPHATEST ) discard;\n#endif\n',
    aomap_fragment:
      '#ifdef USE_AOMAP\n\tfloat ambientOcclusion = ( texture2D( aoMap, vUv2 ).r - 1.0 ) * aoMapIntensity + 1.0;\n\treflectedLight.indirectDiffuse *= ambientOcclusion;\n\t#if defined( USE_ENVMAP ) && defined( PHYSICAL )\n\t\tfloat dotNV = saturate( dot( geometry.normal, geometry.viewDir ) );\n\t\treflectedLight.indirectSpecular *= computeSpecularOcclusion( dotNV, ambientOcclusion, material.specularRoughness );\n\t#endif\n#endif\n',
    aomap_pars_fragment:
      '#ifdef USE_AOMAP\n\tuniform sampler2D aoMap;\n\tuniform float aoMapIntensity;\n#endif',
    begin_vertex: '\nvec3 transformed = vec3( position );\n',
    beginnormal_vertex: '\nvec3 objectNormal = vec3( normal );\n',
    bsdfs:
      'bool testLightInRange( const in float lightDistance, const in float cutoffDistance ) {\n\treturn any( bvec2( cutoffDistance == 0.0, lightDistance < cutoffDistance ) );\n}\nfloat punctualLightIntensityToIrradianceFactor( const in float lightDistance, const in float cutoffDistance, const in float decayExponent ) {\n\t\tif( decayExponent > 0.0 ) {\n#if defined ( PHYSICALLY_CORRECT_LIGHTS )\n\t\t\tfloat distanceFalloff = 1.0 / max( pow( lightDistance, decayExponent ), 0.01 );\n\t\t\tfloat maxDistanceCutoffFactor = pow2( saturate( 1.0 - pow4( lightDistance / cutoffDistance ) ) );\n\t\t\treturn distanceFalloff * maxDistanceCutoffFactor;\n#else\n\t\t\treturn pow( saturate( -lightDistance / cutoffDistance + 1.0 ), decayExponent );\n#endif\n\t\t}\n\t\treturn 1.0;\n}\nvec3 BRDF_Diffuse_Lambert( const in vec3 diffuseColor ) {\n\treturn RECIPROCAL_PI * diffuseColor;\n}\nvec3 F_Schlick( const in vec3 specularColor, const in float dotLH ) {\n\tfloat fresnel = exp2( ( -5.55473 * dotLH - 6.98316 ) * dotLH );\n\treturn ( 1.0 - specularColor ) * fresnel + specularColor;\n}\nfloat G_GGX_Smith( const in float alpha, const in float dotNL, const in float dotNV ) {\n\tfloat a2 = pow2( alpha );\n\tfloat gl = dotNL + sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNL ) );\n\tfloat gv = dotNV + sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNV ) );\n\treturn 1.0 / ( gl * gv );\n}\nfloat G_GGX_SmithCorrelated( const in float alpha, const in float dotNL, const in float dotNV ) {\n\tfloat a2 = pow2( alpha );\n\tfloat gv = dotNL * sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNV ) );\n\tfloat gl = dotNV * sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNL ) );\n\treturn 0.5 / max( gv + gl, EPSILON );\n}\nfloat D_GGX( const in float alpha, const in float dotNH ) {\n\tfloat a2 = pow2( alpha );\n\tfloat denom = pow2( dotNH ) * ( a2 - 1.0 ) + 1.0;\n\treturn RECIPROCAL_PI * a2 / pow2( denom );\n}\nvec3 BRDF_Specular_GGX( const in IncidentLight incidentLight, const in GeometricContext geometry, const in vec3 specularColor, const in float roughness ) {\n\tfloat alpha = pow2( roughness );\n\tvec3 halfDir = normalize( incidentLight.direction + geometry.viewDir );\n\tfloat dotNL = saturate( dot( geometry.normal, incidentLight.direction ) );\n\tfloat dotNV = saturate( dot( geometry.normal, geometry.viewDir ) );\n\tfloat dotNH = saturate( dot( geometry.normal, halfDir ) );\n\tfloat dotLH = saturate( dot( incidentLight.direction, halfDir ) );\n\tvec3 F = F_Schlick( specularColor, dotLH );\n\tfloat G = G_GGX_SmithCorrelated( alpha, dotNL, dotNV );\n\tfloat D = D_GGX( alpha, dotNH );\n\treturn F * ( G * D );\n}\nvec3 BRDF_Specular_GGX_Environment( const in GeometricContext geometry, const in vec3 specularColor, const in float roughness ) {\n\tfloat dotNV = saturate( dot( geometry.normal, geometry.viewDir ) );\n\tconst vec4 c0 = vec4( - 1, - 0.0275, - 0.572, 0.022 );\n\tconst vec4 c1 = vec4( 1, 0.0425, 1.04, - 0.04 );\n\tvec4 r = roughness * c0 + c1;\n\tfloat a004 = min( r.x * r.x, exp2( - 9.28 * dotNV ) ) * r.x + r.y;\n\tvec2 AB = vec2( -1.04, 1.04 ) * a004 + r.zw;\n\treturn specularColor * AB.x + AB.y;\n}\nfloat G_BlinnPhong_Implicit( ) {\n\treturn 0.25;\n}\nfloat D_BlinnPhong( const in float shininess, const in float dotNH ) {\n\treturn RECIPROCAL_PI * ( shininess * 0.5 + 1.0 ) * pow( dotNH, shininess );\n}\nvec3 BRDF_Specular_BlinnPhong( const in IncidentLight incidentLight, const in GeometricContext geometry, const in vec3 specularColor, const in float shininess ) {\n\tvec3 halfDir = normalize( incidentLight.direction + geometry.viewDir );\n\tfloat dotNH = saturate( dot( geometry.normal, halfDir ) );\n\tfloat dotLH = saturate( dot( incidentLight.direction, halfDir ) );\n\tvec3 F = F_Schlick( specularColor, dotLH );\n\tfloat G = G_BlinnPhong_Implicit( );\n\tfloat D = D_BlinnPhong( shininess, dotNH );\n\treturn F * ( G * D );\n}\nfloat GGXRoughnessToBlinnExponent( const in float ggxRoughness ) {\n\treturn ( 2.0 / pow2( ggxRoughness + 0.0001 ) - 2.0 );\n}\nfloat BlinnExponentToGGXRoughness( const in float blinnExponent ) {\n\treturn sqrt( 2.0 / ( blinnExponent + 2.0 ) );\n}\n',
    bumpmap_pars_fragment:
      '#ifdef USE_BUMPMAP\n\tuniform sampler2D bumpMap;\n\tuniform float bumpScale;\n\tvec2 dHdxy_fwd() {\n\t\tvec2 dSTdx = dFdx( vUv );\n\t\tvec2 dSTdy = dFdy( vUv );\n\t\tfloat Hll = bumpScale * texture2D( bumpMap, vUv ).x;\n\t\tfloat dBx = bumpScale * texture2D( bumpMap, vUv + dSTdx ).x - Hll;\n\t\tfloat dBy = bumpScale * texture2D( bumpMap, vUv + dSTdy ).x - Hll;\n\t\treturn vec2( dBx, dBy );\n\t}\n\tvec3 perturbNormalArb( vec3 surf_pos, vec3 surf_norm, vec2 dHdxy ) {\n\t\tvec3 vSigmaX = dFdx( surf_pos );\n\t\tvec3 vSigmaY = dFdy( surf_pos );\n\t\tvec3 vN = surf_norm;\n\t\tvec3 R1 = cross( vSigmaY, vN );\n\t\tvec3 R2 = cross( vN, vSigmaX );\n\t\tfloat fDet = dot( vSigmaX, R1 );\n\t\tvec3 vGrad = sign( fDet ) * ( dHdxy.x * R1 + dHdxy.y * R2 );\n\t\treturn normalize( abs( fDet ) * surf_norm - vGrad );\n\t}\n#endif\n',
    clipping_planes_fragment:
      '#if NUM_CLIPPING_PLANES > 0\n\tfor ( int i = 0; i < NUM_CLIPPING_PLANES; ++ i ) {\n\t\tvec4 plane = clippingPlanes[ i ];\n\t\tif ( dot( vViewPosition, plane.xyz ) > plane.w ) discard;\n\t}\n#endif\n',
    clipping_planes_pars_fragment:
      '#if NUM_CLIPPING_PLANES > 0\n\t#if ! defined( PHYSICAL ) && ! defined( PHONG )\n\t\tvarying vec3 vViewPosition;\n\t#endif\n\tuniform vec4 clippingPlanes[ NUM_CLIPPING_PLANES ];\n#endif\n',
    clipping_planes_pars_vertex:
      '#if NUM_CLIPPING_PLANES > 0 && ! defined( PHYSICAL ) && ! defined( PHONG )\n\tvarying vec3 vViewPosition;\n#endif\n',
    clipping_planes_vertex:
      '#if NUM_CLIPPING_PLANES > 0 && ! defined( PHYSICAL ) && ! defined( PHONG )\n\tvViewPosition = - mvPosition.xyz;\n#endif\n',
    color_fragment: '#ifdef USE_COLOR\n\tdiffuseColor.rgb *= vColor;\n#endif',
    color_pars_fragment: '#ifdef USE_COLOR\n\tvarying vec3 vColor;\n#endif\n',
    color_pars_vertex: '#ifdef USE_COLOR\n\tvarying vec3 vColor;\n#endif',
    color_vertex: '#ifdef USE_COLOR\n\tvColor.xyz = color.xyz;\n#endif',
    common:
      '#define PI 3.14159265359\n#define PI2 6.28318530718\n#define RECIPROCAL_PI 0.31830988618\n#define RECIPROCAL_PI2 0.15915494\n#define LOG2 1.442695\n#define EPSILON 1e-6\n#define saturate(a) clamp( a, 0.0, 1.0 )\n#define whiteCompliment(a) ( 1.0 - saturate( a ) )\nfloat pow2( const in float x ) { return x*x; }\nfloat pow3( const in float x ) { return x*x*x; }\nfloat pow4( const in float x ) { float x2 = x*x; return x2*x2; }\nfloat average( const in vec3 color ) { return dot( color, vec3( 0.3333 ) ); }\nhighp float rand( const in vec2 uv ) {\n\tconst highp float a = 12.9898, b = 78.233, c = 43758.5453;\n\thighp float dt = dot( uv.xy, vec2( a,b ) ), sn = mod( dt, PI );\n\treturn fract(sin(sn) * c);\n}\nstruct IncidentLight {\n\tvec3 color;\n\tvec3 direction;\n\tbool visible;\n};\nstruct ReflectedLight {\n\tvec3 directDiffuse;\n\tvec3 directSpecular;\n\tvec3 indirectDiffuse;\n\tvec3 indirectSpecular;\n};\nstruct GeometricContext {\n\tvec3 position;\n\tvec3 normal;\n\tvec3 viewDir;\n};\nvec3 transformDirection( in vec3 dir, in mat4 matrix ) {\n\treturn normalize( ( matrix * vec4( dir, 0.0 ) ).xyz );\n}\nvec3 inverseTransformDirection( in vec3 dir, in mat4 matrix ) {\n\treturn normalize( ( vec4( dir, 0.0 ) * matrix ).xyz );\n}\nvec3 projectOnPlane(in vec3 point, in vec3 pointOnPlane, in vec3 planeNormal ) {\n\tfloat distance = dot( planeNormal, point - pointOnPlane );\n\treturn - distance * planeNormal + point;\n}\nfloat sideOfPlane( in vec3 point, in vec3 pointOnPlane, in vec3 planeNormal ) {\n\treturn sign( dot( point - pointOnPlane, planeNormal ) );\n}\nvec3 linePlaneIntersect( in vec3 pointOnLine, in vec3 lineDirection, in vec3 pointOnPlane, in vec3 planeNormal ) {\n\treturn lineDirection * ( dot( planeNormal, pointOnPlane - pointOnLine ) / dot( planeNormal, lineDirection ) ) + pointOnLine;\n}\n',
    cube_uv_reflection_fragment:
      '#ifdef ENVMAP_TYPE_CUBE_UV\n#define cubeUV_textureSize (1024.0)\nint getFaceFromDirection(vec3 direction) {\n\tvec3 absDirection = abs(direction);\n\tint face = -1;\n\tif( absDirection.x > absDirection.z ) {\n\t\tif(absDirection.x > absDirection.y )\n\t\t\tface = direction.x > 0.0 ? 0 : 3;\n\t\telse\n\t\t\tface = direction.y > 0.0 ? 1 : 4;\n\t}\n\telse {\n\t\tif(absDirection.z > absDirection.y )\n\t\t\tface = direction.z > 0.0 ? 2 : 5;\n\t\telse\n\t\t\tface = direction.y > 0.0 ? 1 : 4;\n\t}\n\treturn face;\n}\n#define cubeUV_maxLods1  (log2(cubeUV_textureSize*0.25) - 1.0)\n#define cubeUV_rangeClamp (exp2((6.0 - 1.0) * 2.0))\nvec2 MipLevelInfo( vec3 vec, float roughnessLevel, float roughness ) {\n\tfloat scale = exp2(cubeUV_maxLods1 - roughnessLevel);\n\tfloat dxRoughness = dFdx(roughness);\n\tfloat dyRoughness = dFdy(roughness);\n\tvec3 dx = dFdx( vec * scale * dxRoughness );\n\tvec3 dy = dFdy( vec * scale * dyRoughness );\n\tfloat d = max( dot( dx, dx ), dot( dy, dy ) );\n\td = clamp(d, 1.0, cubeUV_rangeClamp);\n\tfloat mipLevel = 0.5 * log2(d);\n\treturn vec2(floor(mipLevel), fract(mipLevel));\n}\n#define cubeUV_maxLods2 (log2(cubeUV_textureSize*0.25) - 2.0)\n#define cubeUV_rcpTextureSize (1.0 / cubeUV_textureSize)\nvec2 getCubeUV(vec3 direction, float roughnessLevel, float mipLevel) {\n\tmipLevel = roughnessLevel > cubeUV_maxLods2 - 3.0 ? 0.0 : mipLevel;\n\tfloat a = 16.0 * cubeUV_rcpTextureSize;\n\tvec2 exp2_packed = exp2( vec2( roughnessLevel, mipLevel ) );\n\tvec2 rcp_exp2_packed = vec2( 1.0 ) / exp2_packed;\n\tfloat powScale = exp2_packed.x * exp2_packed.y;\n\tfloat scale = rcp_exp2_packed.x * rcp_exp2_packed.y * 0.25;\n\tfloat mipOffset = 0.75*(1.0 - rcp_exp2_packed.y) * rcp_exp2_packed.x;\n\tbool bRes = mipLevel == 0.0;\n\tscale =  bRes && (scale < a) ? a : scale;\n\tvec3 r;\n\tvec2 offset;\n\tint face = getFaceFromDirection(direction);\n\tfloat rcpPowScale = 1.0 / powScale;\n\tif( face == 0) {\n\t\tr = vec3(direction.x, -direction.z, direction.y);\n\t\toffset = vec2(0.0+mipOffset,0.75 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  a : offset.y;\n\t}\n\telse if( face == 1) {\n\t\tr = vec3(direction.y, direction.x, direction.z);\n\t\toffset = vec2(scale+mipOffset, 0.75 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  a : offset.y;\n\t}\n\telse if( face == 2) {\n\t\tr = vec3(direction.z, direction.x, direction.y);\n\t\toffset = vec2(2.0*scale+mipOffset, 0.75 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  a : offset.y;\n\t}\n\telse if( face == 3) {\n\t\tr = vec3(direction.x, direction.z, direction.y);\n\t\toffset = vec2(0.0+mipOffset,0.5 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  0.0 : offset.y;\n\t}\n\telse if( face == 4) {\n\t\tr = vec3(direction.y, direction.x, -direction.z);\n\t\toffset = vec2(scale+mipOffset, 0.5 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  0.0 : offset.y;\n\t}\n\telse {\n\t\tr = vec3(direction.z, -direction.x, direction.y);\n\t\toffset = vec2(2.0*scale+mipOffset, 0.5 * rcpPowScale);\n\t\toffset.y = bRes && (offset.y < 2.0*a) ?  0.0 : offset.y;\n\t}\n\tr = normalize(r);\n\tfloat texelOffset = 0.5 * cubeUV_rcpTextureSize;\n\tvec2 s = ( r.yz / abs( r.x ) + vec2( 1.0 ) ) * 0.5;\n\tvec2 base = offset + vec2( texelOffset );\n\treturn base + s * ( scale - 2.0 * texelOffset );\n}\n#define cubeUV_maxLods3 (log2(cubeUV_textureSize*0.25) - 3.0)\nvec4 textureCubeUV(vec3 reflectedDirection, float roughness ) {\n\tfloat roughnessVal = roughness* cubeUV_maxLods3;\n\tfloat r1 = floor(roughnessVal);\n\tfloat r2 = r1 + 1.0;\n\tfloat t = fract(roughnessVal);\n\tvec2 mipInfo = MipLevelInfo(reflectedDirection, r1, roughness);\n\tfloat s = mipInfo.y;\n\tfloat level0 = mipInfo.x;\n\tfloat level1 = level0 + 1.0;\n\tlevel1 = level1 > 5.0 ? 5.0 : level1;\n\tlevel0 += min( floor( s + 0.5 ), 5.0 );\n\tvec2 uv_10 = getCubeUV(reflectedDirection, r1, level0);\n\tvec4 color10 = envMapTexelToLinear(texture2D(envMap, uv_10));\n\tvec2 uv_20 = getCubeUV(reflectedDirection, r2, level0);\n\tvec4 color20 = envMapTexelToLinear(texture2D(envMap, uv_20));\n\tvec4 result = mix(color10, color20, t);\n\treturn vec4(result.rgb, 1.0);\n}\n#endif\n',
    defaultnormal_vertex:
      '#ifdef FLIP_SIDED\n\tobjectNormal = -objectNormal;\n#endif\nvec3 transformedNormal = normalMatrix * objectNormal;\n',
    displacementmap_pars_vertex:
      '#ifdef USE_DISPLACEMENTMAP\n\tuniform sampler2D displacementMap;\n\tuniform float displacementScale;\n\tuniform float displacementBias;\n#endif\n',
    displacementmap_vertex:
      '#ifdef USE_DISPLACEMENTMAP\n\ttransformed += normal * ( texture2D( displacementMap, uv ).x * displacementScale + displacementBias );\n#endif\n',
    emissivemap_fragment:
      '#ifdef USE_EMISSIVEMAP\n\tvec4 emissiveColor = texture2D( emissiveMap, vUv );\n\temissiveColor.rgb = emissiveMapTexelToLinear( emissiveColor ).rgb;\n\ttotalEmissiveRadiance *= emissiveColor.rgb;\n#endif\n',
    emissivemap_pars_fragment:
      '#ifdef USE_EMISSIVEMAP\n\tuniform sampler2D emissiveMap;\n#endif\n',
    encodings_fragment:
      '  gl_FragColor = linearToOutputTexel( gl_FragColor );\n',
    encodings_pars_fragment:
      '\nvec4 LinearToLinear( in vec4 value ) {\n  return value;\n}\nvec4 GammaToLinear( in vec4 value, in float gammaFactor ) {\n  return vec4( pow( value.xyz, vec3( gammaFactor ) ), value.w );\n}\nvec4 LinearToGamma( in vec4 value, in float gammaFactor ) {\n  return vec4( pow( value.xyz, vec3( 1.0 / gammaFactor ) ), value.w );\n}\nvec4 sRGBToLinear( in vec4 value ) {\n  return vec4( mix( pow( value.rgb * 0.9478672986 + vec3( 0.0521327014 ), vec3( 2.4 ) ), value.rgb * 0.0773993808, vec3( lessThanEqual( value.rgb, vec3( 0.04045 ) ) ) ), value.w );\n}\nvec4 LinearTosRGB( in vec4 value ) {\n  return vec4( mix( pow( value.rgb, vec3( 0.41666 ) ) * 1.055 - vec3( 0.055 ), value.rgb * 12.92, vec3( lessThanEqual( value.rgb, vec3( 0.0031308 ) ) ) ), value.w );\n}\nvec4 RGBEToLinear( in vec4 value ) {\n  return vec4( value.rgb * exp2( value.a * 255.0 - 128.0 ), 1.0 );\n}\nvec4 LinearToRGBE( in vec4 value ) {\n  float maxComponent = max( max( value.r, value.g ), value.b );\n  float fExp = clamp( ceil( log2( maxComponent ) ), -128.0, 127.0 );\n  return vec4( value.rgb / exp2( fExp ), ( fExp + 128.0 ) / 255.0 );\n}\nvec4 RGBMToLinear( in vec4 value, in float maxRange ) {\n  return vec4( value.xyz * value.w * maxRange, 1.0 );\n}\nvec4 LinearToRGBM( in vec4 value, in float maxRange ) {\n  float maxRGB = max( value.x, max( value.g, value.b ) );\n  float M      = clamp( maxRGB / maxRange, 0.0, 1.0 );\n  M            = ceil( M * 255.0 ) / 255.0;\n  return vec4( value.rgb / ( M * maxRange ), M );\n}\nvec4 RGBDToLinear( in vec4 value, in float maxRange ) {\n    return vec4( value.rgb * ( ( maxRange / 255.0 ) / value.a ), 1.0 );\n}\nvec4 LinearToRGBD( in vec4 value, in float maxRange ) {\n    float maxRGB = max( value.x, max( value.g, value.b ) );\n    float D      = max( maxRange / maxRGB, 1.0 );\n    D            = min( floor( D ) / 255.0, 1.0 );\n    return vec4( value.rgb * ( D * ( 255.0 / maxRange ) ), D );\n}\nconst mat3 cLogLuvM = mat3( 0.2209, 0.3390, 0.4184, 0.1138, 0.6780, 0.7319, 0.0102, 0.1130, 0.2969 );\nvec4 LinearToLogLuv( in vec4 value )  {\n  vec3 Xp_Y_XYZp = value.rgb * cLogLuvM;\n  Xp_Y_XYZp = max(Xp_Y_XYZp, vec3(1e-6, 1e-6, 1e-6));\n  vec4 vResult;\n  vResult.xy = Xp_Y_XYZp.xy / Xp_Y_XYZp.z;\n  float Le = 2.0 * log2(Xp_Y_XYZp.y) + 127.0;\n  vResult.w = fract(Le);\n  vResult.z = (Le - (floor(vResult.w*255.0))/255.0)/255.0;\n  return vResult;\n}\nconst mat3 cLogLuvInverseM = mat3( 6.0014, -2.7008, -1.7996, -1.3320, 3.1029, -5.7721, 0.3008, -1.0882, 5.6268 );\nvec4 LogLuvToLinear( in vec4 value ) {\n  float Le = value.z * 255.0 + value.w;\n  vec3 Xp_Y_XYZp;\n  Xp_Y_XYZp.y = exp2((Le - 127.0) / 2.0);\n  Xp_Y_XYZp.z = Xp_Y_XYZp.y / value.y;\n  Xp_Y_XYZp.x = value.x * Xp_Y_XYZp.z;\n  vec3 vRGB = Xp_Y_XYZp.rgb * cLogLuvInverseM;\n  return vec4( max(vRGB, 0.0), 1.0 );\n}\n',
    envmap_fragment:
      '#ifdef USE_ENVMAP\n\t#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG )\n\t\tvec3 cameraToVertex = normalize( vWorldPosition - cameraPosition );\n\t\tvec3 worldNormal = inverseTransformDirection( normal, viewMatrix );\n\t\t#ifdef ENVMAP_MODE_REFLECTION\n\t\t\tvec3 reflectVec = reflect( cameraToVertex, worldNormal );\n\t\t#else\n\t\t\tvec3 reflectVec = refract( cameraToVertex, worldNormal, refractionRatio );\n\t\t#endif\n\t#else\n\t\tvec3 reflectVec = vReflect;\n\t#endif\n\t#ifdef ENVMAP_TYPE_CUBE\n\t\tvec4 envColor = textureCube( envMap, flipNormal * vec3( flipEnvMap * reflectVec.x, reflectVec.yz ) );\n\t#elif defined( ENVMAP_TYPE_EQUIREC )\n\t\tvec2 sampleUV;\n\t\tsampleUV.y = saturate( flipNormal * reflectVec.y * 0.5 + 0.5 );\n\t\tsampleUV.x = atan( flipNormal * reflectVec.z, flipNormal * reflectVec.x ) * RECIPROCAL_PI2 + 0.5;\n\t\tvec4 envColor = texture2D( envMap, sampleUV );\n\t#elif defined( ENVMAP_TYPE_SPHERE )\n\t\tvec3 reflectView = flipNormal * normalize( ( viewMatrix * vec4( reflectVec, 0.0 ) ).xyz + vec3( 0.0, 0.0, 1.0 ) );\n\t\tvec4 envColor = texture2D( envMap, reflectView.xy * 0.5 + 0.5 );\n\t#else\n\t\tvec4 envColor = vec4( 0.0 );\n\t#endif\n\tenvColor = envMapTexelToLinear( envColor );\n\t#ifdef ENVMAP_BLENDING_MULTIPLY\n\t\toutgoingLight = mix( outgoingLight, outgoingLight * envColor.xyz, specularStrength * reflectivity );\n\t#elif defined( ENVMAP_BLENDING_MIX )\n\t\toutgoingLight = mix( outgoingLight, envColor.xyz, specularStrength * reflectivity );\n\t#elif defined( ENVMAP_BLENDING_ADD )\n\t\toutgoingLight += envColor.xyz * specularStrength * reflectivity;\n\t#endif\n#endif\n',
    envmap_pars_fragment:
      '#if defined( USE_ENVMAP ) || defined( PHYSICAL )\n\tuniform float reflectivity;\n\tuniform float envMapIntenstiy;\n#endif\n#ifdef USE_ENVMAP\n\t#if ! defined( PHYSICAL ) && ( defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG ) )\n\t\tvarying vec3 vWorldPosition;\n\t#endif\n\t#ifdef ENVMAP_TYPE_CUBE\n\t\tuniform samplerCube envMap;\n\t#else\n\t\tuniform sampler2D envMap;\n\t#endif\n\tuniform float flipEnvMap;\n\t#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG ) || defined( PHYSICAL )\n\t\tuniform float refractionRatio;\n\t#else\n\t\tvarying vec3 vReflect;\n\t#endif\n#endif\n',
    envmap_pars_vertex:
      '#ifdef USE_ENVMAP\n\t#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG )\n\t\tvarying vec3 vWorldPosition;\n\t#else\n\t\tvarying vec3 vReflect;\n\t\tuniform float refractionRatio;\n\t#endif\n#endif\n',
    envmap_vertex:
      '#ifdef USE_ENVMAP\n\t#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG )\n\t\tvWorldPosition = worldPosition.xyz;\n\t#else\n\t\tvec3 cameraToVertex = normalize( worldPosition.xyz - cameraPosition );\n\t\tvec3 worldNormal = inverseTransformDirection( transformedNormal, viewMatrix );\n\t\t#ifdef ENVMAP_MODE_REFLECTION\n\t\t\tvReflect = reflect( cameraToVertex, worldNormal );\n\t\t#else\n\t\t\tvReflect = refract( cameraToVertex, worldNormal, refractionRatio );\n\t\t#endif\n\t#endif\n#endif\n',
    fog_fragment:
      '#ifdef USE_FOG\n\t#ifdef USE_LOGDEPTHBUF_EXT\n\t\tfloat depth = gl_FragDepthEXT / gl_FragCoord.w;\n\t#else\n\t\tfloat depth = gl_FragCoord.z / gl_FragCoord.w;\n\t#endif\n\t#ifdef FOG_EXP2\n\t\tfloat fogFactor = whiteCompliment( exp2( - fogDensity * fogDensity * depth * depth * LOG2 ) );\n\t#else\n\t\tfloat fogFactor = smoothstep( fogNear, fogFar, depth );\n\t#endif\n\tgl_FragColor.rgb = mix( gl_FragColor.rgb, fogColor, fogFactor );\n#endif\n',
    fog_pars_fragment:
      '#ifdef USE_FOG\n\tuniform vec3 fogColor;\n\t#ifdef FOG_EXP2\n\t\tuniform float fogDensity;\n\t#else\n\t\tuniform float fogNear;\n\t\tuniform float fogFar;\n\t#endif\n#endif',
    lightmap_fragment:
      '#ifdef USE_LIGHTMAP\n\treflectedLight.indirectDiffuse += PI * texture2D( lightMap, vUv2 ).xyz * lightMapIntensity;\n#endif\n',
    lightmap_pars_fragment:
      '#ifdef USE_LIGHTMAP\n\tuniform sampler2D lightMap;\n\tuniform float lightMapIntensity;\n#endif',
    lights_lambert_vertex:
      'vec3 diffuse = vec3( 1.0 );\nGeometricContext geometry;\ngeometry.position = mvPosition.xyz;\ngeometry.normal = normalize( transformedNormal );\ngeometry.viewDir = normalize( -mvPosition.xyz );\nGeometricContext backGeometry;\nbackGeometry.position = geometry.position;\nbackGeometry.normal = -geometry.normal;\nbackGeometry.viewDir = geometry.viewDir;\nvLightFront = vec3( 0.0 );\n#ifdef DOUBLE_SIDED\n\tvLightBack = vec3( 0.0 );\n#endif\nIncidentLight directLight;\nfloat dotNL;\nvec3 directLightColor_Diffuse;\n#if NUM_POINT_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_POINT_LIGHTS; i ++ ) {\n\t\tgetPointDirectLightIrradiance( pointLights[ i ], geometry, directLight );\n\t\tdotNL = dot( geometry.normal, directLight.direction );\n\t\tdirectLightColor_Diffuse = PI * directLight.color;\n\t\tvLightFront += saturate( dotNL ) * directLightColor_Diffuse;\n\t\t#ifdef DOUBLE_SIDED\n\t\t\tvLightBack += saturate( -dotNL ) * directLightColor_Diffuse;\n\t\t#endif\n\t}\n#endif\n#if NUM_SPOT_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_SPOT_LIGHTS; i ++ ) {\n\t\tgetSpotDirectLightIrradiance( spotLights[ i ], geometry, directLight );\n\t\tdotNL = dot( geometry.normal, directLight.direction );\n\t\tdirectLightColor_Diffuse = PI * directLight.color;\n\t\tvLightFront += saturate( dotNL ) * directLightColor_Diffuse;\n\t\t#ifdef DOUBLE_SIDED\n\t\t\tvLightBack += saturate( -dotNL ) * directLightColor_Diffuse;\n\t\t#endif\n\t}\n#endif\n#if NUM_DIR_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_DIR_LIGHTS; i ++ ) {\n\t\tgetDirectionalDirectLightIrradiance( directionalLights[ i ], geometry, directLight );\n\t\tdotNL = dot( geometry.normal, directLight.direction );\n\t\tdirectLightColor_Diffuse = PI * directLight.color;\n\t\tvLightFront += saturate( dotNL ) * directLightColor_Diffuse;\n\t\t#ifdef DOUBLE_SIDED\n\t\t\tvLightBack += saturate( -dotNL ) * directLightColor_Diffuse;\n\t\t#endif\n\t}\n#endif\n#if NUM_HEMI_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_HEMI_LIGHTS; i ++ ) {\n\t\tvLightFront += getHemisphereLightIrradiance( hemisphereLights[ i ], geometry );\n\t\t#ifdef DOUBLE_SIDED\n\t\t\tvLightBack += getHemisphereLightIrradiance( hemisphereLights[ i ], backGeometry );\n\t\t#endif\n\t}\n#endif\n',
    lights_pars:
      'uniform vec3 ambientLightColor;\nvec3 getAmbientLightIrradiance( const in vec3 ambientLightColor ) {\n\tvec3 irradiance = ambientLightColor;\n\t#ifndef PHYSICALLY_CORRECT_LIGHTS\n\t\tirradiance *= PI;\n\t#endif\n\treturn irradiance;\n}\n#if NUM_DIR_LIGHTS > 0\n\tstruct DirectionalLight {\n\t\tvec3 direction;\n\t\tvec3 color;\n\t\tint shadow;\n\t\tfloat shadowBias;\n\t\tfloat shadowRadius;\n\t\tvec2 shadowMapSize;\n\t};\n\tuniform DirectionalLight directionalLights[ NUM_DIR_LIGHTS ];\n\tvoid getDirectionalDirectLightIrradiance( const in DirectionalLight directionalLight, const in GeometricContext geometry, out IncidentLight directLight ) {\n\t\tdirectLight.color = directionalLight.color;\n\t\tdirectLight.direction = directionalLight.direction;\n\t\tdirectLight.visible = true;\n\t}\n#endif\n#if NUM_POINT_LIGHTS > 0\n\tstruct PointLight {\n\t\tvec3 position;\n\t\tvec3 color;\n\t\tfloat distance;\n\t\tfloat decay;\n\t\tint shadow;\n\t\tfloat shadowBias;\n\t\tfloat shadowRadius;\n\t\tvec2 shadowMapSize;\n\t};\n\tuniform PointLight pointLights[ NUM_POINT_LIGHTS ];\n\tvoid getPointDirectLightIrradiance( const in PointLight pointLight, const in GeometricContext geometry, out IncidentLight directLight ) {\n\t\tvec3 lVector = pointLight.position - geometry.position;\n\t\tdirectLight.direction = normalize( lVector );\n\t\tfloat lightDistance = length( lVector );\n\t\tif ( testLightInRange( lightDistance, pointLight.distance ) ) {\n\t\t\tdirectLight.color = pointLight.color;\n\t\t\tdirectLight.color *= punctualLightIntensityToIrradianceFactor( lightDistance, pointLight.distance, pointLight.decay );\n\t\t\tdirectLight.visible = true;\n\t\t} else {\n\t\t\tdirectLight.color = vec3( 0.0 );\n\t\t\tdirectLight.visible = false;\n\t\t}\n\t}\n#endif\n#if NUM_SPOT_LIGHTS > 0\n\tstruct SpotLight {\n\t\tvec3 position;\n\t\tvec3 direction;\n\t\tvec3 color;\n\t\tfloat distance;\n\t\tfloat decay;\n\t\tfloat coneCos;\n\t\tfloat penumbraCos;\n\t\tint shadow;\n\t\tfloat shadowBias;\n\t\tfloat shadowRadius;\n\t\tvec2 shadowMapSize;\n\t};\n\tuniform SpotLight spotLights[ NUM_SPOT_LIGHTS ];\n\tvoid getSpotDirectLightIrradiance( const in SpotLight spotLight, const in GeometricContext geometry, out IncidentLight directLight  ) {\n\t\tvec3 lVector = spotLight.position - geometry.position;\n\t\tdirectLight.direction = normalize( lVector );\n\t\tfloat lightDistance = length( lVector );\n\t\tfloat angleCos = dot( directLight.direction, spotLight.direction );\n\t\tif ( all( bvec2( angleCos > spotLight.coneCos, testLightInRange( lightDistance, spotLight.distance ) ) ) ) {\n\t\t\tfloat spotEffect = smoothstep( spotLight.coneCos, spotLight.penumbraCos, angleCos );\n\t\t\tdirectLight.color = spotLight.color;\n\t\t\tdirectLight.color *= spotEffect * punctualLightIntensityToIrradianceFactor( lightDistance, spotLight.distance, spotLight.decay );\n\t\t\tdirectLight.visible = true;\n\t\t} else {\n\t\t\tdirectLight.color = vec3( 0.0 );\n\t\t\tdirectLight.visible = false;\n\t\t}\n\t}\n#endif\n#if NUM_HEMI_LIGHTS > 0\n\tstruct HemisphereLight {\n\t\tvec3 direction;\n\t\tvec3 skyColor;\n\t\tvec3 groundColor;\n\t};\n\tuniform HemisphereLight hemisphereLights[ NUM_HEMI_LIGHTS ];\n\tvec3 getHemisphereLightIrradiance( const in HemisphereLight hemiLight, const in GeometricContext geometry ) {\n\t\tfloat dotNL = dot( geometry.normal, hemiLight.direction );\n\t\tfloat hemiDiffuseWeight = 0.5 * dotNL + 0.5;\n\t\tvec3 irradiance = mix( hemiLight.groundColor, hemiLight.skyColor, hemiDiffuseWeight );\n\t\t#ifndef PHYSICALLY_CORRECT_LIGHTS\n\t\t\tirradiance *= PI;\n\t\t#endif\n\t\treturn irradiance;\n\t}\n#endif\n#if defined( USE_ENVMAP ) && defined( PHYSICAL )\n\tvec3 getLightProbeIndirectIrradiance( const in GeometricContext geometry, const in int maxMIPLevel ) {\n\t\t#include <normal_flip>\n\t\tvec3 worldNormal = inverseTransformDirection( geometry.normal, viewMatrix );\n\t\t#ifdef ENVMAP_TYPE_CUBE\n\t\t\tvec3 queryVec = flipNormal * vec3( flipEnvMap * worldNormal.x, worldNormal.yz );\n\t\t\t#ifdef TEXTURE_LOD_EXT\n\t\t\t\tvec4 envMapColor = textureCubeLodEXT( envMap, queryVec, float( maxMIPLevel ) );\n\t\t\t#else\n\t\t\t\tvec4 envMapColor = textureCube( envMap, queryVec, float( maxMIPLevel ) );\n\t\t\t#endif\n\t\t\tenvMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;\n\t\t#elif defined( ENVMAP_TYPE_CUBE_UV )\n\t\t\tvec3 queryVec = flipNormal * vec3( flipEnvMap * worldNormal.x, worldNormal.yz );\n\t\t\tvec4 envMapColor = textureCubeUV( queryVec, 1.0 );\n\t\t#else\n\t\t\tvec4 envMapColor = vec4( 0.0 );\n\t\t#endif\n\t\treturn PI * envMapColor.rgb * envMapIntensity;\n\t}\n\tfloat getSpecularMIPLevel( const in float blinnShininessExponent, const in int maxMIPLevel ) {\n\t\tfloat maxMIPLevelScalar = float( maxMIPLevel );\n\t\tfloat desiredMIPLevel = maxMIPLevelScalar - 0.79248 - 0.5 * log2( pow2( blinnShininessExponent ) + 1.0 );\n\t\treturn clamp( desiredMIPLevel, 0.0, maxMIPLevelScalar );\n\t}\n\tvec3 getLightProbeIndirectRadiance( const in GeometricContext geometry, const in float blinnShininessExponent, const in int maxMIPLevel ) {\n\t\t#ifdef ENVMAP_MODE_REFLECTION\n\t\t\tvec3 reflectVec = reflect( -geometry.viewDir, geometry.normal );\n\t\t#else\n\t\t\tvec3 reflectVec = refract( -geometry.viewDir, geometry.normal, refractionRatio );\n\t\t#endif\n\t\t#include <normal_flip>\n\t\treflectVec = inverseTransformDirection( reflectVec, viewMatrix );\n\t\tfloat specularMIPLevel = getSpecularMIPLevel( blinnShininessExponent, maxMIPLevel );\n\t\t#ifdef ENVMAP_TYPE_CUBE\n\t\t\tvec3 queryReflectVec = flipNormal * vec3( flipEnvMap * reflectVec.x, reflectVec.yz );\n\t\t\t#ifdef TEXTURE_LOD_EXT\n\t\t\t\tvec4 envMapColor = textureCubeLodEXT( envMap, queryReflectVec, specularMIPLevel );\n\t\t\t#else\n\t\t\t\tvec4 envMapColor = textureCube( envMap, queryReflectVec, specularMIPLevel );\n\t\t\t#endif\n\t\t\tenvMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;\n\t\t#elif defined( ENVMAP_TYPE_CUBE_UV )\n\t\t\tvec3 queryReflectVec = flipNormal * vec3( flipEnvMap * reflectVec.x, reflectVec.yz );\n\t\t\tvec4 envMapColor = textureCubeUV(queryReflectVec, BlinnExponentToGGXRoughness(blinnShininessExponent));\n\t\t#elif defined( ENVMAP_TYPE_EQUIREC )\n\t\t\tvec2 sampleUV;\n\t\t\tsampleUV.y = saturate( flipNormal * reflectVec.y * 0.5 + 0.5 );\n\t\t\tsampleUV.x = atan( flipNormal * reflectVec.z, flipNormal * reflectVec.x ) * RECIPROCAL_PI2 + 0.5;\n\t\t\t#ifdef TEXTURE_LOD_EXT\n\t\t\t\tvec4 envMapColor = texture2DLodEXT( envMap, sampleUV, specularMIPLevel );\n\t\t\t#else\n\t\t\t\tvec4 envMapColor = texture2D( envMap, sampleUV, specularMIPLevel );\n\t\t\t#endif\n\t\t\tenvMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;\n\t\t#elif defined( ENVMAP_TYPE_SPHERE )\n\t\t\tvec3 reflectView = flipNormal * normalize( ( viewMatrix * vec4( reflectVec, 0.0 ) ).xyz + vec3( 0.0,0.0,1.0 ) );\n\t\t\t#ifdef TEXTURE_LOD_EXT\n\t\t\t\tvec4 envMapColor = texture2DLodEXT( envMap, reflectView.xy * 0.5 + 0.5, specularMIPLevel );\n\t\t\t#else\n\t\t\t\tvec4 envMapColor = texture2D( envMap, reflectView.xy * 0.5 + 0.5, specularMIPLevel );\n\t\t\t#endif\n\t\t\tenvMapColor.rgb = envMapTexelToLinear( envMapColor ).rgb;\n\t\t#endif\n\t\treturn envMapColor.rgb * envMapIntensity;\n\t}\n#endif\n',
    lights_phong_fragment:
      'BlinnPhongMaterial material;\nmaterial.diffuseColor = diffuseColor.rgb;\nmaterial.specularColor = specular;\nmaterial.specularShininess = shininess;\nmaterial.specularStrength = specularStrength;\n',
    lights_phong_pars_fragment:
      'varying vec3 vViewPosition;\n#ifndef FLAT_SHADED\n\tvarying vec3 vNormal;\n#endif\nstruct BlinnPhongMaterial {\n\tvec3\tdiffuseColor;\n\tvec3\tspecularColor;\n\tfloat\tspecularShininess;\n\tfloat\tspecularStrength;\n};\nvoid RE_Direct_BlinnPhong( const in IncidentLight directLight, const in GeometricContext geometry, const in BlinnPhongMaterial material, inout ReflectedLight reflectedLight ) {\n\tfloat dotNL = saturate( dot( geometry.normal, directLight.direction ) );\n\tvec3 irradiance = dotNL * directLight.color;\n\t#ifndef PHYSICALLY_CORRECT_LIGHTS\n\t\tirradiance *= PI;\n\t#endif\n\treflectedLight.directDiffuse += irradiance * BRDF_Diffuse_Lambert( material.diffuseColor );\n\treflectedLight.directSpecular += irradiance * BRDF_Specular_BlinnPhong( directLight, geometry, material.specularColor, material.specularShininess ) * material.specularStrength;\n}\nvoid RE_IndirectDiffuse_BlinnPhong( const in vec3 irradiance, const in GeometricContext geometry, const in BlinnPhongMaterial material, inout ReflectedLight reflectedLight ) {\n\treflectedLight.indirectDiffuse += irradiance * BRDF_Diffuse_Lambert( material.diffuseColor );\n}\n#define RE_Direct\t\t\t\tRE_Direct_BlinnPhong\n#define RE_IndirectDiffuse\t\tRE_IndirectDiffuse_BlinnPhong\n#define Material_LightProbeLOD( material )\t(0)\n',
    lights_physical_fragment:
      'PhysicalMaterial material;\nmaterial.diffuseColor = diffuseColor.rgb * ( 1.0 - metalnessFactor );\nmaterial.specularRoughness = clamp( roughnessFactor, 0.04, 1.0 );\n#ifdef STANDARD\n\tmaterial.specularColor = mix( vec3( DEFAULT_SPECULAR_COEFFICIENT ), diffuseColor.rgb, metalnessFactor );\n#else\n\tmaterial.specularColor = mix( vec3( MAXIMUM_SPECULAR_COEFFICIENT * pow2( reflectivity ) ), diffuseColor.rgb, metalnessFactor );\n\tmaterial.clearCoat = saturate( clearCoat );\tmaterial.clearCoatRoughness = clamp( clearCoatRoughness, 0.04, 1.0 );\n#endif\n',
    lights_physical_pars_fragment:
      'struct PhysicalMaterial {\n\tvec3\tdiffuseColor;\n\tfloat\tspecularRoughness;\n\tvec3\tspecularColor;\n\t#ifndef STANDARD\n\t\tfloat clearCoat;\n\t\tfloat clearCoatRoughness;\n\t#endif\n};\n#define MAXIMUM_SPECULAR_COEFFICIENT 0.16\n#define DEFAULT_SPECULAR_COEFFICIENT 0.04\nfloat clearCoatDHRApprox( const in float roughness, const in float dotNL ) {\n\treturn DEFAULT_SPECULAR_COEFFICIENT + ( 1.0 - DEFAULT_SPECULAR_COEFFICIENT ) * ( pow( 1.0 - dotNL, 5.0 ) * pow( 1.0 - roughness, 2.0 ) );\n}\nvoid RE_Direct_Physical( const in IncidentLight directLight, const in GeometricContext geometry, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {\n\tfloat dotNL = saturate( dot( geometry.normal, directLight.direction ) );\n\tvec3 irradiance = dotNL * directLight.color;\n\t#ifndef PHYSICALLY_CORRECT_LIGHTS\n\t\tirradiance *= PI;\n\t#endif\n\t#ifndef STANDARD\n\t\tfloat clearCoatDHR = material.clearCoat * clearCoatDHRApprox( material.clearCoatRoughness, dotNL );\n\t#else\n\t\tfloat clearCoatDHR = 0.0;\n\t#endif\n\treflectedLight.directSpecular += ( 1.0 - clearCoatDHR ) * irradiance * BRDF_Specular_GGX( directLight, geometry, material.specularColor, material.specularRoughness );\n\treflectedLight.directDiffuse += ( 1.0 - clearCoatDHR ) * irradiance * BRDF_Diffuse_Lambert( material.diffuseColor );\n\t#ifndef STANDARD\n\t\treflectedLight.directSpecular += irradiance * material.clearCoat * BRDF_Specular_GGX( directLight, geometry, vec3( DEFAULT_SPECULAR_COEFFICIENT ), material.clearCoatRoughness );\n\t#endif\n}\nvoid RE_IndirectDiffuse_Physical( const in vec3 irradiance, const in GeometricContext geometry, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {\n\treflectedLight.indirectDiffuse += irradiance * BRDF_Diffuse_Lambert( material.diffuseColor );\n}\nvoid RE_IndirectSpecular_Physical( const in vec3 radiance, const in vec3 clearCoatRadiance, const in GeometricContext geometry, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {\n\t#ifndef STANDARD\n\t\tfloat dotNV = saturate( dot( geometry.normal, geometry.viewDir ) );\n\t\tfloat dotNL = dotNV;\n\t\tfloat clearCoatDHR = material.clearCoat * clearCoatDHRApprox( material.clearCoatRoughness, dotNL );\n\t#else\n\t\tfloat clearCoatDHR = 0.0;\n\t#endif\n\treflectedLight.indirectSpecular += ( 1.0 - clearCoatDHR ) * radiance * BRDF_Specular_GGX_Environment( geometry, material.specularColor, material.specularRoughness );\n\t#ifndef STANDARD\n\t\treflectedLight.indirectSpecular += clearCoatRadiance * material.clearCoat * BRDF_Specular_GGX_Environment( geometry, vec3( DEFAULT_SPECULAR_COEFFICIENT ), material.clearCoatRoughness );\n\t#endif\n}\n#define RE_Direct\t\t\t\tRE_Direct_Physical\n#define RE_IndirectDiffuse\t\tRE_IndirectDiffuse_Physical\n#define RE_IndirectSpecular\t\tRE_IndirectSpecular_Physical\n#define Material_BlinnShininessExponent( material )   GGXRoughnessToBlinnExponent( material.specularRoughness )\n#define Material_ClearCoat_BlinnShininessExponent( material )   GGXRoughnessToBlinnExponent( material.clearCoatRoughness )\nfloat computeSpecularOcclusion( const in float dotNV, const in float ambientOcclusion, const in float roughness ) {\n\treturn saturate( pow( dotNV + ambientOcclusion, exp2( - 16.0 * roughness - 1.0 ) ) - 1.0 + ambientOcclusion );\n}\n',
    lights_template:
      '\nGeometricContext geometry;\ngeometry.position = - vViewPosition;\ngeometry.normal = normal;\ngeometry.viewDir = normalize( vViewPosition );\nIncidentLight directLight;\n#if ( NUM_POINT_LIGHTS > 0 ) && defined( RE_Direct )\n\tPointLight pointLight;\n\tfor ( int i = 0; i < NUM_POINT_LIGHTS; i ++ ) {\n\t\tpointLight = pointLights[ i ];\n\t\tgetPointDirectLightIrradiance( pointLight, geometry, directLight );\n\t\t#ifdef USE_SHADOWMAP\n\t\tdirectLight.color *= all( bvec2( pointLight.shadow, directLight.visible ) ) ? getPointShadow( pointShadowMap[ i ], pointLight.shadowMapSize, pointLight.shadowBias, pointLight.shadowRadius, vPointShadowCoord[ i ] ) : 1.0;\n\t\t#endif\n\t\tRE_Direct( directLight, geometry, material, reflectedLight );\n\t}\n#endif\n#if ( NUM_SPOT_LIGHTS > 0 ) && defined( RE_Direct )\n\tSpotLight spotLight;\n\tfor ( int i = 0; i < NUM_SPOT_LIGHTS; i ++ ) {\n\t\tspotLight = spotLights[ i ];\n\t\tgetSpotDirectLightIrradiance( spotLight, geometry, directLight );\n\t\t#ifdef USE_SHADOWMAP\n\t\tdirectLight.color *= all( bvec2( spotLight.shadow, directLight.visible ) ) ? getShadow( spotShadowMap[ i ], spotLight.shadowMapSize, spotLight.shadowBias, spotLight.shadowRadius, vSpotShadowCoord[ i ] ) : 1.0;\n\t\t#endif\n\t\tRE_Direct( directLight, geometry, material, reflectedLight );\n\t}\n#endif\n#if ( NUM_DIR_LIGHTS > 0 ) && defined( RE_Direct )\n\tDirectionalLight directionalLight;\n\tfor ( int i = 0; i < NUM_DIR_LIGHTS; i ++ ) {\n\t\tdirectionalLight = directionalLights[ i ];\n\t\tgetDirectionalDirectLightIrradiance( directionalLight, geometry, directLight );\n\t\t#ifdef USE_SHADOWMAP\n\t\tdirectLight.color *= all( bvec2( directionalLight.shadow, directLight.visible ) ) ? getShadow( directionalShadowMap[ i ], directionalLight.shadowMapSize, directionalLight.shadowBias, directionalLight.shadowRadius, vDirectionalShadowCoord[ i ] ) : 1.0;\n\t\t#endif\n\t\tRE_Direct( directLight, geometry, material, reflectedLight );\n\t}\n#endif\n#if defined( RE_IndirectDiffuse )\n\tvec3 irradiance = getAmbientLightIrradiance( ambientLightColor );\n\t#ifdef USE_LIGHTMAP\n\t\tvec3 lightMapIrradiance = texture2D( lightMap, vUv2 ).xyz * lightMapIntensity;\n\t\t#ifndef PHYSICALLY_CORRECT_LIGHTS\n\t\t\tlightMapIrradiance *= PI;\n\t\t#endif\n\t\tirradiance += lightMapIrradiance;\n\t#endif\n\t#if ( NUM_HEMI_LIGHTS > 0 )\n\t\tfor ( int i = 0; i < NUM_HEMI_LIGHTS; i ++ ) {\n\t\t\tirradiance += getHemisphereLightIrradiance( hemisphereLights[ i ], geometry );\n\t\t}\n\t#endif\n\t#if defined( USE_ENVMAP ) && defined( PHYSICAL ) && defined( ENVMAP_TYPE_CUBE_UV )\n\t \tirradiance += getLightProbeIndirectIrradiance( geometry, 8 );\n\t#endif\n\tRE_IndirectDiffuse( irradiance, geometry, material, reflectedLight );\n#endif\n#if defined( USE_ENVMAP ) && defined( RE_IndirectSpecular )\n\tvec3 radiance = getLightProbeIndirectRadiance( geometry, Material_BlinnShininessExponent( material ), 8 );\n\t#ifndef STANDARD\n\t\tvec3 clearCoatRadiance = getLightProbeIndirectRadiance( geometry, Material_ClearCoat_BlinnShininessExponent( material ), 8 );\n\t#else\n\t\tvec3 clearCoatRadiance = vec3( 0.0 );\n\t#endif\n\t\t\n\tRE_IndirectSpecular( radiance, clearCoatRadiance, geometry, material, reflectedLight );\n#endif\n',
    logdepthbuf_fragment:
      '#if defined(USE_LOGDEPTHBUF) && defined(USE_LOGDEPTHBUF_EXT)\n\tgl_FragDepthEXT = log2(vFragDepth) * logDepthBufFC * 0.5;\n#endif',
    logdepthbuf_pars_fragment:
      '#ifdef USE_LOGDEPTHBUF\n\tuniform float logDepthBufFC;\n\t#ifdef USE_LOGDEPTHBUF_EXT\n\t\tvarying float vFragDepth;\n\t#endif\n#endif\n',
    logdepthbuf_pars_vertex:
      '#ifdef USE_LOGDEPTHBUF\n\t#ifdef USE_LOGDEPTHBUF_EXT\n\t\tvarying float vFragDepth;\n\t#endif\n\tuniform float logDepthBufFC;\n#endif',
    logdepthbuf_vertex:
      '#ifdef USE_LOGDEPTHBUF\n\tgl_Position.z = log2(max( EPSILON, gl_Position.w + 1.0 )) * logDepthBufFC;\n\t#ifdef USE_LOGDEPTHBUF_EXT\n\t\tvFragDepth = 1.0 + gl_Position.w;\n\t#else\n\t\tgl_Position.z = (gl_Position.z - 1.0) * gl_Position.w;\n\t#endif\n#endif\n',
    map_fragment:
      '#ifdef USE_MAP\n\tvec4 texelColor = texture2D( map, vUv );\n\ttexelColor = mapTexelToLinear( texelColor );\n\tdiffuseColor *= texelColor;\n#endif\n',
    map_pars_fragment: '#ifdef USE_MAP\n\tuniform sampler2D map;\n#endif\n',
    map_particle_fragment:
      '#ifdef USE_MAP\n\tvec4 mapTexel = texture2D( map, vec2( gl_PointCoord.x, 1.0 - gl_PointCoord.y ) * offsetRepeat.zw + offsetRepeat.xy );\n\tdiffuseColor *= mapTexelToLinear( mapTexel );\n#endif\n',
    map_particle_pars_fragment:
      '#ifdef USE_MAP\n\tuniform vec4 offsetRepeat;\n\tuniform sampler2D map;\n#endif\n',
    metalnessmap_fragment:
      'float metalnessFactor = metalness;\n#ifdef USE_METALNESSMAP\n\tvec4 texelMetalness = texture2D( metalnessMap, vUv );\n\tmetalnessFactor *= texelMetalness.r;\n#endif\n',
    metalnessmap_pars_fragment:
      '#ifdef USE_METALNESSMAP\n\tuniform sampler2D metalnessMap;\n#endif',
    morphnormal_vertex:
      '#ifdef USE_MORPHNORMALS\n\tobjectNormal += ( morphNormal0 - normal ) * morphTargetInfluences[ 0 ];\n\tobjectNormal += ( morphNormal1 - normal ) * morphTargetInfluences[ 1 ];\n\tobjectNormal += ( morphNormal2 - normal ) * morphTargetInfluences[ 2 ];\n\tobjectNormal += ( morphNormal3 - normal ) * morphTargetInfluences[ 3 ];\n#endif\n',
    morphtarget_pars_vertex:
      '#ifdef USE_MORPHTARGETS\n\t#ifndef USE_MORPHNORMALS\n\tuniform float morphTargetInfluences[ 8 ];\n\t#else\n\tuniform float morphTargetInfluences[ 4 ];\n\t#endif\n#endif',
    morphtarget_vertex:
      '#ifdef USE_MORPHTARGETS\n\ttransformed += ( morphTarget0 - position ) * morphTargetInfluences[ 0 ];\n\ttransformed += ( morphTarget1 - position ) * morphTargetInfluences[ 1 ];\n\ttransformed += ( morphTarget2 - position ) * morphTargetInfluences[ 2 ];\n\ttransformed += ( morphTarget3 - position ) * morphTargetInfluences[ 3 ];\n\t#ifndef USE_MORPHNORMALS\n\ttransformed += ( morphTarget4 - position ) * morphTargetInfluences[ 4 ];\n\ttransformed += ( morphTarget5 - position ) * morphTargetInfluences[ 5 ];\n\ttransformed += ( morphTarget6 - position ) * morphTargetInfluences[ 6 ];\n\ttransformed += ( morphTarget7 - position ) * morphTargetInfluences[ 7 ];\n\t#endif\n#endif\n',
    normal_flip:
      '#ifdef DOUBLE_SIDED\n\tfloat flipNormal = ( float( gl_FrontFacing ) * 2.0 - 1.0 );\n#else\n\tfloat flipNormal = 1.0;\n#endif\n',
    normal_fragment:
      '#ifdef FLAT_SHADED\n\tvec3 fdx = vec3( dFdx( vViewPosition.x ), dFdx( vViewPosition.y ), dFdx( vViewPosition.z ) );\n\tvec3 fdy = vec3( dFdy( vViewPosition.x ), dFdy( vViewPosition.y ), dFdy( vViewPosition.z ) );\n\tvec3 normal = normalize( cross( fdx, fdy ) );\n#else\n\tvec3 normal = normalize( vNormal ) * flipNormal;\n#endif\n#ifdef USE_NORMALMAP\n\tnormal = perturbNormal2Arb( -vViewPosition, normal );\n#elif defined( USE_BUMPMAP )\n\tnormal = perturbNormalArb( -vViewPosition, normal, dHdxy_fwd() );\n#endif\n',
    normalmap_pars_fragment:
      '#ifdef USE_NORMALMAP\n\tuniform sampler2D normalMap;\n\tuniform vec2 normalScale;\n\tvec3 perturbNormal2Arb( vec3 eye_pos, vec3 surf_norm ) {\n\t\tvec3 q0 = dFdx( eye_pos.xyz );\n\t\tvec3 q1 = dFdy( eye_pos.xyz );\n\t\tvec2 st0 = dFdx( vUv.st );\n\t\tvec2 st1 = dFdy( vUv.st );\n\t\tvec3 S = normalize( q0 * st1.t - q1 * st0.t );\n\t\tvec3 T = normalize( -q0 * st1.s + q1 * st0.s );\n\t\tvec3 N = normalize( surf_norm );\n\t\tvec3 mapN = texture2D( normalMap, vUv ).xyz * 2.0 - 1.0;\n\t\tmapN.xy = normalScale * mapN.xy;\n\t\tmat3 tsn = mat3( S, T, N );\n\t\treturn normalize( tsn * mapN );\n\t}\n#endif\n',
    packing:
      'vec3 packNormalToRGB( const in vec3 normal ) {\n  return normalize( normal ) * 0.5 + 0.5;\n}\nvec3 unpackRGBToNormal( const in vec3 rgb ) {\n  return 1.0 - 2.0 * rgb.xyz;\n}\nconst float PackUpscale = 256. / 255.;const float UnpackDownscale = 255. / 256.;\nconst vec3 PackFactors = vec3( 256. * 256. * 256., 256. * 256.,  256. );\nconst vec4 UnpackFactors = UnpackDownscale / vec4( PackFactors, 1. );\nconst float ShiftRight8 = 1. / 256.;\nvec4 packDepthToRGBA( const in float v ) {\n\tvec4 r = vec4( fract( v * PackFactors ), v );\n\tr.yzw -= r.xyz * ShiftRight8;\treturn r * PackUpscale;\n}\nfloat unpackRGBAToDepth( const in vec4 v ) {\n\treturn dot( v, UnpackFactors );\n}\nfloat viewZToOrthographicDepth( const in float viewZ, const in float near, const in float far ) {\n  return ( viewZ + near ) / ( near - far );\n}\nfloat orthographicDepthToViewZ( const in float linearClipZ, const in float near, const in float far ) {\n  return linearClipZ * ( near - far ) - near;\n}\nfloat viewZToPerspectiveDepth( const in float viewZ, const in float near, const in float far ) {\n  return (( near + viewZ ) * far ) / (( far - near ) * viewZ );\n}\nfloat perspectiveDepthToViewZ( const in float invClipZ, const in float near, const in float far ) {\n  return ( near * far ) / ( ( far - near ) * invClipZ - far );\n}\n',
    premultiplied_alpha_fragment:
      '#ifdef PREMULTIPLIED_ALPHA\n\tgl_FragColor.rgb *= gl_FragColor.a;\n#endif\n',
    project_vertex:
      '#ifdef USE_SKINNING\n\tvec4 mvPosition = modelViewMatrix * skinned;\n#else\n\tvec4 mvPosition = modelViewMatrix * vec4( transformed, 1.0 );\n#endif\ngl_Position = projectionMatrix * mvPosition;\n',
    roughnessmap_fragment:
      'float roughnessFactor = roughness;\n#ifdef USE_ROUGHNESSMAP\n\tvec4 texelRoughness = texture2D( roughnessMap, vUv );\n\troughnessFactor *= texelRoughness.r;\n#endif\n',
    roughnessmap_pars_fragment:
      '#ifdef USE_ROUGHNESSMAP\n\tuniform sampler2D roughnessMap;\n#endif',
    shadowmap_pars_fragment:
      '#ifdef USE_SHADOWMAP\n\t#if NUM_DIR_LIGHTS > 0\n\t\tuniform sampler2D directionalShadowMap[ NUM_DIR_LIGHTS ];\n\t\tvarying vec4 vDirectionalShadowCoord[ NUM_DIR_LIGHTS ];\n\t#endif\n\t#if NUM_SPOT_LIGHTS > 0\n\t\tuniform sampler2D spotShadowMap[ NUM_SPOT_LIGHTS ];\n\t\tvarying vec4 vSpotShadowCoord[ NUM_SPOT_LIGHTS ];\n\t#endif\n\t#if NUM_POINT_LIGHTS > 0\n\t\tuniform sampler2D pointShadowMap[ NUM_POINT_LIGHTS ];\n\t\tvarying vec4 vPointShadowCoord[ NUM_POINT_LIGHTS ];\n\t#endif\n\tfloat texture2DCompare( sampler2D depths, vec2 uv, float compare ) {\n\t\treturn step( compare, unpackRGBAToDepth( texture2D( depths, uv ) ) );\n\t}\n\tfloat texture2DShadowLerp( sampler2D depths, vec2 size, vec2 uv, float compare ) {\n\t\tconst vec2 offset = vec2( 0.0, 1.0 );\n\t\tvec2 texelSize = vec2( 1.0 ) / size;\n\t\tvec2 centroidUV = floor( uv * size + 0.5 ) / size;\n\t\tfloat lb = texture2DCompare( depths, centroidUV + texelSize * offset.xx, compare );\n\t\tfloat lt = texture2DCompare( depths, centroidUV + texelSize * offset.xy, compare );\n\t\tfloat rb = texture2DCompare( depths, centroidUV + texelSize * offset.yx, compare );\n\t\tfloat rt = texture2DCompare( depths, centroidUV + texelSize * offset.yy, compare );\n\t\tvec2 f = fract( uv * size + 0.5 );\n\t\tfloat a = mix( lb, lt, f.y );\n\t\tfloat b = mix( rb, rt, f.y );\n\t\tfloat c = mix( a, b, f.x );\n\t\treturn c;\n\t}\n\tfloat getShadow( sampler2D shadowMap, vec2 shadowMapSize, float shadowBias, float shadowRadius, vec4 shadowCoord ) {\n\t\tshadowCoord.xyz /= shadowCoord.w;\n\t\tshadowCoord.z += shadowBias;\n\t\tbvec4 inFrustumVec = bvec4 ( shadowCoord.x >= 0.0, shadowCoord.x <= 1.0, shadowCoord.y >= 0.0, shadowCoord.y <= 1.0 );\n\t\tbool inFrustum = all( inFrustumVec );\n\t\tbvec2 frustumTestVec = bvec2( inFrustum, shadowCoord.z <= 1.0 );\n\t\tbool frustumTest = all( frustumTestVec );\n\t\tif ( frustumTest ) {\n\t\t#if defined( SHADOWMAP_TYPE_PCF )\n\t\t\tvec2 texelSize = vec2( 1.0 ) / shadowMapSize;\n\t\t\tfloat dx0 = - texelSize.x * shadowRadius;\n\t\t\tfloat dy0 = - texelSize.y * shadowRadius;\n\t\t\tfloat dx1 = + texelSize.x * shadowRadius;\n\t\t\tfloat dy1 = + texelSize.y * shadowRadius;\n\t\t\treturn (\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, 0.0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy, shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, 0.0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, dy1 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy1 ), shadowCoord.z ) +\n\t\t\t\ttexture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, dy1 ), shadowCoord.z )\n\t\t\t) * ( 1.0 / 9.0 );\n\t\t#elif defined( SHADOWMAP_TYPE_PCF_SOFT )\n\t\t\tvec2 texelSize = vec2( 1.0 ) / shadowMapSize;\n\t\t\tfloat dx0 = - texelSize.x * shadowRadius;\n\t\t\tfloat dy0 = - texelSize.y * shadowRadius;\n\t\t\tfloat dx1 = + texelSize.x * shadowRadius;\n\t\t\tfloat dy1 = + texelSize.y * shadowRadius;\n\t\t\treturn (\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx0, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( 0.0, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx1, dy0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx0, 0.0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy, shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx1, 0.0 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx0, dy1 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( 0.0, dy1 ), shadowCoord.z ) +\n\t\t\t\ttexture2DShadowLerp( shadowMap, shadowMapSize, shadowCoord.xy + vec2( dx1, dy1 ), shadowCoord.z )\n\t\t\t) * ( 1.0 / 9.0 );\n\t\t#else\n\t\t\treturn texture2DCompare( shadowMap, shadowCoord.xy, shadowCoord.z );\n\t\t#endif\n\t\t}\n\t\treturn 1.0;\n\t}\n\tvec2 cubeToUV( vec3 v, float texelSizeY ) {\n\t\tvec3 absV = abs( v );\n\t\tfloat scaleToCube = 1.0 / max( absV.x, max( absV.y, absV.z ) );\n\t\tabsV *= scaleToCube;\n\t\tv *= scaleToCube * ( 1.0 - 2.0 * texelSizeY );\n\t\tvec2 planar = v.xy;\n\t\tfloat almostATexel = 1.5 * texelSizeY;\n\t\tfloat almostOne = 1.0 - almostATexel;\n\t\tif ( absV.z >= almostOne ) {\n\t\t\tif ( v.z > 0.0 )\n\t\t\t\tplanar.x = 4.0 - v.x;\n\t\t} else if ( absV.x >= almostOne ) {\n\t\t\tfloat signX = sign( v.x );\n\t\t\tplanar.x = v.z * signX + 2.0 * signX;\n\t\t} else if ( absV.y >= almostOne ) {\n\t\t\tfloat signY = sign( v.y );\n\t\t\tplanar.x = v.x + 2.0 * signY + 2.0;\n\t\t\tplanar.y = v.z * signY - 2.0;\n\t\t}\n\t\treturn vec2( 0.125, 0.25 ) * planar + vec2( 0.375, 0.75 );\n\t}\n\tfloat getPointShadow( sampler2D shadowMap, vec2 shadowMapSize, float shadowBias, float shadowRadius, vec4 shadowCoord ) {\n\t\tvec2 texelSize = vec2( 1.0 ) / ( shadowMapSize * vec2( 4.0, 2.0 ) );\n\t\tvec3 lightToPosition = shadowCoord.xyz;\n\t\tvec3 bd3D = normalize( lightToPosition );\n\t\tfloat dp = ( length( lightToPosition ) - shadowBias ) / 1000.0;\n\t\t#if defined( SHADOWMAP_TYPE_PCF ) || defined( SHADOWMAP_TYPE_PCF_SOFT )\n\t\t\tvec2 offset = vec2( - 1, 1 ) * shadowRadius * texelSize.y;\n\t\t\treturn (\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.xyy, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.yyy, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.xyx, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.yyx, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.xxy, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.yxy, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.xxx, texelSize.y ), dp ) +\n\t\t\t\ttexture2DCompare( shadowMap, cubeToUV( bd3D + offset.yxx, texelSize.y ), dp )\n\t\t\t) * ( 1.0 / 9.0 );\n\t\t#else\n\t\t\treturn texture2DCompare( shadowMap, cubeToUV( bd3D, texelSize.y ), dp );\n\t\t#endif\n\t}\n#endif\n',
    shadowmap_pars_vertex:
      '#ifdef USE_SHADOWMAP\n\t#if NUM_DIR_LIGHTS > 0\n\t\tuniform mat4 directionalShadowMatrix[ NUM_DIR_LIGHTS ];\n\t\tvarying vec4 vDirectionalShadowCoord[ NUM_DIR_LIGHTS ];\n\t#endif\n\t#if NUM_SPOT_LIGHTS > 0\n\t\tuniform mat4 spotShadowMatrix[ NUM_SPOT_LIGHTS ];\n\t\tvarying vec4 vSpotShadowCoord[ NUM_SPOT_LIGHTS ];\n\t#endif\n\t#if NUM_POINT_LIGHTS > 0\n\t\tuniform mat4 pointShadowMatrix[ NUM_POINT_LIGHTS ];\n\t\tvarying vec4 vPointShadowCoord[ NUM_POINT_LIGHTS ];\n\t#endif\n#endif\n',
    shadowmap_vertex:
      '#ifdef USE_SHADOWMAP\n\t#if NUM_DIR_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_DIR_LIGHTS; i ++ ) {\n\t\tvDirectionalShadowCoord[ i ] = directionalShadowMatrix[ i ] * worldPosition;\n\t}\n\t#endif\n\t#if NUM_SPOT_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_SPOT_LIGHTS; i ++ ) {\n\t\tvSpotShadowCoord[ i ] = spotShadowMatrix[ i ] * worldPosition;\n\t}\n\t#endif\n\t#if NUM_POINT_LIGHTS > 0\n\tfor ( int i = 0; i < NUM_POINT_LIGHTS; i ++ ) {\n\t\tvPointShadowCoord[ i ] = pointShadowMatrix[ i ] * worldPosition;\n\t}\n\t#endif\n#endif\n',
    shadowmask_pars_fragment:
      'float getShadowMask() {\n\tfloat shadow = 1.0;\n\t#ifdef USE_SHADOWMAP\n\t#if NUM_DIR_LIGHTS > 0\n\tDirectionalLight directionalLight;\n\tfor ( int i = 0; i < NUM_DIR_LIGHTS; i ++ ) {\n\t\tdirectionalLight = directionalLights[ i ];\n\t\tshadow *= bool( directionalLight.shadow ) ? getShadow( directionalShadowMap[ i ], directionalLight.shadowMapSize, directionalLight.shadowBias, directionalLight.shadowRadius, vDirectionalShadowCoord[ i ] ) : 1.0;\n\t}\n\t#endif\n\t#if NUM_SPOT_LIGHTS > 0\n\tSpotLight spotLight;\n\tfor ( int i = 0; i < NUM_SPOT_LIGHTS; i ++ ) {\n\t\tspotLight = spotLights[ i ];\n\t\tshadow *= bool( spotLight.shadow ) ? getShadow( spotShadowMap[ i ], spotLight.shadowMapSize, spotLight.shadowBias, spotLight.shadowRadius, vSpotShadowCoord[ i ] ) : 1.0;\n\t}\n\t#endif\n\t#if NUM_POINT_LIGHTS > 0\n\tPointLight pointLight;\n\tfor ( int i = 0; i < NUM_POINT_LIGHTS; i ++ ) {\n\t\tpointLight = pointLights[ i ];\n\t\tshadow *= bool( pointLight.shadow ) ? getPointShadow( pointShadowMap[ i ], pointLight.shadowMapSize, pointLight.shadowBias, pointLight.shadowRadius, vPointShadowCoord[ i ] ) : 1.0;\n\t}\n\t#endif\n\t#endif\n\treturn shadow;\n}\n',
    skinbase_vertex:
      '#ifdef USE_SKINNING\n\tmat4 boneMatX = getBoneMatrix( skinIndex.x );\n\tmat4 boneMatY = getBoneMatrix( skinIndex.y );\n\tmat4 boneMatZ = getBoneMatrix( skinIndex.z );\n\tmat4 boneMatW = getBoneMatrix( skinIndex.w );\n#endif',
    skinning_pars_vertex:
      '#ifdef USE_SKINNING\n\tuniform mat4 bindMatrix;\n\tuniform mat4 bindMatrixInverse;\n\t#ifdef BONE_TEXTURE\n\t\tuniform sampler2D boneTexture;\n\t\tuniform int boneTextureWidth;\n\t\tuniform int boneTextureHeight;\n\t\tmat4 getBoneMatrix( const in float i ) {\n\t\t\tfloat j = i * 4.0;\n\t\t\tfloat x = mod( j, float( boneTextureWidth ) );\n\t\t\tfloat y = floor( j / float( boneTextureWidth ) );\n\t\t\tfloat dx = 1.0 / float( boneTextureWidth );\n\t\t\tfloat dy = 1.0 / float( boneTextureHeight );\n\t\t\ty = dy * ( y + 0.5 );\n\t\t\tvec4 v1 = texture2D( boneTexture, vec2( dx * ( x + 0.5 ), y ) );\n\t\t\tvec4 v2 = texture2D( boneTexture, vec2( dx * ( x + 1.5 ), y ) );\n\t\t\tvec4 v3 = texture2D( boneTexture, vec2( dx * ( x + 2.5 ), y ) );\n\t\t\tvec4 v4 = texture2D( boneTexture, vec2( dx * ( x + 3.5 ), y ) );\n\t\t\tmat4 bone = mat4( v1, v2, v3, v4 );\n\t\t\treturn bone;\n\t\t}\n\t#else\n\t\tuniform mat4 boneMatrices[ MAX_BONES ];\n\t\tmat4 getBoneMatrix( const in float i ) {\n\t\t\tmat4 bone = boneMatrices[ int(i) ];\n\t\t\treturn bone;\n\t\t}\n\t#endif\n#endif\n',
    skinning_vertex:
      '#ifdef USE_SKINNING\n\tvec4 skinVertex = bindMatrix * vec4( transformed, 1.0 );\n\tvec4 skinned = vec4( 0.0 );\n\tskinned += boneMatX * skinVertex * skinWeight.x;\n\tskinned += boneMatY * skinVertex * skinWeight.y;\n\tskinned += boneMatZ * skinVertex * skinWeight.z;\n\tskinned += boneMatW * skinVertex * skinWeight.w;\n\tskinned  = bindMatrixInverse * skinned;\n#endif\n',
    skinnormal_vertex:
      '#ifdef USE_SKINNING\n\tmat4 skinMatrix = mat4( 0.0 );\n\tskinMatrix += skinWeight.x * boneMatX;\n\tskinMatrix += skinWeight.y * boneMatY;\n\tskinMatrix += skinWeight.z * boneMatZ;\n\tskinMatrix += skinWeight.w * boneMatW;\n\tskinMatrix  = bindMatrixInverse * skinMatrix * bindMatrix;\n\tobjectNormal = vec4( skinMatrix * vec4( objectNormal, 0.0 ) ).xyz;\n#endif\n',
    specularmap_fragment:
      'float specularStrength;\n#ifdef USE_SPECULARMAP\n\tvec4 texelSpecular = texture2D( specularMap, vUv );\n\tspecularStrength = texelSpecular.r;\n#else\n\tspecularStrength = 1.0;\n#endif',
    specularmap_pars_fragment:
      '#ifdef USE_SPECULARMAP\n\tuniform sampler2D specularMap;\n#endif',
    tonemapping_fragment:
      '#if defined( TONE_MAPPING )\n  gl_FragColor.rgb = toneMapping( gl_FragColor.rgb );\n#endif\n',
    tonemapping_pars_fragment:
      '#define saturate(a) clamp( a, 0.0, 1.0 )\nuniform float toneMappingExposure;\nuniform float toneMappingWhitePoint;\nvec3 LinearToneMapping( vec3 color ) {\n  return toneMappingExposure * color;\n}\nvec3 ReinhardToneMapping( vec3 color ) {\n  color *= toneMappingExposure;\n  return saturate( color / ( vec3( 1.0 ) + color ) );\n}\n#define Uncharted2Helper( x ) max( ( ( x * ( 0.15 * x + 0.10 * 0.50 ) + 0.20 * 0.02 ) / ( x * ( 0.15 * x + 0.50 ) + 0.20 * 0.30 ) ) - 0.02 / 0.30, vec3( 0.0 ) )\nvec3 Uncharted2ToneMapping( vec3 color ) {\n  color *= toneMappingExposure;\n  return saturate( Uncharted2Helper( color ) / Uncharted2Helper( vec3( toneMappingWhitePoint ) ) );\n}\nvec3 OptimizedCineonToneMapping( vec3 color ) {\n  color *= toneMappingExposure;\n  color = max( vec3( 0.0 ), color - 0.004 );\n  return pow( ( color * ( 6.2 * color + 0.5 ) ) / ( color * ( 6.2 * color + 1.7 ) + 0.06 ), vec3( 2.2 ) );\n}\n',
    uv_pars_fragment:
      '#if defined( USE_MAP ) || defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( USE_SPECULARMAP ) || defined( USE_ALPHAMAP ) || defined( USE_EMISSIVEMAP ) || defined( USE_ROUGHNESSMAP ) || defined( USE_METALNESSMAP )\n\tvarying vec2 vUv;\n#endif',
    uv_pars_vertex:
      '#if defined( USE_MAP ) || defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( USE_SPECULARMAP ) || defined( USE_ALPHAMAP ) || defined( USE_EMISSIVEMAP ) || defined( USE_ROUGHNESSMAP ) || defined( USE_METALNESSMAP )\n\tvarying vec2 vUv;\n\tuniform vec4 offsetRepeat;\n#endif\n',
    uv_vertex:
      '#if defined( USE_MAP ) || defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( USE_SPECULARMAP ) || defined( USE_ALPHAMAP ) || defined( USE_EMISSIVEMAP ) || defined( USE_ROUGHNESSMAP ) || defined( USE_METALNESSMAP )\n\tvUv = uv * offsetRepeat.zw + offsetRepeat.xy;\n#endif',
    uv2_pars_fragment:
      '#if defined( USE_LIGHTMAP ) || defined( USE_AOMAP )\n\tvarying vec2 vUv2;\n#endif',
    uv2_pars_vertex:
      '#if defined( USE_LIGHTMAP ) || defined( USE_AOMAP )\n\tattribute vec2 uv2;\n\tvarying vec2 vUv2;\n#endif',
    uv2_vertex:
      '#if defined( USE_LIGHTMAP ) || defined( USE_AOMAP )\n\tvUv2 = uv2;\n#endif',
    worldpos_vertex:
      '#if defined( USE_ENVMAP ) || defined( PHONG ) || defined( PHYSICAL ) || defined( LAMBERT ) || defined ( USE_SHADOWMAP )\n\t#ifdef USE_SKINNING\n\t\tvec4 worldPosition = modelMatrix * skinned;\n\t#else\n\t\tvec4 worldPosition = modelMatrix * vec4( transformed, 1.0 );\n\t#endif\n#endif\n',
    cube_frag:
      'uniform samplerCube tCube;\nuniform float tFlip;\nuniform float opacity;\nvarying vec3 vWorldPosition;\n#include <common>\nvoid main() {\n\tgl_FragColor = textureCube( tCube, vec3( tFlip * vWorldPosition.x, vWorldPosition.yz ) );\n\tgl_FragColor.a *= opacity;\n}\n',
    cube_vert:
      'varying vec3 vWorldPosition;\n#include <common>\nvoid main() {\n\tvWorldPosition = transformDirection( position, modelMatrix );\n\t#include <begin_vertex>\n\t#include <project_vertex>\n}\n',
    depth_frag:
      '#if DEPTH_PACKING == 3200\n\tuniform float opacity;\n#endif\n#include <common>\n#include <packing>\n#include <uv_pars_fragment>\n#include <map_pars_fragment>\n#include <alphamap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec4 diffuseColor = vec4( 1.0 );\n\t#if DEPTH_PACKING == 3200\n\t\tdiffuseColor.a = opacity;\n\t#endif\n\t#include <map_fragment>\n\t#include <alphamap_fragment>\n\t#include <alphatest_fragment>\n\t#include <logdepthbuf_fragment>\n\t#if DEPTH_PACKING == 3200\n\t\tgl_FragColor = vec4( vec3( gl_FragCoord.z ), opacity );\n\t#elif DEPTH_PACKING == 3201\n\t\tgl_FragColor = packDepthToRGBA( gl_FragCoord.z );\n\t#endif\n}\n',
    depth_vert:
      '#include <common>\n#include <uv_pars_vertex>\n#include <displacementmap_pars_vertex>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <uv_vertex>\n\t#include <skinbase_vertex>\n\t#include <begin_vertex>\n\t#include <displacementmap_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n}\n',
    distanceRGBA_frag:
      'uniform vec3 lightPos;\nvarying vec4 vWorldPosition;\n#include <common>\n#include <packing>\n#include <clipping_planes_pars_fragment>\nvoid main () {\n\t#include <clipping_planes_fragment>\n\tgl_FragColor = packDepthToRGBA( length( vWorldPosition.xyz - lightPos.xyz ) / 1000.0 );\n}\n',
    distanceRGBA_vert:
      'varying vec4 vWorldPosition;\n#include <common>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <skinbase_vertex>\n\t#include <begin_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <worldpos_vertex>\n\t#include <clipping_planes_vertex>\n\tvWorldPosition = worldPosition;\n}\n',
    equirect_frag:
      'uniform sampler2D tEquirect;\nuniform float tFlip;\nvarying vec3 vWorldPosition;\n#include <common>\nvoid main() {\n\tvec3 direction = normalize( vWorldPosition );\n\tvec2 sampleUV;\n\tsampleUV.y = saturate( tFlip * direction.y * -0.5 + 0.5 );\n\tsampleUV.x = atan( direction.z, direction.x ) * RECIPROCAL_PI2 + 0.5;\n\tgl_FragColor = texture2D( tEquirect, sampleUV );\n}\n',
    equirect_vert:
      'varying vec3 vWorldPosition;\n#include <common>\nvoid main() {\n\tvWorldPosition = transformDirection( position, modelMatrix );\n\t#include <begin_vertex>\n\t#include <project_vertex>\n}\n',
    linedashed_frag:
      'uniform vec3 diffuse;\nuniform float opacity;\nuniform float dashSize;\nuniform float totalSize;\nvarying float vLineDistance;\n#include <common>\n#include <color_pars_fragment>\n#include <fog_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tif ( mod( vLineDistance, totalSize ) > dashSize ) {\n\t\tdiscard;\n\t}\n\tvec3 outgoingLight = vec3( 0.0 );\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\t#include <logdepthbuf_fragment>\n\t#include <color_fragment>\n\toutgoingLight = diffuseColor.rgb;\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n',
    linedashed_vert:
      'uniform float scale;\nattribute float lineDistance;\nvarying float vLineDistance;\n#include <common>\n#include <color_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <color_vertex>\n\tvLineDistance = scale * lineDistance;\n\tvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n\tgl_Position = projectionMatrix * mvPosition;\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n}\n',
    meshbasic_frag:
      'uniform vec3 diffuse;\nuniform float opacity;\n#ifndef FLAT_SHADED\n\tvarying vec3 vNormal;\n#endif\n#include <common>\n#include <color_pars_fragment>\n#include <uv_pars_fragment>\n#include <uv2_pars_fragment>\n#include <map_pars_fragment>\n#include <alphamap_pars_fragment>\n#include <aomap_pars_fragment>\n#include <envmap_pars_fragment>\n#include <fog_pars_fragment>\n#include <specularmap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\t#include <logdepthbuf_fragment>\n\t#include <map_fragment>\n\t#include <color_fragment>\n\t#include <alphamap_fragment>\n\t#include <alphatest_fragment>\n\t#include <specularmap_fragment>\n\tReflectedLight reflectedLight;\n\treflectedLight.directDiffuse = vec3( 0.0 );\n\treflectedLight.directSpecular = vec3( 0.0 );\n\treflectedLight.indirectDiffuse = diffuseColor.rgb;\n\treflectedLight.indirectSpecular = vec3( 0.0 );\n\t#include <aomap_fragment>\n\tvec3 outgoingLight = reflectedLight.indirectDiffuse;\n\t#include <normal_flip>\n\t#include <envmap_fragment>\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n',
    meshbasic_vert:
      '#include <common>\n#include <uv_pars_vertex>\n#include <uv2_pars_vertex>\n#include <envmap_pars_vertex>\n#include <color_pars_vertex>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <uv_vertex>\n\t#include <uv2_vertex>\n\t#include <color_vertex>\n\t#include <skinbase_vertex>\n\t#ifdef USE_ENVMAP\n\t#include <beginnormal_vertex>\n\t#include <morphnormal_vertex>\n\t#include <skinnormal_vertex>\n\t#include <defaultnormal_vertex>\n\t#endif\n\t#include <begin_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <worldpos_vertex>\n\t#include <clipping_planes_vertex>\n\t#include <envmap_vertex>\n}\n',
    meshlambert_frag:
      'uniform vec3 diffuse;\nuniform vec3 emissive;\nuniform float opacity;\nvarying vec3 vLightFront;\n#ifdef DOUBLE_SIDED\n\tvarying vec3 vLightBack;\n#endif\n#include <common>\n#include <packing>\n#include <color_pars_fragment>\n#include <uv_pars_fragment>\n#include <uv2_pars_fragment>\n#include <map_pars_fragment>\n#include <alphamap_pars_fragment>\n#include <aomap_pars_fragment>\n#include <lightmap_pars_fragment>\n#include <emissivemap_pars_fragment>\n#include <envmap_pars_fragment>\n#include <bsdfs>\n#include <lights_pars>\n#include <fog_pars_fragment>\n#include <shadowmap_pars_fragment>\n#include <shadowmask_pars_fragment>\n#include <specularmap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\tReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );\n\tvec3 totalEmissiveRadiance = emissive;\n\t#include <logdepthbuf_fragment>\n\t#include <map_fragment>\n\t#include <color_fragment>\n\t#include <alphamap_fragment>\n\t#include <alphatest_fragment>\n\t#include <specularmap_fragment>\n\t#include <emissivemap_fragment>\n\treflectedLight.indirectDiffuse = getAmbientLightIrradiance( ambientLightColor );\n\t#include <lightmap_fragment>\n\treflectedLight.indirectDiffuse *= BRDF_Diffuse_Lambert( diffuseColor.rgb );\n\t#ifdef DOUBLE_SIDED\n\t\treflectedLight.directDiffuse = ( gl_FrontFacing ) ? vLightFront : vLightBack;\n\t#else\n\t\treflectedLight.directDiffuse = vLightFront;\n\t#endif\n\treflectedLight.directDiffuse *= BRDF_Diffuse_Lambert( diffuseColor.rgb ) * getShadowMask();\n\t#include <aomap_fragment>\n\tvec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + totalEmissiveRadiance;\n\t#include <normal_flip>\n\t#include <envmap_fragment>\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n',
    meshlambert_vert:
      '#define LAMBERT\nvarying vec3 vLightFront;\n#ifdef DOUBLE_SIDED\n\tvarying vec3 vLightBack;\n#endif\n#include <common>\n#include <uv_pars_vertex>\n#include <uv2_pars_vertex>\n#include <envmap_pars_vertex>\n#include <bsdfs>\n#include <lights_pars>\n#include <color_pars_vertex>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <shadowmap_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <uv_vertex>\n\t#include <uv2_vertex>\n\t#include <color_vertex>\n\t#include <beginnormal_vertex>\n\t#include <morphnormal_vertex>\n\t#include <skinbase_vertex>\n\t#include <skinnormal_vertex>\n\t#include <defaultnormal_vertex>\n\t#include <begin_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n\t#include <worldpos_vertex>\n\t#include <envmap_vertex>\n\t#include <lights_lambert_vertex>\n\t#include <shadowmap_vertex>\n}\n',
    meshphong_frag:
      '#define PHONG\nuniform vec3 diffuse;\nuniform vec3 emissive;\nuniform vec3 specular;\nuniform float shininess;\nuniform float opacity;\n#include <common>\n#include <packing>\n#include <color_pars_fragment>\n#include <uv_pars_fragment>\n#include <uv2_pars_fragment>\n#include <map_pars_fragment>\n#include <alphamap_pars_fragment>\n#include <aomap_pars_fragment>\n#include <lightmap_pars_fragment>\n#include <emissivemap_pars_fragment>\n#include <envmap_pars_fragment>\n#include <fog_pars_fragment>\n#include <bsdfs>\n#include <lights_pars>\n#include <lights_phong_pars_fragment>\n#include <shadowmap_pars_fragment>\n#include <bumpmap_pars_fragment>\n#include <normalmap_pars_fragment>\n#include <specularmap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\tReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );\n\tvec3 totalEmissiveRadiance = emissive;\n\t#include <logdepthbuf_fragment>\n\t#include <map_fragment>\n\t#include <color_fragment>\n\t#include <alphamap_fragment>\n\t#include <alphatest_fragment>\n\t#include <specularmap_fragment>\n\t#include <normal_flip>\n\t#include <normal_fragment>\n\t#include <emissivemap_fragment>\n\t#include <lights_phong_fragment>\n\t#include <lights_template>\n\t#include <aomap_fragment>\n\tvec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;\n\t#include <envmap_fragment>\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n',
    meshphong_vert:
      '#define PHONG\nvarying vec3 vViewPosition;\n#ifndef FLAT_SHADED\n\tvarying vec3 vNormal;\n#endif\n#include <common>\n#include <uv_pars_vertex>\n#include <uv2_pars_vertex>\n#include <displacementmap_pars_vertex>\n#include <envmap_pars_vertex>\n#include <color_pars_vertex>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <shadowmap_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <uv_vertex>\n\t#include <uv2_vertex>\n\t#include <color_vertex>\n\t#include <beginnormal_vertex>\n\t#include <morphnormal_vertex>\n\t#include <skinbase_vertex>\n\t#include <skinnormal_vertex>\n\t#include <defaultnormal_vertex>\n#ifndef FLAT_SHADED\n\tvNormal = normalize( transformedNormal );\n#endif\n\t#include <begin_vertex>\n\t#include <displacementmap_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n\tvViewPosition = - mvPosition.xyz;\n\t#include <worldpos_vertex>\n\t#include <envmap_vertex>\n\t#include <shadowmap_vertex>\n}\n',
    meshphysical_frag:
      '#define PHYSICAL\nuniform vec3 diffuse;\nuniform vec3 emissive;\nuniform float roughness;\nuniform float metalness;\nuniform float opacity;\n#ifndef STANDARD\n\tuniform float clearCoat;\n\tuniform float clearCoatRoughness;\n#endif\nuniform float envMapIntensity;\nvarying vec3 vViewPosition;\n#ifndef FLAT_SHADED\n\tvarying vec3 vNormal;\n#endif\n#include <common>\n#include <packing>\n#include <color_pars_fragment>\n#include <uv_pars_fragment>\n#include <uv2_pars_fragment>\n#include <map_pars_fragment>\n#include <alphamap_pars_fragment>\n#include <aomap_pars_fragment>\n#include <lightmap_pars_fragment>\n#include <emissivemap_pars_fragment>\n#include <envmap_pars_fragment>\n#include <fog_pars_fragment>\n#include <bsdfs>\n#include <cube_uv_reflection_fragment>\n#include <lights_pars>\n#include <lights_physical_pars_fragment>\n#include <shadowmap_pars_fragment>\n#include <bumpmap_pars_fragment>\n#include <normalmap_pars_fragment>\n#include <roughnessmap_pars_fragment>\n#include <metalnessmap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\tReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );\n\tvec3 totalEmissiveRadiance = emissive;\n\t#include <logdepthbuf_fragment>\n\t#include <map_fragment>\n\t#include <color_fragment>\n\t#include <alphamap_fragment>\n\t#include <alphatest_fragment>\n\t#include <specularmap_fragment>\n\t#include <roughnessmap_fragment>\n\t#include <metalnessmap_fragment>\n\t#include <normal_flip>\n\t#include <normal_fragment>\n\t#include <emissivemap_fragment>\n\t#include <lights_physical_fragment>\n\t#include <lights_template>\n\t#include <aomap_fragment>\n\tvec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n',
    meshphysical_vert:
      '#define PHYSICAL\nvarying vec3 vViewPosition;\n#ifndef FLAT_SHADED\n\tvarying vec3 vNormal;\n#endif\n#include <common>\n#include <uv_pars_vertex>\n#include <uv2_pars_vertex>\n#include <displacementmap_pars_vertex>\n#include <color_pars_vertex>\n#include <morphtarget_pars_vertex>\n#include <skinning_pars_vertex>\n#include <shadowmap_pars_vertex>\n#include <specularmap_pars_fragment>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <uv_vertex>\n\t#include <uv2_vertex>\n\t#include <color_vertex>\n\t#include <beginnormal_vertex>\n\t#include <morphnormal_vertex>\n\t#include <skinbase_vertex>\n\t#include <skinnormal_vertex>\n\t#include <defaultnormal_vertex>\n#ifndef FLAT_SHADED\n\tvNormal = normalize( transformedNormal );\n#endif\n\t#include <begin_vertex>\n\t#include <displacementmap_vertex>\n\t#include <morphtarget_vertex>\n\t#include <skinning_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n\tvViewPosition = - mvPosition.xyz;\n\t#include <worldpos_vertex>\n\t#include <shadowmap_vertex>\n}\n',
    normal_frag:
      'uniform float opacity;\nvarying vec3 vNormal;\n#include <common>\n#include <packing>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tgl_FragColor = vec4( packNormalToRGB( vNormal ), opacity );\n\t#include <logdepthbuf_fragment>\n}\n',
    normal_vert:
      'varying vec3 vNormal;\n#include <common>\n#include <morphtarget_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\tvNormal = normalize( normalMatrix * normal );\n\t#include <begin_vertex>\n\t#include <morphtarget_vertex>\n\t#include <project_vertex>\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n}\n',
    points_frag:
      'uniform vec3 diffuse;\nuniform float opacity;\n#include <common>\n#include <packing>\n#include <color_pars_fragment>\n#include <map_particle_pars_fragment>\n#include <fog_pars_fragment>\n#include <shadowmap_pars_fragment>\n#include <logdepthbuf_pars_fragment>\n#include <clipping_planes_pars_fragment>\nvoid main() {\n\t#include <clipping_planes_fragment>\n\tvec3 outgoingLight = vec3( 0.0 );\n\tvec4 diffuseColor = vec4( diffuse, opacity );\n\t#include <logdepthbuf_fragment>\n\t#include <map_particle_fragment>\n\t#include <color_fragment>\n\t#include <alphatest_fragment>\n\toutgoingLight = diffuseColor.rgb;\n\tgl_FragColor = vec4( outgoingLight, diffuseColor.a );\n\t#include <premultiplied_alpha_fragment>\n\t#include <tonemapping_fragment>\n\t#include <encodings_fragment>\n\t#include <fog_fragment>\n}\n',
    points_vert:
      'uniform float size;\nuniform float scale;\n#include <common>\n#include <color_pars_vertex>\n#include <shadowmap_pars_vertex>\n#include <logdepthbuf_pars_vertex>\n#include <clipping_planes_pars_vertex>\nvoid main() {\n\t#include <color_vertex>\n\t#include <begin_vertex>\n\t#include <project_vertex>\n\t#ifdef USE_SIZEATTENUATION\n\t\tgl_PointSize = size * ( scale / - mvPosition.z );\n\t#else\n\t\tgl_PointSize = size;\n\t#endif\n\t#include <logdepthbuf_vertex>\n\t#include <clipping_planes_vertex>\n\t#include <worldpos_vertex>\n\t#include <shadowmap_vertex>\n}\n',
    shadow_frag:
      'uniform float opacity;\n#include <common>\n#include <packing>\n#include <bsdfs>\n#include <lights_pars>\n#include <shadowmap_pars_fragment>\n#include <shadowmask_pars_fragment>\nvoid main() {\n\tgl_FragColor = vec4( 0.0, 0.0, 0.0, opacity * ( 1.0  - getShadowMask() ) );\n}\n',
    shadow_vert:
      '#include <shadowmap_pars_vertex>\nvoid main() {\n\t#include <begin_vertex>\n\t#include <project_vertex>\n\t#include <worldpos_vertex>\n\t#include <shadowmap_vertex>\n}\n'
  };
  (V.prototype = {
    constructor: V,
    isColor: !0,
    r: 1,
    g: 1,
    b: 1,
    set: function(t) {
      return (
        t && t.isColor
          ? this.copy(t)
          : 'number' == typeof t
            ? this.setHex(t)
            : 'string' == typeof t && this.setStyle(t),
        this
      );
    },
    setScalar: function(t) {
      this.b = this.g = this.r = t;
    },
    setHex: function(t) {
      return (
        (t = Math.floor(t)),
        (this.r = ((t >> 16) & 255) / 255),
        (this.g = ((t >> 8) & 255) / 255),
        (this.b = (255 & t) / 255),
        this
      );
    },
    setRGB: function(t, e, i) {
      return (this.r = t), (this.g = e), (this.b = i), this;
    },
    setHSL: (function() {
      function e(t, e, i) {
        return (
          0 > i && (i += 1),
          1 < i && --i,
          i < 1 / 6
            ? t + 6 * (e - t) * i
            : 0.5 > i ? e : i < 2 / 3 ? t + 6 * (e - t) * (2 / 3 - i) : t
        );
      }
      return function(i, n, r) {
        return (
          (i = t.Math.euclideanModulo(i, 1)),
          (n = t.Math.clamp(n, 0, 1)),
          (r = t.Math.clamp(r, 0, 1)),
          0 === n
            ? (this.r = this.g = this.b = r)
            : ((n = 0.5 >= r ? r * (1 + n) : r + n - r * n),
              (r = 2 * r - n),
              (this.r = e(r, n, i + 1 / 3)),
              (this.g = e(r, n, i)),
              (this.b = e(r, n, i - 1 / 3))),
          this
        );
      };
    })(),
    setStyle: function(e) {
      function i(t) {
        void 0 !== t &&
          1 > parseFloat(t) &&
          console.warn(
            'THREE.Color: Alpha component of ' + e + ' will be ignored.'
          );
      }
      var n;
      if ((n = /^((?:rgb|hsl)a?)\(\s*([^\)]*)\)/.exec(e))) {
        var r = n[2];
        switch (n[1]) {
          case 'rgb':
          case 'rgba':
            if (
              (n = /^(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*(,\s*([0-9]*\.?[0-9]+)\s*)?$/.exec(
                r
              ))
            )
              return (
                (this.r = Math.min(255, parseInt(n[1], 10)) / 255),
                (this.g = Math.min(255, parseInt(n[2], 10)) / 255),
                (this.b = Math.min(255, parseInt(n[3], 10)) / 255),
                i(n[5]),
                this
              );
            if (
              (n = /^(\d+)\%\s*,\s*(\d+)\%\s*,\s*(\d+)\%\s*(,\s*([0-9]*\.?[0-9]+)\s*)?$/.exec(
                r
              ))
            )
              return (
                (this.r = Math.min(100, parseInt(n[1], 10)) / 100),
                (this.g = Math.min(100, parseInt(n[2], 10)) / 100),
                (this.b = Math.min(100, parseInt(n[3], 10)) / 100),
                i(n[5]),
                this
              );
            break;
          case 'hsl':
          case 'hsla':
            if (
              (n = /^([0-9]*\.?[0-9]+)\s*,\s*(\d+)\%\s*,\s*(\d+)\%\s*(,\s*([0-9]*\.?[0-9]+)\s*)?$/.exec(
                r
              ))
            ) {
              var r = parseFloat(n[1]) / 360,
                a = parseInt(n[2], 10) / 100,
                o = parseInt(n[3], 10) / 100;
              return i(n[5]), this.setHSL(r, a, o);
            }
        }
      } else if ((n = /^\#([A-Fa-f0-9]+)$/.exec(e))) {
        if (((n = n[1]), 3 === (r = n.length)))
          return (
            (this.r = parseInt(n.charAt(0) + n.charAt(0), 16) / 255),
            (this.g = parseInt(n.charAt(1) + n.charAt(1), 16) / 255),
            (this.b = parseInt(n.charAt(2) + n.charAt(2), 16) / 255),
            this
          );
        if (6 === r)
          return (
            (this.r = parseInt(n.charAt(0) + n.charAt(1), 16) / 255),
            (this.g = parseInt(n.charAt(2) + n.charAt(3), 16) / 255),
            (this.b = parseInt(n.charAt(4) + n.charAt(5), 16) / 255),
            this
          );
      }
      return (
        e &&
          0 < e.length &&
          ((n = t.ColorKeywords[e]),
          void 0 !== n
            ? this.setHex(n)
            : console.warn('THREE.Color: Unknown color ' + e)),
        this
      );
    },
    clone: function() {
      return new this.constructor(this.r, this.g, this.b);
    },
    copy: function(t) {
      return (this.r = t.r), (this.g = t.g), (this.b = t.b), this;
    },
    copyGammaToLinear: function(t, e) {
      return (
        void 0 === e && (e = 2),
        (this.r = Math.pow(t.r, e)),
        (this.g = Math.pow(t.g, e)),
        (this.b = Math.pow(t.b, e)),
        this
      );
    },
    copyLinearToGamma: function(t, e) {
      void 0 === e && (e = 2);
      var i = 0 < e ? 1 / e : 1;
      return (
        (this.r = Math.pow(t.r, i)),
        (this.g = Math.pow(t.g, i)),
        (this.b = Math.pow(t.b, i)),
        this
      );
    },
    convertGammaToLinear: function() {
      var t = this.r,
        e = this.g,
        i = this.b;
      return (this.r = t * t), (this.g = e * e), (this.b = i * i), this;
    },
    convertLinearToGamma: function() {
      return (
        (this.r = Math.sqrt(this.r)),
        (this.g = Math.sqrt(this.g)),
        (this.b = Math.sqrt(this.b)),
        this
      );
    },
    getHex: function() {
      return (
        ((255 * this.r) << 16) ^ ((255 * this.g) << 8) ^ ((255 * this.b) << 0)
      );
    },
    getHexString: function() {
      return ('000000' + this.getHex().toString(16)).slice(-6);
    },
    getHSL: function(t) {
      t = t || { h: 0, s: 0, l: 0 };
      var e,
        i = this.r,
        n = this.g,
        r = this.b,
        a = Math.max(i, n, r),
        o = Math.min(i, n, r),
        s = (o + a) / 2;
      if (o === a) o = e = 0;
      else {
        var c = a - o,
          o = 0.5 >= s ? c / (a + o) : c / (2 - a - o);
        switch (a) {
          case i:
            e = (n - r) / c + (n < r ? 6 : 0);
            break;
          case n:
            e = (r - i) / c + 2;
            break;
          case r:
            e = (i - n) / c + 4;
        }
        e /= 6;
      }
      return (t.h = e), (t.s = o), (t.l = s), t;
    },
    getStyle: function() {
      return (
        'rgb(' +
        ((255 * this.r) | 0) +
        ',' +
        ((255 * this.g) | 0) +
        ',' +
        ((255 * this.b) | 0) +
        ')'
      );
    },
    offsetHSL: function(t, e, i) {
      var n = this.getHSL();
      return (
        (n.h += t), (n.s += e), (n.l += i), this.setHSL(n.h, n.s, n.l), this
      );
    },
    add: function(t) {
      return (this.r += t.r), (this.g += t.g), (this.b += t.b), this;
    },
    addColors: function(t, e) {
      return (
        (this.r = t.r + e.r), (this.g = t.g + e.g), (this.b = t.b + e.b), this
      );
    },
    addScalar: function(t) {
      return (this.r += t), (this.g += t), (this.b += t), this;
    },
    sub: function(t) {
      return (
        (this.r = Math.max(0, this.r - t.r)),
        (this.g = Math.max(0, this.g - t.g)),
        (this.b = Math.max(0, this.b - t.b)),
        this
      );
    },
    multiply: function(t) {
      return (this.r *= t.r), (this.g *= t.g), (this.b *= t.b), this;
    },
    multiplyScalar: function(t) {
      return (this.r *= t), (this.g *= t), (this.b *= t), this;
    },
    lerp: function(t, e) {
      return (
        (this.r += (t.r - this.r) * e),
        (this.g += (t.g - this.g) * e),
        (this.b += (t.b - this.b) * e),
        this
      );
    },
    equals: function(t) {
      return t.r === this.r && t.g === this.g && t.b === this.b;
    },
    fromArray: function(t, e) {
      return (
        void 0 === e && (e = 0),
        (this.r = t[e]),
        (this.g = t[e + 1]),
        (this.b = t[e + 2]),
        this
      );
    },
    toArray: function(t, e) {
      return (
        void 0 === t && (t = []),
        void 0 === e && (e = 0),
        (t[e] = this.r),
        (t[e + 1] = this.g),
        (t[e + 2] = this.b),
        t
      );
    },
    toJSON: function() {
      return this.getHex();
    }
  }),
    (t.ColorKeywords = {
      aliceblue: 15792383,
      antiquewhite: 16444375,
      aqua: 65535,
      aquamarine: 8388564,
      azure: 15794175,
      beige: 16119260,
      bisque: 16770244,
      black: 0,
      blanchedalmond: 16772045,
      blue: 255,
      blueviolet: 9055202,
      brown: 10824234,
      burlywood: 14596231,
      cadetblue: 6266528,
      chartreuse: 8388352,
      chocolate: 13789470,
      coral: 16744272,
      cornflowerblue: 6591981,
      cornsilk: 16775388,
      crimson: 14423100,
      cyan: 65535,
      darkblue: 139,
      darkcyan: 35723,
      darkgoldenrod: 12092939,
      darkgray: 11119017,
      darkgreen: 25600,
      darkgrey: 11119017,
      darkkhaki: 12433259,
      darkmagenta: 9109643,
      darkolivegreen: 5597999,
      darkorange: 16747520,
      darkorchid: 10040012,
      darkred: 9109504,
      darksalmon: 15308410,
      darkseagreen: 9419919,
      darkslateblue: 4734347,
      darkslategray: 3100495,
      darkslategrey: 3100495,
      darkturquoise: 52945,
      darkviolet: 9699539,
      deeppink: 16716947,
      deepskyblue: 49151,
      dimgray: 6908265,
      dimgrey: 6908265,
      dodgerblue: 2003199,
      firebrick: 11674146,
      floralwhite: 16775920,
      forestgreen: 2263842,
      fuchsia: 16711935,
      gainsboro: 14474460,
      ghostwhite: 16316671,
      gold: 16766720,
      goldenrod: 14329120,
      gray: 8421504,
      green: 32768,
      greenyellow: 11403055,
      grey: 8421504,
      honeydew: 15794160,
      hotpink: 16738740,
      indianred: 13458524,
      indigo: 4915330,
      ivory: 16777200,
      khaki: 15787660,
      lavender: 15132410,
      lavenderblush: 16773365,
      lawngreen: 8190976,
      lemonchiffon: 16775885,
      lightblue: 11393254,
      lightcoral: 15761536,
      lightcyan: 14745599,
      lightgoldenrodyellow: 16448210,
      lightgray: 13882323,
      lightgreen: 9498256,
      lightgrey: 13882323,
      lightpink: 16758465,
      lightsalmon: 16752762,
      lightseagreen: 2142890,
      lightskyblue: 8900346,
      lightslategray: 7833753,
      lightslategrey: 7833753,
      lightsteelblue: 11584734,
      lightyellow: 16777184,
      lime: 65280,
      limegreen: 3329330,
      linen: 16445670,
      magenta: 16711935,
      maroon: 8388608,
      mediumaquamarine: 6737322,
      mediumblue: 205,
      mediumorchid: 12211667,
      mediumpurple: 9662683,
      mediumseagreen: 3978097,
      mediumslateblue: 8087790,
      mediumspringgreen: 64154,
      mediumturquoise: 4772300,
      mediumvioletred: 13047173,
      midnightblue: 1644912,
      mintcream: 16121850,
      mistyrose: 16770273,
      moccasin: 16770229,
      navajowhite: 16768685,
      navy: 128,
      oldlace: 16643558,
      olive: 8421376,
      olivedrab: 7048739,
      orange: 16753920,
      orangered: 16729344,
      orchid: 14315734,
      palegoldenrod: 15657130,
      palegreen: 10025880,
      paleturquoise: 11529966,
      palevioletred: 14381203,
      papayawhip: 16773077,
      peachpuff: 16767673,
      peru: 13468991,
      pink: 16761035,
      plum: 14524637,
      powderblue: 11591910,
      purple: 8388736,
      red: 16711680,
      rosybrown: 12357519,
      royalblue: 4286945,
      saddlebrown: 9127187,
      salmon: 16416882,
      sandybrown: 16032864,
      seagreen: 3050327,
      seashell: 16774638,
      sienna: 10506797,
      silver: 12632256,
      skyblue: 8900331,
      slateblue: 6970061,
      slategray: 7372944,
      slategrey: 7372944,
      snow: 16775930,
      springgreen: 65407,
      steelblue: 4620980,
      tan: 13808780,
      teal: 32896,
      thistle: 14204888,
      tomato: 16737095,
      turquoise: 4251856,
      violet: 15631086,
      wheat: 16113331,
      white: 16777215,
      whitesmoke: 16119285,
      yellow: 16776960,
      yellowgreen: 10145074
    });
  var Xn = {
      common: {
        diffuse: { value: new V(15658734) },
        opacity: { value: 1 },
        map: { value: null },
        offsetRepeat: { value: new r(0, 0, 1, 1) },
        specularMap: { value: null },
        alphaMap: { value: null },
        envMap: { value: null },
        flipEnvMap: { value: -1 },
        reflectivity: { value: 1 },
        refractionRatio: { value: 0.98 }
      },
      aomap: { aoMap: { value: null }, aoMapIntensity: { value: 1 } },
      lightmap: { lightMap: { value: null }, lightMapIntensity: { value: 1 } },
      emissivemap: { emissiveMap: { value: null } },
      bumpmap: { bumpMap: { value: null }, bumpScale: { value: 1 } },
      normalmap: {
        normalMap: { value: null },
        normalScale: { value: new i(1, 1) }
      },
      displacementmap: {
        displacementMap: { value: null },
        displacementScale: { value: 1 },
        displacementBias: { value: 0 }
      },
      roughnessmap: { roughnessMap: { value: null } },
      metalnessmap: { metalnessMap: { value: null } },
      fog: {
        fogDensity: { value: 25e-5 },
        fogNear: { value: 1 },
        fogFar: { value: 2e3 },
        fogColor: { value: new V(16777215) }
      },
      lights: {
        ambientLightColor: { value: [] },
        directionalLights: {
          value: [],
          properties: {
            direction: {},
            color: {},
            shadow: {},
            shadowBias: {},
            shadowRadius: {},
            shadowMapSize: {}
          }
        },
        directionalShadowMap: { value: [] },
        directionalShadowMatrix: { value: [] },
        spotLights: {
          value: [],
          properties: {
            color: {},
            position: {},
            direction: {},
            distance: {},
            coneCos: {},
            penumbraCos: {},
            decay: {},
            shadow: {},
            shadowBias: {},
            shadowRadius: {},
            shadowMapSize: {}
          }
        },
        spotShadowMap: { value: [] },
        spotShadowMatrix: { value: [] },
        pointLights: {
          value: [],
          properties: {
            color: {},
            position: {},
            decay: {},
            distance: {},
            shadow: {},
            shadowBias: {},
            shadowRadius: {},
            shadowMapSize: {}
          }
        },
        pointShadowMap: { value: [] },
        pointShadowMatrix: { value: [] },
        hemisphereLights: {
          value: [],
          properties: { direction: {}, skyColor: {}, groundColor: {} }
        }
      },
      points: {
        diffuse: { value: new V(15658734) },
        opacity: { value: 1 },
        size: { value: 1 },
        scale: { value: 1 },
        map: { value: null },
        offsetRepeat: { value: new r(0, 0, 1, 1) }
      }
    },
    qn = {
      basic: {
        uniforms: t.UniformsUtils.merge([Xn.common, Xn.aomap, Xn.fog]),
        vertexShader: Wn.meshbasic_vert,
        fragmentShader: Wn.meshbasic_frag
      },
      lambert: {
        uniforms: t.UniformsUtils.merge([
          Xn.common,
          Xn.aomap,
          Xn.lightmap,
          Xn.emissivemap,
          Xn.fog,
          Xn.lights,
          { emissive: { value: new V(0) } }
        ]),
        vertexShader: Wn.meshlambert_vert,
        fragmentShader: Wn.meshlambert_frag
      },
      phong: {
        uniforms: t.UniformsUtils.merge([
          Xn.common,
          Xn.aomap,
          Xn.lightmap,
          Xn.emissivemap,
          Xn.bumpmap,
          Xn.normalmap,
          Xn.displacementmap,
          Xn.fog,
          Xn.lights,
          {
            emissive: { value: new V(0) },
            specular: { value: new V(1118481) },
            shininess: { value: 30 }
          }
        ]),
        vertexShader: Wn.meshphong_vert,
        fragmentShader: Wn.meshphong_frag
      },
      standard: {
        uniforms: t.UniformsUtils.merge([
          Xn.common,
          Xn.aomap,
          Xn.lightmap,
          Xn.emissivemap,
          Xn.bumpmap,
          Xn.normalmap,
          Xn.displacementmap,
          Xn.roughnessmap,
          Xn.metalnessmap,
          Xn.fog,
          Xn.lights,
          {
            emissive: { value: new V(0) },
            roughness: { value: 0.5 },
            metalness: { value: 0 },
            envMapIntensity: { value: 1 }
          }
        ]),
        vertexShader: Wn.meshphysical_vert,
        fragmentShader: Wn.meshphysical_frag
      },
      points: {
        uniforms: t.UniformsUtils.merge([Xn.points, Xn.fog]),
        vertexShader: Wn.points_vert,
        fragmentShader: Wn.points_frag
      },
      dashed: {
        uniforms: t.UniformsUtils.merge([
          Xn.common,
          Xn.fog,
          {
            scale: { value: 1 },
            dashSize: { value: 1 },
            totalSize: { value: 2 }
          }
        ]),
        vertexShader: Wn.linedashed_vert,
        fragmentShader: Wn.linedashed_frag
      },
      depth: {
        uniforms: t.UniformsUtils.merge([Xn.common, Xn.displacementmap]),
        vertexShader: Wn.depth_vert,
        fragmentShader: Wn.depth_frag
      },
      normal: {
        uniforms: { opacity: { value: 1 } },
        vertexShader: Wn.normal_vert,
        fragmentShader: Wn.normal_frag
      },
      cube: {
        uniforms: {
          tCube: { value: null },
          tFlip: { value: -1 },
          opacity: { value: 1 }
        },
        vertexShader: Wn.cube_vert,
        fragmentShader: Wn.cube_frag
      },
      equirect: {
        uniforms: { tEquirect: { value: null }, tFlip: { value: -1 } },
        vertexShader: Wn.equirect_vert,
        fragmentShader: Wn.equirect_frag
      },
      distanceRGBA: {
        uniforms: { lightPos: { value: new c() } },
        vertexShader: Wn.distanceRGBA_vert,
        fragmentShader: Wn.distanceRGBA_frag
      }
    };
  (qn.physical = {
    uniforms: t.UniformsUtils.merge([
      qn.standard.uniforms,
      { clearCoat: { value: 0 }, clearCoatRoughness: { value: 0 } }
    ]),
    vertexShader: Wn.meshphysical_vert,
    fragmentShader: Wn.meshphysical_frag
  }),
    (k.prototype = {
      constructor: k,
      set: function(t, e) {
        return this.min.copy(t), this.max.copy(e), this;
      },
      setFromPoints: function(t) {
        this.makeEmpty();
        for (var e = 0, i = t.length; e < i; e++) this.expandByPoint(t[e]);
        return this;
      },
      setFromCenterAndSize: (function() {
        var t = new i();
        return function(e, i) {
          var n = t.copy(i).multiplyScalar(0.5);
          return this.min.copy(e).sub(n), this.max.copy(e).add(n), this;
        };
      })(),
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        return this.min.copy(t.min), this.max.copy(t.max), this;
      },
      makeEmpty: function() {
        return (
          (this.min.x = this.min.y = Infinity),
          (this.max.x = this.max.y = -Infinity),
          this
        );
      },
      isEmpty: function() {
        return this.max.x < this.min.x || this.max.y < this.min.y;
      },
      getCenter: function(t) {
        return (
          (t = t || new i()),
          this.isEmpty()
            ? t.set(0, 0)
            : t.addVectors(this.min, this.max).multiplyScalar(0.5)
        );
      },
      getSize: function(t) {
        return (
          (t = t || new i()),
          this.isEmpty() ? t.set(0, 0) : t.subVectors(this.max, this.min)
        );
      },
      expandByPoint: function(t) {
        return this.min.min(t), this.max.max(t), this;
      },
      expandByVector: function(t) {
        return this.min.sub(t), this.max.add(t), this;
      },
      expandByScalar: function(t) {
        return this.min.addScalar(-t), this.max.addScalar(t), this;
      },
      containsPoint: function(t) {
        return !(
          t.x < this.min.x ||
          t.x > this.max.x ||
          t.y < this.min.y ||
          t.y > this.max.y
        );
      },
      containsBox: function(t) {
        return (
          this.min.x <= t.min.x &&
          t.max.x <= this.max.x &&
          this.min.y <= t.min.y &&
          t.max.y <= this.max.y
        );
      },
      getParameter: function(t, e) {
        return (e || new i()).set(
          (t.x - this.min.x) / (this.max.x - this.min.x),
          (t.y - this.min.y) / (this.max.y - this.min.y)
        );
      },
      intersectsBox: function(t) {
        return !(
          t.max.x < this.min.x ||
          t.min.x > this.max.x ||
          t.max.y < this.min.y ||
          t.min.y > this.max.y
        );
      },
      clampPoint: function(t, e) {
        return (e || new i()).copy(t).clamp(this.min, this.max);
      },
      distanceToPoint: (function() {
        var t = new i();
        return function(e) {
          return t
            .copy(e)
            .clamp(this.min, this.max)
            .sub(e)
            .length();
        };
      })(),
      intersect: function(t) {
        return this.min.max(t.min), this.max.min(t.max), this;
      },
      union: function(t) {
        return this.min.min(t.min), this.max.max(t.max), this;
      },
      translate: function(t) {
        return this.min.add(t), this.max.add(t), this;
      },
      equals: function(t) {
        return t.min.equals(this.min) && t.max.equals(this.max);
      }
    }),
    (X.prototype = {
      constructor: X,
      isMaterial: !0,
      get needsUpdate() {
        return this._needsUpdate;
      },
      set needsUpdate(t) {
        !0 === t && this.update(), (this._needsUpdate = t);
      },
      setValues: function(t) {
        if (void 0 !== t)
          for (var e in t) {
            var i = t[e];
            if (void 0 === i)
              console.warn(
                "THREE.Material: '" + e + "' parameter is undefined."
              );
            else {
              var n = this[e];
              void 0 === n
                ? console.warn(
                    'THREE.' +
                      this.type +
                      ": '" +
                      e +
                      "' is not a property of this material."
                  )
                : n && n.isColor
                  ? n.set(i)
                  : n && n.isVector3 && i && i.isVector3
                    ? n.copy(i)
                    : (this[e] = 'overdraw' === e ? Number(i) : i);
            }
          }
      },
      toJSON: function(t) {
        function e(t) {
          var e,
            i = [];
          for (e in t) {
            var n = t[e];
            delete n.metadata, i.push(n);
          }
          return i;
        }
        var i = void 0 === t;
        i && (t = { textures: {}, images: {} });
        var n = {
          metadata: {
            version: 4.4,
            type: 'Material',
            generator: 'Material.toJSON'
          }
        };
        return (
          (n.uuid = this.uuid),
          (n.type = this.type),
          '' !== this.name && (n.name = this.name),
          this.color && this.color.isColor && (n.color = this.color.getHex()),
          void 0 !== this.roughness && (n.roughness = this.roughness),
          void 0 !== this.metalness && (n.metalness = this.metalness),
          this.emissive &&
            this.emissive.isColor &&
            (n.emissive = this.emissive.getHex()),
          this.specular &&
            this.specular.isColor &&
            (n.specular = this.specular.getHex()),
          void 0 !== this.shininess && (n.shininess = this.shininess),
          this.map && this.map.isTexture && (n.map = this.map.toJSON(t).uuid),
          this.alphaMap &&
            this.alphaMap.isTexture &&
            (n.alphaMap = this.alphaMap.toJSON(t).uuid),
          this.lightMap &&
            this.lightMap.isTexture &&
            (n.lightMap = this.lightMap.toJSON(t).uuid),
          this.bumpMap &&
            this.bumpMap.isTexture &&
            ((n.bumpMap = this.bumpMap.toJSON(t).uuid),
            (n.bumpScale = this.bumpScale)),
          this.normalMap &&
            this.normalMap.isTexture &&
            ((n.normalMap = this.normalMap.toJSON(t).uuid),
            (n.normalScale = this.normalScale.toArray())),
          this.displacementMap &&
            this.displacementMap.isTexture &&
            ((n.displacementMap = this.displacementMap.toJSON(t).uuid),
            (n.displacementScale = this.displacementScale),
            (n.displacementBias = this.displacementBias)),
          this.roughnessMap &&
            this.roughnessMap.isTexture &&
            (n.roughnessMap = this.roughnessMap.toJSON(t).uuid),
          this.metalnessMap &&
            this.metalnessMap.isTexture &&
            (n.metalnessMap = this.metalnessMap.toJSON(t).uuid),
          this.emissiveMap &&
            this.emissiveMap.isTexture &&
            (n.emissiveMap = this.emissiveMap.toJSON(t).uuid),
          this.specularMap &&
            this.specularMap.isTexture &&
            (n.specularMap = this.specularMap.toJSON(t).uuid),
          this.envMap &&
            this.envMap.isTexture &&
            ((n.envMap = this.envMap.toJSON(t).uuid),
            (n.reflectivity = this.reflectivity)),
          void 0 !== this.size && (n.size = this.size),
          void 0 !== this.sizeAttenuation &&
            (n.sizeAttenuation = this.sizeAttenuation),
          1 !== this.blending && (n.blending = this.blending),
          2 !== this.shading && (n.shading = this.shading),
          0 !== this.side && (n.side = this.side),
          0 !== this.vertexColors && (n.vertexColors = this.vertexColors),
          1 > this.opacity && (n.opacity = this.opacity),
          !0 === this.transparent && (n.transparent = this.transparent),
          (n.depthFunc = this.depthFunc),
          (n.depthTest = this.depthTest),
          (n.depthWrite = this.depthWrite),
          0 < this.alphaTest && (n.alphaTest = this.alphaTest),
          !0 === this.premultipliedAlpha &&
            (n.premultipliedAlpha = this.premultipliedAlpha),
          !0 === this.wireframe && (n.wireframe = this.wireframe),
          1 < this.wireframeLinewidth &&
            (n.wireframeLinewidth = this.wireframeLinewidth),
          'round' !== this.wireframeLinecap &&
            (n.wireframeLinecap = this.wireframeLinecap),
          'round' !== this.wireframeLinejoin &&
            (n.wireframeLinejoin = this.wireframeLinejoin),
          (n.skinning = this.skinning),
          (n.morphTargets = this.morphTargets),
          i &&
            ((i = e(t.textures)),
            (t = e(t.images)),
            0 < i.length && (n.textures = i),
            0 < t.length && (n.images = t)),
          n
        );
      },
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        (this.name = t.name),
          (this.fog = t.fog),
          (this.lights = t.lights),
          (this.blending = t.blending),
          (this.side = t.side),
          (this.shading = t.shading),
          (this.vertexColors = t.vertexColors),
          (this.opacity = t.opacity),
          (this.transparent = t.transparent),
          (this.blendSrc = t.blendSrc),
          (this.blendDst = t.blendDst),
          (this.blendEquation = t.blendEquation),
          (this.blendSrcAlpha = t.blendSrcAlpha),
          (this.blendDstAlpha = t.blendDstAlpha),
          (this.blendEquationAlpha = t.blendEquationAlpha),
          (this.depthFunc = t.depthFunc),
          (this.depthTest = t.depthTest),
          (this.depthWrite = t.depthWrite),
          (this.colorWrite = t.colorWrite),
          (this.precision = t.precision),
          (this.polygonOffset = t.polygonOffset),
          (this.polygonOffsetFactor = t.polygonOffsetFactor),
          (this.polygonOffsetUnits = t.polygonOffsetUnits),
          (this.alphaTest = t.alphaTest),
          (this.premultipliedAlpha = t.premultipliedAlpha),
          (this.overdraw = t.overdraw),
          (this.visible = t.visible),
          (this.clipShadows = t.clipShadows),
          (t = t.clippingPlanes);
        var e = null;
        if (null !== t)
          for (var i = t.length, e = Array(i), n = 0; n !== i; ++n)
            e[n] = t[n].clone();
        return (this.clippingPlanes = e), this;
      },
      update: function() {
        this.dispatchEvent({ type: 'update' });
      },
      dispose: function() {
        this.dispatchEvent({ type: 'dispose' });
      }
    }),
    Object.assign(X.prototype, e.prototype);
  var Yn = 0;
  (q.prototype = Object.create(X.prototype)),
    (q.prototype.constructor = q),
    (q.prototype.isShaderMaterial = !0),
    (q.prototype.copy = function(e) {
      return (
        X.prototype.copy.call(this, e),
        (this.fragmentShader = e.fragmentShader),
        (this.vertexShader = e.vertexShader),
        (this.uniforms = t.UniformsUtils.clone(e.uniforms)),
        (this.defines = e.defines),
        (this.wireframe = e.wireframe),
        (this.wireframeLinewidth = e.wireframeLinewidth),
        (this.lights = e.lights),
        (this.clipping = e.clipping),
        (this.skinning = e.skinning),
        (this.morphTargets = e.morphTargets),
        (this.morphNormals = e.morphNormals),
        (this.extensions = e.extensions),
        this
      );
    }),
    (q.prototype.toJSON = function(t) {
      return (
        (t = X.prototype.toJSON.call(this, t)),
        (t.uniforms = this.uniforms),
        (t.vertexShader = this.vertexShader),
        (t.fragmentShader = this.fragmentShader),
        t
      );
    }),
    (Y.prototype = Object.create(X.prototype)),
    (Y.prototype.constructor = Y),
    (Y.prototype.isMeshDepthMaterial = !0),
    (Y.prototype.copy = function(t) {
      return (
        X.prototype.copy.call(this, t),
        (this.depthPacking = t.depthPacking),
        (this.skinning = t.skinning),
        (this.morphTargets = t.morphTargets),
        (this.map = t.map),
        (this.alphaMap = t.alphaMap),
        (this.displacementMap = t.displacementMap),
        (this.displacementScale = t.displacementScale),
        (this.displacementBias = t.displacementBias),
        (this.wireframe = t.wireframe),
        (this.wireframeLinewidth = t.wireframeLinewidth),
        this
      );
    }),
    (Z.prototype = {
      constructor: Z,
      isBox3: !0,
      set: function(t, e) {
        return this.min.copy(t), this.max.copy(e), this;
      },
      setFromArray: function(t) {
        for (
          var e = Infinity,
            i = Infinity,
            n = Infinity,
            r = -Infinity,
            a = -Infinity,
            o = -Infinity,
            s = 0,
            c = t.length;
          s < c;
          s += 3
        ) {
          var h = t[s],
            l = t[s + 1],
            u = t[s + 2];
          h < e && (e = h),
            l < i && (i = l),
            u < n && (n = u),
            h > r && (r = h),
            l > a && (a = l),
            u > o && (o = u);
        }
        this.min.set(e, i, n), this.max.set(r, a, o);
      },
      setFromPoints: function(t) {
        this.makeEmpty();
        for (var e = 0, i = t.length; e < i; e++) this.expandByPoint(t[e]);
        return this;
      },
      setFromCenterAndSize: (function() {
        var t = new c();
        return function(e, i) {
          var n = t.copy(i).multiplyScalar(0.5);
          return this.min.copy(e).sub(n), this.max.copy(e).add(n), this;
        };
      })(),
      setFromObject: (function() {
        var t = new c();
        return function(e) {
          var i = this;
          return (
            e.updateMatrixWorld(!0),
            this.makeEmpty(),
            e.traverse(function(e) {
              var n = e.geometry;
              if (void 0 !== n)
                if (n && n.isGeometry)
                  for (var n = n.vertices, r = 0, a = n.length; r < a; r++)
                    t.copy(n[r]),
                      t.applyMatrix4(e.matrixWorld),
                      i.expandByPoint(t);
                else if (
                  n &&
                  n.isBufferGeometry &&
                  void 0 !== (a = n.attributes.position)
                ) {
                  var o;
                  for (
                    a && a.isInterleavedBufferAttribute
                      ? ((n = a.data.array),
                        (r = a.offset),
                        (o = a.data.stride))
                      : ((n = a.array), (r = 0), (o = 3)),
                      a = n.length;
                    r < a;
                    r += o
                  )
                    t.fromArray(n, r),
                      t.applyMatrix4(e.matrixWorld),
                      i.expandByPoint(t);
                }
            }),
            this
          );
        };
      })(),
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        return this.min.copy(t.min), this.max.copy(t.max), this;
      },
      makeEmpty: function() {
        return (
          (this.min.x = this.min.y = this.min.z = Infinity),
          (this.max.x = this.max.y = this.max.z = -Infinity),
          this
        );
      },
      isEmpty: function() {
        return (
          this.max.x < this.min.x ||
          this.max.y < this.min.y ||
          this.max.z < this.min.z
        );
      },
      getCenter: function(t) {
        return (
          (t = t || new c()),
          this.isEmpty()
            ? t.set(0, 0, 0)
            : t.addVectors(this.min, this.max).multiplyScalar(0.5)
        );
      },
      getSize: function(t) {
        return (
          (t = t || new c()),
          this.isEmpty() ? t.set(0, 0, 0) : t.subVectors(this.max, this.min)
        );
      },
      expandByPoint: function(t) {
        return this.min.min(t), this.max.max(t), this;
      },
      expandByVector: function(t) {
        return this.min.sub(t), this.max.add(t), this;
      },
      expandByScalar: function(t) {
        return this.min.addScalar(-t), this.max.addScalar(t), this;
      },
      containsPoint: function(t) {
        return !(
          t.x < this.min.x ||
          t.x > this.max.x ||
          t.y < this.min.y ||
          t.y > this.max.y ||
          t.z < this.min.z ||
          t.z > this.max.z
        );
      },
      containsBox: function(t) {
        return (
          this.min.x <= t.min.x &&
          t.max.x <= this.max.x &&
          this.min.y <= t.min.y &&
          t.max.y <= this.max.y &&
          this.min.z <= t.min.z &&
          t.max.z <= this.max.z
        );
      },
      getParameter: function(t, e) {
        return (e || new c()).set(
          (t.x - this.min.x) / (this.max.x - this.min.x),
          (t.y - this.min.y) / (this.max.y - this.min.y),
          (t.z - this.min.z) / (this.max.z - this.min.z)
        );
      },
      intersectsBox: function(t) {
        return !(
          t.max.x < this.min.x ||
          t.min.x > this.max.x ||
          t.max.y < this.min.y ||
          t.min.y > this.max.y ||
          t.max.z < this.min.z ||
          t.min.z > this.max.z
        );
      },
      intersectsSphere: (function() {
        var t;
        return function(e) {
          return (
            void 0 === t && (t = new c()),
            this.clampPoint(e.center, t),
            t.distanceToSquared(e.center) <= e.radius * e.radius
          );
        };
      })(),
      intersectsPlane: function(t) {
        var e, i;
        return (
          0 < t.normal.x
            ? ((e = t.normal.x * this.min.x), (i = t.normal.x * this.max.x))
            : ((e = t.normal.x * this.max.x), (i = t.normal.x * this.min.x)),
          0 < t.normal.y
            ? ((e += t.normal.y * this.min.y), (i += t.normal.y * this.max.y))
            : ((e += t.normal.y * this.max.y), (i += t.normal.y * this.min.y)),
          0 < t.normal.z
            ? ((e += t.normal.z * this.min.z), (i += t.normal.z * this.max.z))
            : ((e += t.normal.z * this.max.z), (i += t.normal.z * this.min.z)),
          e <= t.constant && i >= t.constant
        );
      },
      clampPoint: function(t, e) {
        return (e || new c()).copy(t).clamp(this.min, this.max);
      },
      distanceToPoint: (function() {
        var t = new c();
        return function(e) {
          return t
            .copy(e)
            .clamp(this.min, this.max)
            .sub(e)
            .length();
        };
      })(),
      getBoundingSphere: (function() {
        var t = new c();
        return function(e) {
          return (
            (e = e || new J()),
            this.getCenter(e.center),
            (e.radius = 0.5 * this.size(t).length()),
            e
          );
        };
      })(),
      intersect: function(t) {
        return (
          this.min.max(t.min),
          this.max.min(t.max),
          this.isEmpty() && this.makeEmpty(),
          this
        );
      },
      union: function(t) {
        return this.min.min(t.min), this.max.max(t.max), this;
      },
      applyMatrix4: (function() {
        var t = [
          new c(),
          new c(),
          new c(),
          new c(),
          new c(),
          new c(),
          new c(),
          new c()
        ];
        return function(e) {
          return this.isEmpty()
            ? this
            : (t[0].set(this.min.x, this.min.y, this.min.z).applyMatrix4(e),
              t[1].set(this.min.x, this.min.y, this.max.z).applyMatrix4(e),
              t[2].set(this.min.x, this.max.y, this.min.z).applyMatrix4(e),
              t[3].set(this.min.x, this.max.y, this.max.z).applyMatrix4(e),
              t[4].set(this.max.x, this.min.y, this.min.z).applyMatrix4(e),
              t[5].set(this.max.x, this.min.y, this.max.z).applyMatrix4(e),
              t[6].set(this.max.x, this.max.y, this.min.z).applyMatrix4(e),
              t[7].set(this.max.x, this.max.y, this.max.z).applyMatrix4(e),
              this.setFromPoints(t),
              this);
        };
      })(),
      translate: function(t) {
        return this.min.add(t), this.max.add(t), this;
      },
      equals: function(t) {
        return t.min.equals(this.min) && t.max.equals(this.max);
      }
    }),
    (J.prototype = {
      constructor: J,
      set: function(t, e) {
        return this.center.copy(t), (this.radius = e), this;
      },
      setFromPoints: (function() {
        var t = new Z();
        return function(e, i) {
          var n = this.center;
          void 0 !== i ? n.copy(i) : t.setFromPoints(e).getCenter(n);
          for (var r = 0, a = 0, o = e.length; a < o; a++)
            r = Math.max(r, n.distanceToSquared(e[a]));
          return (this.radius = Math.sqrt(r)), this;
        };
      })(),
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        return this.center.copy(t.center), (this.radius = t.radius), this;
      },
      empty: function() {
        return 0 >= this.radius;
      },
      containsPoint: function(t) {
        return t.distanceToSquared(this.center) <= this.radius * this.radius;
      },
      distanceToPoint: function(t) {
        return t.distanceTo(this.center) - this.radius;
      },
      intersectsSphere: function(t) {
        var e = this.radius + t.radius;
        return t.center.distanceToSquared(this.center) <= e * e;
      },
      intersectsBox: function(t) {
        return t.intersectsSphere(this);
      },
      intersectsPlane: function(t) {
        return Math.abs(this.center.dot(t.normal) - t.constant) <= this.radius;
      },
      clampPoint: function(t, e) {
        var i = this.center.distanceToSquared(t),
          n = e || new c();
        return (
          n.copy(t),
          i > this.radius * this.radius &&
            (n.sub(this.center).normalize(),
            n.multiplyScalar(this.radius).add(this.center)),
          n
        );
      },
      getBoundingBox: function(t) {
        return (
          (t = t || new Z()),
          t.set(this.center, this.center),
          t.expandByScalar(this.radius),
          t
        );
      },
      applyMatrix4: function(t) {
        return (
          this.center.applyMatrix4(t),
          (this.radius *= t.getMaxScaleOnAxis()),
          this
        );
      },
      translate: function(t) {
        return this.center.add(t), this;
      },
      equals: function(t) {
        return t.center.equals(this.center) && t.radius === this.radius;
      }
    }),
    (Q.prototype = {
      constructor: Q,
      isMatrix3: !0,
      set: function(t, e, i, n, r, a, o, s, c) {
        var h = this.elements;
        return (
          (h[0] = t),
          (h[1] = n),
          (h[2] = o),
          (h[3] = e),
          (h[4] = r),
          (h[5] = s),
          (h[6] = i),
          (h[7] = a),
          (h[8] = c),
          this
        );
      },
      identity: function() {
        return this.set(1, 0, 0, 0, 1, 0, 0, 0, 1), this;
      },
      clone: function() {
        return new this.constructor().fromArray(this.elements);
      },
      copy: function(t) {
        return (
          (t = t.elements),
          this.set(t[0], t[3], t[6], t[1], t[4], t[7], t[2], t[5], t[8]),
          this
        );
      },
      setFromMatrix4: function(t) {
        return (
          (t = t.elements),
          this.set(t[0], t[4], t[8], t[1], t[5], t[9], t[2], t[6], t[10]),
          this
        );
      },
      applyToVector3Array: (function() {
        var t;
        return function(e, i, n) {
          void 0 === t && (t = new c()),
            void 0 === i && (i = 0),
            void 0 === n && (n = e.length);
          for (var r = 0; r < n; r += 3, i += 3)
            t.fromArray(e, i), t.applyMatrix3(this), t.toArray(e, i);
          return e;
        };
      })(),
      applyToBuffer: (function() {
        var t;
        return function(e, i, n) {
          void 0 === t && (t = new c()),
            void 0 === i && (i = 0),
            void 0 === n && (n = e.length / e.itemSize);
          for (var r = 0; r < n; r++, i++)
            (t.x = e.getX(i)),
              (t.y = e.getY(i)),
              (t.z = e.getZ(i)),
              t.applyMatrix3(this),
              e.setXYZ(t.x, t.y, t.z);
          return e;
        };
      })(),
      multiplyScalar: function(t) {
        var e = this.elements;
        return (
          (e[0] *= t),
          (e[3] *= t),
          (e[6] *= t),
          (e[1] *= t),
          (e[4] *= t),
          (e[7] *= t),
          (e[2] *= t),
          (e[5] *= t),
          (e[8] *= t),
          this
        );
      },
      determinant: function() {
        var t = this.elements,
          e = t[0],
          i = t[1],
          n = t[2],
          r = t[3],
          a = t[4],
          o = t[5],
          s = t[6],
          c = t[7],
          t = t[8];
        return (
          e * a * t - e * o * c - i * r * t + i * o * s + n * r * c - n * a * s
        );
      },
      getInverse: function(t, e) {
        t &&
          t.isMatrix4 &&
          console.error(
            'THREE.Matrix3.getInverse no longer takes a Matrix4 argument.'
          );
        var i = t.elements,
          n = this.elements,
          r = i[0],
          a = i[1],
          o = i[2],
          s = i[3],
          c = i[4],
          h = i[5],
          l = i[6],
          u = i[7],
          i = i[8],
          d = i * c - h * u,
          p = h * l - i * s,
          f = u * s - c * l,
          m = r * d + a * p + o * f;
        if (0 === m) {
          if (!0 === e)
            throw Error(
              "THREE.Matrix3.getInverse(): can't invert matrix, determinant is 0"
            );
          return (
            console.warn(
              "THREE.Matrix3.getInverse(): can't invert matrix, determinant is 0"
            ),
            this.identity()
          );
        }
        return (
          (m = 1 / m),
          (n[0] = d * m),
          (n[1] = (o * u - i * a) * m),
          (n[2] = (h * a - o * c) * m),
          (n[3] = p * m),
          (n[4] = (i * r - o * l) * m),
          (n[5] = (o * s - h * r) * m),
          (n[6] = f * m),
          (n[7] = (a * l - u * r) * m),
          (n[8] = (c * r - a * s) * m),
          this
        );
      },
      transpose: function() {
        var t,
          e = this.elements;
        return (
          (t = e[1]),
          (e[1] = e[3]),
          (e[3] = t),
          (t = e[2]),
          (e[2] = e[6]),
          (e[6] = t),
          (t = e[5]),
          (e[5] = e[7]),
          (e[7] = t),
          this
        );
      },
      flattenToArrayOffset: function(t, e) {
        return (
          console.warn(
            'THREE.Matrix3: .flattenToArrayOffset is deprecated - just use .toArray instead.'
          ),
          this.toArray(t, e)
        );
      },
      getNormalMatrix: function(t) {
        return this.setFromMatrix4(t)
          .getInverse(this)
          .transpose();
      },
      transposeIntoArray: function(t) {
        var e = this.elements;
        return (
          (t[0] = e[0]),
          (t[1] = e[3]),
          (t[2] = e[6]),
          (t[3] = e[1]),
          (t[4] = e[4]),
          (t[5] = e[7]),
          (t[6] = e[2]),
          (t[7] = e[5]),
          (t[8] = e[8]),
          this
        );
      },
      fromArray: function(t, e) {
        void 0 === e && (e = 0);
        for (var i = 0; 9 > i; i++) this.elements[i] = t[i + e];
        return this;
      },
      toArray: function(t, e) {
        void 0 === t && (t = []), void 0 === e && (e = 0);
        var i = this.elements;
        return (
          (t[e] = i[0]),
          (t[e + 1] = i[1]),
          (t[e + 2] = i[2]),
          (t[e + 3] = i[3]),
          (t[e + 4] = i[4]),
          (t[e + 5] = i[5]),
          (t[e + 6] = i[6]),
          (t[e + 7] = i[7]),
          (t[e + 8] = i[8]),
          t
        );
      }
    }),
    (K.prototype = {
      constructor: K,
      set: function(t, e) {
        return this.normal.copy(t), (this.constant = e), this;
      },
      setComponents: function(t, e, i, n) {
        return this.normal.set(t, e, i), (this.constant = n), this;
      },
      setFromNormalAndCoplanarPoint: function(t, e) {
        return this.normal.copy(t), (this.constant = -e.dot(this.normal)), this;
      },
      setFromCoplanarPoints: (function() {
        var t = new c(),
          e = new c();
        return function(i, n, r) {
          return (
            (n = t
              .subVectors(r, n)
              .cross(e.subVectors(i, n))
              .normalize()),
            this.setFromNormalAndCoplanarPoint(n, i),
            this
          );
        };
      })(),
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        return this.normal.copy(t.normal), (this.constant = t.constant), this;
      },
      normalize: function() {
        var t = 1 / this.normal.length();
        return this.normal.multiplyScalar(t), (this.constant *= t), this;
      },
      negate: function() {
        return (this.constant *= -1), this.normal.negate(), this;
      },
      distanceToPoint: function(t) {
        return this.normal.dot(t) + this.constant;
      },
      distanceToSphere: function(t) {
        return this.distanceToPoint(t.center) - t.radius;
      },
      projectPoint: function(t, e) {
        return this.orthoPoint(t, e)
          .sub(t)
          .negate();
      },
      orthoPoint: function(t, e) {
        var i = this.distanceToPoint(t);
        return (e || new c()).copy(this.normal).multiplyScalar(i);
      },
      intersectLine: (function() {
        var t = new c();
        return function(e, i) {
          var n = i || new c(),
            r = e.delta(t),
            a = this.normal.dot(r);
          return 0 !== a
            ? ((a = -(e.start.dot(this.normal) + this.constant) / a),
              0 > a || 1 < a
                ? void 0
                : n
                    .copy(r)
                    .multiplyScalar(a)
                    .add(e.start))
            : 0 === this.distanceToPoint(e.start) ? n.copy(e.start) : void 0;
        };
      })(),
      intersectsLine: function(t) {
        var e = this.distanceToPoint(t.start);
        return (
          (t = this.distanceToPoint(t.end)),
          (0 > e && 0 < t) || (0 > t && 0 < e)
        );
      },
      intersectsBox: function(t) {
        return t.intersectsPlane(this);
      },
      intersectsSphere: function(t) {
        return t.intersectsPlane(this);
      },
      coplanarPoint: function(t) {
        return (t || new c()).copy(this.normal).multiplyScalar(-this.constant);
      },
      applyMatrix4: (function() {
        var t = new c(),
          e = new Q();
        return function(i, n) {
          var r = this.coplanarPoint(t).applyMatrix4(i),
            a = n || e.getNormalMatrix(i),
            a = this.normal.applyMatrix3(a).normalize();
          return (this.constant = -r.dot(a)), this;
        };
      })(),
      translate: function(t) {
        return (this.constant -= t.dot(this.normal)), this;
      },
      equals: function(t) {
        return t.normal.equals(this.normal) && t.constant === this.constant;
      }
    }),
    ($.prototype = {
      constructor: $,
      set: function(t, e, i, n, r, a) {
        var o = this.planes;
        return (
          o[0].copy(t),
          o[1].copy(e),
          o[2].copy(i),
          o[3].copy(n),
          o[4].copy(r),
          o[5].copy(a),
          this
        );
      },
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        for (var e = this.planes, i = 0; 6 > i; i++) e[i].copy(t.planes[i]);
        return this;
      },
      setFromMatrix: function(t) {
        var e = this.planes,
          i = t.elements;
        t = i[0];
        var n = i[1],
          r = i[2],
          a = i[3],
          o = i[4],
          s = i[5],
          c = i[6],
          h = i[7],
          l = i[8],
          u = i[9],
          d = i[10],
          p = i[11],
          f = i[12],
          m = i[13],
          g = i[14],
          i = i[15];
        return (
          e[0].setComponents(a - t, h - o, p - l, i - f).normalize(),
          e[1].setComponents(a + t, h + o, p + l, i + f).normalize(),
          e[2].setComponents(a + n, h + s, p + u, i + m).normalize(),
          e[3].setComponents(a - n, h - s, p - u, i - m).normalize(),
          e[4].setComponents(a - r, h - c, p - d, i - g).normalize(),
          e[5].setComponents(a + r, h + c, p + d, i + g).normalize(),
          this
        );
      },
      intersectsObject: (function() {
        var t = new J();
        return function(e) {
          var i = e.geometry;
          return (
            null === i.boundingSphere && i.computeBoundingSphere(),
            t.copy(i.boundingSphere).applyMatrix4(e.matrixWorld),
            this.intersectsSphere(t)
          );
        };
      })(),
      intersectsSprite: (function() {
        var t = new J();
        return function(e) {
          return (
            t.center.set(0, 0, 0),
            (t.radius = 0.7071067811865476),
            t.applyMatrix4(e.matrixWorld),
            this.intersectsSphere(t)
          );
        };
      })(),
      intersectsSphere: function(t) {
        var e = this.planes,
          i = t.center;
        t = -t.radius;
        for (var n = 0; 6 > n; n++) if (e[n].distanceToPoint(i) < t) return !1;
        return !0;
      },
      intersectsBox: (function() {
        var t = new c(),
          e = new c();
        return function(i) {
          for (var n = this.planes, r = 0; 6 > r; r++) {
            var a = n[r];
            (t.x = 0 < a.normal.x ? i.min.x : i.max.x),
              (e.x = 0 < a.normal.x ? i.max.x : i.min.x),
              (t.y = 0 < a.normal.y ? i.min.y : i.max.y),
              (e.y = 0 < a.normal.y ? i.max.y : i.min.y),
              (t.z = 0 < a.normal.z ? i.min.z : i.max.z),
              (e.z = 0 < a.normal.z ? i.max.z : i.min.z);
            var o = a.distanceToPoint(t),
              a = a.distanceToPoint(e);
            if (0 > o && 0 > a) return !1;
          }
          return !0;
        };
      })(),
      containsPoint: function(t) {
        for (var e = this.planes, i = 0; 6 > i; i++)
          if (0 > e[i].distanceToPoint(t)) return !1;
        return !0;
      }
    }),
    (et.prototype = {
      constructor: et,
      set: function(t, e) {
        return this.origin.copy(t), this.direction.copy(e), this;
      },
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        return (
          this.origin.copy(t.origin), this.direction.copy(t.direction), this
        );
      },
      at: function(t, e) {
        return (e || new c())
          .copy(this.direction)
          .multiplyScalar(t)
          .add(this.origin);
      },
      lookAt: function(t) {
        return (
          this.direction
            .copy(t)
            .sub(this.origin)
            .normalize(),
          this
        );
      },
      recast: (function() {
        var t = new c();
        return function(e) {
          return this.origin.copy(this.at(e, t)), this;
        };
      })(),
      closestPointToPoint: function(t, e) {
        var i = e || new c();
        i.subVectors(t, this.origin);
        var n = i.dot(this.direction);
        return 0 > n
          ? i.copy(this.origin)
          : i
              .copy(this.direction)
              .multiplyScalar(n)
              .add(this.origin);
      },
      distanceToPoint: function(t) {
        return Math.sqrt(this.distanceSqToPoint(t));
      },
      distanceSqToPoint: (function() {
        var t = new c();
        return function(e) {
          var i = t.subVectors(e, this.origin).dot(this.direction);
          return 0 > i
            ? this.origin.distanceToSquared(e)
            : (t
                .copy(this.direction)
                .multiplyScalar(i)
                .add(this.origin),
              t.distanceToSquared(e));
        };
      })(),
      distanceSqToSegment: (function() {
        var t = new c(),
          e = new c(),
          i = new c();
        return function(n, r, a, o) {
          t
            .copy(n)
            .add(r)
            .multiplyScalar(0.5),
            e
              .copy(r)
              .sub(n)
              .normalize(),
            i.copy(this.origin).sub(t);
          var s,
            c = 0.5 * n.distanceTo(r),
            h = -this.direction.dot(e),
            l = i.dot(this.direction),
            u = -i.dot(e),
            d = i.lengthSq(),
            p = Math.abs(1 - h * h);
          return (
            0 < p
              ? ((n = h * u - l),
                (r = h * l - u),
                (s = c * p),
                0 <= n
                  ? r >= -s
                    ? r <= s
                      ? ((c = 1 / p),
                        (n *= c),
                        (r *= c),
                        (h =
                          n * (n + h * r + 2 * l) +
                          r * (h * n + r + 2 * u) +
                          d))
                      : ((r = c),
                        (n = Math.max(0, -(h * r + l))),
                        (h = -n * n + r * (r + 2 * u) + d))
                    : ((r = -c),
                      (n = Math.max(0, -(h * r + l))),
                      (h = -n * n + r * (r + 2 * u) + d))
                  : r <= -s
                    ? ((n = Math.max(0, -(-h * c + l))),
                      (r = 0 < n ? -c : Math.min(Math.max(-c, -u), c)),
                      (h = -n * n + r * (r + 2 * u) + d))
                    : r <= s
                      ? ((n = 0),
                        (r = Math.min(Math.max(-c, -u), c)),
                        (h = r * (r + 2 * u) + d))
                      : ((n = Math.max(0, -(h * c + l))),
                        (r = 0 < n ? c : Math.min(Math.max(-c, -u), c)),
                        (h = -n * n + r * (r + 2 * u) + d)))
              : ((r = 0 < h ? -c : c),
                (n = Math.max(0, -(h * r + l))),
                (h = -n * n + r * (r + 2 * u) + d)),
            a &&
              a
                .copy(this.direction)
                .multiplyScalar(n)
                .add(this.origin),
            o &&
              o
                .copy(e)
                .multiplyScalar(r)
                .add(t),
            h
          );
        };
      })(),
      intersectSphere: (function() {
        var t = new c();
        return function(e, i) {
          t.subVectors(e.center, this.origin);
          var n = t.dot(this.direction),
            r = t.dot(t) - n * n,
            a = e.radius * e.radius;
          return r > a
            ? null
            : ((a = Math.sqrt(a - r)),
              (r = n - a),
              (n += a),
              0 > r && 0 > n ? null : 0 > r ? this.at(n, i) : this.at(r, i));
        };
      })(),
      intersectsSphere: function(t) {
        return this.distanceToPoint(t.center) <= t.radius;
      },
      distanceToPlane: function(t) {
        var e = t.normal.dot(this.direction);
        return 0 === e
          ? 0 === t.distanceToPoint(this.origin) ? 0 : null
          : ((t = -(this.origin.dot(t.normal) + t.constant) / e),
            0 <= t ? t : null);
      },
      intersectPlane: function(t, e) {
        var i = this.distanceToPlane(t);
        return null === i ? null : this.at(i, e);
      },
      intersectsPlane: function(t) {
        var e = t.distanceToPoint(this.origin);
        return 0 === e || 0 > t.normal.dot(this.direction) * e;
      },
      intersectBox: function(t, e) {
        var i, n, r, a, o;
        (n = 1 / this.direction.x),
          (a = 1 / this.direction.y),
          (o = 1 / this.direction.z);
        var s = this.origin;
        return (
          0 <= n
            ? ((i = (t.min.x - s.x) * n), (n *= t.max.x - s.x))
            : ((i = (t.max.x - s.x) * n), (n *= t.min.x - s.x)),
          0 <= a
            ? ((r = (t.min.y - s.y) * a), (a *= t.max.y - s.y))
            : ((r = (t.max.y - s.y) * a), (a *= t.min.y - s.y)),
          i > a || r > n
            ? null
            : ((r > i || i !== i) && (i = r),
              (a < n || n !== n) && (n = a),
              0 <= o
                ? ((r = (t.min.z - s.z) * o), (o *= t.max.z - s.z))
                : ((r = (t.max.z - s.z) * o), (o *= t.min.z - s.z)),
              i > o || r > n
                ? null
                : ((r > i || i !== i) && (i = r),
                  (o < n || n !== n) && (n = o),
                  0 > n ? null : this.at(0 <= i ? i : n, e)))
        );
      },
      intersectsBox: (function() {
        var t = new c();
        return function(e) {
          return null !== this.intersectBox(e, t);
        };
      })(),
      intersectTriangle: (function() {
        var t = new c(),
          e = new c(),
          i = new c(),
          n = new c();
        return function(r, a, o, s, c) {
          if (
            (e.subVectors(a, r),
            i.subVectors(o, r),
            n.crossVectors(e, i),
            0 < (a = this.direction.dot(n)))
          ) {
            if (s) return null;
            s = 1;
          } else {
            if (!(0 > a)) return null;
            (s = -1), (a = -a);
          }
          return (
            t.subVectors(this.origin, r),
            0 > (r = s * this.direction.dot(i.crossVectors(t, i)))
              ? null
              : 0 > (o = s * this.direction.dot(e.cross(t))) || r + o > a
                ? null
                : ((r = -s * t.dot(n)), 0 > r ? null : this.at(r / a, c))
          );
        };
      })(),
      applyMatrix4: function(t) {
        return (
          this.direction.add(this.origin).applyMatrix4(t),
          this.origin.applyMatrix4(t),
          this.direction.sub(this.origin),
          this.direction.normalize(),
          this
        );
      },
      equals: function(t) {
        return (
          t.origin.equals(this.origin) && t.direction.equals(this.direction)
        );
      }
    }),
    (it.RotationOrders = 'XYZ YZX ZXY XZY YXZ ZYX'.split(' ')),
    (it.DefaultOrder = 'XYZ'),
    (it.prototype = {
      constructor: it,
      isEuler: !0,
      get x() {
        return this._x;
      },
      set x(t) {
        (this._x = t), this.onChangeCallback();
      },
      get y() {
        return this._y;
      },
      set y(t) {
        (this._y = t), this.onChangeCallback();
      },
      get z() {
        return this._z;
      },
      set z(t) {
        (this._z = t), this.onChangeCallback();
      },
      get order() {
        return this._order;
      },
      set order(t) {
        (this._order = t), this.onChangeCallback();
      },
      set: function(t, e, i, n) {
        return (
          (this._x = t),
          (this._y = e),
          (this._z = i),
          (this._order = n || this._order),
          this.onChangeCallback(),
          this
        );
      },
      clone: function() {
        return new this.constructor(this._x, this._y, this._z, this._order);
      },
      copy: function(t) {
        return (
          (this._x = t._x),
          (this._y = t._y),
          (this._z = t._z),
          (this._order = t._order),
          this.onChangeCallback(),
          this
        );
      },
      setFromRotationMatrix: function(e, i, n) {
        var r = t.Math.clamp,
          a = e.elements;
        e = a[0];
        var o = a[4],
          s = a[8],
          c = a[1],
          h = a[5],
          l = a[9],
          u = a[2],
          d = a[6],
          a = a[10];
        return (
          (i = i || this._order),
          'XYZ' === i
            ? ((this._y = Math.asin(r(s, -1, 1))),
              0.99999 > Math.abs(s)
                ? ((this._x = Math.atan2(-l, a)), (this._z = Math.atan2(-o, e)))
                : ((this._x = Math.atan2(d, h)), (this._z = 0)))
            : 'YXZ' === i
              ? ((this._x = Math.asin(-r(l, -1, 1))),
                0.99999 > Math.abs(l)
                  ? ((this._y = Math.atan2(s, a)), (this._z = Math.atan2(c, h)))
                  : ((this._y = Math.atan2(-u, e)), (this._z = 0)))
              : 'ZXY' === i
                ? ((this._x = Math.asin(r(d, -1, 1))),
                  0.99999 > Math.abs(d)
                    ? ((this._y = Math.atan2(-u, a)),
                      (this._z = Math.atan2(-o, h)))
                    : ((this._y = 0), (this._z = Math.atan2(c, e))))
                : 'ZYX' === i
                  ? ((this._y = Math.asin(-r(u, -1, 1))),
                    0.99999 > Math.abs(u)
                      ? ((this._x = Math.atan2(d, a)),
                        (this._z = Math.atan2(c, e)))
                      : ((this._x = 0), (this._z = Math.atan2(-o, h))))
                  : 'YZX' === i
                    ? ((this._z = Math.asin(r(c, -1, 1))),
                      0.99999 > Math.abs(c)
                        ? ((this._x = Math.atan2(-l, h)),
                          (this._y = Math.atan2(-u, e)))
                        : ((this._x = 0), (this._y = Math.atan2(s, a))))
                    : 'XZY' === i
                      ? ((this._z = Math.asin(-r(o, -1, 1))),
                        0.99999 > Math.abs(o)
                          ? ((this._x = Math.atan2(d, h)),
                            (this._y = Math.atan2(s, e)))
                          : ((this._x = Math.atan2(-l, a)), (this._y = 0)))
                      : console.warn(
                          'THREE.Euler: .setFromRotationMatrix() given unsupported order: ' +
                            i
                        ),
          (this._order = i),
          !1 !== n && this.onChangeCallback(),
          this
        );
      },
      setFromQuaternion: (function() {
        var t;
        return function(e, i, n) {
          return (
            void 0 === t && (t = new h()),
            t.makeRotationFromQuaternion(e),
            this.setFromRotationMatrix(t, i, n)
          );
        };
      })(),
      setFromVector3: function(t, e) {
        return this.set(t.x, t.y, t.z, e || this._order);
      },
      reorder: (function() {
        var t = new s();
        return function(e) {
          return t.setFromEuler(this), this.setFromQuaternion(t, e);
        };
      })(),
      equals: function(t) {
        return (
          t._x === this._x &&
          t._y === this._y &&
          t._z === this._z &&
          t._order === this._order
        );
      },
      fromArray: function(t) {
        return (
          (this._x = t[0]),
          (this._y = t[1]),
          (this._z = t[2]),
          void 0 !== t[3] && (this._order = t[3]),
          this.onChangeCallback(),
          this
        );
      },
      toArray: function(t, e) {
        return (
          void 0 === t && (t = []),
          void 0 === e && (e = 0),
          (t[e] = this._x),
          (t[e + 1] = this._y),
          (t[e + 2] = this._z),
          (t[e + 3] = this._order),
          t
        );
      },
      toVector3: function(t) {
        return t
          ? t.set(this._x, this._y, this._z)
          : new c(this._x, this._y, this._z);
      },
      onChange: function(t) {
        return (this.onChangeCallback = t), this;
      },
      onChangeCallback: function() {}
    }),
    (nt.prototype = {
      constructor: nt,
      set: function(t) {
        this.mask = 1 << t;
      },
      enable: function(t) {
        this.mask |= 1 << t;
      },
      toggle: function(t) {
        this.mask ^= 1 << t;
      },
      disable: function(t) {
        this.mask &= ~(1 << t);
      },
      test: function(t) {
        return 0 != (this.mask & t.mask);
      }
    }),
    (rt.DefaultUp = new c(0, 1, 0)),
    (rt.DefaultMatrixAutoUpdate = !0),
    Object.assign(rt.prototype, e.prototype, {
      isObject3D: !0,
      applyMatrix: function(t) {
        this.matrix.multiplyMatrices(t, this.matrix),
          this.matrix.decompose(this.position, this.quaternion, this.scale);
      },
      setRotationFromAxisAngle: function(t, e) {
        this.quaternion.setFromAxisAngle(t, e);
      },
      setRotationFromEuler: function(t) {
        this.quaternion.setFromEuler(t, !0);
      },
      setRotationFromMatrix: function(t) {
        this.quaternion.setFromRotationMatrix(t);
      },
      setRotationFromQuaternion: function(t) {
        this.quaternion.copy(t);
      },
      rotateOnAxis: (function() {
        var t = new s();
        return function(e, i) {
          return t.setFromAxisAngle(e, i), this.quaternion.multiply(t), this;
        };
      })(),
      rotateX: (function() {
        var t = new c(1, 0, 0);
        return function(e) {
          return this.rotateOnAxis(t, e);
        };
      })(),
      rotateY: (function() {
        var t = new c(0, 1, 0);
        return function(e) {
          return this.rotateOnAxis(t, e);
        };
      })(),
      rotateZ: (function() {
        var t = new c(0, 0, 1);
        return function(e) {
          return this.rotateOnAxis(t, e);
        };
      })(),
      translateOnAxis: (function() {
        var t = new c();
        return function(e, i) {
          return (
            t.copy(e).applyQuaternion(this.quaternion),
            this.position.add(t.multiplyScalar(i)),
            this
          );
        };
      })(),
      translateX: (function() {
        var t = new c(1, 0, 0);
        return function(e) {
          return this.translateOnAxis(t, e);
        };
      })(),
      translateY: (function() {
        var t = new c(0, 1, 0);
        return function(e) {
          return this.translateOnAxis(t, e);
        };
      })(),
      translateZ: (function() {
        var t = new c(0, 0, 1);
        return function(e) {
          return this.translateOnAxis(t, e);
        };
      })(),
      localToWorld: function(t) {
        return t.applyMatrix4(this.matrixWorld);
      },
      worldToLocal: (function() {
        var t = new h();
        return function(e) {
          return e.applyMatrix4(t.getInverse(this.matrixWorld));
        };
      })(),
      lookAt: (function() {
        var t = new h();
        return function(e) {
          t.lookAt(e, this.position, this.up),
            this.quaternion.setFromRotationMatrix(t);
        };
      })(),
      add: function(t) {
        if (1 < arguments.length) {
          for (var e = 0; e < arguments.length; e++) this.add(arguments[e]);
          return this;
        }
        return t === this
          ? (console.error(
              "THREE.Object3D.add: object can't be added as a child of itself.",
              t
            ),
            this)
          : (t && t.isObject3D
              ? (null !== t.parent && t.parent.remove(t),
                (t.parent = this),
                t.dispatchEvent({ type: 'added' }),
                this.children.push(t))
              : console.error(
                  'THREE.Object3D.add: object not an instance of THREE.Object3D.',
                  t
                ),
            this);
      },
      remove: function(t) {
        if (1 < arguments.length)
          for (var e = 0; e < arguments.length; e++) this.remove(arguments[e]);
        -1 !== (e = this.children.indexOf(t)) &&
          ((t.parent = null),
          t.dispatchEvent({ type: 'removed' }),
          this.children.splice(e, 1));
      },
      getObjectById: function(t) {
        return this.getObjectByProperty('id', t);
      },
      getObjectByName: function(t) {
        return this.getObjectByProperty('name', t);
      },
      getObjectByProperty: function(t, e) {
        if (this[t] === e) return this;
        for (var i = 0, n = this.children.length; i < n; i++) {
          var r = this.children[i].getObjectByProperty(t, e);
          if (void 0 !== r) return r;
        }
      },
      getWorldPosition: function(t) {
        return (
          (t = t || new c()),
          this.updateMatrixWorld(!0),
          t.setFromMatrixPosition(this.matrixWorld)
        );
      },
      getWorldQuaternion: (function() {
        var t = new c(),
          e = new c();
        return function(i) {
          return (
            (i = i || new s()),
            this.updateMatrixWorld(!0),
            this.matrixWorld.decompose(t, i, e),
            i
          );
        };
      })(),
      getWorldRotation: (function() {
        var t = new s();
        return function(e) {
          return (
            (e = e || new it()),
            this.getWorldQuaternion(t),
            e.setFromQuaternion(t, this.rotation.order, !1)
          );
        };
      })(),
      getWorldScale: (function() {
        var t = new c(),
          e = new s();
        return function(i) {
          return (
            (i = i || new c()),
            this.updateMatrixWorld(!0),
            this.matrixWorld.decompose(t, e, i),
            i
          );
        };
      })(),
      getWorldDirection: (function() {
        var t = new s();
        return function(e) {
          return (
            (e = e || new c()),
            this.getWorldQuaternion(t),
            e.set(0, 0, 1).applyQuaternion(t)
          );
        };
      })(),
      raycast: function() {},
      traverse: function(t) {
        t(this);
        for (var e = this.children, i = 0, n = e.length; i < n; i++)
          e[i].traverse(t);
      },
      traverseVisible: function(t) {
        if (!1 !== this.visible) {
          t(this);
          for (var e = this.children, i = 0, n = e.length; i < n; i++)
            e[i].traverseVisible(t);
        }
      },
      traverseAncestors: function(t) {
        var e = this.parent;
        null !== e && (t(e), e.traverseAncestors(t));
      },
      updateMatrix: function() {
        this.matrix.compose(this.position, this.quaternion, this.scale),
          (this.matrixWorldNeedsUpdate = !0);
      },
      updateMatrixWorld: function(t) {
        !0 === this.matrixAutoUpdate && this.updateMatrix(),
          (!0 !== this.matrixWorldNeedsUpdate && !0 !== t) ||
            (null === this.parent
              ? this.matrixWorld.copy(this.matrix)
              : this.matrixWorld.multiplyMatrices(
                  this.parent.matrixWorld,
                  this.matrix
                ),
            (this.matrixWorldNeedsUpdate = !1),
            (t = !0));
        for (var e = this.children, i = 0, n = e.length; i < n; i++)
          e[i].updateMatrixWorld(t);
      },
      toJSON: function(t) {
        function e(t) {
          var e,
            i = [];
          for (e in t) {
            var n = t[e];
            delete n.metadata, i.push(n);
          }
          return i;
        }
        var i = void 0 === t || '' === t,
          n = {};
        i &&
          ((t = { geometries: {}, materials: {}, textures: {}, images: {} }),
          (n.metadata = {
            version: 4.4,
            type: 'Object',
            generator: 'Object3D.toJSON'
          }));
        var r = {};
        if (
          ((r.uuid = this.uuid),
          (r.type = this.type),
          '' !== this.name && (r.name = this.name),
          '{}' !== JSON.stringify(this.userData) &&
            (r.userData = this.userData),
          !0 === this.castShadow && (r.castShadow = !0),
          !0 === this.receiveShadow && (r.receiveShadow = !0),
          !1 === this.visible && (r.visible = !1),
          (r.matrix = this.matrix.toArray()),
          void 0 !== this.geometry &&
            (void 0 === t.geometries[this.geometry.uuid] &&
              (t.geometries[this.geometry.uuid] = this.geometry.toJSON(t)),
            (r.geometry = this.geometry.uuid)),
          void 0 !== this.material &&
            (void 0 === t.materials[this.material.uuid] &&
              (t.materials[this.material.uuid] = this.material.toJSON(t)),
            (r.material = this.material.uuid)),
          0 < this.children.length)
        ) {
          r.children = [];
          for (var a = 0; a < this.children.length; a++)
            r.children.push(this.children[a].toJSON(t).object);
        }
        if (i) {
          var i = e(t.geometries),
            a = e(t.materials),
            o = e(t.textures);
          (t = e(t.images)),
            0 < i.length && (n.geometries = i),
            0 < a.length && (n.materials = a),
            0 < o.length && (n.textures = o),
            0 < t.length && (n.images = t);
        }
        return (n.object = r), n;
      },
      clone: function(t) {
        return new this.constructor().copy(this, t);
      },
      copy: function(t, e) {
        if (
          (void 0 === e && (e = !0),
          (this.name = t.name),
          this.up.copy(t.up),
          this.position.copy(t.position),
          this.quaternion.copy(t.quaternion),
          this.scale.copy(t.scale),
          this.matrix.copy(t.matrix),
          this.matrixWorld.copy(t.matrixWorld),
          (this.matrixAutoUpdate = t.matrixAutoUpdate),
          (this.matrixWorldNeedsUpdate = t.matrixWorldNeedsUpdate),
          (this.visible = t.visible),
          (this.castShadow = t.castShadow),
          (this.receiveShadow = t.receiveShadow),
          (this.frustumCulled = t.frustumCulled),
          (this.renderOrder = t.renderOrder),
          (this.userData = JSON.parse(JSON.stringify(t.userData))),
          !0 === e)
        )
          for (var i = 0; i < t.children.length; i++)
            this.add(t.children[i].clone());
        return this;
      }
    });
  var Zn = 0;
  (at.prototype = {
    constructor: at,
    set: function(t, e) {
      return this.start.copy(t), this.end.copy(e), this;
    },
    clone: function() {
      return new this.constructor().copy(this);
    },
    copy: function(t) {
      return this.start.copy(t.start), this.end.copy(t.end), this;
    },
    getCenter: function(t) {
      return (t || new c())
        .addVectors(this.start, this.end)
        .multiplyScalar(0.5);
    },
    delta: function(t) {
      return (t || new c()).subVectors(this.end, this.start);
    },
    distanceSq: function() {
      return this.start.distanceToSquared(this.end);
    },
    distance: function() {
      return this.start.distanceTo(this.end);
    },
    at: function(t, e) {
      var i = e || new c();
      return this.delta(i)
        .multiplyScalar(t)
        .add(this.start);
    },
    closestPointToPointParameter: (function() {
      var e = new c(),
        i = new c();
      return function(n, r) {
        e.subVectors(n, this.start), i.subVectors(this.end, this.start);
        var a = i.dot(i),
          a = i.dot(e) / a;
        return r && (a = t.Math.clamp(a, 0, 1)), a;
      };
    })(),
    closestPointToPoint: function(t, e, i) {
      return (
        (t = this.closestPointToPointParameter(t, e)),
        (i = i || new c()),
        this.delta(i)
          .multiplyScalar(t)
          .add(this.start)
      );
    },
    applyMatrix4: function(t) {
      return this.start.applyMatrix4(t), this.end.applyMatrix4(t), this;
    },
    equals: function(t) {
      return t.start.equals(this.start) && t.end.equals(this.end);
    }
  }),
    (ot.normal = (function() {
      var t = new c();
      return function(e, i, n, r) {
        return (
          (r = r || new c()),
          r.subVectors(n, i),
          t.subVectors(e, i),
          r.cross(t),
          (e = r.lengthSq()),
          0 < e ? r.multiplyScalar(1 / Math.sqrt(e)) : r.set(0, 0, 0)
        );
      };
    })()),
    (ot.barycoordFromPoint = (function() {
      var t = new c(),
        e = new c(),
        i = new c();
      return function(n, r, a, o, s) {
        t.subVectors(o, r),
          e.subVectors(a, r),
          i.subVectors(n, r),
          (n = t.dot(t)),
          (r = t.dot(e)),
          (a = t.dot(i));
        var h = e.dot(e);
        o = e.dot(i);
        var l = n * h - r * r;
        return (
          (s = s || new c()),
          0 === l
            ? s.set(-2, -1, -1)
            : ((l = 1 / l),
              (h = (h * a - r * o) * l),
              (n = (n * o - r * a) * l),
              s.set(1 - h - n, n, h))
        );
      };
    })()),
    (ot.containsPoint = (function() {
      var t = new c();
      return function(e, i, n, r) {
        return (
          (e = ot.barycoordFromPoint(e, i, n, r, t)),
          0 <= e.x && 0 <= e.y && 1 >= e.x + e.y
        );
      };
    })()),
    (ot.prototype = {
      constructor: ot,
      set: function(t, e, i) {
        return this.a.copy(t), this.b.copy(e), this.c.copy(i), this;
      },
      setFromPointsAndIndices: function(t, e, i, n) {
        return this.a.copy(t[e]), this.b.copy(t[i]), this.c.copy(t[n]), this;
      },
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        return this.a.copy(t.a), this.b.copy(t.b), this.c.copy(t.c), this;
      },
      area: (function() {
        var t = new c(),
          e = new c();
        return function() {
          return (
            t.subVectors(this.c, this.b),
            e.subVectors(this.a, this.b),
            0.5 * t.cross(e).length()
          );
        };
      })(),
      midpoint: function(t) {
        return (t || new c())
          .addVectors(this.a, this.b)
          .add(this.c)
          .multiplyScalar(1 / 3);
      },
      normal: function(t) {
        return ot.normal(this.a, this.b, this.c, t);
      },
      plane: function(t) {
        return (t || new K()).setFromCoplanarPoints(this.a, this.b, this.c);
      },
      barycoordFromPoint: function(t, e) {
        return ot.barycoordFromPoint(t, this.a, this.b, this.c, e);
      },
      containsPoint: function(t) {
        return ot.containsPoint(t, this.a, this.b, this.c);
      },
      closestPointToPoint: (function() {
        var t, e, i, n;
        return function(r, a) {
          void 0 === t &&
            ((t = new K()),
            (e = [new at(), new at(), new at()]),
            (i = new c()),
            (n = new c()));
          var o = a || new c(),
            s = Infinity;
          if (
            (t.setFromCoplanarPoints(this.a, this.b, this.c),
            t.projectPoint(r, i),
            !0 === this.containsPoint(i))
          )
            o.copy(i);
          else {
            e[0].set(this.a, this.b),
              e[1].set(this.b, this.c),
              e[2].set(this.c, this.a);
            for (var h = 0; h < e.length; h++) {
              e[h].closestPointToPoint(i, !0, n);
              var l = i.distanceToSquared(n);
              l < s && ((s = l), o.copy(n));
            }
          }
          return o;
        };
      })(),
      equals: function(t) {
        return t.a.equals(this.a) && t.b.equals(this.b) && t.c.equals(this.c);
      }
    }),
    (st.prototype = {
      constructor: st,
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        (this.a = t.a),
          (this.b = t.b),
          (this.c = t.c),
          this.normal.copy(t.normal),
          this.color.copy(t.color),
          (this.materialIndex = t.materialIndex);
        for (var e = 0, i = t.vertexNormals.length; e < i; e++)
          this.vertexNormals[e] = t.vertexNormals[e].clone();
        for (e = 0, i = t.vertexColors.length; e < i; e++)
          this.vertexColors[e] = t.vertexColors[e].clone();
        return this;
      }
    }),
    (ct.prototype = Object.create(X.prototype)),
    (ct.prototype.constructor = ct),
    (ct.prototype.isMeshBasicMaterial = !0),
    (ct.prototype.copy = function(t) {
      return (
        X.prototype.copy.call(this, t),
        this.color.copy(t.color),
        (this.map = t.map),
        (this.aoMap = t.aoMap),
        (this.aoMapIntensity = t.aoMapIntensity),
        (this.specularMap = t.specularMap),
        (this.alphaMap = t.alphaMap),
        (this.envMap = t.envMap),
        (this.combine = t.combine),
        (this.reflectivity = t.reflectivity),
        (this.refractionRatio = t.refractionRatio),
        (this.wireframe = t.wireframe),
        (this.wireframeLinewidth = t.wireframeLinewidth),
        (this.wireframeLinecap = t.wireframeLinecap),
        (this.wireframeLinejoin = t.wireframeLinejoin),
        (this.skinning = t.skinning),
        (this.morphTargets = t.morphTargets),
        this
      );
    }),
    (ht.prototype = {
      constructor: ht,
      isBufferAttribute: !0,
      set needsUpdate(t) {
        !0 === t && this.version++;
      },
      setDynamic: function(t) {
        return (this.dynamic = t), this;
      },
      copy: function(t) {
        return (
          (this.array = new t.array.constructor(t.array)),
          (this.itemSize = t.itemSize),
          (this.count = t.count),
          (this.normalized = t.normalized),
          (this.dynamic = t.dynamic),
          this
        );
      },
      copyAt: function(t, e, i) {
        (t *= this.itemSize), (i *= e.itemSize);
        for (var n = 0, r = this.itemSize; n < r; n++)
          this.array[t + n] = e.array[i + n];
        return this;
      },
      copyArray: function(t) {
        return this.array.set(t), this;
      },
      copyColorsArray: function(t) {
        for (var e = this.array, i = 0, n = 0, r = t.length; n < r; n++) {
          var a = t[n];
          void 0 === a &&
            (console.warn(
              'THREE.BufferAttribute.copyColorsArray(): color is undefined',
              n
            ),
            (a = new V())),
            (e[i++] = a.r),
            (e[i++] = a.g),
            (e[i++] = a.b);
        }
        return this;
      },
      copyIndicesArray: function(t) {
        for (var e = this.array, i = 0, n = 0, r = t.length; n < r; n++) {
          var a = t[n];
          (e[i++] = a.a), (e[i++] = a.b), (e[i++] = a.c);
        }
        return this;
      },
      copyVector2sArray: function(t) {
        for (var e = this.array, n = 0, r = 0, a = t.length; r < a; r++) {
          var o = t[r];
          void 0 === o &&
            (console.warn(
              'THREE.BufferAttribute.copyVector2sArray(): vector is undefined',
              r
            ),
            (o = new i())),
            (e[n++] = o.x),
            (e[n++] = o.y);
        }
        return this;
      },
      copyVector3sArray: function(t) {
        for (var e = this.array, i = 0, n = 0, r = t.length; n < r; n++) {
          var a = t[n];
          void 0 === a &&
            (console.warn(
              'THREE.BufferAttribute.copyVector3sArray(): vector is undefined',
              n
            ),
            (a = new c())),
            (e[i++] = a.x),
            (e[i++] = a.y),
            (e[i++] = a.z);
        }
        return this;
      },
      copyVector4sArray: function(t) {
        for (var e = this.array, i = 0, n = 0, a = t.length; n < a; n++) {
          var o = t[n];
          void 0 === o &&
            (console.warn(
              'THREE.BufferAttribute.copyVector4sArray(): vector is undefined',
              n
            ),
            (o = new r())),
            (e[i++] = o.x),
            (e[i++] = o.y),
            (e[i++] = o.z),
            (e[i++] = o.w);
        }
        return this;
      },
      set: function(t, e) {
        return void 0 === e && (e = 0), this.array.set(t, e), this;
      },
      getX: function(t) {
        return this.array[t * this.itemSize];
      },
      setX: function(t, e) {
        return (this.array[t * this.itemSize] = e), this;
      },
      getY: function(t) {
        return this.array[t * this.itemSize + 1];
      },
      setY: function(t, e) {
        return (this.array[t * this.itemSize + 1] = e), this;
      },
      getZ: function(t) {
        return this.array[t * this.itemSize + 2];
      },
      setZ: function(t, e) {
        return (this.array[t * this.itemSize + 2] = e), this;
      },
      getW: function(t) {
        return this.array[t * this.itemSize + 3];
      },
      setW: function(t, e) {
        return (this.array[t * this.itemSize + 3] = e), this;
      },
      setXY: function(t, e, i) {
        return (
          (t *= this.itemSize),
          (this.array[t + 0] = e),
          (this.array[t + 1] = i),
          this
        );
      },
      setXYZ: function(t, e, i, n) {
        return (
          (t *= this.itemSize),
          (this.array[t + 0] = e),
          (this.array[t + 1] = i),
          (this.array[t + 2] = n),
          this
        );
      },
      setXYZW: function(t, e, i, n, r) {
        return (
          (t *= this.itemSize),
          (this.array[t + 0] = e),
          (this.array[t + 1] = i),
          (this.array[t + 2] = n),
          (this.array[t + 3] = r),
          this
        );
      },
      clone: function() {
        return new this.constructor().copy(this);
      }
    }),
    Object.assign(pt.prototype, e.prototype, {
      isGeometry: !0,
      applyMatrix: function(t) {
        for (
          var e = new Q().getNormalMatrix(t), i = 0, n = this.vertices.length;
          i < n;
          i++
        )
          this.vertices[i].applyMatrix4(t);
        for (i = 0, n = this.faces.length; i < n; i++) {
          (t = this.faces[i]), t.normal.applyMatrix3(e).normalize();
          for (var r = 0, a = t.vertexNormals.length; r < a; r++)
            t.vertexNormals[r].applyMatrix3(e).normalize();
        }
        return (
          null !== this.boundingBox && this.computeBoundingBox(),
          null !== this.boundingSphere && this.computeBoundingSphere(),
          (this.normalsNeedUpdate = this.verticesNeedUpdate = !0),
          this
        );
      },
      rotateX: (function() {
        var t;
        return function(e) {
          return (
            void 0 === t && (t = new h()),
            t.makeRotationX(e),
            this.applyMatrix(t),
            this
          );
        };
      })(),
      rotateY: (function() {
        var t;
        return function(e) {
          return (
            void 0 === t && (t = new h()),
            t.makeRotationY(e),
            this.applyMatrix(t),
            this
          );
        };
      })(),
      rotateZ: (function() {
        var t;
        return function(e) {
          return (
            void 0 === t && (t = new h()),
            t.makeRotationZ(e),
            this.applyMatrix(t),
            this
          );
        };
      })(),
      translate: (function() {
        var t;
        return function(e, i, n) {
          return (
            void 0 === t && (t = new h()),
            t.makeTranslation(e, i, n),
            this.applyMatrix(t),
            this
          );
        };
      })(),
      scale: (function() {
        var t;
        return function(e, i, n) {
          return (
            void 0 === t && (t = new h()),
            t.makeScale(e, i, n),
            this.applyMatrix(t),
            this
          );
        };
      })(),
      lookAt: (function() {
        var t;
        return function(e) {
          void 0 === t && (t = new rt()),
            t.lookAt(e),
            t.updateMatrix(),
            this.applyMatrix(t.matrix);
        };
      })(),
      fromBufferGeometry: function(t) {
        function e(t, e, i, r) {
          (r = new st(
            t,
            e,
            i,
            void 0 !== s ? [d[t].clone(), d[e].clone(), d[i].clone()] : [],
            void 0 !== h
              ? [n.colors[t].clone(), n.colors[e].clone(), n.colors[i].clone()]
              : [],
            r
          )),
            n.faces.push(r),
            void 0 !== l &&
              n.faceVertexUvs[0].push([
                p[t].clone(),
                p[e].clone(),
                p[i].clone()
              ]),
            void 0 !== u &&
              n.faceVertexUvs[1].push([
                f[t].clone(),
                f[e].clone(),
                f[i].clone()
              ]);
        }
        var n = this,
          r = null !== t.index ? t.index.array : void 0,
          a = t.attributes,
          o = a.position.array,
          s = void 0 !== a.normal ? a.normal.array : void 0,
          h = void 0 !== a.color ? a.color.array : void 0,
          l = void 0 !== a.uv ? a.uv.array : void 0,
          u = void 0 !== a.uv2 ? a.uv2.array : void 0;
        void 0 !== u && (this.faceVertexUvs[1] = []);
        for (
          var d = [], p = [], f = [], m = (a = 0);
          a < o.length;
          a += 3, m += 2
        )
          n.vertices.push(new c(o[a], o[a + 1], o[a + 2])),
            void 0 !== s && d.push(new c(s[a], s[a + 1], s[a + 2])),
            void 0 !== h && n.colors.push(new V(h[a], h[a + 1], h[a + 2])),
            void 0 !== l && p.push(new i(l[m], l[m + 1])),
            void 0 !== u && f.push(new i(u[m], u[m + 1]));
        if (void 0 !== r)
          if (((o = t.groups), 0 < o.length))
            for (a = 0; a < o.length; a++)
              for (
                var g = o[a], v = g.start, y = g.count, m = v, v = v + y;
                m < v;
                m += 3
              )
                e(r[m], r[m + 1], r[m + 2], g.materialIndex);
          else for (a = 0; a < r.length; a += 3) e(r[a], r[a + 1], r[a + 2]);
        else for (a = 0; a < o.length / 3; a += 3) e(a, a + 1, a + 2);
        return (
          this.computeFaceNormals(),
          null !== t.boundingBox && (this.boundingBox = t.boundingBox.clone()),
          null !== t.boundingSphere &&
            (this.boundingSphere = t.boundingSphere.clone()),
          this
        );
      },
      center: function() {
        this.computeBoundingBox();
        var t = this.boundingBox.getCenter().negate();
        return this.translate(t.x, t.y, t.z), t;
      },
      normalize: function() {
        this.computeBoundingSphere();
        var t = this.boundingSphere.center,
          e = this.boundingSphere.radius,
          e = 0 === e ? 1 : 1 / e,
          i = new h();
        return (
          i.set(
            e,
            0,
            0,
            -e * t.x,
            0,
            e,
            0,
            -e * t.y,
            0,
            0,
            e,
            -e * t.z,
            0,
            0,
            0,
            1
          ),
          this.applyMatrix(i),
          this
        );
      },
      computeFaceNormals: function() {
        for (
          var t = new c(), e = new c(), i = 0, n = this.faces.length;
          i < n;
          i++
        ) {
          var r = this.faces[i],
            a = this.vertices[r.a],
            o = this.vertices[r.b];
          t.subVectors(this.vertices[r.c], o),
            e.subVectors(a, o),
            t.cross(e),
            t.normalize(),
            r.normal.copy(t);
        }
      },
      computeVertexNormals: function(t) {
        void 0 === t && (t = !0);
        var e, i, n;
        for (
          n = Array(this.vertices.length), e = 0, i = this.vertices.length;
          e < i;
          e++
        )
          n[e] = new c();
        if (t) {
          var r,
            a,
            o,
            s = new c(),
            h = new c();
          for (t = 0, e = this.faces.length; t < e; t++)
            (i = this.faces[t]),
              (r = this.vertices[i.a]),
              (a = this.vertices[i.b]),
              (o = this.vertices[i.c]),
              s.subVectors(o, a),
              h.subVectors(r, a),
              s.cross(h),
              n[i.a].add(s),
              n[i.b].add(s),
              n[i.c].add(s);
        } else
          for (t = 0, e = this.faces.length; t < e; t++)
            (i = this.faces[t]),
              n[i.a].add(i.normal),
              n[i.b].add(i.normal),
              n[i.c].add(i.normal);
        for (e = 0, i = this.vertices.length; e < i; e++) n[e].normalize();
        for (t = 0, e = this.faces.length; t < e; t++)
          (i = this.faces[t]),
            (r = i.vertexNormals),
            3 === r.length
              ? (r[0].copy(n[i.a]), r[1].copy(n[i.b]), r[2].copy(n[i.c]))
              : ((r[0] = n[i.a].clone()),
                (r[1] = n[i.b].clone()),
                (r[2] = n[i.c].clone()));
        0 < this.faces.length && (this.normalsNeedUpdate = !0);
      },
      computeMorphNormals: function() {
        var t, e, i, n, r;
        for (i = 0, n = this.faces.length; i < n; i++)
          for (
            r = this.faces[i],
              r.__originalFaceNormal
                ? r.__originalFaceNormal.copy(r.normal)
                : (r.__originalFaceNormal = r.normal.clone()),
              r.__originalVertexNormals || (r.__originalVertexNormals = []),
              t = 0,
              e = r.vertexNormals.length;
            t < e;
            t++
          )
            r.__originalVertexNormals[t]
              ? r.__originalVertexNormals[t].copy(r.vertexNormals[t])
              : (r.__originalVertexNormals[t] = r.vertexNormals[t].clone());
        var a = new pt();
        for (
          a.faces = this.faces, t = 0, e = this.morphTargets.length;
          t < e;
          t++
        ) {
          if (!this.morphNormals[t]) {
            (this.morphNormals[t] = {}),
              (this.morphNormals[t].faceNormals = []),
              (this.morphNormals[t].vertexNormals = []),
              (r = this.morphNormals[t].faceNormals);
            var o,
              s,
              h = this.morphNormals[t].vertexNormals;
            for (i = 0, n = this.faces.length; i < n; i++)
              (o = new c()),
                (s = { a: new c(), b: new c(), c: new c() }),
                r.push(o),
                h.push(s);
          }
          for (
            h = this.morphNormals[t],
              a.vertices = this.morphTargets[t].vertices,
              a.computeFaceNormals(),
              a.computeVertexNormals(),
              i = 0,
              n = this.faces.length;
            i < n;
            i++
          )
            (r = this.faces[i]),
              (o = h.faceNormals[i]),
              (s = h.vertexNormals[i]),
              o.copy(r.normal),
              s.a.copy(r.vertexNormals[0]),
              s.b.copy(r.vertexNormals[1]),
              s.c.copy(r.vertexNormals[2]);
        }
        for (i = 0, n = this.faces.length; i < n; i++)
          (r = this.faces[i]),
            (r.normal = r.__originalFaceNormal),
            (r.vertexNormals = r.__originalVertexNormals);
      },
      computeTangents: function() {
        console.warn('THREE.Geometry: .computeTangents() has been removed.');
      },
      computeLineDistances: function() {
        for (var t = 0, e = this.vertices, i = 0, n = e.length; i < n; i++)
          0 < i && (t += e[i].distanceTo(e[i - 1])),
            (this.lineDistances[i] = t);
      },
      computeBoundingBox: function() {
        null === this.boundingBox && (this.boundingBox = new Z()),
          this.boundingBox.setFromPoints(this.vertices);
      },
      computeBoundingSphere: function() {
        null === this.boundingSphere && (this.boundingSphere = new J()),
          this.boundingSphere.setFromPoints(this.vertices);
      },
      merge: function(t, e, i) {
        if (!1 === (t && t.isGeometry))
          console.error(
            'THREE.Geometry.merge(): geometry not an instance of THREE.Geometry.',
            t
          );
        else {
          var n,
            r = this.vertices.length,
            a = this.vertices,
            o = t.vertices,
            s = this.faces,
            c = t.faces,
            h = this.faceVertexUvs[0],
            l = t.faceVertexUvs[0],
            u = this.colors,
            d = t.colors;
          void 0 === i && (i = 0),
            void 0 !== e && (n = new Q().getNormalMatrix(e)),
            (t = 0);
          for (var p = o.length; t < p; t++) {
            var f = o[t].clone();
            void 0 !== e && f.applyMatrix4(e), a.push(f);
          }
          for (t = 0, p = d.length; t < p; t++) u.push(d[t].clone());
          for (t = 0, p = c.length; t < p; t++) {
            var o = c[t],
              m = o.vertexNormals,
              d = o.vertexColors,
              u = new st(o.a + r, o.b + r, o.c + r);
            for (
              u.normal.copy(o.normal),
                void 0 !== n && u.normal.applyMatrix3(n).normalize(),
                e = 0,
                a = m.length;
              e < a;
              e++
            )
              (f = m[e].clone()),
                void 0 !== n && f.applyMatrix3(n).normalize(),
                u.vertexNormals.push(f);
            for (u.color.copy(o.color), e = 0, a = d.length; e < a; e++)
              (f = d[e]), u.vertexColors.push(f.clone());
            (u.materialIndex = o.materialIndex + i), s.push(u);
          }
          for (t = 0, p = l.length; t < p; t++)
            if (((i = l[t]), (n = []), void 0 !== i)) {
              for (e = 0, a = i.length; e < a; e++) n.push(i[e].clone());
              h.push(n);
            }
        }
      },
      mergeMesh: function(t) {
        !1 === (t && t.isMesh)
          ? console.error(
              'THREE.Geometry.mergeMesh(): mesh not an instance of THREE.Mesh.',
              t
            )
          : (t.matrixAutoUpdate && t.updateMatrix(),
            this.merge(t.geometry, t.matrix));
      },
      mergeVertices: function() {
        var t,
          e,
          i,
          n = {},
          r = [],
          a = [],
          o = Math.pow(10, 4);
        for (e = 0, i = this.vertices.length; e < i; e++)
          (t = this.vertices[e]),
            (t =
              Math.round(t.x * o) +
              '_' +
              Math.round(t.y * o) +
              '_' +
              Math.round(t.z * o)),
            void 0 === n[t]
              ? ((n[t] = e), r.push(this.vertices[e]), (a[e] = r.length - 1))
              : (a[e] = a[n[t]]);
        for (n = [], e = 0, i = this.faces.length; e < i; e++)
          for (
            o = this.faces[e],
              o.a = a[o.a],
              o.b = a[o.b],
              o.c = a[o.c],
              o = [o.a, o.b, o.c],
              t = 0;
            3 > t;
            t++
          )
            if (o[t] === o[(t + 1) % 3]) {
              n.push(e);
              break;
            }
        for (e = n.length - 1; 0 <= e; e--)
          for (
            o = n[e],
              this.faces.splice(o, 1),
              a = 0,
              i = this.faceVertexUvs.length;
            a < i;
            a++
          )
            this.faceVertexUvs[a].splice(o, 1);
        return (e = this.vertices.length - r.length), (this.vertices = r), e;
      },
      sortFacesByMaterialIndex: function() {
        for (var t = this.faces, e = t.length, i = 0; i < e; i++) t[i]._id = i;
        t.sort(function(t, e) {
          return t.materialIndex - e.materialIndex;
        });
        var n,
          r,
          a = this.faceVertexUvs[0],
          o = this.faceVertexUvs[1];
        for (
          a && a.length === e && (n = []),
            o && o.length === e && (r = []),
            i = 0;
          i < e;
          i++
        ) {
          var s = t[i]._id;
          n && n.push(a[s]), r && r.push(o[s]);
        }
        n && (this.faceVertexUvs[0] = n), r && (this.faceVertexUvs[1] = r);
      },
      toJSON: function() {
        function t(t, e, i) {
          return i ? t | (1 << e) : t & ~(1 << e);
        }
        function e(t) {
          var e = t.x.toString() + t.y.toString() + t.z.toString();
          return void 0 !== h[e]
            ? h[e]
            : ((h[e] = c.length / 3), c.push(t.x, t.y, t.z), h[e]);
        }
        function i(t) {
          var e = t.r.toString() + t.g.toString() + t.b.toString();
          return void 0 !== u[e]
            ? u[e]
            : ((u[e] = l.length), l.push(t.getHex()), u[e]);
        }
        function n(t) {
          var e = t.x.toString() + t.y.toString();
          return void 0 !== p[e]
            ? p[e]
            : ((p[e] = d.length / 2), d.push(t.x, t.y), p[e]);
        }
        var r = {
          metadata: {
            version: 4.4,
            type: 'Geometry',
            generator: 'Geometry.toJSON'
          }
        };
        if (
          ((r.uuid = this.uuid),
          (r.type = this.type),
          '' !== this.name && (r.name = this.name),
          void 0 !== this.parameters)
        ) {
          var a,
            o = this.parameters;
          for (a in o) void 0 !== o[a] && (r[a] = o[a]);
          return r;
        }
        for (o = [], a = 0; a < this.vertices.length; a++) {
          var s = this.vertices[a];
          o.push(s.x, s.y, s.z);
        }
        var s = [],
          c = [],
          h = {},
          l = [],
          u = {},
          d = [],
          p = {};
        for (a = 0; a < this.faces.length; a++) {
          var f = this.faces[a],
            m = void 0 !== this.faceVertexUvs[0][a],
            g = 0 < f.normal.length(),
            v = 0 < f.vertexNormals.length,
            y = 1 !== f.color.r || 1 !== f.color.g || 1 !== f.color.b,
            x = 0 < f.vertexColors.length,
            _ = 0,
            _ = t(_, 0, 0),
            _ = t(_, 1, !0),
            _ = t(_, 2, !1),
            _ = t(_, 3, m),
            _ = t(_, 4, g),
            _ = t(_, 5, v),
            _ = t(_, 6, y),
            _ = t(_, 7, x);
          s.push(_),
            s.push(f.a, f.b, f.c),
            s.push(f.materialIndex),
            m &&
              ((m = this.faceVertexUvs[0][a]),
              s.push(n(m[0]), n(m[1]), n(m[2]))),
            g && s.push(e(f.normal)),
            v && ((g = f.vertexNormals), s.push(e(g[0]), e(g[1]), e(g[2]))),
            y && s.push(i(f.color)),
            x && ((f = f.vertexColors), s.push(i(f[0]), i(f[1]), i(f[2])));
        }
        return (
          (r.data = {}),
          (r.data.vertices = o),
          (r.data.normals = c),
          0 < l.length && (r.data.colors = l),
          0 < d.length && (r.data.uvs = [d]),
          (r.data.faces = s),
          r
        );
      },
      clone: function() {
        return new pt().copy(this);
      },
      copy: function(t) {
        (this.vertices = []),
          (this.faces = []),
          (this.faceVertexUvs = [[]]),
          (this.colors = []);
        for (var e = t.vertices, i = 0, n = e.length; i < n; i++)
          this.vertices.push(e[i].clone());
        for (e = t.colors, i = 0, n = e.length; i < n; i++)
          this.colors.push(e[i].clone());
        for (e = t.faces, i = 0, n = e.length; i < n; i++)
          this.faces.push(e[i].clone());
        for (i = 0, n = t.faceVertexUvs.length; i < n; i++) {
          (e = t.faceVertexUvs[i]),
            void 0 === this.faceVertexUvs[i] && (this.faceVertexUvs[i] = []);
          for (var r = 0, a = e.length; r < a; r++) {
            for (var o = e[r], s = [], c = 0, h = o.length; c < h; c++)
              s.push(o[c].clone());
            this.faceVertexUvs[i].push(s);
          }
        }
        return this;
      },
      dispose: function() {
        this.dispatchEvent({ type: 'dispose' });
      }
    });
  var Jn = 0;
  Object.assign(ft.prototype, e.prototype, {
    computeBoundingBox: pt.prototype.computeBoundingBox,
    computeBoundingSphere: pt.prototype.computeBoundingSphere,
    computeFaceNormals: function() {
      console.warn(
        'THREE.DirectGeometry: computeFaceNormals() is not a method of this type of geometry.'
      );
    },
    computeVertexNormals: function() {
      console.warn(
        'THREE.DirectGeometry: computeVertexNormals() is not a method of this type of geometry.'
      );
    },
    computeGroups: function(t) {
      var e,
        i,
        n = [];
      t = t.faces;
      for (var r = 0; r < t.length; r++) {
        var a = t[r];
        a.materialIndex !== i &&
          ((i = a.materialIndex),
          void 0 !== e && ((e.count = 3 * r - e.start), n.push(e)),
          (e = { start: 3 * r, materialIndex: i }));
      }
      void 0 !== e && ((e.count = 3 * r - e.start), n.push(e)),
        (this.groups = n);
    },
    fromGeometry: function(t) {
      var e,
        n = t.faces,
        r = t.vertices,
        a = t.faceVertexUvs,
        o = a[0] && 0 < a[0].length,
        s = a[1] && 0 < a[1].length,
        c = t.morphTargets,
        h = c.length;
      if (0 < h) {
        e = [];
        for (var l = 0; l < h; l++) e[l] = [];
        this.morphTargets.position = e;
      }
      var u,
        d = t.morphNormals,
        p = d.length;
      if (0 < p) {
        for (u = [], l = 0; l < p; l++) u[l] = [];
        this.morphTargets.normal = u;
      }
      for (
        var f = t.skinIndices,
          m = t.skinWeights,
          g = f.length === r.length,
          v = m.length === r.length,
          l = 0;
        l < n.length;
        l++
      ) {
        var y = n[l];
        this.vertices.push(r[y.a], r[y.b], r[y.c]);
        var x = y.vertexNormals;
        for (
          3 === x.length
            ? this.normals.push(x[0], x[1], x[2])
            : ((x = y.normal), this.normals.push(x, x, x)),
            x = y.vertexColors,
            3 === x.length
              ? this.colors.push(x[0], x[1], x[2])
              : ((x = y.color), this.colors.push(x, x, x)),
            !0 === o &&
              ((x = a[0][l]),
              void 0 !== x
                ? this.uvs.push(x[0], x[1], x[2])
                : (console.warn(
                    'THREE.DirectGeometry.fromGeometry(): Undefined vertexUv ',
                    l
                  ),
                  this.uvs.push(new i(), new i(), new i()))),
            !0 === s &&
              ((x = a[1][l]),
              void 0 !== x
                ? this.uvs2.push(x[0], x[1], x[2])
                : (console.warn(
                    'THREE.DirectGeometry.fromGeometry(): Undefined vertexUv2 ',
                    l
                  ),
                  this.uvs2.push(new i(), new i(), new i()))),
            x = 0;
          x < h;
          x++
        ) {
          var _ = c[x].vertices;
          e[x].push(_[y.a], _[y.b], _[y.c]);
        }
        for (x = 0; x < p; x++)
          (_ = d[x].vertexNormals[l]), u[x].push(_.a, _.b, _.c);
        g && this.skinIndices.push(f[y.a], f[y.b], f[y.c]),
          v && this.skinWeights.push(m[y.a], m[y.b], m[y.c]);
      }
      return (
        this.computeGroups(t),
        (this.verticesNeedUpdate = t.verticesNeedUpdate),
        (this.normalsNeedUpdate = t.normalsNeedUpdate),
        (this.colorsNeedUpdate = t.colorsNeedUpdate),
        (this.uvsNeedUpdate = t.uvsNeedUpdate),
        (this.groupsNeedUpdate = t.groupsNeedUpdate),
        this
      );
    },
    dispose: function() {
      this.dispatchEvent({ type: 'dispose' });
    }
  }),
    Object.assign(mt.prototype, e.prototype, {
      isBufferGeometry: !0,
      getIndex: function() {
        return this.index;
      },
      setIndex: function(t) {
        this.index = t;
      },
      addAttribute: function(t, e, i) {
        if (
          !1 === (e && e.isBufferAttribute) &&
          !1 === (e && e.isInterleavedBufferAttribute)
        )
          console.warn(
            'THREE.BufferGeometry: .addAttribute() now expects ( name, attribute ).'
          ),
            this.addAttribute(t, new ht(e, i));
        else {
          if ('index' !== t) return (this.attributes[t] = e), this;
          console.warn(
            'THREE.BufferGeometry.addAttribute: Use .setIndex() for index attribute.'
          ),
            this.setIndex(e);
        }
      },
      getAttribute: function(t) {
        return this.attributes[t];
      },
      removeAttribute: function(t) {
        return delete this.attributes[t], this;
      },
      addGroup: function(t, e, i) {
        this.groups.push({
          start: t,
          count: e,
          materialIndex: void 0 !== i ? i : 0
        });
      },
      clearGroups: function() {
        this.groups = [];
      },
      setDrawRange: function(t, e) {
        (this.drawRange.start = t), (this.drawRange.count = e);
      },
      applyMatrix: function(t) {
        var e = this.attributes.position;
        return (
          void 0 !== e &&
            (t.applyToVector3Array(e.array), (e.needsUpdate = !0)),
          (e = this.attributes.normal),
          void 0 !== e &&
            (new Q().getNormalMatrix(t).applyToVector3Array(e.array),
            (e.needsUpdate = !0)),
          null !== this.boundingBox && this.computeBoundingBox(),
          null !== this.boundingSphere && this.computeBoundingSphere(),
          this
        );
      },
      rotateX: (function() {
        var t;
        return function(e) {
          return (
            void 0 === t && (t = new h()),
            t.makeRotationX(e),
            this.applyMatrix(t),
            this
          );
        };
      })(),
      rotateY: (function() {
        var t;
        return function(e) {
          return (
            void 0 === t && (t = new h()),
            t.makeRotationY(e),
            this.applyMatrix(t),
            this
          );
        };
      })(),
      rotateZ: (function() {
        var t;
        return function(e) {
          return (
            void 0 === t && (t = new h()),
            t.makeRotationZ(e),
            this.applyMatrix(t),
            this
          );
        };
      })(),
      translate: (function() {
        var t;
        return function(e, i, n) {
          return (
            void 0 === t && (t = new h()),
            t.makeTranslation(e, i, n),
            this.applyMatrix(t),
            this
          );
        };
      })(),
      scale: (function() {
        var t;
        return function(e, i, n) {
          return (
            void 0 === t && (t = new h()),
            t.makeScale(e, i, n),
            this.applyMatrix(t),
            this
          );
        };
      })(),
      lookAt: (function() {
        var t;
        return function(e) {
          void 0 === t && (t = new rt()),
            t.lookAt(e),
            t.updateMatrix(),
            this.applyMatrix(t.matrix);
        };
      })(),
      center: function() {
        this.computeBoundingBox();
        var t = this.boundingBox.getCenter().negate();
        return this.translate(t.x, t.y, t.z), t;
      },
      setFromObject: function(t) {
        var e = t.geometry;
        if ((t && t.isPoints) || (t && t.isLine)) {
          t = new dt(3 * e.vertices.length, 3);
          var i = new dt(3 * e.colors.length, 3);
          this.addAttribute('position', t.copyVector3sArray(e.vertices)),
            this.addAttribute('color', i.copyColorsArray(e.colors)),
            e.lineDistances &&
              e.lineDistances.length === e.vertices.length &&
              ((t = new dt(e.lineDistances.length, 1)),
              this.addAttribute('lineDistance', t.copyArray(e.lineDistances))),
            null !== e.boundingSphere &&
              (this.boundingSphere = e.boundingSphere.clone()),
            null !== e.boundingBox &&
              (this.boundingBox = e.boundingBox.clone());
        } else t && t.isMesh && e && e.isGeometry && this.fromGeometry(e);
        return this;
      },
      updateFromObject: function(t) {
        var e = t.geometry;
        if (t && t.isMesh) {
          var i = e.__directGeometry;
          if (
            (!0 === e.elementsNeedUpdate &&
              ((i = void 0), (e.elementsNeedUpdate = !1)),
            void 0 === i)
          )
            return this.fromGeometry(e);
          (i.verticesNeedUpdate = e.verticesNeedUpdate),
            (i.normalsNeedUpdate = e.normalsNeedUpdate),
            (i.colorsNeedUpdate = e.colorsNeedUpdate),
            (i.uvsNeedUpdate = e.uvsNeedUpdate),
            (i.groupsNeedUpdate = e.groupsNeedUpdate),
            (e.verticesNeedUpdate = !1),
            (e.normalsNeedUpdate = !1),
            (e.colorsNeedUpdate = !1),
            (e.uvsNeedUpdate = !1),
            (e.groupsNeedUpdate = !1),
            (e = i);
        }
        return (
          !0 === e.verticesNeedUpdate &&
            ((i = this.attributes.position),
            void 0 !== i &&
              (i.copyVector3sArray(e.vertices), (i.needsUpdate = !0)),
            (e.verticesNeedUpdate = !1)),
          !0 === e.normalsNeedUpdate &&
            ((i = this.attributes.normal),
            void 0 !== i &&
              (i.copyVector3sArray(e.normals), (i.needsUpdate = !0)),
            (e.normalsNeedUpdate = !1)),
          !0 === e.colorsNeedUpdate &&
            ((i = this.attributes.color),
            void 0 !== i && (i.copyColorsArray(e.colors), (i.needsUpdate = !0)),
            (e.colorsNeedUpdate = !1)),
          e.uvsNeedUpdate &&
            ((i = this.attributes.uv),
            void 0 !== i && (i.copyVector2sArray(e.uvs), (i.needsUpdate = !0)),
            (e.uvsNeedUpdate = !1)),
          e.lineDistancesNeedUpdate &&
            ((i = this.attributes.lineDistance),
            void 0 !== i &&
              (i.copyArray(e.lineDistances), (i.needsUpdate = !0)),
            (e.lineDistancesNeedUpdate = !1)),
          e.groupsNeedUpdate &&
            (e.computeGroups(t.geometry),
            (this.groups = e.groups),
            (e.groupsNeedUpdate = !1)),
          this
        );
      },
      fromGeometry: function(t) {
        return (
          (t.__directGeometry = new ft().fromGeometry(t)),
          this.fromDirectGeometry(t.__directGeometry)
        );
      },
      fromDirectGeometry: function(t) {
        var e = new Float32Array(3 * t.vertices.length);
        this.addAttribute(
          'position',
          new ht(e, 3).copyVector3sArray(t.vertices)
        ),
          0 < t.normals.length &&
            ((e = new Float32Array(3 * t.normals.length)),
            this.addAttribute(
              'normal',
              new ht(e, 3).copyVector3sArray(t.normals)
            )),
          0 < t.colors.length &&
            ((e = new Float32Array(3 * t.colors.length)),
            this.addAttribute('color', new ht(e, 3).copyColorsArray(t.colors))),
          0 < t.uvs.length &&
            ((e = new Float32Array(2 * t.uvs.length)),
            this.addAttribute('uv', new ht(e, 2).copyVector2sArray(t.uvs))),
          0 < t.uvs2.length &&
            ((e = new Float32Array(2 * t.uvs2.length)),
            this.addAttribute('uv2', new ht(e, 2).copyVector2sArray(t.uvs2))),
          0 < t.indices.length &&
            ((e = new (65535 < t.vertices.length ? Uint32Array : Uint16Array)(
              3 * t.indices.length
            )),
            this.setIndex(new ht(e, 1).copyIndicesArray(t.indices))),
          (this.groups = t.groups);
        for (var i in t.morphTargets) {
          for (
            var e = [], n = t.morphTargets[i], r = 0, a = n.length;
            r < a;
            r++
          ) {
            var o = n[r],
              s = new dt(3 * o.length, 3);
            e.push(s.copyVector3sArray(o));
          }
          this.morphAttributes[i] = e;
        }
        return (
          0 < t.skinIndices.length &&
            ((i = new dt(4 * t.skinIndices.length, 4)),
            this.addAttribute('skinIndex', i.copyVector4sArray(t.skinIndices))),
          0 < t.skinWeights.length &&
            ((i = new dt(4 * t.skinWeights.length, 4)),
            this.addAttribute(
              'skinWeight',
              i.copyVector4sArray(t.skinWeights)
            )),
          null !== t.boundingSphere &&
            (this.boundingSphere = t.boundingSphere.clone()),
          null !== t.boundingBox && (this.boundingBox = t.boundingBox.clone()),
          this
        );
      },
      computeBoundingBox: function() {
        null === this.boundingBox && (this.boundingBox = new Z());
        var t = this.attributes.position.array;
        void 0 !== t
          ? this.boundingBox.setFromArray(t)
          : this.boundingBox.makeEmpty(),
          (isNaN(this.boundingBox.min.x) ||
            isNaN(this.boundingBox.min.y) ||
            isNaN(this.boundingBox.min.z)) &&
            console.error(
              'THREE.BufferGeometry.computeBoundingBox: Computed min/max have NaN values. The "position" attribute is likely to have NaN values.',
              this
            );
      },
      computeBoundingSphere: (function() {
        var t = new Z(),
          e = new c();
        return function() {
          null === this.boundingSphere && (this.boundingSphere = new J());
          var i = this.attributes.position;
          if (i) {
            var i = i.array,
              n = this.boundingSphere.center;
            t.setFromArray(i), t.getCenter(n);
            for (var r = 0, a = 0, o = i.length; a < o; a += 3)
              e.fromArray(i, a), (r = Math.max(r, n.distanceToSquared(e)));
            (this.boundingSphere.radius = Math.sqrt(r)),
              isNaN(this.boundingSphere.radius) &&
                console.error(
                  'THREE.BufferGeometry.computeBoundingSphere(): Computed radius is NaN. The "position" attribute is likely to have NaN values.',
                  this
                );
          }
        };
      })(),
      computeFaceNormals: function() {},
      computeVertexNormals: function() {
        var t = this.index,
          e = this.attributes,
          i = this.groups;
        if (e.position) {
          var n = e.position.array;
          if (void 0 === e.normal)
            this.addAttribute('normal', new ht(new Float32Array(n.length), 3));
          else
            for (var r = e.normal.array, a = 0, o = r.length; a < o; a++)
              r[a] = 0;
          var s,
            h,
            l,
            r = e.normal.array,
            u = new c(),
            d = new c(),
            p = new c(),
            f = new c(),
            m = new c();
          if (t) {
            (t = t.array), 0 === i.length && this.addGroup(0, t.length);
            for (var g = 0, v = i.length; g < v; ++g)
              for (
                a = i[g], o = a.start, s = a.count, a = o, o += s;
                a < o;
                a += 3
              )
                (s = 3 * t[a + 0]),
                  (h = 3 * t[a + 1]),
                  (l = 3 * t[a + 2]),
                  u.fromArray(n, s),
                  d.fromArray(n, h),
                  p.fromArray(n, l),
                  f.subVectors(p, d),
                  m.subVectors(u, d),
                  f.cross(m),
                  (r[s] += f.x),
                  (r[s + 1] += f.y),
                  (r[s + 2] += f.z),
                  (r[h] += f.x),
                  (r[h + 1] += f.y),
                  (r[h + 2] += f.z),
                  (r[l] += f.x),
                  (r[l + 1] += f.y),
                  (r[l + 2] += f.z);
          } else
            for (a = 0, o = n.length; a < o; a += 9)
              u.fromArray(n, a),
                d.fromArray(n, a + 3),
                p.fromArray(n, a + 6),
                f.subVectors(p, d),
                m.subVectors(u, d),
                f.cross(m),
                (r[a] = f.x),
                (r[a + 1] = f.y),
                (r[a + 2] = f.z),
                (r[a + 3] = f.x),
                (r[a + 4] = f.y),
                (r[a + 5] = f.z),
                (r[a + 6] = f.x),
                (r[a + 7] = f.y),
                (r[a + 8] = f.z);
          this.normalizeNormals(), (e.normal.needsUpdate = !0);
        }
      },
      merge: function(t, e) {
        if (!1 !== (t && t.isBufferGeometry)) {
          void 0 === e && (e = 0);
          var i,
            n = this.attributes;
          for (i in n)
            if (void 0 !== t.attributes[i])
              for (
                var r = n[i].array,
                  a = t.attributes[i],
                  o = a.array,
                  s = 0,
                  a = a.itemSize * e;
                s < o.length;
                s++, a++
              )
                r[a] = o[s];
          return this;
        }
        console.error(
          'THREE.BufferGeometry.merge(): geometry not an instance of THREE.BufferGeometry.',
          t
        );
      },
      normalizeNormals: function() {
        for (
          var t, e, i, n = this.attributes.normal.array, r = 0, a = n.length;
          r < a;
          r += 3
        )
          (t = n[r]),
            (e = n[r + 1]),
            (i = n[r + 2]),
            (t = 1 / Math.sqrt(t * t + e * e + i * i)),
            (n[r] *= t),
            (n[r + 1] *= t),
            (n[r + 2] *= t);
      },
      toNonIndexed: function() {
        if (null === this.index)
          return (
            console.warn(
              'THREE.BufferGeometry.toNonIndexed(): Geometry is already non-indexed.'
            ),
            this
          );
        var t,
          e = new mt(),
          i = this.index.array,
          n = this.attributes;
        for (t in n) {
          for (
            var r,
              a = n[t],
              o = a.array,
              a = a.itemSize,
              s = new o.constructor(i.length * a),
              c = 0,
              h = 0,
              l = i.length;
            h < l;
            h++
          ) {
            r = i[h] * a;
            for (var u = 0; u < a; u++) s[c++] = o[r++];
          }
          e.addAttribute(t, new ht(s, a));
        }
        return e;
      },
      toJSON: function() {
        var t = {
          metadata: {
            version: 4.4,
            type: 'BufferGeometry',
            generator: 'BufferGeometry.toJSON'
          }
        };
        if (
          ((t.uuid = this.uuid),
          (t.type = this.type),
          '' !== this.name && (t.name = this.name),
          void 0 !== this.parameters)
        ) {
          var e,
            i = this.parameters;
          for (e in i) void 0 !== i[e] && (t[e] = i[e]);
          return t;
        }
        t.data = { attributes: {} };
        var n = this.index;
        null !== n &&
          ((i = Array.prototype.slice.call(n.array)),
          (t.data.index = { type: n.array.constructor.name, array: i })),
          (n = this.attributes);
        for (e in n) {
          var r = n[e],
            i = Array.prototype.slice.call(r.array);
          t.data.attributes[e] = {
            itemSize: r.itemSize,
            type: r.array.constructor.name,
            array: i,
            normalized: r.normalized
          };
        }
        return (
          (e = this.groups),
          0 < e.length && (t.data.groups = JSON.parse(JSON.stringify(e))),
          (e = this.boundingSphere),
          null !== e &&
            (t.data.boundingSphere = {
              center: e.center.toArray(),
              radius: e.radius
            }),
          t
        );
      },
      clone: function() {
        return new mt().copy(this);
      },
      copy: function(t) {
        var e = t.index;
        null !== e && this.setIndex(e.clone());
        var i,
          e = t.attributes;
        for (i in e) this.addAttribute(i, e[i].clone());
        for (t = t.groups, i = 0, e = t.length; i < e; i++) {
          var n = t[i];
          this.addGroup(n.start, n.count, n.materialIndex);
        }
        return this;
      },
      dispose: function() {
        this.dispatchEvent({ type: 'dispose' });
      }
    }),
    (mt.MaxIndex = 65535),
    (gt.prototype = Object.assign(Object.create(rt.prototype), {
      constructor: gt,
      isMesh: !0,
      setDrawMode: function(t) {
        this.drawMode = t;
      },
      copy: function(t) {
        return (
          rt.prototype.copy.call(this, t), (this.drawMode = t.drawMode), this
        );
      },
      updateMorphTargets: function() {
        var t = this.geometry.morphTargets;
        if (void 0 !== t && 0 < t.length) {
          (this.morphTargetInfluences = []), (this.morphTargetDictionary = {});
          for (var e = 0, i = t.length; e < i; e++)
            this.morphTargetInfluences.push(0),
              (this.morphTargetDictionary[t[e].name] = e);
        }
      },
      raycast: (function() {
        function t(t, e, i, n, r, a, o) {
          return (
            ot.barycoordFromPoint(t, e, i, n, y),
            r.multiplyScalar(y.x),
            a.multiplyScalar(y.y),
            o.multiplyScalar(y.z),
            r.add(a).add(o),
            r.clone()
          );
        }
        function e(t, e, i, n, r, a, o) {
          var s = t.material;
          return null ===
            (1 === s.side
              ? i.intersectTriangle(a, r, n, !0, o)
              : i.intersectTriangle(n, r, a, 2 !== s.side, o))
            ? null
            : (_.copy(o),
              _.applyMatrix4(t.matrixWorld),
              (i = e.ray.origin.distanceTo(_)),
              i < e.near || i > e.far
                ? null
                : { distance: i, point: _.clone(), object: t });
        }
        function n(i, n, r, a, o, c, h, d) {
          return (
            s.fromArray(a, 3 * c),
            l.fromArray(a, 3 * h),
            u.fromArray(a, 3 * d),
            (i = e(i, n, r, s, l, u, x)) &&
              (o &&
                (m.fromArray(o, 2 * c),
                g.fromArray(o, 2 * h),
                v.fromArray(o, 2 * d),
                (i.uv = t(x, s, l, u, m, g, v))),
              (i.face = new st(c, h, d, ot.normal(s, l, u))),
              (i.faceIndex = c)),
            i
          );
        }
        var r = new h(),
          a = new et(),
          o = new J(),
          s = new c(),
          l = new c(),
          u = new c(),
          d = new c(),
          p = new c(),
          f = new c(),
          m = new i(),
          g = new i(),
          v = new i(),
          y = new c(),
          x = new c(),
          _ = new c();
        return function(i, c) {
          var h = this.geometry,
            y = this.material,
            _ = this.matrixWorld;
          if (
            void 0 !== y &&
            (null === h.boundingSphere && h.computeBoundingSphere(),
            o.copy(h.boundingSphere),
            o.applyMatrix4(_),
            !1 !== i.ray.intersectsSphere(o) &&
              (r.getInverse(_),
              a.copy(i.ray).applyMatrix4(r),
              null === h.boundingBox || !1 !== a.intersectsBox(h.boundingBox)))
          ) {
            var b, w;
            if (h && h.isBufferGeometry) {
              var M,
                E,
                y = h.index,
                _ = h.attributes,
                h = _.position.array;
              if ((void 0 !== _.uv && (b = _.uv.array), null !== y))
                for (var _ = y.array, S = 0, T = _.length; S < T; S += 3)
                  (y = _[S]),
                    (M = _[S + 1]),
                    (E = _[S + 2]),
                    (w = n(this, i, a, h, b, y, M, E)) &&
                      ((w.faceIndex = Math.floor(S / 3)), c.push(w));
              else
                for (S = 0, T = h.length; S < T; S += 9)
                  (y = S / 3),
                    (M = y + 1),
                    (E = y + 2),
                    (w = n(this, i, a, h, b, y, M, E)) &&
                      ((w.index = y), c.push(w));
            } else if (h && h.isGeometry) {
              var A,
                L,
                _ = y && y.isMultiMaterial,
                S = !0 === _ ? y.materials : null,
                T = h.vertices;
              (M = h.faces), (E = h.faceVertexUvs[0]), 0 < E.length && (b = E);
              for (var R = 0, P = M.length; R < P; R++) {
                var C = M[R];
                if (void 0 !== (w = !0 === _ ? S[C.materialIndex] : y)) {
                  if (
                    ((E = T[C.a]),
                    (A = T[C.b]),
                    (L = T[C.c]),
                    !0 === w.morphTargets)
                  ) {
                    w = h.morphTargets;
                    var I = this.morphTargetInfluences;
                    s.set(0, 0, 0), l.set(0, 0, 0), u.set(0, 0, 0);
                    for (var U = 0, D = w.length; U < D; U++) {
                      var N = I[U];
                      if (0 !== N) {
                        var F = w[U].vertices;
                        s.addScaledVector(d.subVectors(F[C.a], E), N),
                          l.addScaledVector(p.subVectors(F[C.b], A), N),
                          u.addScaledVector(f.subVectors(F[C.c], L), N);
                      }
                    }
                    s.add(E), l.add(A), u.add(L), (E = s), (A = l), (L = u);
                  }
                  (w = e(this, i, a, E, A, L, x)) &&
                    (b &&
                      ((I = b[R]),
                      m.copy(I[0]),
                      g.copy(I[1]),
                      v.copy(I[2]),
                      (w.uv = t(x, E, A, L, m, g, v))),
                    (w.face = C),
                    (w.faceIndex = R),
                    c.push(w));
                }
              }
            }
          }
        };
      })(),
      clone: function() {
        return new this.constructor(this.geometry, this.material).copy(this);
      }
    })),
    (vt.prototype = Object.create(mt.prototype)),
    (vt.prototype.constructor = vt),
    (yt.prototype = Object.create(mt.prototype)),
    (yt.prototype.constructor = yt),
    (xt.prototype = Object.create(rt.prototype)),
    (xt.prototype.constructor = xt),
    (xt.prototype.isCamera = !0),
    (xt.prototype.getWorldDirection = (function() {
      var t = new s();
      return function(e) {
        return (
          (e = e || new c()),
          this.getWorldQuaternion(t),
          e.set(0, 0, -1).applyQuaternion(t)
        );
      };
    })()),
    (xt.prototype.lookAt = (function() {
      var t = new h();
      return function(e) {
        t.lookAt(this.position, e, this.up),
          this.quaternion.setFromRotationMatrix(t);
      };
    })()),
    (xt.prototype.clone = function() {
      return new this.constructor().copy(this);
    }),
    (xt.prototype.copy = function(t) {
      return (
        rt.prototype.copy.call(this, t),
        this.matrixWorldInverse.copy(t.matrixWorldInverse),
        this.projectionMatrix.copy(t.projectionMatrix),
        this
      );
    }),
    (_t.prototype = Object.assign(Object.create(xt.prototype), {
      constructor: _t,
      isPerspectiveCamera: !0,
      copy: function(t) {
        return (
          xt.prototype.copy.call(this, t),
          (this.fov = t.fov),
          (this.zoom = t.zoom),
          (this.near = t.near),
          (this.far = t.far),
          (this.focus = t.focus),
          (this.aspect = t.aspect),
          (this.view = null === t.view ? null : Object.assign({}, t.view)),
          (this.filmGauge = t.filmGauge),
          (this.filmOffset = t.filmOffset),
          this
        );
      },
      setFocalLength: function(e) {
        (e = 0.5 * this.getFilmHeight() / e),
          (this.fov = 2 * t.Math.RAD2DEG * Math.atan(e)),
          this.updateProjectionMatrix();
      },
      getFocalLength: function() {
        var e = Math.tan(0.5 * t.Math.DEG2RAD * this.fov);
        return 0.5 * this.getFilmHeight() / e;
      },
      getEffectiveFOV: function() {
        return (
          2 *
          t.Math.RAD2DEG *
          Math.atan(Math.tan(0.5 * t.Math.DEG2RAD * this.fov) / this.zoom)
        );
      },
      getFilmWidth: function() {
        return this.filmGauge * Math.min(this.aspect, 1);
      },
      getFilmHeight: function() {
        return this.filmGauge / Math.max(this.aspect, 1);
      },
      setViewOffset: function(t, e, i, n, r, a) {
        (this.aspect = t / e),
          (this.view = {
            fullWidth: t,
            fullHeight: e,
            offsetX: i,
            offsetY: n,
            width: r,
            height: a
          }),
          this.updateProjectionMatrix();
      },
      clearViewOffset: function() {
        (this.view = null), this.updateProjectionMatrix();
      },
      updateProjectionMatrix: function() {
        var e = this.near,
          i = e * Math.tan(0.5 * t.Math.DEG2RAD * this.fov) / this.zoom,
          n = 2 * i,
          r = this.aspect * n,
          a = -0.5 * r,
          o = this.view;
        if (null !== o)
          var s = o.fullWidth,
            c = o.fullHeight,
            a = a + o.offsetX * r / s,
            i = i - o.offsetY * n / c,
            r = o.width / s * r,
            n = o.height / c * n;
        (o = this.filmOffset),
          0 !== o && (a += e * o / this.getFilmWidth()),
          this.projectionMatrix.makeFrustum(a, a + r, i - n, i, e, this.far);
      },
      toJSON: function(t) {
        return (
          (t = rt.prototype.toJSON.call(this, t)),
          (t.object.fov = this.fov),
          (t.object.zoom = this.zoom),
          (t.object.near = this.near),
          (t.object.far = this.far),
          (t.object.focus = this.focus),
          (t.object.aspect = this.aspect),
          null !== this.view && (t.object.view = Object.assign({}, this.view)),
          (t.object.filmGauge = this.filmGauge),
          (t.object.filmOffset = this.filmOffset),
          t
        );
      }
    })),
    (bt.prototype = Object.assign(Object.create(xt.prototype), {
      constructor: bt,
      isOrthographicCamera: !0,
      copy: function(t) {
        return (
          xt.prototype.copy.call(this, t),
          (this.left = t.left),
          (this.right = t.right),
          (this.top = t.top),
          (this.bottom = t.bottom),
          (this.near = t.near),
          (this.far = t.far),
          (this.zoom = t.zoom),
          (this.view = null === t.view ? null : Object.assign({}, t.view)),
          this
        );
      },
      setViewOffset: function(t, e, i, n, r, a) {
        (this.view = {
          fullWidth: t,
          fullHeight: e,
          offsetX: i,
          offsetY: n,
          width: r,
          height: a
        }),
          this.updateProjectionMatrix();
      },
      clearViewOffset: function() {
        (this.view = null), this.updateProjectionMatrix();
      },
      updateProjectionMatrix: function() {
        var t = (this.right - this.left) / (2 * this.zoom),
          e = (this.top - this.bottom) / (2 * this.zoom),
          i = (this.right + this.left) / 2,
          n = (this.top + this.bottom) / 2,
          r = i - t,
          i = i + t,
          t = n + e,
          e = n - e;
        if (null !== this.view)
          var i = this.zoom / (this.view.width / this.view.fullWidth),
            e = this.zoom / (this.view.height / this.view.fullHeight),
            a = (this.right - this.left) / this.view.width,
            n = (this.top - this.bottom) / this.view.height,
            r = r + this.view.offsetX / i * a,
            i = r + this.view.width / i * a,
            t = t - this.view.offsetY / e * n,
            e = t - this.view.height / e * n;
        this.projectionMatrix.makeOrthographic(r, i, t, e, this.near, this.far);
      },
      toJSON: function(t) {
        return (
          (t = rt.prototype.toJSON.call(this, t)),
          (t.object.zoom = this.zoom),
          (t.object.left = this.left),
          (t.object.right = this.right),
          (t.object.top = this.top),
          (t.object.bottom = this.bottom),
          (t.object.near = this.near),
          (t.object.far = this.far),
          null !== this.view && (t.object.view = Object.assign({}, this.view)),
          t
        );
      }
    }));
  var Qn = 0;
  (Yt.prototype.isFogExp2 = !0),
    (Yt.prototype.clone = function() {
      return new Yt(this.color.getHex(), this.density);
    }),
    (Yt.prototype.toJSON = function() {
      return {
        type: 'FogExp2',
        color: this.color.getHex(),
        density: this.density
      };
    }),
    (Zt.prototype.isFog = !0),
    (Zt.prototype.clone = function() {
      return new Zt(this.color.getHex(), this.near, this.far);
    }),
    (Zt.prototype.toJSON = function() {
      return {
        type: 'Fog',
        color: this.color.getHex(),
        near: this.near,
        far: this.far
      };
    }),
    (Jt.prototype = Object.create(rt.prototype)),
    (Jt.prototype.constructor = Jt),
    (Jt.prototype.copy = function(t, e) {
      return (
        rt.prototype.copy.call(this, t, e),
        null !== t.background && (this.background = t.background.clone()),
        null !== t.fog && (this.fog = t.fog.clone()),
        null !== t.overrideMaterial &&
          (this.overrideMaterial = t.overrideMaterial.clone()),
        (this.autoUpdate = t.autoUpdate),
        (this.matrixAutoUpdate = t.matrixAutoUpdate),
        this
      );
    }),
    (Jt.prototype.toJSON = function(t) {
      var e = rt.prototype.toJSON.call(this, t);
      return (
        null !== this.background &&
          (e.object.background = this.background.toJSON(t)),
        null !== this.fog && (e.object.fog = this.fog.toJSON()),
        e
      );
    }),
    (Qt.prototype = Object.assign(Object.create(rt.prototype), {
      constructor: Qt,
      isLensFlare: !0,
      copy: function(t) {
        rt.prototype.copy.call(this, t),
          this.positionScreen.copy(t.positionScreen),
          (this.customUpdateCallback = t.customUpdateCallback);
        for (var e = 0, i = t.lensFlares.length; e < i; e++)
          this.lensFlares.push(t.lensFlares[e]);
        return this;
      },
      add: function(t, e, i, n, r, a) {
        void 0 === e && (e = -1),
          void 0 === i && (i = 0),
          void 0 === a && (a = 1),
          void 0 === r && (r = new V(16777215)),
          void 0 === n && (n = 1),
          (i = Math.min(i, Math.max(0, i))),
          this.lensFlares.push({
            texture: t,
            size: e,
            distance: i,
            x: 0,
            y: 0,
            z: 0,
            scale: 1,
            rotation: 0,
            opacity: a,
            color: r,
            blending: n
          });
      },
      updateLensFlares: function() {
        var t,
          e,
          i = this.lensFlares.length,
          n = 2 * -this.positionScreen.x,
          r = 2 * -this.positionScreen.y;
        for (t = 0; t < i; t++)
          (e = this.lensFlares[t]),
            (e.x = this.positionScreen.x + n * e.distance),
            (e.y = this.positionScreen.y + r * e.distance),
            (e.wantedRotation = e.x * Math.PI * 0.25),
            (e.rotation += 0.25 * (e.wantedRotation - e.rotation));
      }
    })),
    (Kt.prototype = Object.create(X.prototype)),
    (Kt.prototype.constructor = Kt),
    (Kt.prototype.copy = function(t) {
      return (
        X.prototype.copy.call(this, t),
        this.color.copy(t.color),
        (this.map = t.map),
        (this.rotation = t.rotation),
        this
      );
    }),
    ($t.prototype = Object.assign(Object.create(rt.prototype), {
      constructor: $t,
      isSprite: !0,
      raycast: (function() {
        var t = new c();
        return function(e, i) {
          t.setFromMatrixPosition(this.matrixWorld);
          var n = e.ray.distanceSqToPoint(t);
          n > this.scale.x * this.scale.y / 4 ||
            i.push({
              distance: Math.sqrt(n),
              point: this.position,
              face: null,
              object: this
            });
        };
      })(),
      clone: function() {
        return new this.constructor(this.material).copy(this);
      }
    })),
    (te.prototype = Object.assign(Object.create(rt.prototype), {
      constructor: te,
      copy: function(t) {
        rt.prototype.copy.call(this, t, !1), (t = t.levels);
        for (var e = 0, i = t.length; e < i; e++) {
          var n = t[e];
          this.addLevel(n.object.clone(), n.distance);
        }
        return this;
      },
      addLevel: function(t, e) {
        void 0 === e && (e = 0), (e = Math.abs(e));
        for (
          var i = this.levels, n = 0;
          n < i.length && !(e < i[n].distance);
          n++
        );
        i.splice(n, 0, { distance: e, object: t }), this.add(t);
      },
      getObjectForDistance: function(t) {
        for (
          var e = this.levels, i = 1, n = e.length;
          i < n && !(t < e[i].distance);
          i++
        );
        return e[i - 1].object;
      },
      raycast: (function() {
        var t = new c();
        return function(e, i) {
          t.setFromMatrixPosition(this.matrixWorld);
          var n = e.ray.origin.distanceTo(t);
          this.getObjectForDistance(n).raycast(e, i);
        };
      })(),
      update: (function() {
        var t = new c(),
          e = new c();
        return function(i) {
          var n = this.levels;
          if (1 < n.length) {
            t.setFromMatrixPosition(i.matrixWorld),
              e.setFromMatrixPosition(this.matrixWorld),
              (i = t.distanceTo(e)),
              (n[0].object.visible = !0);
            for (var r = 1, a = n.length; r < a && i >= n[r].distance; r++)
              (n[r - 1].object.visible = !1), (n[r].object.visible = !0);
            for (; r < a; r++) n[r].object.visible = !1;
          }
        };
      })(),
      toJSON: function(t) {
        (t = rt.prototype.toJSON.call(this, t)), (t.object.levels = []);
        for (var e = this.levels, i = 0, n = e.length; i < n; i++) {
          var r = e[i];
          t.object.levels.push({ object: r.object.uuid, distance: r.distance });
        }
        return t;
      }
    })),
    (ee.prototype = Object.create(n.prototype)),
    (ee.prototype.constructor = ee),
    (ee.prototype.isDataTexture = !0),
    Object.assign(ie.prototype, {
      calculateInverses: function() {
        this.boneInverses = [];
        for (var t = 0, e = this.bones.length; t < e; t++) {
          var i = new h();
          this.bones[t] && i.getInverse(this.bones[t].matrixWorld),
            this.boneInverses.push(i);
        }
      },
      pose: function() {
        for (var t, e = 0, i = this.bones.length; e < i; e++)
          (t = this.bones[e]) && t.matrixWorld.getInverse(this.boneInverses[e]);
        for (e = 0, i = this.bones.length; e < i; e++)
          (t = this.bones[e]) &&
            (t.parent && t.parent.isBone
              ? (t.matrix.getInverse(t.parent.matrixWorld),
                t.matrix.multiply(t.matrixWorld))
              : t.matrix.copy(t.matrixWorld),
            t.matrix.decompose(t.position, t.quaternion, t.scale));
      },
      update: (function() {
        var t = new h();
        return function() {
          for (var e = 0, i = this.bones.length; e < i; e++)
            t.multiplyMatrices(
              this.bones[e] ? this.bones[e].matrixWorld : this.identityMatrix,
              this.boneInverses[e]
            ),
              t.toArray(this.boneMatrices, 16 * e);
          this.useVertexTexture && (this.boneTexture.needsUpdate = !0);
        };
      })(),
      clone: function() {
        return new ie(this.bones, this.boneInverses, this.useVertexTexture);
      }
    }),
    (ne.prototype = Object.assign(Object.create(rt.prototype), {
      constructor: ne,
      isBone: !0,
      copy: function(t) {
        return rt.prototype.copy.call(this, t), (this.skin = t.skin), this;
      }
    })),
    (re.prototype = Object.assign(Object.create(gt.prototype), {
      constructor: re,
      isSkinnedMesh: !0,
      bind: function(t, e) {
        (this.skeleton = t),
          void 0 === e &&
            (this.updateMatrixWorld(!0),
            this.skeleton.calculateInverses(),
            (e = this.matrixWorld)),
          this.bindMatrix.copy(e),
          this.bindMatrixInverse.getInverse(e);
      },
      pose: function() {
        this.skeleton.pose();
      },
      normalizeSkinWeights: function() {
        if (this.geometry && this.geometry.isGeometry)
          for (var t = 0; t < this.geometry.skinWeights.length; t++) {
            var e = this.geometry.skinWeights[t],
              i = 1 / e.lengthManhattan();
            Infinity !== i ? e.multiplyScalar(i) : e.set(1, 0, 0, 0);
          }
        else if (this.geometry && this.geometry.isBufferGeometry)
          for (
            var e = new r(), n = this.geometry.attributes.skinWeight, t = 0;
            t < n.count;
            t++
          )
            (e.x = n.getX(t)),
              (e.y = n.getY(t)),
              (e.z = n.getZ(t)),
              (e.w = n.getW(t)),
              (i = 1 / e.lengthManhattan()),
              Infinity !== i ? e.multiplyScalar(i) : e.set(1, 0, 0, 0),
              n.setXYZW(t, e.x, e.y, e.z, e.w);
      },
      updateMatrixWorld: function() {
        gt.prototype.updateMatrixWorld.call(this, !0),
          'attached' === this.bindMode
            ? this.bindMatrixInverse.getInverse(this.matrixWorld)
            : 'detached' === this.bindMode
              ? this.bindMatrixInverse.getInverse(this.bindMatrix)
              : console.warn(
                  'THREE.SkinnedMesh unrecognized bindMode: ' + this.bindMode
                );
      },
      clone: function() {
        return new this.constructor(
          this.geometry,
          this.material,
          this.skeleton.useVertexTexture
        ).copy(this);
      }
    })),
    (ae.prototype = Object.create(X.prototype)),
    (ae.prototype.constructor = ae),
    (ae.prototype.isLineBasicMaterial = !0),
    (ae.prototype.copy = function(t) {
      return (
        X.prototype.copy.call(this, t),
        this.color.copy(t.color),
        (this.linewidth = t.linewidth),
        (this.linecap = t.linecap),
        (this.linejoin = t.linejoin),
        this
      );
    }),
    (oe.prototype = Object.assign(Object.create(rt.prototype), {
      constructor: oe,
      isLine: !0,
      raycast: (function() {
        var t = new h(),
          e = new et(),
          i = new J();
        return function(n, r) {
          var a = n.linePrecision,
            a = a * a,
            o = this.geometry,
            s = this.matrixWorld;
          if (
            (null === o.boundingSphere && o.computeBoundingSphere(),
            i.copy(o.boundingSphere),
            i.applyMatrix4(s),
            !1 !== n.ray.intersectsSphere(i))
          ) {
            t.getInverse(s), e.copy(n.ray).applyMatrix4(t);
            var h = new c(),
              l = new c(),
              s = new c(),
              u = new c(),
              d = this && this.isLineSegments ? 2 : 1;
            if (o && o.isBufferGeometry) {
              var p = o.index,
                f = o.attributes.position.array;
              if (null !== p)
                for (var p = p.array, o = 0, m = p.length - 1; o < m; o += d) {
                  var g = p[o + 1];
                  h.fromArray(f, 3 * p[o]),
                    l.fromArray(f, 3 * g),
                    (g = e.distanceSqToSegment(h, l, u, s)),
                    g > a ||
                      (u.applyMatrix4(this.matrixWorld),
                      (g = n.ray.origin.distanceTo(u)) < n.near ||
                        g > n.far ||
                        r.push({
                          distance: g,
                          point: s.clone().applyMatrix4(this.matrixWorld),
                          index: o,
                          face: null,
                          faceIndex: null,
                          object: this
                        }));
                }
              else
                for (o = 0, m = f.length / 3 - 1; o < m; o += d)
                  h.fromArray(f, 3 * o),
                    l.fromArray(f, 3 * o + 3),
                    (g = e.distanceSqToSegment(h, l, u, s)) > a ||
                      (u.applyMatrix4(this.matrixWorld),
                      (g = n.ray.origin.distanceTo(u)) < n.near ||
                        g > n.far ||
                        r.push({
                          distance: g,
                          point: s.clone().applyMatrix4(this.matrixWorld),
                          index: o,
                          face: null,
                          faceIndex: null,
                          object: this
                        }));
            } else if (o && o.isGeometry)
              for (h = o.vertices, l = h.length, o = 0; o < l - 1; o += d)
                (g = e.distanceSqToSegment(h[o], h[o + 1], u, s)) > a ||
                  (u.applyMatrix4(this.matrixWorld),
                  (g = n.ray.origin.distanceTo(u)) < n.near ||
                    g > n.far ||
                    r.push({
                      distance: g,
                      point: s.clone().applyMatrix4(this.matrixWorld),
                      index: o,
                      face: null,
                      faceIndex: null,
                      object: this
                    }));
          }
        };
      })(),
      clone: function() {
        return new this.constructor(this.geometry, this.material).copy(this);
      }
    })),
    (se.prototype = Object.assign(Object.create(oe.prototype), {
      constructor: se,
      isLineSegments: !0
    })),
    (ce.prototype = Object.create(X.prototype)),
    (ce.prototype.constructor = ce),
    (ce.prototype.isPointsMaterial = !0),
    (ce.prototype.copy = function(t) {
      return (
        X.prototype.copy.call(this, t),
        this.color.copy(t.color),
        (this.map = t.map),
        (this.size = t.size),
        (this.sizeAttenuation = t.sizeAttenuation),
        this
      );
    }),
    (he.prototype = Object.assign(Object.create(rt.prototype), {
      constructor: he,
      isPoints: !0,
      raycast: (function() {
        var t = new h(),
          e = new et(),
          i = new J();
        return function(n, r) {
          function a(t, i) {
            var a = e.distanceSqToPoint(t);
            if (a < u) {
              var s = e.closestPointToPoint(t);
              s.applyMatrix4(h);
              var c = n.ray.origin.distanceTo(s);
              c < n.near ||
                c > n.far ||
                r.push({
                  distance: c,
                  distanceToRay: Math.sqrt(a),
                  point: s.clone(),
                  index: i,
                  face: null,
                  object: o
                });
            }
          }
          var o = this,
            s = this.geometry,
            h = this.matrixWorld,
            l = n.params.Points.threshold;
          if (
            (null === s.boundingSphere && s.computeBoundingSphere(),
            i.copy(s.boundingSphere),
            i.applyMatrix4(h),
            !1 !== n.ray.intersectsSphere(i))
          ) {
            t.getInverse(h), e.copy(n.ray).applyMatrix4(t);
            var l = l / ((this.scale.x + this.scale.y + this.scale.z) / 3),
              u = l * l,
              l = new c();
            if (s && s.isBufferGeometry) {
              var d = s.index,
                s = s.attributes.position.array;
              if (null !== d)
                for (var p = d.array, d = 0, f = p.length; d < f; d++) {
                  var m = p[d];
                  l.fromArray(s, 3 * m), a(l, m);
                }
              else
                for (d = 0, p = s.length / 3; d < p; d++)
                  l.fromArray(s, 3 * d), a(l, d);
            } else
              for (l = s.vertices, d = 0, p = l.length; d < p; d++) a(l[d], d);
          }
        };
      })(),
      clone: function() {
        return new this.constructor(this.geometry, this.material).copy(this);
      }
    })),
    (le.prototype = Object.assign(Object.create(rt.prototype), {
      constructor: le
    })),
    (ue.prototype = Object.create(n.prototype)),
    (ue.prototype.constructor = ue),
    (de.prototype = Object.create(n.prototype)),
    (de.prototype.constructor = de),
    (de.prototype.isCompressedTexture = !0),
    (pe.prototype = Object.create(n.prototype)),
    (pe.prototype.constructor = pe),
    (fe.prototype = Object.create(n.prototype)),
    (fe.prototype.constructor = fe),
    (fe.prototype.isDepthTexture = !0),
    (me.prototype = Object.create(mt.prototype)),
    (me.prototype.constructor = me),
    (ge.prototype = Object.create(pt.prototype)),
    (ge.prototype.constructor = ge),
    (ve.prototype = Object.create(pt.prototype)),
    (ve.prototype.constructor = ve),
    (ye.prototype = Object.create(ve.prototype)),
    (ye.prototype.constructor = ye),
    (xe.prototype = Object.create(ve.prototype)),
    (xe.prototype.constructor = xe),
    (_e.prototype = Object.create(ve.prototype)),
    (_e.prototype.constructor = _e),
    (be.prototype = Object.create(ve.prototype)),
    (be.prototype.constructor = be),
    (we.prototype = Object.create(pt.prototype)),
    (we.prototype.constructor = we),
    (we.NoTaper = function() {
      return 1;
    }),
    (we.SinusoidalTaper = function(t) {
      return Math.sin(Math.PI * t);
    }),
    (we.FrenetFrames = function(e, i, n) {
      var r = new c(),
        a = [],
        o = [],
        s = [],
        l = new c(),
        u = new h();
      i += 1;
      var d, p, f;
      for (
        this.tangents = a, this.normals = o, this.binormals = s, d = 0;
        d < i;
        d++
      )
        (p = d / (i - 1)), (a[d] = e.getTangentAt(p)), a[d].normalize();
      for (
        o[0] = new c(),
          s[0] = new c(),
          e = Number.MAX_VALUE,
          d = Math.abs(a[0].x),
          p = Math.abs(a[0].y),
          f = Math.abs(a[0].z),
          d <= e && ((e = d), r.set(1, 0, 0)),
          p <= e && ((e = p), r.set(0, 1, 0)),
          f <= e && r.set(0, 0, 1),
          l.crossVectors(a[0], r).normalize(),
          o[0].crossVectors(a[0], l),
          s[0].crossVectors(a[0], o[0]),
          d = 1;
        d < i;
        d++
      )
        (o[d] = o[d - 1].clone()),
          (s[d] = s[d - 1].clone()),
          l.crossVectors(a[d - 1], a[d]),
          l.length() > Number.EPSILON &&
            (l.normalize(),
            (r = Math.acos(t.Math.clamp(a[d - 1].dot(a[d]), -1, 1))),
            o[d].applyMatrix4(u.makeRotationAxis(l, r))),
          s[d].crossVectors(a[d], o[d]);
      if (n)
        for (
          r = Math.acos(t.Math.clamp(o[0].dot(o[i - 1]), -1, 1)),
            r /= i - 1,
            0 < a[0].dot(l.crossVectors(o[0], o[i - 1])) && (r = -r),
            d = 1;
          d < i;
          d++
        )
          o[d].applyMatrix4(u.makeRotationAxis(a[d], r * d)),
            s[d].crossVectors(a[d], o[d]);
    }),
    (Me.prototype = Object.create(mt.prototype)),
    (Me.prototype.constructor = Me),
    (Ee.prototype = Object.create(pt.prototype)),
    (Ee.prototype.constructor = Ee),
    (Se.prototype = Object.create(mt.prototype)),
    (Se.prototype.constructor = Se),
    (Te.prototype = Object.create(pt.prototype)),
    (Te.prototype.constructor = Te),
    (t.ShapeUtils = {
      area: function(t) {
        for (var e = t.length, i = 0, n = e - 1, r = 0; r < e; n = r++)
          i += t[n].x * t[r].y - t[r].x * t[n].y;
        return 0.5 * i;
      },
      triangulate: (function() {
        return function(e, i) {
          var n = e.length;
          if (3 > n) return null;
          var r,
            a,
            o,
            s = [],
            c = [],
            h = [];
          if (0 < t.ShapeUtils.area(e)) for (a = 0; a < n; a++) c[a] = a;
          else for (a = 0; a < n; a++) c[a] = n - 1 - a;
          var l = 2 * n;
          for (a = n - 1; 2 < n; ) {
            if (0 >= l--) {
              console.warn(
                'THREE.ShapeUtils: Unable to triangulate polygon! in triangulate()'
              );
              break;
            }
            (r = a),
              n <= r && (r = 0),
              (a = r + 1),
              n <= a && (a = 0),
              (o = a + 1),
              n <= o && (o = 0);
            var u;
            t: {
              var d, p, f, m, g, v, y, x;
              if (
                ((d = e[c[r]].x),
                (p = e[c[r]].y),
                (f = e[c[a]].x),
                (m = e[c[a]].y),
                (g = e[c[o]].x),
                (v = e[c[o]].y),
                Number.EPSILON > (f - d) * (v - p) - (m - p) * (g - d))
              )
                u = !1;
              else {
                var _, b, w, M, E, S, T, A, L, R;
                for (
                  _ = g - f,
                    b = v - m,
                    w = d - g,
                    M = p - v,
                    E = f - d,
                    S = m - p,
                    u = 0;
                  u < n;
                  u++
                )
                  if (
                    ((y = e[c[u]].x),
                    (x = e[c[u]].y),
                    !(
                      (y === d && x === p) ||
                      (y === f && x === m) ||
                      (y === g && x === v)
                    ) &&
                      ((T = y - d),
                      (A = x - p),
                      (L = y - f),
                      (R = x - m),
                      (y -= g),
                      (x -= v),
                      (L = _ * R - b * L),
                      (T = E * A - S * T),
                      (A = w * x - M * y),
                      L >= -Number.EPSILON &&
                        A >= -Number.EPSILON &&
                        T >= -Number.EPSILON))
                  ) {
                    u = !1;
                    break t;
                  }
                u = !0;
              }
            }
            if (u) {
              for (
                s.push([e[c[r]], e[c[a]], e[c[o]]]),
                  h.push([c[r], c[a], c[o]]),
                  r = a,
                  o = a + 1;
                o < n;
                r++, o++
              )
                c[r] = c[o];
              n--, (l = 2 * n);
            }
          }
          return i ? h : s;
        };
      })(),
      triangulateShape: function(e, i) {
        function n(t) {
          var e = t.length;
          2 < e && t[e - 1].equals(t[0]) && t.pop();
        }
        function r(t, e, i) {
          return t.x !== e.x
            ? t.x < e.x ? t.x <= i.x && i.x <= e.x : e.x <= i.x && i.x <= t.x
            : t.y < e.y ? t.y <= i.y && i.y <= e.y : e.y <= i.y && i.y <= t.y;
        }
        function a(t, e, i, n, a) {
          var o = e.x - t.x,
            s = e.y - t.y,
            c = n.x - i.x,
            h = n.y - i.y,
            l = t.x - i.x,
            u = t.y - i.y,
            d = s * c - o * h,
            p = s * l - o * u;
          if (Math.abs(d) > Number.EPSILON) {
            if (0 < d) {
              if (0 > p || p > d) return [];
              if (0 > (c = h * l - c * u) || c > d) return [];
            } else {
              if (0 < p || p < d) return [];
              if (0 < (c = h * l - c * u) || c < d) return [];
            }
            return 0 === c
              ? !a || (0 !== p && p !== d) ? [t] : []
              : c === d
                ? !a || (0 !== p && p !== d) ? [e] : []
                : 0 === p
                  ? [i]
                  : p === d
                    ? [n]
                    : ((a = c / d), [{ x: t.x + a * o, y: t.y + a * s }]);
          }
          return 0 !== p || h * l != c * u
            ? []
            : ((s = 0 === o && 0 === s),
              (c = 0 === c && 0 === h),
              s && c
                ? t.x !== i.x || t.y !== i.y ? [] : [t]
                : s
                  ? r(i, n, t) ? [t] : []
                  : c
                    ? r(t, e, i) ? [i] : []
                    : (0 !== o
                        ? (t.x < e.x
                            ? ((o = t), (c = t.x), (s = e), (t = e.x))
                            : ((o = e), (c = e.x), (s = t), (t = t.x)),
                          i.x < n.x
                            ? ((e = i), (d = i.x), (h = n), (i = n.x))
                            : ((e = n), (d = n.x), (h = i), (i = i.x)))
                        : (t.y < e.y
                            ? ((o = t), (c = t.y), (s = e), (t = e.y))
                            : ((o = e), (c = e.y), (s = t), (t = t.y)),
                          i.y < n.y
                            ? ((e = i), (d = i.y), (h = n), (i = n.y))
                            : ((e = n), (d = n.y), (h = i), (i = i.y))),
                      c <= d
                        ? t < d
                          ? []
                          : t === d ? (a ? [] : [e]) : t <= i ? [e, s] : [e, h]
                        : c > i
                          ? []
                          : c === i
                            ? a ? [] : [o]
                            : t <= i ? [o, s] : [o, h]));
        }
        function o(t, e, i, n) {
          var r = e.x - t.x,
            a = e.y - t.y;
          (e = i.x - t.x), (i = i.y - t.y);
          var o = n.x - t.x;
          return (
            (n = n.y - t.y),
            (t = r * i - a * e),
            (r = r * n - a * o),
            Math.abs(t) > Number.EPSILON
              ? ((e = o * i - n * e),
                0 < t ? 0 <= r && 0 <= e : 0 <= r || 0 <= e)
              : 0 < r
          );
        }
        n(e), i.forEach(n);
        var s,
          c,
          h,
          l,
          u,
          d = {};
        for (h = e.concat(), s = 0, c = i.length; s < c; s++)
          Array.prototype.push.apply(h, i[s]);
        for (s = 0, c = h.length; s < c; s++)
          (u = h[s].x + ':' + h[s].y),
            void 0 !== d[u] &&
              console.warn('THREE.ShapeUtils: Duplicate point', u, s),
            (d[u] = s);
        s = (function(t, e) {
          function i(t, e) {
            var i = g.length - 1,
              n = t - 1;
            0 > n && (n = i);
            var r = t + 1;
            return (
              r > i && (r = 0),
              !!(i = o(g[t], g[n], g[r], s[e])) &&
                ((i = s.length - 1),
                (n = e - 1),
                0 > n && (n = i),
                (r = e + 1),
                r > i && (r = 0),
                !!(i = o(s[e], s[n], s[r], g[t])))
            );
          }
          function n(t, e) {
            var i, n;
            for (i = 0; i < g.length; i++)
              if (
                ((n = i + 1),
                (n %= g.length),
                (n = a(t, e, g[i], g[n], !0)),
                0 < n.length)
              )
                return !0;
            return !1;
          }
          function r(t, i) {
            var n, r, o, s;
            for (n = 0; n < v.length; n++)
              for (r = e[v[n]], o = 0; o < r.length; o++)
                if (
                  ((s = o + 1),
                  (s %= r.length),
                  (s = a(t, i, r[o], r[s], !0)),
                  0 < s.length)
                )
                  return !0;
            return !1;
          }
          var s,
            c,
            h,
            l,
            u,
            d,
            p,
            f,
            m,
            g = t.concat(),
            v = [],
            y = [],
            x = 0;
          for (c = e.length; x < c; x++) v.push(x);
          p = 0;
          for (var _ = 2 * v.length; 0 < v.length; ) {
            if (0 > --_) {
              console.log(
                'Infinite Loop! Holes left:' +
                  v.length +
                  ', Probably Hole outside Shape!'
              );
              break;
            }
            for (h = p; h < g.length; h++) {
              for (l = g[h], c = -1, x = 0; x < v.length; x++)
                if (
                  ((u = v[x]), (d = l.x + ':' + l.y + ':' + u), void 0 === y[d])
                ) {
                  for (s = e[u], f = 0; f < s.length; f++)
                    if (((u = s[f]), i(h, f) && !n(l, u) && !r(l, u))) {
                      (c = f),
                        v.splice(x, 1),
                        (p = g.slice(0, h + 1)),
                        (u = g.slice(h)),
                        (f = s.slice(c)),
                        (m = s.slice(0, c + 1)),
                        (g = p
                          .concat(f)
                          .concat(m)
                          .concat(u)),
                        (p = h);
                      break;
                    }
                  if (0 <= c) break;
                  y[d] = !0;
                }
              if (0 <= c) break;
            }
          }
          return g;
        })(e, i);
        var p = t.ShapeUtils.triangulate(s, !1);
        for (s = 0, c = p.length; s < c; s++)
          for (l = p[s], h = 0; 3 > h; h++)
            (u = l[h].x + ':' + l[h].y), void 0 !== (u = d[u]) && (l[h] = u);
        return p.concat();
      },
      isClockWise: function(e) {
        return 0 > t.ShapeUtils.area(e);
      },
      b2: (function() {
        return function(t, e, i, n) {
          var r = 1 - t;
          return r * r * e + 2 * (1 - t) * t * i + t * t * n;
        };
      })(),
      b3: (function() {
        return function(t, e, i, n, r) {
          var a = 1 - t,
            o = 1 - t;
          return (
            a * a * a * e +
            3 * o * o * t * i +
            3 * (1 - t) * t * t * n +
            t * t * t * r
          );
        };
      })()
    }),
    (Ae.prototype = Object.create(pt.prototype)),
    (Ae.prototype.constructor = Ae),
    (Ae.prototype.addShapeList = function(t, e) {
      for (var i = t.length, n = 0; n < i; n++) this.addShape(t[n], e);
    }),
    (Ae.prototype.addShape = function(e, n) {
      function r(t, e, i) {
        return (
          e || console.error('THREE.ExtrudeGeometry: vec does not exist'),
          e
            .clone()
            .multiplyScalar(i)
            .add(t)
        );
      }
      function a(t, e, n) {
        var r, a, o;
        (a = t.x - e.x), (o = t.y - e.y), (r = n.x - t.x);
        var s = n.y - t.y,
          c = a * a + o * o;
        if (Math.abs(a * s - o * r) > Number.EPSILON) {
          var h = Math.sqrt(c),
            l = Math.sqrt(r * r + s * s),
            c = e.x - o / h;
          if (
            ((e = e.y + a / h),
            (s =
              ((n.x - s / l - c) * s - (n.y + r / l - e) * r) /
              (a * s - o * r)),
            (r = c + a * s - t.x),
            (a = e + o * s - t.y),
            2 >= (o = r * r + a * a))
          )
            return new i(r, a);
          o = Math.sqrt(o / 2);
        } else
          (t = !1),
            a > Number.EPSILON
              ? r > Number.EPSILON && (t = !0)
              : a < -Number.EPSILON
                ? r < -Number.EPSILON && (t = !0)
                : Math.sign(o) === Math.sign(s) && (t = !0),
            t
              ? ((r = -o), (o = Math.sqrt(c)))
              : ((r = a), (a = o), (o = Math.sqrt(c / 2)));
        return new i(r / o, a / o);
      }
      function o(t, e) {
        var i, n;
        for (H = t.length; 0 <= --H; ) {
          (i = H), (n = H - 1), 0 > n && (n = t.length - 1);
          var r,
            a = b + 2 * y;
          for (r = 0; r < a; r++) {
            var o = B * r,
              s = B * (r + 1),
              c = e + i + o,
              o = e + n + o,
              h = e + n + s,
              s = e + i + s,
              c = c + R,
              o = o + R,
              h = h + R,
              s = s + R;
            L.faces.push(new st(c, o, s, null, null, 1)),
              L.faces.push(new st(o, h, s, null, null, 1)),
              (c = E.generateSideWallUV(L, c, o, h, s)),
              L.faceVertexUvs[0].push([c[0], c[1], c[3]]),
              L.faceVertexUvs[0].push([c[1], c[2], c[3]]);
          }
        }
      }
      function s(t, e, i) {
        L.vertices.push(new c(t, e, i));
      }
      function h(t, e, i) {
        (t += R),
          (e += R),
          (i += R),
          L.faces.push(new st(t, e, i, null, null, 0)),
          (t = E.generateTopUV(L, t, e, i)),
          L.faceVertexUvs[0].push(t);
      }
      var l,
        u,
        d,
        p,
        f,
        m = void 0 !== n.amount ? n.amount : 100,
        g = void 0 !== n.bevelThickness ? n.bevelThickness : 6,
        v = void 0 !== n.bevelSize ? n.bevelSize : g - 2,
        y = void 0 !== n.bevelSegments ? n.bevelSegments : 3,
        x = void 0 === n.bevelEnabled || n.bevelEnabled,
        _ = void 0 !== n.curveSegments ? n.curveSegments : 12,
        b = void 0 !== n.steps ? n.steps : 1,
        w = n.extrudePath,
        M = !1,
        E = void 0 !== n.UVGenerator ? n.UVGenerator : Ae.WorldUVGenerator;
      w &&
        ((l = w.getSpacedPoints(b)),
        (M = !0),
        (x = !1),
        (u = void 0 !== n.frames ? n.frames : new we.FrenetFrames(w, b, !1)),
        (d = new c()),
        (p = new c()),
        (f = new c())),
        x || (v = g = y = 0);
      var S,
        T,
        A,
        L = this,
        R = this.vertices.length,
        w = e.extractPoints(_),
        _ = w.shape,
        P = w.holes;
      if ((w = !t.ShapeUtils.isClockWise(_))) {
        for (_ = _.reverse(), T = 0, A = P.length; T < A; T++)
          (S = P[T]), t.ShapeUtils.isClockWise(S) && (P[T] = S.reverse());
        w = !1;
      }
      var C = t.ShapeUtils.triangulateShape(_, P),
        I = _;
      for (T = 0, A = P.length; T < A; T++) (S = P[T]), (_ = _.concat(S));
      var U,
        D,
        N,
        F,
        O,
        z,
        B = _.length,
        G = C.length,
        w = [],
        H = 0;
      for (N = I.length, U = N - 1, D = H + 1; H < N; H++, U++, D++)
        U === N && (U = 0), D === N && (D = 0), (w[H] = a(I[H], I[U], I[D]));
      var V,
        k = [],
        j = w.concat();
      for (T = 0, A = P.length; T < A; T++) {
        for (
          S = P[T], V = [], H = 0, N = S.length, U = N - 1, D = H + 1;
          H < N;
          H++, U++, D++
        )
          U === N && (U = 0), D === N && (D = 0), (V[H] = a(S[H], S[U], S[D]));
        k.push(V), (j = j.concat(V));
      }
      for (U = 0; U < y; U++) {
        for (
          N = U / y,
            F = g * Math.cos(N * Math.PI / 2),
            D = v * Math.sin(N * Math.PI / 2),
            H = 0,
            N = I.length;
          H < N;
          H++
        )
          (O = r(I[H], w[H], D)), s(O.x, O.y, -F);
        for (T = 0, A = P.length; T < A; T++)
          for (S = P[T], V = k[T], H = 0, N = S.length; H < N; H++)
            (O = r(S[H], V[H], D)), s(O.x, O.y, -F);
      }
      for (D = v, H = 0; H < B; H++)
        (O = x ? r(_[H], j[H], D) : _[H]),
          M
            ? (p.copy(u.normals[0]).multiplyScalar(O.x),
              d.copy(u.binormals[0]).multiplyScalar(O.y),
              f
                .copy(l[0])
                .add(p)
                .add(d),
              s(f.x, f.y, f.z))
            : s(O.x, O.y, 0);
      for (N = 1; N <= b; N++)
        for (H = 0; H < B; H++)
          (O = x ? r(_[H], j[H], D) : _[H]),
            M
              ? (p.copy(u.normals[N]).multiplyScalar(O.x),
                d.copy(u.binormals[N]).multiplyScalar(O.y),
                f
                  .copy(l[N])
                  .add(p)
                  .add(d),
                s(f.x, f.y, f.z))
              : s(O.x, O.y, m / b * N);
      for (U = y - 1; 0 <= U; U--) {
        for (
          N = U / y,
            F = g * Math.cos(N * Math.PI / 2),
            D = v * Math.sin(N * Math.PI / 2),
            H = 0,
            N = I.length;
          H < N;
          H++
        )
          (O = r(I[H], w[H], D)), s(O.x, O.y, m + F);
        for (T = 0, A = P.length; T < A; T++)
          for (S = P[T], V = k[T], H = 0, N = S.length; H < N; H++)
            (O = r(S[H], V[H], D)),
              M ? s(O.x, O.y + l[b - 1].y, l[b - 1].x + F) : s(O.x, O.y, m + F);
      }
      !(function() {
        if (x) {
          var t = 0 * B;
          for (H = 0; H < G; H++) (z = C[H]), h(z[2] + t, z[1] + t, z[0] + t);
          for (t = B * (b + 2 * y), H = 0; H < G; H++)
            (z = C[H]), h(z[0] + t, z[1] + t, z[2] + t);
        } else {
          for (H = 0; H < G; H++) (z = C[H]), h(z[2], z[1], z[0]);
          for (H = 0; H < G; H++)
            (z = C[H]), h(z[0] + B * b, z[1] + B * b, z[2] + B * b);
        }
      })(),
        (function() {
          var t = 0;
          for (o(I, t), t += I.length, T = 0, A = P.length; T < A; T++)
            (S = P[T]), o(S, t), (t += S.length);
        })();
    }),
    (Ae.WorldUVGenerator = {
      generateTopUV: function(t, e, n, r) {
        return (
          (t = t.vertices),
          (e = t[e]),
          (n = t[n]),
          (r = t[r]),
          [new i(e.x, e.y), new i(n.x, n.y), new i(r.x, r.y)]
        );
      },
      generateSideWallUV: function(t, e, n, r, a) {
        return (
          (t = t.vertices),
          (e = t[e]),
          (n = t[n]),
          (r = t[r]),
          (a = t[a]),
          0.01 > Math.abs(e.y - n.y)
            ? [
                new i(e.x, 1 - e.z),
                new i(n.x, 1 - n.z),
                new i(r.x, 1 - r.z),
                new i(a.x, 1 - a.z)
              ]
            : [
                new i(e.y, 1 - e.z),
                new i(n.y, 1 - n.z),
                new i(r.y, 1 - r.z),
                new i(a.y, 1 - a.z)
              ]
        );
      }
    }),
    (Le.prototype = Object.create(Ae.prototype)),
    (Le.prototype.constructor = Le),
    (Re.prototype = Object.create(mt.prototype)),
    (Re.prototype.constructor = Re),
    (Pe.prototype = Object.create(pt.prototype)),
    (Pe.prototype.constructor = Pe),
    (Ce.prototype = Object.create(mt.prototype)),
    (Ce.prototype.constructor = Ce),
    (Ie.prototype = Object.create(pt.prototype)),
    (Ie.prototype.constructor = Ie),
    (Ue.prototype = Object.create(pt.prototype)),
    (Ue.prototype.constructor = Ue),
    (De.prototype = Object.create(mt.prototype)),
    (De.prototype.constructor = De),
    (Ne.prototype = Object.create(pt.prototype)),
    (Ne.prototype.constructor = Ne),
    (Fe.prototype = Object.create(pt.prototype)),
    (Fe.prototype.constructor = Fe),
    (Fe.prototype.addShapeList = function(t, e) {
      for (var i = 0, n = t.length; i < n; i++) this.addShape(t[i], e);
      return this;
    }),
    (Fe.prototype.addShape = function(e, i) {
      void 0 === i && (i = {});
      var n,
        r,
        a,
        o = i.material,
        s = void 0 === i.UVGenerator ? Ae.WorldUVGenerator : i.UVGenerator,
        h = this.vertices.length;
      n = e.extractPoints(void 0 !== i.curveSegments ? i.curveSegments : 12);
      var l = n.shape,
        u = n.holes;
      if (!t.ShapeUtils.isClockWise(l))
        for (l = l.reverse(), n = 0, r = u.length; n < r; n++)
          (a = u[n]), t.ShapeUtils.isClockWise(a) && (u[n] = a.reverse());
      var d = t.ShapeUtils.triangulateShape(l, u);
      for (n = 0, r = u.length; n < r; n++) (a = u[n]), (l = l.concat(a));
      for (u = l.length, r = d.length, n = 0; n < u; n++)
        (a = l[n]), this.vertices.push(new c(a.x, a.y, 0));
      for (n = 0; n < r; n++)
        (u = d[n]),
          (l = u[0] + h),
          (a = u[1] + h),
          (u = u[2] + h),
          this.faces.push(new st(l, a, u, null, null, o)),
          this.faceVertexUvs[0].push(s.generateTopUV(this, l, a, u));
    }),
    (Oe.prototype = Object.create(mt.prototype)),
    (Oe.prototype.constructor = Oe),
    (ze.prototype = Object.create(mt.prototype)),
    (ze.prototype.constructor = ze),
    (Be.prototype = Object.create(pt.prototype)),
    (Be.prototype.constructor = Be),
    (Ge.prototype = Object.create(Be.prototype)),
    (Ge.prototype.constructor = Ge),
    (He.prototype = Object.create(mt.prototype)),
    (He.prototype.constructor = He),
    (Ve.prototype = Object.create(mt.prototype)),
    (Ve.prototype.constructor = Ve),
    (ke.prototype = Object.create(pt.prototype)),
    (ke.prototype.constructor = ke),
    (je.prototype = Object.create(pt.prototype)),
    (je.prototype.constructor = je);
  var Kn = Object.freeze({
    WireframeGeometry: me,
    ParametricGeometry: ge,
    TetrahedronGeometry: ye,
    OctahedronGeometry: xe,
    IcosahedronGeometry: _e,
    DodecahedronGeometry: be,
    PolyhedronGeometry: ve,
    TubeGeometry: we,
    TorusKnotGeometry: Ee,
    TorusKnotBufferGeometry: Me,
    TorusGeometry: Te,
    TorusBufferGeometry: Se,
    TextGeometry: Le,
    SphereBufferGeometry: Re,
    SphereGeometry: Pe,
    RingGeometry: Ie,
    RingBufferGeometry: Ce,
    PlaneBufferGeometry: yt,
    PlaneGeometry: Ue,
    LatheGeometry: Ne,
    LatheBufferGeometry: De,
    ShapeGeometry: Fe,
    ExtrudeGeometry: Ae,
    EdgesGeometry: Oe,
    ConeGeometry: Ge,
    ConeBufferGeometry: He,
    CylinderGeometry: Be,
    CylinderBufferGeometry: ze,
    CircleBufferGeometry: Ve,
    CircleGeometry: ke,
    BoxBufferGeometry: vt,
    BoxGeometry: je
  });
  (We.prototype = Object.create(q.prototype)),
    (We.prototype.constructor = We),
    (We.prototype.isShadowMaterial = !0),
    (Xe.prototype = Object.create(q.prototype)),
    (Xe.prototype.constructor = Xe),
    (Xe.prototype.isRawShaderMaterial = !0),
    (qe.prototype = {
      constructor: qe,
      isMultiMaterial: !0,
      toJSON: function(t) {
        for (
          var e = {
              metadata: {
                version: 4.2,
                type: 'material',
                generator: 'MaterialExporter'
              },
              uuid: this.uuid,
              type: this.type,
              materials: []
            },
            i = this.materials,
            n = 0,
            r = i.length;
          n < r;
          n++
        ) {
          var a = i[n].toJSON(t);
          delete a.metadata, e.materials.push(a);
        }
        return (e.visible = this.visible), e;
      },
      clone: function() {
        for (
          var t = new this.constructor(), e = 0;
          e < this.materials.length;
          e++
        )
          t.materials.push(this.materials[e].clone());
        return (t.visible = this.visible), t;
      }
    }),
    (Ye.prototype = Object.create(X.prototype)),
    (Ye.prototype.constructor = Ye),
    (Ye.prototype.isMeshStandardMaterial = !0),
    (Ye.prototype.copy = function(t) {
      return (
        X.prototype.copy.call(this, t),
        (this.defines = { STANDARD: '' }),
        this.color.copy(t.color),
        (this.roughness = t.roughness),
        (this.metalness = t.metalness),
        (this.map = t.map),
        (this.lightMap = t.lightMap),
        (this.lightMapIntensity = t.lightMapIntensity),
        (this.aoMap = t.aoMap),
        (this.aoMapIntensity = t.aoMapIntensity),
        this.emissive.copy(t.emissive),
        (this.emissiveMap = t.emissiveMap),
        (this.emissiveIntensity = t.emissiveIntensity),
        (this.bumpMap = t.bumpMap),
        (this.bumpScale = t.bumpScale),
        (this.normalMap = t.normalMap),
        this.normalScale.copy(t.normalScale),
        (this.displacementMap = t.displacementMap),
        (this.displacementScale = t.displacementScale),
        (this.displacementBias = t.displacementBias),
        (this.roughnessMap = t.roughnessMap),
        (this.metalnessMap = t.metalnessMap),
        (this.alphaMap = t.alphaMap),
        (this.envMap = t.envMap),
        (this.envMapIntensity = t.envMapIntensity),
        (this.refractionRatio = t.refractionRatio),
        (this.wireframe = t.wireframe),
        (this.wireframeLinewidth = t.wireframeLinewidth),
        (this.wireframeLinecap = t.wireframeLinecap),
        (this.wireframeLinejoin = t.wireframeLinejoin),
        (this.skinning = t.skinning),
        (this.morphTargets = t.morphTargets),
        (this.morphNormals = t.morphNormals),
        this
      );
    }),
    (Ze.prototype = Object.create(Ye.prototype)),
    (Ze.prototype.constructor = Ze),
    (Ze.prototype.isMeshPhysicalMaterial = !0),
    (Ze.prototype.copy = function(t) {
      return (
        Ye.prototype.copy.call(this, t),
        (this.defines = { PHYSICAL: '' }),
        (this.reflectivity = t.reflectivity),
        (this.clearCoat = t.clearCoat),
        (this.clearCoatRoughness = t.clearCoatRoughness),
        this
      );
    }),
    (Je.prototype = Object.create(X.prototype)),
    (Je.prototype.constructor = Je),
    (Je.prototype.isMeshPhongMaterial = !0),
    (Je.prototype.copy = function(t) {
      return (
        X.prototype.copy.call(this, t),
        this.color.copy(t.color),
        this.specular.copy(t.specular),
        (this.shininess = t.shininess),
        (this.map = t.map),
        (this.lightMap = t.lightMap),
        (this.lightMapIntensity = t.lightMapIntensity),
        (this.aoMap = t.aoMap),
        (this.aoMapIntensity = t.aoMapIntensity),
        this.emissive.copy(t.emissive),
        (this.emissiveMap = t.emissiveMap),
        (this.emissiveIntensity = t.emissiveIntensity),
        (this.bumpMap = t.bumpMap),
        (this.bumpScale = t.bumpScale),
        (this.normalMap = t.normalMap),
        this.normalScale.copy(t.normalScale),
        (this.displacementMap = t.displacementMap),
        (this.displacementScale = t.displacementScale),
        (this.displacementBias = t.displacementBias),
        (this.specularMap = t.specularMap),
        (this.alphaMap = t.alphaMap),
        (this.envMap = t.envMap),
        (this.combine = t.combine),
        (this.reflectivity = t.reflectivity),
        (this.refractionRatio = t.refractionRatio),
        (this.wireframe = t.wireframe),
        (this.wireframeLinewidth = t.wireframeLinewidth),
        (this.wireframeLinecap = t.wireframeLinecap),
        (this.wireframeLinejoin = t.wireframeLinejoin),
        (this.skinning = t.skinning),
        (this.morphTargets = t.morphTargets),
        (this.morphNormals = t.morphNormals),
        this
      );
    }),
    (Qe.prototype = Object.create(X.prototype)),
    (Qe.prototype.constructor = Qe),
    (Qe.prototype.isMeshNormalMaterial = !0),
    (Qe.prototype.copy = function(t) {
      return (
        X.prototype.copy.call(this, t),
        (this.wireframe = t.wireframe),
        (this.wireframeLinewidth = t.wireframeLinewidth),
        this
      );
    }),
    (Ke.prototype = Object.create(X.prototype)),
    (Ke.prototype.constructor = Ke),
    (Ke.prototype.isMeshLambertMaterial = !0),
    (Ke.prototype.copy = function(t) {
      return (
        X.prototype.copy.call(this, t),
        this.color.copy(t.color),
        (this.map = t.map),
        (this.lightMap = t.lightMap),
        (this.lightMapIntensity = t.lightMapIntensity),
        (this.aoMap = t.aoMap),
        (this.aoMapIntensity = t.aoMapIntensity),
        this.emissive.copy(t.emissive),
        (this.emissiveMap = t.emissiveMap),
        (this.emissiveIntensity = t.emissiveIntensity),
        (this.specularMap = t.specularMap),
        (this.alphaMap = t.alphaMap),
        (this.envMap = t.envMap),
        (this.combine = t.combine),
        (this.reflectivity = t.reflectivity),
        (this.refractionRatio = t.refractionRatio),
        (this.wireframe = t.wireframe),
        (this.wireframeLinewidth = t.wireframeLinewidth),
        (this.wireframeLinecap = t.wireframeLinecap),
        (this.wireframeLinejoin = t.wireframeLinejoin),
        (this.skinning = t.skinning),
        (this.morphTargets = t.morphTargets),
        (this.morphNormals = t.morphNormals),
        this
      );
    }),
    ($e.prototype = Object.create(X.prototype)),
    ($e.prototype.constructor = $e),
    ($e.prototype.isLineDashedMaterial = !0),
    ($e.prototype.copy = function(t) {
      return (
        X.prototype.copy.call(this, t),
        this.color.copy(t.color),
        (this.linewidth = t.linewidth),
        (this.scale = t.scale),
        (this.dashSize = t.dashSize),
        (this.gapSize = t.gapSize),
        this
      );
    });
  var $n = Object.freeze({
    ShadowMaterial: We,
    SpriteMaterial: Kt,
    RawShaderMaterial: Xe,
    ShaderMaterial: q,
    PointsMaterial: ce,
    MultiMaterial: qe,
    MeshPhysicalMaterial: Ze,
    MeshStandardMaterial: Ye,
    MeshPhongMaterial: Je,
    MeshNormalMaterial: Qe,
    MeshLambertMaterial: Ke,
    MeshDepthMaterial: Y,
    MeshBasicMaterial: ct,
    LineDashedMaterial: $e,
    LineBasicMaterial: ae,
    Material: X
  });
  (t.Cache = {
    enabled: !1,
    files: {},
    add: function(t, e) {
      !1 !== this.enabled && (this.files[t] = e);
    },
    get: function(t) {
      if (!1 !== this.enabled) return this.files[t];
    },
    remove: function(t) {
      delete this.files[t];
    },
    clear: function() {
      this.files = {};
    }
  }),
    (t.DefaultLoadingManager = new ti()),
    Object.assign(ei.prototype, {
      load: function(e, i, n, r) {
        void 0 !== this.path && (e = this.path + e);
        var a = this,
          o = t.Cache.get(e);
        if (void 0 !== o)
          return (
            a.manager.itemStart(e),
            setTimeout(function() {
              i && i(o), a.manager.itemEnd(e);
            }, 0),
            o
          );
        var s = new XMLHttpRequest();
        return (
          s.open('GET', e, !0),
          s.addEventListener(
            'load',
            function(n) {
              var o = n.target.response;
              t.Cache.add(e, o),
                200 === this.status
                  ? (i && i(o), a.manager.itemEnd(e))
                  : 0 === this.status
                    ? (console.warn('THREE.XHRLoader: HTTP Status 0 received.'),
                      i && i(o),
                      a.manager.itemEnd(e))
                    : (r && r(n), a.manager.itemError(e));
            },
            !1
          ),
          void 0 !== n &&
            s.addEventListener(
              'progress',
              function(t) {
                n(t);
              },
              !1
            ),
          s.addEventListener(
            'error',
            function(t) {
              r && r(t), a.manager.itemError(e);
            },
            !1
          ),
          void 0 !== this.responseType && (s.responseType = this.responseType),
          void 0 !== this.withCredentials &&
            (s.withCredentials = this.withCredentials),
          s.overrideMimeType && s.overrideMimeType('text/plain'),
          s.send(null),
          a.manager.itemStart(e),
          s
        );
      },
      setPath: function(t) {
        return (this.path = t), this;
      },
      setResponseType: function(t) {
        return (this.responseType = t), this;
      },
      setWithCredentials: function(t) {
        return (this.withCredentials = t), this;
      }
    }),
    Object.assign(ii.prototype, {
      load: function(t, e, i, n) {
        function r(r) {
          c.load(
            t[r],
            function(t) {
              (t = a._parser(t, !0)),
                (o[r] = {
                  width: t.width,
                  height: t.height,
                  format: t.format,
                  mipmaps: t.mipmaps
                }),
                6 === (h += 1) &&
                  (1 === t.mipmapCount && (s.minFilter = 1006),
                  (s.format = t.format),
                  (s.needsUpdate = !0),
                  e && e(s));
            },
            i,
            n
          );
        }
        var a = this,
          o = [],
          s = new de();
        s.image = o;
        var c = new ei(this.manager);
        if (
          (c.setPath(this.path),
          c.setResponseType('arraybuffer'),
          Array.isArray(t))
        )
          for (var h = 0, l = 0, u = t.length; l < u; ++l) r(l);
        else
          c.load(
            t,
            function(t) {
              if (((t = a._parser(t, !0)), t.isCubemap))
                for (
                  var i = t.mipmaps.length / t.mipmapCount, n = 0;
                  n < i;
                  n++
                ) {
                  o[n] = { mipmaps: [] };
                  for (var r = 0; r < t.mipmapCount; r++)
                    o[n].mipmaps.push(t.mipmaps[n * t.mipmapCount + r]),
                      (o[n].format = t.format),
                      (o[n].width = t.width),
                      (o[n].height = t.height);
                }
              else
                (s.image.width = t.width),
                  (s.image.height = t.height),
                  (s.mipmaps = t.mipmaps);
              1 === t.mipmapCount && (s.minFilter = 1006),
                (s.format = t.format),
                (s.needsUpdate = !0),
                e && e(s);
            },
            i,
            n
          );
        return s;
      },
      setPath: function(t) {
        return (this.path = t), this;
      }
    }),
    Object.assign(ni.prototype, {
      load: function(t, e, i, n) {
        var r = this,
          a = new ee(),
          o = new ei(this.manager);
        return (
          o.setResponseType('arraybuffer'),
          o.load(
            t,
            function(t) {
              (t = r._parser(t)) &&
                (void 0 !== t.image
                  ? (a.image = t.image)
                  : void 0 !== t.data &&
                    ((a.image.width = t.width),
                    (a.image.height = t.height),
                    (a.image.data = t.data)),
                (a.wrapS = void 0 !== t.wrapS ? t.wrapS : 1001),
                (a.wrapT = void 0 !== t.wrapT ? t.wrapT : 1001),
                (a.magFilter = void 0 !== t.magFilter ? t.magFilter : 1006),
                (a.minFilter = void 0 !== t.minFilter ? t.minFilter : 1008),
                (a.anisotropy = void 0 !== t.anisotropy ? t.anisotropy : 1),
                void 0 !== t.format && (a.format = t.format),
                void 0 !== t.type && (a.type = t.type),
                void 0 !== t.mipmaps && (a.mipmaps = t.mipmaps),
                1 === t.mipmapCount && (a.minFilter = 1006),
                (a.needsUpdate = !0),
                e && e(a, t));
            },
            i,
            n
          ),
          a
        );
      }
    }),
    Object.assign(ri.prototype, {
      load: function(t, e, i, n) {
        var r = this,
          a = document.createElementNS('http://www.w3.org/1999/xhtml', 'img');
        if (
          ((a.onload = function() {
            (a.onload = null),
              URL.revokeObjectURL(a.src),
              e && e(a),
              r.manager.itemEnd(t);
          }),
          0 === t.indexOf('data:'))
        )
          a.src = t;
        else {
          var o = new ei();
          o.setPath(this.path),
            o.setResponseType('blob'),
            o.setWithCredentials(this.withCredentials),
            o.load(
              t,
              function(t) {
                a.src = URL.createObjectURL(t);
              },
              i,
              n
            );
        }
        return r.manager.itemStart(t), a;
      },
      setCrossOrigin: function(t) {
        return (this.crossOrigin = t), this;
      },
      setWithCredentials: function(t) {
        return (this.withCredentials = t), this;
      },
      setPath: function(t) {
        return (this.path = t), this;
      }
    }),
    Object.assign(ai.prototype, {
      load: function(t, e, i, n) {
        function r(i) {
          o.load(
            t[i],
            function(t) {
              (a.images[i] = t), 6 === ++s && ((a.needsUpdate = !0), e && e(a));
            },
            void 0,
            n
          );
        }
        var a = new l(),
          o = new ri(this.manager);
        o.setCrossOrigin(this.crossOrigin), o.setPath(this.path);
        var s = 0;
        for (i = 0; i < t.length; ++i) r(i);
        return a;
      },
      setCrossOrigin: function(t) {
        return (this.crossOrigin = t), this;
      },
      setPath: function(t) {
        return (this.path = t), this;
      }
    }),
    Object.assign(oi.prototype, {
      load: function(t, e, i, r) {
        var a = new n(),
          o = new ri(this.manager);
        return (
          o.setCrossOrigin(this.crossOrigin),
          o.setWithCredentials(this.withCredentials),
          o.setPath(this.path),
          o.load(
            t,
            function(i) {
              var n =
                0 < t.search(/\.(jpg|jpeg)$/) ||
                0 === t.search(/^data\:image\/jpeg/);
              (a.format = n ? 1022 : 1023),
                (a.image = i),
                (a.needsUpdate = !0),
                void 0 !== e && e(a);
            },
            i,
            r
          ),
          a
        );
      },
      setCrossOrigin: function(t) {
        return (this.crossOrigin = t), this;
      },
      setWithCredentials: function(t) {
        return (this.withCredentials = t), this;
      },
      setPath: function(t) {
        return (this.path = t), this;
      }
    }),
    (si.prototype = Object.assign(Object.create(rt.prototype), {
      constructor: si,
      isLight: !0,
      copy: function(t) {
        return (
          rt.prototype.copy.call(this, t),
          this.color.copy(t.color),
          (this.intensity = t.intensity),
          this
        );
      },
      toJSON: function(t) {
        return (
          (t = rt.prototype.toJSON.call(this, t)),
          (t.object.color = this.color.getHex()),
          (t.object.intensity = this.intensity),
          void 0 !== this.groundColor &&
            (t.object.groundColor = this.groundColor.getHex()),
          void 0 !== this.distance && (t.object.distance = this.distance),
          void 0 !== this.angle && (t.object.angle = this.angle),
          void 0 !== this.decay && (t.object.decay = this.decay),
          void 0 !== this.penumbra && (t.object.penumbra = this.penumbra),
          void 0 !== this.shadow && (t.object.shadow = this.shadow.toJSON()),
          t
        );
      }
    })),
    (ci.prototype = Object.assign(Object.create(si.prototype), {
      constructor: ci,
      isHemisphereLight: !0,
      copy: function(t) {
        return (
          si.prototype.copy.call(this, t),
          this.groundColor.copy(t.groundColor),
          this
        );
      }
    })),
    Object.assign(hi.prototype, {
      copy: function(t) {
        return (
          (this.camera = t.camera.clone()),
          (this.bias = t.bias),
          (this.radius = t.radius),
          this.mapSize.copy(t.mapSize),
          this
        );
      },
      clone: function() {
        return new this.constructor().copy(this);
      },
      toJSON: function() {
        var t = {};
        return (
          0 !== this.bias && (t.bias = this.bias),
          1 !== this.radius && (t.radius = this.radius),
          (512 === this.mapSize.x && 512 === this.mapSize.y) ||
            (t.mapSize = this.mapSize.toArray()),
          (t.camera = this.camera.toJSON(!1).object),
          delete t.camera.matrix,
          t
        );
      }
    }),
    (li.prototype = Object.assign(Object.create(hi.prototype), {
      constructor: li,
      isSpotLightShadow: !0,
      update: function(e) {
        var i = 2 * t.Math.RAD2DEG * e.angle,
          n = this.mapSize.width / this.mapSize.height;
        e = e.distance || 500;
        var r = this.camera;
        (i === r.fov && n === r.aspect && e === r.far) ||
          ((r.fov = i),
          (r.aspect = n),
          (r.far = e),
          r.updateProjectionMatrix());
      }
    })),
    (ui.prototype = Object.assign(Object.create(si.prototype), {
      constructor: ui,
      isSpotLight: !0,
      copy: function(t) {
        return (
          si.prototype.copy.call(this, t),
          (this.distance = t.distance),
          (this.angle = t.angle),
          (this.penumbra = t.penumbra),
          (this.decay = t.decay),
          (this.target = t.target.clone()),
          (this.shadow = t.shadow.clone()),
          this
        );
      }
    })),
    (di.prototype = Object.assign(Object.create(si.prototype), {
      constructor: di,
      isPointLight: !0,
      copy: function(t) {
        return (
          si.prototype.copy.call(this, t),
          (this.distance = t.distance),
          (this.decay = t.decay),
          (this.shadow = t.shadow.clone()),
          this
        );
      }
    })),
    (pi.prototype = Object.assign(Object.create(hi.prototype), {
      constructor: pi
    })),
    (fi.prototype = Object.assign(Object.create(si.prototype), {
      constructor: fi,
      isDirectionalLight: !0,
      copy: function(t) {
        return (
          si.prototype.copy.call(this, t),
          (this.target = t.target.clone()),
          (this.shadow = t.shadow.clone()),
          this
        );
      }
    })),
    (mi.prototype = Object.assign(Object.create(si.prototype), {
      constructor: mi,
      isAmbientLight: !0
    })),
    (t.AnimationUtils = {
      arraySlice: function(e, i, n) {
        return t.AnimationUtils.isTypedArray(e)
          ? new e.constructor(e.subarray(i, n))
          : e.slice(i, n);
      },
      convertArray: function(t, e, i) {
        return !t || (!i && t.constructor === e)
          ? t
          : 'number' == typeof e.BYTES_PER_ELEMENT
            ? new e(t)
            : Array.prototype.slice.call(t);
      },
      isTypedArray: function(t) {
        return ArrayBuffer.isView(t) && !(t instanceof DataView);
      },
      getKeyframeOrder: function(t) {
        for (var e = t.length, i = Array(e), n = 0; n !== e; ++n) i[n] = n;
        return (
          i.sort(function(e, i) {
            return t[e] - t[i];
          }),
          i
        );
      },
      sortedArray: function(t, e, i) {
        for (
          var n = t.length, r = new t.constructor(n), a = 0, o = 0;
          o !== n;
          ++a
        )
          for (var s = i[a] * e, c = 0; c !== e; ++c) r[o++] = t[s + c];
        return r;
      },
      flattenJSON: function(t, e, i, n) {
        for (var r = 1, a = t[0]; void 0 !== a && void 0 === a[n]; ) a = t[r++];
        if (void 0 !== a) {
          var o = a[n];
          if (void 0 !== o)
            if (Array.isArray(o)) {
              do {
                (o = a[n]),
                  void 0 !== o && (e.push(a.time), i.push.apply(i, o)),
                  (a = t[r++]);
              } while (void 0 !== a);
            } else if (void 0 !== o.toArray) {
              do {
                (o = a[n]),
                  void 0 !== o && (e.push(a.time), o.toArray(i, i.length)),
                  (a = t[r++]);
              } while (void 0 !== a);
            } else
              do {
                (o = a[n]),
                  void 0 !== o && (e.push(a.time), i.push(o)),
                  (a = t[r++]);
              } while (void 0 !== a);
        }
      }
    }),
    (gi.prototype = {
      constructor: gi,
      evaluate: function(t) {
        var e = this.parameterPositions,
          i = this._cachedIndex,
          n = e[i],
          r = e[i - 1];
        t: {
          e: {
            i: {
              n: if (!(t < n)) {
                for (var a = i + 2; ; ) {
                  if (void 0 === n) {
                    if (t < r) break n;
                    return (
                      (this._cachedIndex = i = e.length),
                      this.afterEnd_(i - 1, t, r)
                    );
                  }
                  if (i === a) break;
                  if (((r = n), (n = e[++i]), t < n)) break e;
                }
                n = e.length;
                break i;
              }
              if (t >= r) break t;
              for (a = e[1], t < a && ((i = 2), (r = a)), a = i - 2; ; ) {
                if (void 0 === r)
                  return (this._cachedIndex = 0), this.beforeStart_(0, t, n);
                if (i === a) break;
                if (((n = r), (r = e[--i - 1]), t >= r)) break e;
              }
              (n = i), (i = 0);
            }
            for (; i < n; )
              (r = (i + n) >>> 1), t < e[r] ? (n = r) : (i = r + 1);
            if (((n = e[i]), void 0 === (r = e[i - 1])))
              return (this._cachedIndex = 0), this.beforeStart_(0, t, n);
            if (void 0 === n)
              return (
                (this._cachedIndex = i = e.length), this.afterEnd_(i - 1, r, t)
              );
          }
          (this._cachedIndex = i), this.intervalChanged_(i, r, n);
        }
        return this.interpolate_(i, r, t, n);
      },
      settings: null,
      DefaultSettings_: {},
      getSettings_: function() {
        return this.settings || this.DefaultSettings_;
      },
      copySampleValue_: function(t) {
        var e = this.resultBuffer,
          i = this.sampleValues,
          n = this.valueSize;
        t *= n;
        for (var r = 0; r !== n; ++r) e[r] = i[t + r];
        return e;
      },
      interpolate_: function() {
        throw Error('call to abstract method');
      },
      intervalChanged_: function() {}
    }),
    Object.assign(gi.prototype, {
      beforeStart_: gi.prototype.copySampleValue_,
      afterEnd_: gi.prototype.copySampleValue_
    }),
    (vi.prototype = Object.assign(Object.create(gi.prototype), {
      constructor: vi,
      DefaultSettings_: { endingStart: 2400, endingEnd: 2400 },
      intervalChanged_: function(t, e, i) {
        var n = this.parameterPositions,
          r = t - 2,
          a = t + 1,
          o = n[r],
          s = n[a];
        if (void 0 === o)
          switch (this.getSettings_().endingStart) {
            case 2401:
              (r = t), (o = 2 * e - i);
              break;
            case 2402:
              (r = n.length - 2), (o = e + n[r] - n[r + 1]);
              break;
            default:
              (r = t), (o = i);
          }
        if (void 0 === s)
          switch (this.getSettings_().endingEnd) {
            case 2401:
              (a = t), (s = 2 * i - e);
              break;
            case 2402:
              (a = 1), (s = i + n[1] - n[0]);
              break;
            default:
              (a = t - 1), (s = e);
          }
        (t = 0.5 * (i - e)),
          (n = this.valueSize),
          (this._weightPrev = t / (e - o)),
          (this._weightNext = t / (s - i)),
          (this._offsetPrev = r * n),
          (this._offsetNext = a * n);
      },
      interpolate_: function(t, e, i, n) {
        var r = this.resultBuffer,
          a = this.sampleValues,
          o = this.valueSize;
        t *= o;
        var s = t - o,
          c = this._offsetPrev,
          h = this._offsetNext,
          l = this._weightPrev,
          u = this._weightNext,
          d = (i - e) / (n - e);
        for (
          i = d * d,
            n = i * d,
            e = -l * n + 2 * l * i - l * d,
            l = (1 + l) * n + (-1.5 - 2 * l) * i + (-0.5 + l) * d + 1,
            d = (-1 - u) * n + (1.5 + u) * i + 0.5 * d,
            u = u * n - u * i,
            i = 0;
          i !== o;
          ++i
        )
          r[i] = e * a[c + i] + l * a[s + i] + d * a[t + i] + u * a[h + i];
        return r;
      }
    })),
    (yi.prototype = Object.assign(Object.create(gi.prototype), {
      constructor: yi,
      interpolate_: function(t, e, i, n) {
        var r = this.resultBuffer,
          a = this.sampleValues,
          o = this.valueSize;
        t *= o;
        var s = t - o;
        for (e = (i - e) / (n - e), i = 1 - e, n = 0; n !== o; ++n)
          r[n] = a[s + n] * i + a[t + n] * e;
        return r;
      }
    })),
    (xi.prototype = Object.assign(Object.create(gi.prototype), {
      constructor: xi,
      interpolate_: function(t) {
        return this.copySampleValue_(t - 1);
      }
    }));
  var tr;
  (tr = {
    TimeBufferType: Float32Array,
    ValueBufferType: Float32Array,
    DefaultInterpolation: 2301,
    InterpolantFactoryMethodDiscrete: function(t) {
      return new xi(this.times, this.values, this.getValueSize(), t);
    },
    InterpolantFactoryMethodLinear: function(t) {
      return new yi(this.times, this.values, this.getValueSize(), t);
    },
    InterpolantFactoryMethodSmooth: function(t) {
      return new vi(this.times, this.values, this.getValueSize(), t);
    },
    setInterpolation: function(t) {
      var e;
      switch (t) {
        case 2300:
          e = this.InterpolantFactoryMethodDiscrete;
          break;
        case 2301:
          e = this.InterpolantFactoryMethodLinear;
          break;
        case 2302:
          e = this.InterpolantFactoryMethodSmooth;
      }
      if (void 0 === e) {
        if (
          ((e =
            'unsupported interpolation for ' +
            this.ValueTypeName +
            ' keyframe track named ' +
            this.name),
          void 0 === this.createInterpolant)
        ) {
          if (t === this.DefaultInterpolation) throw Error(e);
          this.setInterpolation(this.DefaultInterpolation);
        }
        console.warn(e);
      } else this.createInterpolant = e;
    },
    getInterpolation: function() {
      switch (this.createInterpolant) {
        case this.InterpolantFactoryMethodDiscrete:
          return 2300;
        case this.InterpolantFactoryMethodLinear:
          return 2301;
        case this.InterpolantFactoryMethodSmooth:
          return 2302;
      }
    },
    getValueSize: function() {
      return this.values.length / this.times.length;
    },
    shift: function(t) {
      if (0 !== t)
        for (var e = this.times, i = 0, n = e.length; i !== n; ++i) e[i] += t;
      return this;
    },
    scale: function(t) {
      if (1 !== t)
        for (var e = this.times, i = 0, n = e.length; i !== n; ++i) e[i] *= t;
      return this;
    },
    trim: function(e, i) {
      for (
        var n = this.times, r = n.length, a = 0, o = r - 1;
        a !== r && n[a] < e;

      )
        ++a;
      for (; -1 !== o && n[o] > i; ) --o;
      return (
        ++o,
        (0 === a && o === r) ||
          (a >= o && ((o = Math.max(o, 1)), (a = o - 1)),
          (r = this.getValueSize()),
          (this.times = t.AnimationUtils.arraySlice(n, a, o)),
          (this.values = t.AnimationUtils.arraySlice(
            this.values,
            a * r,
            o * r
          ))),
        this
      );
    },
    validate: function() {
      var e = !0,
        i = this.getValueSize();
      0 != i - Math.floor(i) &&
        (console.error('invalid value size in track', this), (e = !1));
      var n = this.times,
        i = this.values,
        r = n.length;
      0 === r && (console.error('track is empty', this), (e = !1));
      for (var a = null, o = 0; o !== r; o++) {
        var s = n[o];
        if ('number' == typeof s && isNaN(s)) {
          console.error('time is not a valid number', this, o, s), (e = !1);
          break;
        }
        if (null !== a && a > s) {
          console.error('out of order keys', this, o, s, a), (e = !1);
          break;
        }
        a = s;
      }
      if (void 0 !== i && t.AnimationUtils.isTypedArray(i))
        for (o = 0, n = i.length; o !== n; ++o)
          if (((r = i[o]), isNaN(r))) {
            console.error('value is not a valid number', this, o, r), (e = !1);
            break;
          }
      return e;
    },
    optimize: function() {
      for (
        var e = this.times,
          i = this.values,
          n = this.getValueSize(),
          r = 2302 === this.getInterpolation(),
          a = 1,
          o = e.length - 1,
          s = 1;
        s < o;
        ++s
      ) {
        var c = !1,
          h = e[s];
        if (h !== e[s + 1] && (1 !== s || h !== h[0]))
          if (r) c = !0;
          else
            for (var l = s * n, u = l - n, d = l + n, h = 0; h !== n; ++h) {
              var p = i[l + h];
              if (p !== i[u + h] || p !== i[d + h]) {
                c = !0;
                break;
              }
            }
        if (c) {
          if (s !== a)
            for (e[a] = e[s], c = s * n, l = a * n, h = 0; h !== n; ++h)
              i[l + h] = i[c + h];
          ++a;
        }
      }
      if (0 < o) {
        for (e[a] = e[o], c = o * n, l = a * n, h = 0; h !== n; ++h)
          i[l + h] = i[c + h];
        ++a;
      }
      return (
        a !== e.length &&
          ((this.times = t.AnimationUtils.arraySlice(e, 0, a)),
          (this.values = t.AnimationUtils.arraySlice(i, 0, a * n))),
        this
      );
    }
  }),
    (bi.prototype = Object.assign(Object.create(tr), {
      constructor: bi,
      ValueTypeName: 'vector'
    })),
    (wi.prototype = Object.assign(Object.create(gi.prototype), {
      constructor: wi,
      interpolate_: function(t, e, i, n) {
        var r = this.resultBuffer,
          a = this.sampleValues,
          o = this.valueSize;
        for (t *= o, e = (i - e) / (n - e), i = t + o; t !== i; t += 4)
          s.slerpFlat(r, 0, a, t - o, a, t, e);
        return r;
      }
    })),
    (Mi.prototype = Object.assign(Object.create(tr), {
      constructor: Mi,
      ValueTypeName: 'quaternion',
      DefaultInterpolation: 2301,
      InterpolantFactoryMethodLinear: function(t) {
        return new wi(this.times, this.values, this.getValueSize(), t);
      },
      InterpolantFactoryMethodSmooth: void 0
    })),
    (Ei.prototype = Object.assign(Object.create(tr), {
      constructor: Ei,
      ValueTypeName: 'number'
    })),
    (Si.prototype = Object.assign(Object.create(tr), {
      constructor: Si,
      ValueTypeName: 'string',
      ValueBufferType: Array,
      DefaultInterpolation: 2300,
      InterpolantFactoryMethodLinear: void 0,
      InterpolantFactoryMethodSmooth: void 0
    })),
    (Ti.prototype = Object.assign(Object.create(tr), {
      constructor: Ti,
      ValueTypeName: 'bool',
      ValueBufferType: Array,
      DefaultInterpolation: 2300,
      InterpolantFactoryMethodLinear: void 0,
      InterpolantFactoryMethodSmooth: void 0
    })),
    (Ai.prototype = Object.assign(Object.create(tr), {
      constructor: Ai,
      ValueTypeName: 'color'
    })),
    (Li.prototype = tr),
    (tr.constructor = Li),
    Object.assign(Li, {
      parse: function(e) {
        if (void 0 === e.type)
          throw Error('track type undefined, can not parse');
        var i = Li._getTrackTypeForValueTypeName(e.type);
        if (void 0 === e.times) {
          var n = [],
            r = [];
          t.AnimationUtils.flattenJSON(e.keys, n, r, 'value'),
            (e.times = n),
            (e.values = r);
        }
        return void 0 !== i.parse
          ? i.parse(e)
          : new i(e.name, e.times, e.values, e.interpolation);
      },
      toJSON: function(e) {
        var i = e.constructor;
        if (void 0 !== i.toJSON) i = i.toJSON(e);
        else {
          var i = {
              name: e.name,
              times: t.AnimationUtils.convertArray(e.times, Array),
              values: t.AnimationUtils.convertArray(e.values, Array)
            },
            n = e.getInterpolation();
          n !== e.DefaultInterpolation && (i.interpolation = n);
        }
        return (i.type = e.ValueTypeName), i;
      },
      _getTrackTypeForValueTypeName: function(t) {
        switch (t.toLowerCase()) {
          case 'scalar':
          case 'double':
          case 'float':
          case 'number':
          case 'integer':
            return Ei;
          case 'vector':
          case 'vector2':
          case 'vector3':
          case 'vector4':
            return bi;
          case 'color':
            return Ai;
          case 'quaternion':
            return Mi;
          case 'bool':
          case 'boolean':
            return Ti;
          case 'string':
            return Si;
        }
        throw Error('Unsupported typeName: ' + t);
      }
    }),
    (Ri.prototype = {
      constructor: Ri,
      resetDuration: function() {
        for (var t = 0, e = 0, i = this.tracks.length; e !== i; ++e)
          var n = this.tracks[e], t = Math.max(t, n.times[n.times.length - 1]);
        this.duration = t;
      },
      trim: function() {
        for (var t = 0; t < this.tracks.length; t++)
          this.tracks[t].trim(0, this.duration);
        return this;
      },
      optimize: function() {
        for (var t = 0; t < this.tracks.length; t++) this.tracks[t].optimize();
        return this;
      }
    }),
    Object.assign(Ri, {
      parse: function(t) {
        for (
          var e = [], i = t.tracks, n = 1 / (t.fps || 1), r = 0, a = i.length;
          r !== a;
          ++r
        )
          e.push(Li.parse(i[r]).scale(n));
        return new Ri(t.name, t.duration, e);
      },
      toJSON: function(t) {
        var e = [],
          i = t.tracks;
        t = { name: t.name, duration: t.duration, tracks: e };
        for (var n = 0, r = i.length; n !== r; ++n) e.push(Li.toJSON(i[n]));
        return t;
      },
      CreateFromMorphTargetSequence: function(e, i, n, r) {
        for (var a = i.length, o = [], s = 0; s < a; s++) {
          var c = [],
            h = [];
          c.push((s + a - 1) % a, s, (s + 1) % a), h.push(0, 1, 0);
          var l = t.AnimationUtils.getKeyframeOrder(c),
            c = t.AnimationUtils.sortedArray(c, 1, l),
            h = t.AnimationUtils.sortedArray(h, 1, l);
          r || 0 !== c[0] || (c.push(a), h.push(h[0])),
            o.push(
              new Ei('.morphTargetInfluences[' + i[s].name + ']', c, h).scale(
                1 / n
              )
            );
        }
        return new Ri(e, -1, o);
      },
      findByName: function(t, e) {
        var i = t;
        Array.isArray(t) ||
          (i = (t.geometry && t.geometry.animations) || t.animations);
        for (var n = 0; n < i.length; n++) if (i[n].name === e) return i[n];
        return null;
      },
      CreateClipsFromMorphTargetSequences: function(t, e, i) {
        for (
          var n = {}, r = /^([\w-]*?)([\d]+)$/, a = 0, o = t.length;
          a < o;
          a++
        ) {
          var s = t[a],
            c = s.name.match(r);
          if (c && 1 < c.length) {
            var h = c[1];
            (c = n[h]) || (n[h] = c = []), c.push(s);
          }
        }
        t = [];
        for (h in n) t.push(Ri.CreateFromMorphTargetSequence(h, n[h], e, i));
        return t;
      },
      parseAnimation: function(e, i) {
        if (!e) return console.error('  no animation in JSONLoader data'), null;
        for (
          var n = function(e, i, n, r, a) {
              if (0 !== n.length) {
                var o = [],
                  s = [];
                t.AnimationUtils.flattenJSON(n, o, s, r),
                  0 !== o.length && a.push(new e(i, o, s));
              }
            },
            r = [],
            a = e.name || 'default',
            o = e.length || -1,
            s = e.fps || 30,
            c = e.hierarchy || [],
            h = 0;
          h < c.length;
          h++
        ) {
          var l = c[h].keys;
          if (l && 0 !== l.length)
            if (l[0].morphTargets) {
              for (var o = {}, u = 0; u < l.length; u++)
                if (l[u].morphTargets)
                  for (var d = 0; d < l[u].morphTargets.length; d++)
                    o[l[u].morphTargets[d]] = -1;
              for (var p in o) {
                for (
                  var f = [], m = [], d = 0;
                  d !== l[u].morphTargets.length;
                  ++d
                ) {
                  var g = l[u];
                  f.push(g.time), m.push(g.morphTarget === p ? 1 : 0);
                }
                r.push(new Ei('.morphTargetInfluence[' + p + ']', f, m));
              }
              o = o.length * (s || 1);
            } else
              (u = '.bones[' + i[h].name + ']'),
                n(bi, u + '.position', l, 'pos', r),
                n(Mi, u + '.quaternion', l, 'rot', r),
                n(bi, u + '.scale', l, 'scl', r);
        }
        return 0 === r.length ? null : new Ri(a, o, r);
      }
    }),
    Object.assign(Pi.prototype, {
      load: function(t, e, i, n) {
        var r = this;
        new ei(r.manager).load(
          t,
          function(t) {
            e(r.parse(JSON.parse(t)));
          },
          i,
          n
        );
      },
      setTextures: function(t) {
        this.textures = t;
      },
      parse: function(t) {
        function e(t) {
          return (
            void 0 === n[t] &&
              console.warn('THREE.MaterialLoader: Undefined texture', t),
            n[t]
          );
        }
        var n = this.textures,
          r = new $n[t.type]();
        if (
          (void 0 !== t.uuid && (r.uuid = t.uuid),
          void 0 !== t.name && (r.name = t.name),
          void 0 !== t.color && r.color.setHex(t.color),
          void 0 !== t.roughness && (r.roughness = t.roughness),
          void 0 !== t.metalness && (r.metalness = t.metalness),
          void 0 !== t.emissive && r.emissive.setHex(t.emissive),
          void 0 !== t.specular && r.specular.setHex(t.specular),
          void 0 !== t.shininess && (r.shininess = t.shininess),
          void 0 !== t.uniforms && (r.uniforms = t.uniforms),
          void 0 !== t.vertexShader && (r.vertexShader = t.vertexShader),
          void 0 !== t.fragmentShader && (r.fragmentShader = t.fragmentShader),
          void 0 !== t.vertexColors && (r.vertexColors = t.vertexColors),
          void 0 !== t.fog && (r.fog = t.fog),
          void 0 !== t.shading && (r.shading = t.shading),
          void 0 !== t.blending && (r.blending = t.blending),
          void 0 !== t.side && (r.side = t.side),
          void 0 !== t.opacity && (r.opacity = t.opacity),
          void 0 !== t.transparent && (r.transparent = t.transparent),
          void 0 !== t.alphaTest && (r.alphaTest = t.alphaTest),
          void 0 !== t.depthTest && (r.depthTest = t.depthTest),
          void 0 !== t.depthWrite && (r.depthWrite = t.depthWrite),
          void 0 !== t.colorWrite && (r.colorWrite = t.colorWrite),
          void 0 !== t.wireframe && (r.wireframe = t.wireframe),
          void 0 !== t.wireframeLinewidth &&
            (r.wireframeLinewidth = t.wireframeLinewidth),
          void 0 !== t.wireframeLinecap &&
            (r.wireframeLinecap = t.wireframeLinecap),
          void 0 !== t.wireframeLinejoin &&
            (r.wireframeLinejoin = t.wireframeLinejoin),
          void 0 !== t.skinning && (r.skinning = t.skinning),
          void 0 !== t.morphTargets && (r.morphTargets = t.morphTargets),
          void 0 !== t.size && (r.size = t.size),
          void 0 !== t.sizeAttenuation &&
            (r.sizeAttenuation = t.sizeAttenuation),
          void 0 !== t.map && (r.map = e(t.map)),
          void 0 !== t.alphaMap &&
            ((r.alphaMap = e(t.alphaMap)), (r.transparent = !0)),
          void 0 !== t.bumpMap && (r.bumpMap = e(t.bumpMap)),
          void 0 !== t.bumpScale && (r.bumpScale = t.bumpScale),
          void 0 !== t.normalMap && (r.normalMap = e(t.normalMap)),
          void 0 !== t.normalScale)
        ) {
          var a = t.normalScale;
          !1 === Array.isArray(a) && (a = [a, a]),
            (r.normalScale = new i().fromArray(a));
        }
        if (
          (void 0 !== t.displacementMap &&
            (r.displacementMap = e(t.displacementMap)),
          void 0 !== t.displacementScale &&
            (r.displacementScale = t.displacementScale),
          void 0 !== t.displacementBias &&
            (r.displacementBias = t.displacementBias),
          void 0 !== t.roughnessMap && (r.roughnessMap = e(t.roughnessMap)),
          void 0 !== t.metalnessMap && (r.metalnessMap = e(t.metalnessMap)),
          void 0 !== t.emissiveMap && (r.emissiveMap = e(t.emissiveMap)),
          void 0 !== t.emissiveIntensity &&
            (r.emissiveIntensity = t.emissiveIntensity),
          void 0 !== t.specularMap && (r.specularMap = e(t.specularMap)),
          void 0 !== t.envMap && (r.envMap = e(t.envMap)),
          void 0 !== t.reflectivity && (r.reflectivity = t.reflectivity),
          void 0 !== t.lightMap && (r.lightMap = e(t.lightMap)),
          void 0 !== t.lightMapIntensity &&
            (r.lightMapIntensity = t.lightMapIntensity),
          void 0 !== t.aoMap && (r.aoMap = e(t.aoMap)),
          void 0 !== t.aoMapIntensity && (r.aoMapIntensity = t.aoMapIntensity),
          void 0 !== t.materials)
        )
          for (var a = 0, o = t.materials.length; a < o; a++)
            r.materials.push(this.parse(t.materials[a]));
        return r;
      }
    }),
    Object.assign(Ci.prototype, {
      load: function(t, e, i, n) {
        var r = this;
        new ei(r.manager).load(
          t,
          function(t) {
            e(r.parse(JSON.parse(t)));
          },
          i,
          n
        );
      },
      parse: function(t) {
        var e = new mt(),
          i = t.data.index,
          n = {
            Int8Array: Int8Array,
            Uint8Array: Uint8Array,
            Uint8ClampedArray: Uint8ClampedArray,
            Int16Array: Int16Array,
            Uint16Array: Uint16Array,
            Int32Array: Int32Array,
            Uint32Array: Uint32Array,
            Float32Array: Float32Array,
            Float64Array: Float64Array
          };
        void 0 !== i &&
          ((i = new n[i.type](i.array)), e.setIndex(new ht(i, 1)));
        var r,
          a = t.data.attributes;
        for (r in a) {
          var o = a[r],
            i = new n[o.type](o.array);
          e.addAttribute(r, new ht(i, o.itemSize, o.normalized));
        }
        if (
          void 0 !== (n = t.data.groups || t.data.drawcalls || t.data.offsets)
        )
          for (r = 0, i = n.length; r !== i; ++r)
            (a = n[r]), e.addGroup(a.start, a.count, a.materialIndex);
        return (
          (t = t.data.boundingSphere),
          void 0 !== t &&
            ((n = new c()),
            void 0 !== t.center && n.fromArray(t.center),
            (e.boundingSphere = new J(n, t.radius))),
          e
        );
      }
    }),
    (Ii.prototype = {
      constructor: Ii,
      crossOrigin: void 0,
      extractUrlBase: function(t) {
        return (
          (t = t.split('/')),
          1 === t.length ? './' : (t.pop(), t.join('/') + '/')
        );
      },
      initMaterials: function(t, e, i) {
        for (var n = [], r = 0; r < t.length; ++r)
          n[r] = this.createMaterial(t[r], e, i);
        return n;
      },
      createMaterial: (function() {
        var e, i, n;
        return function(r, a, o) {
          function s(e, n, r, s, c) {
            e = a + e;
            var l = Ii.Handlers.get(e);
            return (
              null !== l
                ? (e = l.load(e))
                : (i.setCrossOrigin(o), (e = i.load(e))),
              void 0 !== n &&
                (e.repeat.fromArray(n),
                1 !== n[0] && (e.wrapS = 1e3),
                1 !== n[1] && (e.wrapT = 1e3)),
              void 0 !== r && e.offset.fromArray(r),
              void 0 !== s &&
                ('repeat' === s[0] && (e.wrapS = 1e3),
                'mirror' === s[0] && (e.wrapS = 1002),
                'repeat' === s[1] && (e.wrapT = 1e3),
                'mirror' === s[1] && (e.wrapT = 1002)),
              void 0 !== c && (e.anisotropy = c),
              (n = t.Math.generateUUID()),
              (h[n] = e),
              n
            );
          }
          void 0 === e && (e = new V()),
            void 0 === i && (i = new oi()),
            void 0 === n && (n = new Pi());
          var c,
            h = {},
            l = { uuid: t.Math.generateUUID(), type: 'MeshLambertMaterial' };
          for (c in r) {
            var u = r[c];
            switch (c) {
              case 'DbgColor':
              case 'DbgIndex':
              case 'opticalDensity':
              case 'illumination':
                break;
              case 'DbgName':
                l.name = u;
                break;
              case 'blending':
                l.blending = Nn[u];
                break;
              case 'colorAmbient':
              case 'mapAmbient':
                console.warn(
                  'THREE.Loader.createMaterial:',
                  c,
                  'is no longer supported.'
                );
                break;
              case 'colorDiffuse':
                l.color = e.fromArray(u).getHex();
                break;
              case 'colorSpecular':
                l.specular = e.fromArray(u).getHex();
                break;
              case 'colorEmissive':
                l.emissive = e.fromArray(u).getHex();
                break;
              case 'specularCoef':
                l.shininess = u;
                break;
              case 'shading':
                'basic' === u.toLowerCase() && (l.type = 'MeshBasicMaterial'),
                  'phong' === u.toLowerCase() && (l.type = 'MeshPhongMaterial'),
                  'standard' === u.toLowerCase() &&
                    (l.type = 'MeshStandardMaterial');
                break;
              case 'mapDiffuse':
                l.map = s(
                  u,
                  r.mapDiffuseRepeat,
                  r.mapDiffuseOffset,
                  r.mapDiffuseWrap,
                  r.mapDiffuseAnisotropy
                );
                break;
              case 'mapDiffuseRepeat':
              case 'mapDiffuseOffset':
              case 'mapDiffuseWrap':
              case 'mapDiffuseAnisotropy':
                break;
              case 'mapEmissive':
                l.emissiveMap = s(
                  u,
                  r.mapEmissiveRepeat,
                  r.mapEmissiveOffset,
                  r.mapEmissiveWrap,
                  r.mapEmissiveAnisotropy
                );
                break;
              case 'mapEmissiveRepeat':
              case 'mapEmissiveOffset':
              case 'mapEmissiveWrap':
              case 'mapEmissiveAnisotropy':
                break;
              case 'mapLight':
                l.lightMap = s(
                  u,
                  r.mapLightRepeat,
                  r.mapLightOffset,
                  r.mapLightWrap,
                  r.mapLightAnisotropy
                );
                break;
              case 'mapLightRepeat':
              case 'mapLightOffset':
              case 'mapLightWrap':
              case 'mapLightAnisotropy':
                break;
              case 'mapAO':
                l.aoMap = s(
                  u,
                  r.mapAORepeat,
                  r.mapAOOffset,
                  r.mapAOWrap,
                  r.mapAOAnisotropy
                );
                break;
              case 'mapAORepeat':
              case 'mapAOOffset':
              case 'mapAOWrap':
              case 'mapAOAnisotropy':
                break;
              case 'mapBump':
                l.bumpMap = s(
                  u,
                  r.mapBumpRepeat,
                  r.mapBumpOffset,
                  r.mapBumpWrap,
                  r.mapBumpAnisotropy
                );
                break;
              case 'mapBumpScale':
                l.bumpScale = u;
                break;
              case 'mapBumpRepeat':
              case 'mapBumpOffset':
              case 'mapBumpWrap':
              case 'mapBumpAnisotropy':
                break;
              case 'mapNormal':
                l.normalMap = s(
                  u,
                  r.mapNormalRepeat,
                  r.mapNormalOffset,
                  r.mapNormalWrap,
                  r.mapNormalAnisotropy
                );
                break;
              case 'mapNormalFactor':
                l.normalScale = [u, u];
                break;
              case 'mapNormalRepeat':
              case 'mapNormalOffset':
              case 'mapNormalWrap':
              case 'mapNormalAnisotropy':
                break;
              case 'mapSpecular':
                l.specularMap = s(
                  u,
                  r.mapSpecularRepeat,
                  r.mapSpecularOffset,
                  r.mapSpecularWrap,
                  r.mapSpecularAnisotropy
                );
                break;
              case 'mapSpecularRepeat':
              case 'mapSpecularOffset':
              case 'mapSpecularWrap':
              case 'mapSpecularAnisotropy':
                break;
              case 'mapMetalness':
                l.metalnessMap = s(
                  u,
                  r.mapMetalnessRepeat,
                  r.mapMetalnessOffset,
                  r.mapMetalnessWrap,
                  r.mapMetalnessAnisotropy
                );
                break;
              case 'mapMetalnessRepeat':
              case 'mapMetalnessOffset':
              case 'mapMetalnessWrap':
              case 'mapMetalnessAnisotropy':
                break;
              case 'mapRoughness':
                l.roughnessMap = s(
                  u,
                  r.mapRoughnessRepeat,
                  r.mapRoughnessOffset,
                  r.mapRoughnessWrap,
                  r.mapRoughnessAnisotropy
                );
                break;
              case 'mapRoughnessRepeat':
              case 'mapRoughnessOffset':
              case 'mapRoughnessWrap':
              case 'mapRoughnessAnisotropy':
                break;
              case 'mapAlpha':
                l.alphaMap = s(
                  u,
                  r.mapAlphaRepeat,
                  r.mapAlphaOffset,
                  r.mapAlphaWrap,
                  r.mapAlphaAnisotropy
                );
                break;
              case 'mapAlphaRepeat':
              case 'mapAlphaOffset':
              case 'mapAlphaWrap':
              case 'mapAlphaAnisotropy':
                break;
              case 'flipSided':
                l.side = 1;
                break;
              case 'doubleSided':
                l.side = 2;
                break;
              case 'transparency':
                console.warn(
                  'THREE.Loader.createMaterial: transparency has been renamed to opacity'
                ),
                  (l.opacity = u);
                break;
              case 'depthTest':
              case 'depthWrite':
              case 'colorWrite':
              case 'opacity':
              case 'reflectivity':
              case 'transparent':
              case 'visible':
              case 'wireframe':
                l[c] = u;
                break;
              case 'vertexColors':
                !0 === u && (l.vertexColors = 2),
                  'face' === u && (l.vertexColors = 1);
                break;
              default:
                console.error('THREE.Loader.createMaterial: Unsupported', c, u);
            }
          }
          return (
            'MeshBasicMaterial' === l.type && delete l.emissive,
            'MeshPhongMaterial' !== l.type && delete l.specular,
            1 > l.opacity && (l.transparent = !0),
            n.setTextures(h),
            n.parse(l)
          );
        };
      })()
    }),
    (Ii.Handlers = {
      handlers: [],
      add: function(t, e) {
        this.handlers.push(t, e);
      },
      get: function(t) {
        for (var e = this.handlers, i = 0, n = e.length; i < n; i += 2) {
          var r = e[i + 1];
          if (e[i].test(t)) return r;
        }
        return null;
      }
    }),
    Object.assign(Ui.prototype, {
      load: function(t, e, i, n) {
        var r = this,
          a =
            this.texturePath && 'string' == typeof this.texturePath
              ? this.texturePath
              : Ii.prototype.extractUrlBase(t),
          o = new ei(this.manager);
        o.setWithCredentials(this.withCredentials),
          o.load(
            t,
            function(i) {
              i = JSON.parse(i);
              var n = i.metadata;
              if (void 0 !== n && void 0 !== (n = n.type)) {
                if ('object' === n.toLowerCase())
                  return void console.error(
                    'THREE.JSONLoader: ' +
                      t +
                      ' should be loaded with THREE.ObjectLoader instead.'
                  );
                if ('scene' === n.toLowerCase())
                  return void console.error(
                    'THREE.JSONLoader: ' +
                      t +
                      ' should be loaded with THREE.SceneLoader instead.'
                  );
              }
              (i = r.parse(i, a)), e(i.geometry, i.materials);
            },
            i,
            n
          );
      },
      setTexturePath: function(t) {
        this.texturePath = t;
      },
      parse: function(t, e) {
        var n = new pt(),
          a = void 0 !== t.scale ? 1 / t.scale : 1;
        return (
          (function(e) {
            var r,
              a,
              o,
              s,
              h,
              l,
              u,
              d,
              p,
              f,
              m,
              g,
              v,
              y = t.faces;
            l = t.vertices;
            var x = t.normals,
              _ = t.colors,
              b = 0;
            if (void 0 !== t.uvs) {
              for (r = 0; r < t.uvs.length; r++) t.uvs[r].length && b++;
              for (r = 0; r < b; r++) n.faceVertexUvs[r] = [];
            }
            for (s = 0, h = l.length; s < h; )
              (r = new c()),
                (r.x = l[s++] * e),
                (r.y = l[s++] * e),
                (r.z = l[s++] * e),
                n.vertices.push(r);
            for (s = 0, h = y.length; s < h; )
              if (
                ((e = y[s++]),
                (p = 1 & e),
                (o = 2 & e),
                (r = 8 & e),
                (u = 16 & e),
                (f = 32 & e),
                (l = 64 & e),
                (e &= 128),
                p)
              ) {
                if (
                  ((p = new st()),
                  (p.a = y[s]),
                  (p.b = y[s + 1]),
                  (p.c = y[s + 3]),
                  (m = new st()),
                  (m.a = y[s + 1]),
                  (m.b = y[s + 2]),
                  (m.c = y[s + 3]),
                  (s += 4),
                  o &&
                    ((o = y[s++]),
                    (p.materialIndex = o),
                    (m.materialIndex = o)),
                  (o = n.faces.length),
                  r)
                )
                  for (r = 0; r < b; r++)
                    for (
                      g = t.uvs[r],
                        n.faceVertexUvs[r][o] = [],
                        n.faceVertexUvs[r][o + 1] = [],
                        a = 0;
                      4 > a;
                      a++
                    )
                      (d = y[s++]),
                        (v = g[2 * d]),
                        (d = g[2 * d + 1]),
                        (v = new i(v, d)),
                        2 !== a && n.faceVertexUvs[r][o].push(v),
                        0 !== a && n.faceVertexUvs[r][o + 1].push(v);
                if (
                  (u &&
                    ((u = 3 * y[s++]),
                    p.normal.set(x[u++], x[u++], x[u]),
                    m.normal.copy(p.normal)),
                  f)
                )
                  for (r = 0; 4 > r; r++)
                    (u = 3 * y[s++]),
                      (f = new c(x[u++], x[u++], x[u])),
                      2 !== r && p.vertexNormals.push(f),
                      0 !== r && m.vertexNormals.push(f);
                if (
                  (l &&
                    ((l = y[s++]),
                    (l = _[l]),
                    p.color.setHex(l),
                    m.color.setHex(l)),
                  e)
                )
                  for (r = 0; 4 > r; r++)
                    (l = y[s++]),
                      (l = _[l]),
                      2 !== r && p.vertexColors.push(new V(l)),
                      0 !== r && m.vertexColors.push(new V(l));
                n.faces.push(p), n.faces.push(m);
              } else {
                if (
                  ((p = new st()),
                  (p.a = y[s++]),
                  (p.b = y[s++]),
                  (p.c = y[s++]),
                  o && ((o = y[s++]), (p.materialIndex = o)),
                  (o = n.faces.length),
                  r)
                )
                  for (r = 0; r < b; r++)
                    for (
                      g = t.uvs[r], n.faceVertexUvs[r][o] = [], a = 0;
                      3 > a;
                      a++
                    )
                      (d = y[s++]),
                        (v = g[2 * d]),
                        (d = g[2 * d + 1]),
                        (v = new i(v, d)),
                        n.faceVertexUvs[r][o].push(v);
                if (
                  (u && ((u = 3 * y[s++]), p.normal.set(x[u++], x[u++], x[u])),
                  f)
                )
                  for (r = 0; 3 > r; r++)
                    (u = 3 * y[s++]),
                      (f = new c(x[u++], x[u++], x[u])),
                      p.vertexNormals.push(f);
                if ((l && ((l = y[s++]), p.color.setHex(_[l])), e))
                  for (r = 0; 3 > r; r++)
                    (l = y[s++]), p.vertexColors.push(new V(_[l]));
                n.faces.push(p);
              }
          })(a),
          (function() {
            var e =
              void 0 !== t.influencesPerVertex ? t.influencesPerVertex : 2;
            if (t.skinWeights)
              for (var i = 0, a = t.skinWeights.length; i < a; i += e)
                n.skinWeights.push(
                  new r(
                    t.skinWeights[i],
                    1 < e ? t.skinWeights[i + 1] : 0,
                    2 < e ? t.skinWeights[i + 2] : 0,
                    3 < e ? t.skinWeights[i + 3] : 0
                  )
                );
            if (t.skinIndices)
              for (i = 0, a = t.skinIndices.length; i < a; i += e)
                n.skinIndices.push(
                  new r(
                    t.skinIndices[i],
                    1 < e ? t.skinIndices[i + 1] : 0,
                    2 < e ? t.skinIndices[i + 2] : 0,
                    3 < e ? t.skinIndices[i + 3] : 0
                  )
                );
            (n.bones = t.bones),
              n.bones &&
                0 < n.bones.length &&
                (n.skinWeights.length !== n.skinIndices.length ||
                  n.skinIndices.length !== n.vertices.length) &&
                console.warn(
                  'When skinning, number of vertices (' +
                    n.vertices.length +
                    '), skinIndices (' +
                    n.skinIndices.length +
                    '), and skinWeights (' +
                    n.skinWeights.length +
                    ') should match.'
                );
          })(),
          (function(e) {
            if (void 0 !== t.morphTargets)
              for (var i = 0, r = t.morphTargets.length; i < r; i++) {
                (n.morphTargets[i] = {}),
                  (n.morphTargets[i].name = t.morphTargets[i].name),
                  (n.morphTargets[i].vertices = []);
                for (
                  var a = n.morphTargets[i].vertices,
                    o = t.morphTargets[i].vertices,
                    s = 0,
                    h = o.length;
                  s < h;
                  s += 3
                ) {
                  var l = new c();
                  (l.x = o[s] * e),
                    (l.y = o[s + 1] * e),
                    (l.z = o[s + 2] * e),
                    a.push(l);
                }
              }
            if (void 0 !== t.morphColors && 0 < t.morphColors.length)
              for (
                console.warn(
                  'THREE.JSONLoader: "morphColors" no longer supported. Using them as face colors.'
                ),
                  e = n.faces,
                  a = t.morphColors[0].colors,
                  i = 0,
                  r = e.length;
                i < r;
                i++
              )
                e[i].color.fromArray(a, 3 * i);
          })(a),
          (function() {
            var e = [],
              i = [];
            void 0 !== t.animation && i.push(t.animation),
              void 0 !== t.animations &&
                (t.animations.length
                  ? (i = i.concat(t.animations))
                  : i.push(t.animations));
            for (var r = 0; r < i.length; r++) {
              var a = Ri.parseAnimation(i[r], n.bones);
              a && e.push(a);
            }
            n.morphTargets &&
              ((i = Ri.CreateClipsFromMorphTargetSequences(n.morphTargets, 10)),
              (e = e.concat(i))),
              0 < e.length && (n.animations = e);
          })(),
          n.computeFaceNormals(),
          n.computeBoundingSphere(),
          void 0 === t.materials || 0 === t.materials.length
            ? { geometry: n }
            : ((a = Ii.prototype.initMaterials(
                t.materials,
                e,
                this.crossOrigin
              )),
              { geometry: n, materials: a })
        );
      }
    }),
    Object.assign(Di.prototype, {
      load: function(t, e, i, n) {
        '' === this.texturePath &&
          (this.texturePath = t.substring(0, t.lastIndexOf('/') + 1));
        var r = this;
        new ei(r.manager).load(
          t,
          function(t) {
            r.parse(JSON.parse(t), e);
          },
          i,
          n
        );
      },
      setTexturePath: function(t) {
        this.texturePath = t;
      },
      setCrossOrigin: function(t) {
        this.crossOrigin = t;
      },
      parse: function(t, e) {
        var i = this.parseGeometries(t.geometries),
          n = this.parseImages(t.images, function() {
            void 0 !== e && e(r);
          }),
          n = this.parseTextures(t.textures, n),
          n = this.parseMaterials(t.materials, n),
          r = this.parseObject(t.object, i, n);
        return (
          t.animations && (r.animations = this.parseAnimations(t.animations)),
          (void 0 !== t.images && 0 !== t.images.length) ||
            void 0 === e ||
            e(r),
          r
        );
      },
      parseGeometries: function(t) {
        var e = {};
        if (void 0 !== t)
          for (
            var i = new Ui(), n = new Ci(), r = 0, a = t.length;
            r < a;
            r++
          ) {
            var o,
              s = t[r];
            switch (s.type) {
              case 'PlaneGeometry':
              case 'PlaneBufferGeometry':
                o = new Kn[s.type](
                  s.width,
                  s.height,
                  s.widthSegments,
                  s.heightSegments
                );
                break;
              case 'BoxGeometry':
              case 'BoxBufferGeometry':
              case 'CubeGeometry':
                o = new Kn[s.type](
                  s.width,
                  s.height,
                  s.depth,
                  s.widthSegments,
                  s.heightSegments,
                  s.depthSegments
                );
                break;
              case 'CircleGeometry':
              case 'CircleBufferGeometry':
                o = new Kn[s.type](
                  s.radius,
                  s.segments,
                  s.thetaStart,
                  s.thetaLength
                );
                break;
              case 'CylinderGeometry':
              case 'CylinderBufferGeometry':
                o = new Kn[s.type](
                  s.radiusTop,
                  s.radiusBottom,
                  s.height,
                  s.radialSegments,
                  s.heightSegments,
                  s.openEnded,
                  s.thetaStart,
                  s.thetaLength
                );
                break;
              case 'ConeGeometry':
              case 'ConeBufferGeometry':
                o = new Kn[s.type](
                  s.radius,
                  s.height,
                  s.radialSegments,
                  s.heightSegments,
                  s.openEnded,
                  s.thetaStart,
                  s.thetaLength
                );
                break;
              case 'SphereGeometry':
              case 'SphereBufferGeometry':
                o = new Kn[s.type](
                  s.radius,
                  s.widthSegments,
                  s.heightSegments,
                  s.phiStart,
                  s.phiLength,
                  s.thetaStart,
                  s.thetaLength
                );
                break;
              case 'DodecahedronGeometry':
              case 'IcosahedronGeometry':
              case 'OctahedronGeometry':
              case 'TetrahedronGeometry':
                o = new Kn[s.type](s.radius, s.detail);
                break;
              case 'RingGeometry':
              case 'RingBufferGeometry':
                o = new Kn[s.type](
                  s.innerRadius,
                  s.outerRadius,
                  s.thetaSegments,
                  s.phiSegments,
                  s.thetaStart,
                  s.thetaLength
                );
                break;
              case 'TorusGeometry':
              case 'TorusBufferGeometry':
                o = new Kn[s.type](
                  s.radius,
                  s.tube,
                  s.radialSegments,
                  s.tubularSegments,
                  s.arc
                );
                break;
              case 'TorusKnotGeometry':
              case 'TorusKnotBufferGeometry':
                o = new Kn[s.type](
                  s.radius,
                  s.tube,
                  s.tubularSegments,
                  s.radialSegments,
                  s.p,
                  s.q
                );
                break;
              case 'LatheGeometry':
              case 'LatheBufferGeometry':
                o = new Kn[s.type](
                  s.points,
                  s.segments,
                  s.phiStart,
                  s.phiLength
                );
                break;
              case 'BufferGeometry':
                o = n.parse(s);
                break;
              case 'Geometry':
                o = i.parse(s.data, this.texturePath).geometry;
                break;
              default:
                console.warn(
                  'THREE.ObjectLoader: Unsupported geometry type "' +
                    s.type +
                    '"'
                );
                continue;
            }
            (o.uuid = s.uuid),
              void 0 !== s.name && (o.name = s.name),
              (e[s.uuid] = o);
          }
        return e;
      },
      parseMaterials: function(t, e) {
        var i = {};
        if (void 0 !== t) {
          var n = new Pi();
          n.setTextures(e);
          for (var r = 0, a = t.length; r < a; r++) {
            var o = n.parse(t[r]);
            i[o.uuid] = o;
          }
        }
        return i;
      },
      parseAnimations: function(t) {
        for (var e = [], i = 0; i < t.length; i++) {
          var n = Ri.parse(t[i]);
          e.push(n);
        }
        return e;
      },
      parseImages: function(t, e) {
        function i(t) {
          return (
            n.manager.itemStart(t),
            o.load(
              t,
              function() {
                n.manager.itemEnd(t);
              },
              void 0,
              function() {
                n.manager.itemError(t);
              }
            )
          );
        }
        var n = this,
          r = {};
        if (void 0 !== t && 0 < t.length) {
          var a = new ti(e),
            o = new ri(a);
          o.setCrossOrigin(this.crossOrigin);
          for (var a = 0, s = t.length; a < s; a++) {
            var c = t[a],
              h = /^(\/\/)|([a-z]+:(\/\/)?)/i.test(c.url)
                ? c.url
                : n.texturePath + c.url;
            r[c.uuid] = i(h);
          }
        }
        return r;
      },
      parseTextures: function(t, e) {
        function i(t, e) {
          return 'number' == typeof t
            ? t
            : (console.warn(
                'THREE.ObjectLoader.parseTexture: Constant should be in numeric form.',
                t
              ),
              e[t]);
        }
        var r = {};
        if (void 0 !== t)
          for (var a = 0, o = t.length; a < o; a++) {
            var s = t[a];
            void 0 === s.image &&
              console.warn(
                'THREE.ObjectLoader: No "image" specified for',
                s.uuid
              ),
              void 0 === e[s.image] &&
                console.warn('THREE.ObjectLoader: Undefined image', s.image);
            var c = new n(e[s.image]);
            (c.needsUpdate = !0),
              (c.uuid = s.uuid),
              void 0 !== s.name && (c.name = s.name),
              void 0 !== s.mapping && (c.mapping = i(s.mapping, Fn)),
              void 0 !== s.offset && c.offset.fromArray(s.offset),
              void 0 !== s.repeat && c.repeat.fromArray(s.repeat),
              void 0 !== s.wrap &&
                ((c.wrapS = i(s.wrap[0], On)), (c.wrapT = i(s.wrap[1], On))),
              void 0 !== s.minFilter && (c.minFilter = i(s.minFilter, zn)),
              void 0 !== s.magFilter && (c.magFilter = i(s.magFilter, zn)),
              void 0 !== s.anisotropy && (c.anisotropy = s.anisotropy),
              void 0 !== s.flipY && (c.flipY = s.flipY),
              (r[s.uuid] = c);
          }
        return r;
      },
      parseObject: (function() {
        var t = new h();
        return function(e, i, n) {
          function r(t) {
            return (
              void 0 === i[t] &&
                console.warn('THREE.ObjectLoader: Undefined geometry', t),
              i[t]
            );
          }
          function a(t) {
            if (void 0 !== t)
              return (
                void 0 === n[t] &&
                  console.warn('THREE.ObjectLoader: Undefined material', t),
                n[t]
              );
          }
          var o;
          switch (e.type) {
            case 'Scene':
              (o = new Jt()),
                void 0 !== e.background &&
                  Number.isInteger(e.background) &&
                  (o.background = new V(e.background)),
                void 0 !== e.fog &&
                  ('Fog' === e.fog.type
                    ? (o.fog = new Zt(e.fog.color, e.fog.near, e.fog.far))
                    : 'FogExp2' === e.fog.type &&
                      (o.fog = new Yt(e.fog.color, e.fog.density)));
              break;
            case 'PerspectiveCamera':
              (o = new _t(e.fov, e.aspect, e.near, e.far)),
                void 0 !== e.focus && (o.focus = e.focus),
                void 0 !== e.zoom && (o.zoom = e.zoom),
                void 0 !== e.filmGauge && (o.filmGauge = e.filmGauge),
                void 0 !== e.filmOffset && (o.filmOffset = e.filmOffset),
                void 0 !== e.view && (o.view = Object.assign({}, e.view));
              break;
            case 'OrthographicCamera':
              o = new bt(e.left, e.right, e.top, e.bottom, e.near, e.far);
              break;
            case 'AmbientLight':
              o = new mi(e.color, e.intensity);
              break;
            case 'DirectionalLight':
              o = new fi(e.color, e.intensity);
              break;
            case 'PointLight':
              o = new di(e.color, e.intensity, e.distance, e.decay);
              break;
            case 'SpotLight':
              o = new ui(
                e.color,
                e.intensity,
                e.distance,
                e.angle,
                e.penumbra,
                e.decay
              );
              break;
            case 'HemisphereLight':
              o = new ci(e.color, e.groundColor, e.intensity);
              break;
            case 'Mesh':
              o = r(e.geometry);
              var s = a(e.material);
              o = o.bones && 0 < o.bones.length ? new re(o, s) : new gt(o, s);
              break;
            case 'LOD':
              o = new te();
              break;
            case 'Line':
              o = new oe(r(e.geometry), a(e.material), e.mode);
              break;
            case 'LineSegments':
              o = new se(r(e.geometry), a(e.material));
              break;
            case 'PointCloud':
            case 'Points':
              o = new he(r(e.geometry), a(e.material));
              break;
            case 'Sprite':
              o = new $t(a(e.material));
              break;
            case 'Group':
              o = new le();
              break;
            default:
              o = new rt();
          }
          if (
            ((o.uuid = e.uuid),
            void 0 !== e.name && (o.name = e.name),
            void 0 !== e.matrix
              ? (t.fromArray(e.matrix),
                t.decompose(o.position, o.quaternion, o.scale))
              : (void 0 !== e.position && o.position.fromArray(e.position),
                void 0 !== e.rotation && o.rotation.fromArray(e.rotation),
                void 0 !== e.quaternion && o.quaternion.fromArray(e.quaternion),
                void 0 !== e.scale && o.scale.fromArray(e.scale)),
            void 0 !== e.castShadow && (o.castShadow = e.castShadow),
            void 0 !== e.receiveShadow && (o.receiveShadow = e.receiveShadow),
            e.shadow &&
              (void 0 !== e.shadow.bias && (o.shadow.bias = e.shadow.bias),
              void 0 !== e.shadow.radius && (o.shadow.radius = e.shadow.radius),
              void 0 !== e.shadow.mapSize &&
                o.shadow.mapSize.fromArray(e.shadow.mapSize),
              void 0 !== e.shadow.camera &&
                (o.shadow.camera = this.parseObject(e.shadow.camera))),
            void 0 !== e.visible && (o.visible = e.visible),
            void 0 !== e.userData && (o.userData = e.userData),
            void 0 !== e.children)
          )
            for (var c in e.children)
              o.add(this.parseObject(e.children[c], i, n));
          if ('LOD' === e.type)
            for (e = e.levels, s = 0; s < e.length; s++) {
              var h = e[s];
              (c = o.getObjectByProperty('uuid', h.object)),
                void 0 !== c && o.addLevel(c, h.distance);
            }
          return o;
        };
      })()
    }),
    (Ni.prototype = {
      constructor: Ni,
      getPoint: function() {
        return (
          console.warn('THREE.Curve: Warning, getPoint() not implemented!'),
          null
        );
      },
      getPointAt: function(t) {
        return (t = this.getUtoTmapping(t)), this.getPoint(t);
      },
      getPoints: function(t) {
        t || (t = 5);
        for (var e = [], i = 0; i <= t; i++) e.push(this.getPoint(i / t));
        return e;
      },
      getSpacedPoints: function(t) {
        t || (t = 5);
        for (var e = [], i = 0; i <= t; i++) e.push(this.getPointAt(i / t));
        return e;
      },
      getLength: function() {
        var t = this.getLengths();
        return t[t.length - 1];
      },
      getLengths: function(t) {
        if (
          (t ||
            (t = this.__arcLengthDivisions ? this.__arcLengthDivisions : 200),
          this.cacheArcLengths &&
            this.cacheArcLengths.length === t + 1 &&
            !this.needsUpdate)
        )
          return this.cacheArcLengths;
        this.needsUpdate = !1;
        var e,
          i,
          n = [],
          r = this.getPoint(0),
          a = 0;
        for (n.push(0), i = 1; i <= t; i++)
          (e = this.getPoint(i / t)),
            (a += e.distanceTo(r)),
            n.push(a),
            (r = e);
        return (this.cacheArcLengths = n);
      },
      updateArcLengths: function() {
        (this.needsUpdate = !0), this.getLengths();
      },
      getUtoTmapping: function(t, e) {
        var i,
          n,
          r = this.getLengths(),
          a = r.length;
        n = e || t * r[a - 1];
        for (var o, s = 0, c = a - 1; s <= c; )
          if (((i = Math.floor(s + (c - s) / 2)), 0 > (o = r[i] - n)))
            s = i + 1;
          else {
            if (!(0 < o)) {
              c = i;
              break;
            }
            c = i - 1;
          }
        return (
          (i = c),
          r[i] === n
            ? i / (a - 1)
            : ((s = r[i]), (i + (n - s) / (r[i + 1] - s)) / (a - 1))
        );
      },
      getTangent: function(t) {
        var e = t - 1e-4;
        return (
          (t += 1e-4),
          0 > e && (e = 0),
          1 < t && (t = 1),
          (e = this.getPoint(e)),
          this.getPoint(t)
            .clone()
            .sub(e)
            .normalize()
        );
      },
      getTangentAt: function(t) {
        return (t = this.getUtoTmapping(t)), this.getTangent(t);
      }
    }),
    (Ni.create = function(t, e) {
      return (
        (t.prototype = Object.create(Ni.prototype)),
        (t.prototype.constructor = t),
        (t.prototype.getPoint = e),
        t
      );
    }),
    (Fi.prototype = Object.create(Ni.prototype)),
    (Fi.prototype.constructor = Fi),
    (Fi.prototype.isLineCurve = !0),
    (Fi.prototype.getPoint = function(t) {
      if (1 === t) return this.v2.clone();
      var e = this.v2.clone().sub(this.v1);
      return e.multiplyScalar(t).add(this.v1), e;
    }),
    (Fi.prototype.getPointAt = function(t) {
      return this.getPoint(t);
    }),
    (Fi.prototype.getTangent = function() {
      return this.v2
        .clone()
        .sub(this.v1)
        .normalize();
    }),
    (Oi.prototype = Object.assign(Object.create(Ni.prototype), {
      constructor: Oi,
      add: function(t) {
        this.curves.push(t);
      },
      closePath: function() {
        var t = this.curves[0].getPoint(0),
          e = this.curves[this.curves.length - 1].getPoint(1);
        t.equals(e) || this.curves.push(new Fi(e, t));
      },
      getPoint: function(t) {
        var e = t * this.getLength(),
          i = this.getCurveLengths();
        for (t = 0; t < i.length; ) {
          if (i[t] >= e)
            return (
              (e = i[t] - e),
              (t = this.curves[t]),
              (i = t.getLength()),
              t.getPointAt(0 === i ? 0 : 1 - e / i)
            );
          t++;
        }
        return null;
      },
      getLength: function() {
        var t = this.getCurveLengths();
        return t[t.length - 1];
      },
      updateArcLengths: function() {
        (this.needsUpdate = !0), (this.cacheLengths = null), this.getLengths();
      },
      getCurveLengths: function() {
        if (
          this.cacheLengths &&
          this.cacheLengths.length === this.curves.length
        )
          return this.cacheLengths;
        for (var t = [], e = 0, i = 0, n = this.curves.length; i < n; i++)
          (e += this.curves[i].getLength()), t.push(e);
        return (this.cacheLengths = t);
      },
      getSpacedPoints: function(t) {
        t || (t = 40);
        for (var e = [], i = 0; i <= t; i++) e.push(this.getPoint(i / t));
        return this.autoClose && e.push(e[0]), e;
      },
      getPoints: function(t) {
        t = t || 12;
        for (var e, i = [], n = 0, r = this.curves; n < r.length; n++)
          for (
            var a = r[n],
              a = a.getPoints(
                a && a.isEllipseCurve
                  ? 2 * t
                  : a && a.isLineCurve
                    ? 1
                    : a && a.isSplineCurve ? t * a.points.length : t
              ),
              o = 0;
            o < a.length;
            o++
          ) {
            var s = a[o];
            (e && e.equals(s)) || (i.push(s), (e = s));
          }
        return (
          this.autoClose &&
            1 < i.length &&
            !i[i.length - 1].equals(i[0]) &&
            i.push(i[0]),
          i
        );
      },
      createPointsGeometry: function(t) {
        return (t = this.getPoints(t)), this.createGeometry(t);
      },
      createSpacedPointsGeometry: function(t) {
        return (t = this.getSpacedPoints(t)), this.createGeometry(t);
      },
      createGeometry: function(t) {
        for (var e = new pt(), i = 0, n = t.length; i < n; i++) {
          var r = t[i];
          e.vertices.push(new c(r.x, r.y, r.z || 0));
        }
        return e;
      }
    })),
    (zi.prototype = Object.create(Ni.prototype)),
    (zi.prototype.constructor = zi),
    (zi.prototype.isEllipseCurve = !0),
    (zi.prototype.getPoint = function(t) {
      for (
        var e = 2 * Math.PI,
          n = this.aEndAngle - this.aStartAngle,
          r = Math.abs(n) < Number.EPSILON;
        0 > n;

      )
        n += e;
      for (; n > e; ) n -= e;
      n < Number.EPSILON && (n = r ? 0 : e),
        !0 !== this.aClockwise || r || (n = n === e ? -e : n - e),
        (e = this.aStartAngle + t * n),
        (t = this.aX + this.xRadius * Math.cos(e));
      var a = this.aY + this.yRadius * Math.sin(e);
      return (
        0 !== this.aRotation &&
          ((e = Math.cos(this.aRotation)),
          (n = Math.sin(this.aRotation)),
          (r = t - this.aX),
          (a -= this.aY),
          (t = r * e - a * n + this.aX),
          (a = r * n + a * e + this.aY)),
        new i(t, a)
      );
    }),
    (t.CurveUtils = {
      tangentQuadraticBezier: function(t, e, i, n) {
        return 2 * (1 - t) * (i - e) + 2 * t * (n - i);
      },
      tangentCubicBezier: function(t, e, i, n, r) {
        return (
          -3 * e * (1 - t) * (1 - t) +
          3 * i * (1 - t) * (1 - t) -
          6 * t * i * (1 - t) +
          6 * t * n * (1 - t) -
          3 * t * t * n +
          3 * t * t * r
        );
      },
      tangentSpline: function(t) {
        return (
          6 * t * t -
          6 * t +
          (3 * t * t - 4 * t + 1) +
          (-6 * t * t + 6 * t) +
          (3 * t * t - 2 * t)
        );
      },
      interpolate: function(t, e, i, n, r) {
        (t = 0.5 * (i - t)), (n = 0.5 * (n - e));
        var a = r * r;
        return (
          (2 * e - 2 * i + t + n) * r * a +
          (-3 * e + 3 * i - 2 * t - n) * a +
          t * r +
          e
        );
      }
    }),
    (Bi.prototype = Object.create(Ni.prototype)),
    (Bi.prototype.constructor = Bi),
    (Bi.prototype.isSplineCurve = !0),
    (Bi.prototype.getPoint = function(e) {
      var n = this.points;
      e *= n.length - 1;
      var r = Math.floor(e);
      e -= r;
      var a = n[0 === r ? r : r - 1],
        o = n[r],
        s = n[r > n.length - 2 ? n.length - 1 : r + 1],
        n = n[r > n.length - 3 ? n.length - 1 : r + 2],
        r = t.CurveUtils.interpolate;
      return new i(r(a.x, o.x, s.x, n.x, e), r(a.y, o.y, s.y, n.y, e));
    }),
    (Gi.prototype = Object.create(Ni.prototype)),
    (Gi.prototype.constructor = Gi),
    (Gi.prototype.getPoint = function(e) {
      var n = t.ShapeUtils.b3;
      return new i(
        n(e, this.v0.x, this.v1.x, this.v2.x, this.v3.x),
        n(e, this.v0.y, this.v1.y, this.v2.y, this.v3.y)
      );
    }),
    (Gi.prototype.getTangent = function(e) {
      var n = t.CurveUtils.tangentCubicBezier;
      return new i(
        n(e, this.v0.x, this.v1.x, this.v2.x, this.v3.x),
        n(e, this.v0.y, this.v1.y, this.v2.y, this.v3.y)
      ).normalize();
    }),
    (Hi.prototype = Object.create(Ni.prototype)),
    (Hi.prototype.constructor = Hi),
    (Hi.prototype.getPoint = function(e) {
      var n = t.ShapeUtils.b2;
      return new i(
        n(e, this.v0.x, this.v1.x, this.v2.x),
        n(e, this.v0.y, this.v1.y, this.v2.y)
      );
    }),
    (Hi.prototype.getTangent = function(e) {
      var n = t.CurveUtils.tangentQuadraticBezier;
      return new i(
        n(e, this.v0.x, this.v1.x, this.v2.x),
        n(e, this.v0.y, this.v1.y, this.v2.y)
      ).normalize();
    });
  var er = Object.assign(Object.create(Oi.prototype), {
    fromPoints: function(t) {
      this.moveTo(t[0].x, t[0].y);
      for (var e = 1, i = t.length; e < i; e++) this.lineTo(t[e].x, t[e].y);
    },
    moveTo: function(t, e) {
      this.currentPoint.set(t, e);
    },
    lineTo: function(t, e) {
      var n = new Fi(this.currentPoint.clone(), new i(t, e));
      this.curves.push(n), this.currentPoint.set(t, e);
    },
    quadraticCurveTo: function(t, e, n, r) {
      (t = new Hi(this.currentPoint.clone(), new i(t, e), new i(n, r))),
        this.curves.push(t),
        this.currentPoint.set(n, r);
    },
    bezierCurveTo: function(t, e, n, r, a, o) {
      (t = new Gi(
        this.currentPoint.clone(),
        new i(t, e),
        new i(n, r),
        new i(a, o)
      )),
        this.curves.push(t),
        this.currentPoint.set(a, o);
    },
    splineThru: function(t) {
      var e = [this.currentPoint.clone()].concat(t),
        e = new Bi(e);
      this.curves.push(e), this.currentPoint.copy(t[t.length - 1]);
    },
    arc: function(t, e, i, n, r, a) {
      this.absarc(t + this.currentPoint.x, e + this.currentPoint.y, i, n, r, a);
    },
    absarc: function(t, e, i, n, r, a) {
      this.absellipse(t, e, i, i, n, r, a);
    },
    ellipse: function(t, e, i, n, r, a, o, s) {
      this.absellipse(
        t + this.currentPoint.x,
        e + this.currentPoint.y,
        i,
        n,
        r,
        a,
        o,
        s
      );
    },
    absellipse: function(t, e, i, n, r, a, o, s) {
      (t = new zi(t, e, i, n, r, a, o, s)),
        0 < this.curves.length &&
          ((e = t.getPoint(0)),
          e.equals(this.currentPoint) || this.lineTo(e.x, e.y)),
        this.curves.push(t),
        (t = t.getPoint(1)),
        this.currentPoint.copy(t);
    }
  });
  (Vi.prototype = Object.assign(Object.create(er), {
    constructor: Vi,
    getPointsHoles: function(t) {
      for (var e = [], i = 0, n = this.holes.length; i < n; i++)
        e[i] = this.holes[i].getPoints(t);
      return e;
    },
    extractAllPoints: function(t) {
      return { shape: this.getPoints(t), holes: this.getPointsHoles(t) };
    },
    extractPoints: function(t) {
      return this.extractAllPoints(t);
    }
  })),
    (ki.prototype = er),
    (er.constructor = ki),
    (ji.prototype = {
      moveTo: function(t, e) {
        (this.currentPath = new ki()),
          this.subPaths.push(this.currentPath),
          this.currentPath.moveTo(t, e);
      },
      lineTo: function(t, e) {
        this.currentPath.lineTo(t, e);
      },
      quadraticCurveTo: function(t, e, i, n) {
        this.currentPath.quadraticCurveTo(t, e, i, n);
      },
      bezierCurveTo: function(t, e, i, n, r, a) {
        this.currentPath.bezierCurveTo(t, e, i, n, r, a);
      },
      splineThru: function(t) {
        this.currentPath.splineThru(t);
      },
      toShapes: function(e, i) {
        function n(t) {
          for (var e = [], i = 0, n = t.length; i < n; i++) {
            var r = t[i],
              a = new Vi();
            (a.curves = r.curves), e.push(a);
          }
          return e;
        }
        function r(t, e) {
          for (var i = e.length, n = !1, r = i - 1, a = 0; a < i; r = a++) {
            var o = e[r],
              s = e[a],
              c = s.x - o.x,
              h = s.y - o.y;
            if (Math.abs(h) > Number.EPSILON) {
              if (
                (0 > h && ((o = e[a]), (c = -c), (s = e[r]), (h = -h)),
                !(t.y < o.y || t.y > s.y))
              )
                if (t.y === o.y) {
                  if (t.x === o.x) return !0;
                } else {
                  if (0 === (r = h * (t.x - o.x) - c * (t.y - o.y))) return !0;
                  0 > r || (n = !n);
                }
            } else if (
              t.y === o.y &&
              ((s.x <= t.x && t.x <= o.x) || (o.x <= t.x && t.x <= s.x))
            )
              return !0;
          }
          return n;
        }
        var a = t.ShapeUtils.isClockWise,
          o = this.subPaths;
        if (0 === o.length) return [];
        if (!0 === i) return n(o);
        var s,
          c,
          h,
          l = [];
        if (1 === o.length)
          return (
            (c = o[0]), (h = new Vi()), (h.curves = c.curves), l.push(h), l
          );
        var u = !a(o[0].getPoints()),
          u = e ? !u : u;
        h = [];
        var d,
          p = [],
          f = [],
          m = 0;
        (p[m] = void 0), (f[m] = []);
        for (var g = 0, v = o.length; g < v; g++)
          (c = o[g]),
            (d = c.getPoints()),
            (s = a(d)),
            (s = e ? !s : s)
              ? (!u && p[m] && m++,
                (p[m] = { s: new Vi(), p: d }),
                (p[m].s.curves = c.curves),
                u && m++,
                (f[m] = []))
              : f[m].push({ h: c, p: d[0] });
        if (!p[0]) return n(o);
        if (1 < p.length) {
          for (g = !1, c = [], a = 0, o = p.length; a < o; a++) h[a] = [];
          for (a = 0, o = p.length; a < o; a++)
            for (s = f[a], u = 0; u < s.length; u++) {
              for (m = s[u], d = !0, v = 0; v < p.length; v++)
                r(m.p, p[v].p) &&
                  (a !== v && c.push({ froms: a, tos: v, hole: u }),
                  d ? ((d = !1), h[v].push(m)) : (g = !0));
              d && h[a].push(m);
            }
          0 < c.length && (g || (f = h));
        }
        for (g = 0, a = p.length; g < a; g++)
          for (h = p[g].s, l.push(h), c = f[g], o = 0, s = c.length; o < s; o++)
            h.holes.push(c[o].h);
        return l;
      }
    }),
    Object.assign(Wi.prototype, {
      isFont: !0,
      generateShapes: function(e, i, n) {
        void 0 === i && (i = 100), void 0 === n && (n = 4);
        var r = this.data;
        e = String(e).split('');
        var a = i / r.resolution,
          o = 0;
        i = [];
        for (var s = 0; s < e.length; s++) {
          var c;
          c = a;
          var h = o,
            l = r.glyphs[e[s]] || r.glyphs['?'];
          if (l) {
            var u,
              d,
              p,
              f,
              m,
              g,
              v,
              y,
              x = new ji(),
              _ = [],
              b = t.ShapeUtils.b2,
              w = t.ShapeUtils.b3;
            if (l.o)
              for (
                var M = l._cachedOutline || (l._cachedOutline = l.o.split(' ')),
                  E = 0,
                  S = M.length;
                E < S;

              )
                switch (M[E++]) {
                  case 'm':
                    (u = M[E++] * c + h), (d = M[E++] * c), x.moveTo(u, d);
                    break;
                  case 'l':
                    (u = M[E++] * c + h), (d = M[E++] * c), x.lineTo(u, d);
                    break;
                  case 'q':
                    if (
                      ((u = M[E++] * c + h),
                      (d = M[E++] * c),
                      (m = M[E++] * c + h),
                      (g = M[E++] * c),
                      x.quadraticCurveTo(m, g, u, d),
                      (f = _[_.length - 1]))
                    ) {
                      (p = f.x), (f = f.y);
                      for (var T = 1; T <= n; T++) {
                        var A = T / n;
                        b(A, p, m, u), b(A, f, g, d);
                      }
                    }
                    break;
                  case 'b':
                    if (
                      ((u = M[E++] * c + h),
                      (d = M[E++] * c),
                      (m = M[E++] * c + h),
                      (g = M[E++] * c),
                      (v = M[E++] * c + h),
                      (y = M[E++] * c),
                      x.bezierCurveTo(m, g, v, y, u, d),
                      (f = _[_.length - 1]))
                    )
                      for (p = f.x, f = f.y, T = 1; T <= n; T++)
                        (A = T / n), w(A, p, m, v, u), w(A, f, g, y, d);
                }
            c = { offset: l.ha * c, path: x };
          } else c = void 0;
          (o += c.offset), i.push(c.path);
        }
        for (n = [], r = 0, e = i.length; r < e; r++)
          Array.prototype.push.apply(n, i[r].toShapes());
        return n;
      }
    }),
    Object.assign(Xi.prototype, {
      load: function(t, e, i, n) {
        var r = this;
        new ei(this.manager).load(
          t,
          function(t) {
            var i;
            try {
              i = JSON.parse(t);
            } catch (e) {
              console.warn(
                'THREE.FontLoader: typeface.js support is being deprecated. Use typeface.json instead.'
              ),
                (i = JSON.parse(t.substring(65, t.length - 2)));
            }
            (t = r.parse(i)), e && e(t);
          },
          i,
          n
        );
      },
      parse: function(t) {
        return new Wi(t);
      }
    });
  var ir;
  Object.assign(Yi.prototype, {
    load: function(t, e, i, n) {
      var r = new ei(this.manager);
      r.setResponseType('arraybuffer'),
        r.load(
          t,
          function(t) {
            qi().decodeAudioData(t, function(t) {
              e(t);
            });
          },
          i,
          n
        );
    }
  }),
    Object.assign(Zi.prototype, {
      update: (function() {
        var e,
          i,
          n,
          r,
          a,
          o,
          s,
          c = new h(),
          l = new h();
        return function(h) {
          if (
            e !== this ||
            i !== h.focus ||
            n !== h.fov ||
            r !== h.aspect * this.aspect ||
            a !== h.near ||
            o !== h.far ||
            s !== h.zoom
          ) {
            (e = this),
              (i = h.focus),
              (n = h.fov),
              (r = h.aspect * this.aspect),
              (a = h.near),
              (o = h.far),
              (s = h.zoom);
            var u,
              d = h.projectionMatrix.clone(),
              p = this.eyeSep / 2,
              f = p * a / i,
              m = a * Math.tan(t.Math.DEG2RAD * n * 0.5) / s;
            (l.elements[12] = -p),
              (c.elements[12] = p),
              (p = -m * r + f),
              (u = m * r + f),
              (d.elements[0] = 2 * a / (u - p)),
              (d.elements[8] = (u + p) / (u - p)),
              this.cameraL.projectionMatrix.copy(d),
              (p = -m * r - f),
              (u = m * r - f),
              (d.elements[0] = 2 * a / (u - p)),
              (d.elements[8] = (u + p) / (u - p)),
              this.cameraR.projectionMatrix.copy(d);
          }
          this.cameraL.matrixWorld.copy(h.matrixWorld).multiply(l),
            this.cameraR.matrixWorld.copy(h.matrixWorld).multiply(c);
        };
      })()
    }),
    (Ji.prototype = Object.create(rt.prototype)),
    (Ji.prototype.constructor = Ji),
    (Qi.prototype = Object.assign(Object.create(rt.prototype), {
      constructor: Qi,
      getInput: function() {
        return this.gain;
      },
      removeFilter: function() {
        null !== this.filter &&
          (this.gain.disconnect(this.filter),
          this.filter.disconnect(this.context.destination),
          this.gain.connect(this.context.destination),
          (this.filter = null));
      },
      getFilter: function() {
        return this.filter;
      },
      setFilter: function(t) {
        null !== this.filter
          ? (this.gain.disconnect(this.filter),
            this.filter.disconnect(this.context.destination))
          : this.gain.disconnect(this.context.destination),
          (this.filter = t),
          this.gain.connect(this.filter),
          this.filter.connect(this.context.destination);
      },
      getMasterVolume: function() {
        return this.gain.gain.value;
      },
      setMasterVolume: function(t) {
        this.gain.gain.value = t;
      },
      updateMatrixWorld: (function() {
        var t = new c(),
          e = new s(),
          i = new c(),
          n = new c();
        return function(r) {
          rt.prototype.updateMatrixWorld.call(this, r),
            (r = this.context.listener);
          var a = this.up;
          this.matrixWorld.decompose(t, e, i),
            n.set(0, 0, -1).applyQuaternion(e),
            r.setPosition(t.x, t.y, t.z),
            r.setOrientation(n.x, n.y, n.z, a.x, a.y, a.z);
        };
      })()
    })),
    (Ki.prototype = Object.assign(Object.create(rt.prototype), {
      constructor: Ki,
      getOutput: function() {
        return this.gain;
      },
      setNodeSource: function(t) {
        return (
          (this.hasPlaybackControl = !1),
          (this.sourceType = 'audioNode'),
          (this.source = t),
          this.connect(),
          this
        );
      },
      setBuffer: function(t) {
        return (
          (this.source.buffer = t),
          (this.sourceType = 'buffer'),
          this.autoplay && this.play(),
          this
        );
      },
      play: function() {
        if (!0 === this.isPlaying)
          console.warn('THREE.Audio: Audio is already playing.');
        else {
          if (!1 !== this.hasPlaybackControl) {
            var t = this.context.createBufferSource();
            return (
              (t.buffer = this.source.buffer),
              (t.loop = this.source.loop),
              (t.onended = this.source.onended),
              t.start(0, this.startTime),
              (t.playbackRate.value = this.playbackRate),
              (this.isPlaying = !0),
              (this.source = t),
              this.connect()
            );
          }
          console.warn('THREE.Audio: this Audio has no playback control.');
        }
      },
      pause: function() {
        if (!1 !== this.hasPlaybackControl)
          return (
            this.source.stop(),
            (this.startTime = this.context.currentTime),
            (this.isPlaying = !1),
            this
          );
        console.warn('THREE.Audio: this Audio has no playback control.');
      },
      stop: function() {
        if (!1 !== this.hasPlaybackControl)
          return (
            this.source.stop(),
            (this.startTime = 0),
            (this.isPlaying = !1),
            this
          );
        console.warn('THREE.Audio: this Audio has no playback control.');
      },
      connect: function() {
        if (0 < this.filters.length) {
          this.source.connect(this.filters[0]);
          for (var t = 1, e = this.filters.length; t < e; t++)
            this.filters[t - 1].connect(this.filters[t]);
          this.filters[this.filters.length - 1].connect(this.getOutput());
        } else this.source.connect(this.getOutput());
        return this;
      },
      disconnect: function() {
        if (0 < this.filters.length) {
          this.source.disconnect(this.filters[0]);
          for (var t = 1, e = this.filters.length; t < e; t++)
            this.filters[t - 1].disconnect(this.filters[t]);
          this.filters[this.filters.length - 1].disconnect(this.getOutput());
        } else this.source.disconnect(this.getOutput());
        return this;
      },
      getFilters: function() {
        return this.filters;
      },
      setFilters: function(t) {
        return (
          t || (t = []),
          !0 === this.isPlaying
            ? (this.disconnect(), (this.filters = t), this.connect())
            : (this.filters = t),
          this
        );
      },
      getFilter: function() {
        return this.getFilters()[0];
      },
      setFilter: function(t) {
        return this.setFilters(t ? [t] : []);
      },
      setPlaybackRate: function(t) {
        if (!1 !== this.hasPlaybackControl)
          return (
            (this.playbackRate = t),
            !0 === this.isPlaying &&
              (this.source.playbackRate.value = this.playbackRate),
            this
          );
        console.warn('THREE.Audio: this Audio has no playback control.');
      },
      getPlaybackRate: function() {
        return this.playbackRate;
      },
      onEnded: function() {
        this.isPlaying = !1;
      },
      getLoop: function() {
        return !1 === this.hasPlaybackControl
          ? (console.warn('THREE.Audio: this Audio has no playback control.'),
            !1)
          : this.source.loop;
      },
      setLoop: function(t) {
        !1 === this.hasPlaybackControl
          ? console.warn('THREE.Audio: this Audio has no playback control.')
          : (this.source.loop = t);
      },
      getVolume: function() {
        return this.gain.gain.value;
      },
      setVolume: function(t) {
        return (this.gain.gain.value = t), this;
      }
    })),
    ($i.prototype = Object.assign(Object.create(Ki.prototype), {
      constructor: $i,
      getOutput: function() {
        return this.panner;
      },
      getRefDistance: function() {
        return this.panner.refDistance;
      },
      setRefDistance: function(t) {
        this.panner.refDistance = t;
      },
      getRolloffFactor: function() {
        return this.panner.rolloffFactor;
      },
      setRolloffFactor: function(t) {
        this.panner.rolloffFactor = t;
      },
      getDistanceModel: function() {
        return this.panner.distanceModel;
      },
      setDistanceModel: function(t) {
        this.panner.distanceModel = t;
      },
      getMaxDistance: function() {
        return this.panner.maxDistance;
      },
      setMaxDistance: function(t) {
        this.panner.maxDistance = t;
      },
      updateMatrixWorld: (function() {
        var t = new c();
        return function(e) {
          rt.prototype.updateMatrixWorld.call(this, e),
            t.setFromMatrixPosition(this.matrixWorld),
            this.panner.setPosition(t.x, t.y, t.z);
        };
      })()
    })),
    Object.assign(tn.prototype, {
      getFrequencyData: function() {
        return this.analyser.getByteFrequencyData(this.data), this.data;
      },
      getAverageFrequency: function() {
        for (var t = 0, e = this.getFrequencyData(), i = 0; i < e.length; i++)
          t += e[i];
        return t / e.length;
      }
    }),
    (en.prototype = {
      constructor: en,
      accumulate: function(t, e) {
        var i = this.buffer,
          n = this.valueSize,
          r = t * n + n,
          a = this.cumulativeWeight;
        if (0 === a) {
          for (a = 0; a !== n; ++a) i[r + a] = i[a];
          a = e;
        } else (a += e), this._mixBufferRegion(i, r, 0, e / a, n);
        this.cumulativeWeight = a;
      },
      apply: function(t) {
        var e = this.valueSize,
          i = this.buffer;
        t = t * e + e;
        var n = this.cumulativeWeight,
          r = this.binding;
        (this.cumulativeWeight = 0),
          1 > n && this._mixBufferRegion(i, t, 3 * e, 1 - n, e);
        for (var n = e, a = e + e; n !== a; ++n)
          if (i[n] !== i[n + e]) {
            r.setValue(i, t);
            break;
          }
      },
      saveOriginalState: function() {
        var t = this.buffer,
          e = this.valueSize,
          i = 3 * e;
        this.binding.getValue(t, i);
        for (var n = e; n !== i; ++n) t[n] = t[i + n % e];
        this.cumulativeWeight = 0;
      },
      restoreOriginalState: function() {
        this.binding.setValue(this.buffer, 3 * this.valueSize);
      },
      _select: function(t, e, i, n, r) {
        if (0.5 <= n) for (n = 0; n !== r; ++n) t[e + n] = t[i + n];
      },
      _slerp: function(t, e, i, n) {
        s.slerpFlat(t, e, t, e, t, i, n);
      },
      _lerp: function(t, e, i, n, r) {
        for (var a = 1 - n, o = 0; o !== r; ++o) {
          var s = e + o;
          t[s] = t[s] * a + t[i + o] * n;
        }
      }
    }),
    (nn.prototype = {
      constructor: nn,
      getValue: function(t, e) {
        this.bind(), this.getValue(t, e);
      },
      setValue: function(t, e) {
        this.bind(), this.setValue(t, e);
      },
      bind: function() {
        var t = this.node,
          e = this.parsedPath,
          i = e.objectName,
          n = e.propertyName,
          r = e.propertyIndex;
        if (
          (t ||
            (this.node = t =
              nn.findNode(this.rootNode, e.nodeName) || this.rootNode),
          (this.getValue = this._getValue_unavailable),
          (this.setValue = this._setValue_unavailable),
          t)
        ) {
          if (i) {
            var a = e.objectIndex;
            switch (i) {
              case 'materials':
                if (!t.material)
                  return void console.error(
                    '  can not bind to material as node does not have a material',
                    this
                  );
                if (!t.material.materials)
                  return void console.error(
                    '  can not bind to material.materials as node.material does not have a materials array',
                    this
                  );
                t = t.material.materials;
                break;
              case 'bones':
                if (!t.skeleton)
                  return void console.error(
                    '  can not bind to bones as node does not have a skeleton',
                    this
                  );
                for (t = t.skeleton.bones, i = 0; i < t.length; i++)
                  if (t[i].name === a) {
                    a = i;
                    break;
                  }
                break;
              default:
                if (void 0 === t[i])
                  return void console.error(
                    '  can not bind to objectName of node, undefined',
                    this
                  );
                t = t[i];
            }
            if (void 0 !== a) {
              if (void 0 === t[a])
                return void console.error(
                  '  trying to bind to objectIndex of objectName, but is undefined:',
                  this,
                  t
                );
              t = t[a];
            }
          }
          if (void 0 === (a = t[n]))
            console.error(
              '  trying to update property for track: ' +
                e.nodeName +
                '.' +
                n +
                " but it wasn't found.",
              t
            );
          else {
            if (
              ((e = this.Versioning.None),
              void 0 !== t.needsUpdate
                ? ((e = this.Versioning.NeedsUpdate), (this.targetObject = t))
                : void 0 !== t.matrixWorldNeedsUpdate &&
                  ((e = this.Versioning.MatrixWorldNeedsUpdate),
                  (this.targetObject = t)),
              (i = this.BindingType.Direct),
              void 0 !== r)
            ) {
              if ('morphTargetInfluences' === n) {
                if (!t.geometry)
                  return void console.error(
                    '  can not bind to morphTargetInfluences becasuse node does not have a geometry',
                    this
                  );
                if (!t.geometry.morphTargets)
                  return void console.error(
                    '  can not bind to morphTargetInfluences becasuse node does not have a geometry.morphTargets',
                    this
                  );
                for (i = 0; i < this.node.geometry.morphTargets.length; i++)
                  if (t.geometry.morphTargets[i].name === r) {
                    r = i;
                    break;
                  }
              }
              (i = this.BindingType.ArrayElement),
                (this.resolvedProperty = a),
                (this.propertyIndex = r);
            } else
              void 0 !== a.fromArray && void 0 !== a.toArray
                ? ((i = this.BindingType.HasFromToArray),
                  (this.resolvedProperty = a))
                : void 0 !== a.length
                  ? ((i = this.BindingType.EntireArray),
                    (this.resolvedProperty = a))
                  : (this.propertyName = n);
            (this.getValue = this.GetterByBindingType[i]),
              (this.setValue = this.SetterByBindingTypeAndVersioning[i][e]);
          }
        } else
          console.error(
            '  trying to update node for track: ' +
              this.path +
              " but it wasn't found."
          );
      },
      unbind: function() {
        (this.node = null),
          (this.getValue = this._getValue_unbound),
          (this.setValue = this._setValue_unbound);
      }
    }),
    Object.assign(nn.prototype, {
      _getValue_unavailable: function() {},
      _setValue_unavailable: function() {},
      _getValue_unbound: nn.prototype.getValue,
      _setValue_unbound: nn.prototype.setValue,
      BindingType: {
        Direct: 0,
        EntireArray: 1,
        ArrayElement: 2,
        HasFromToArray: 3
      },
      Versioning: { None: 0, NeedsUpdate: 1, MatrixWorldNeedsUpdate: 2 },
      GetterByBindingType: [
        function(t, e) {
          t[e] = this.node[this.propertyName];
        },
        function(t, e) {
          for (var i = this.resolvedProperty, n = 0, r = i.length; n !== r; ++n)
            t[e++] = i[n];
        },
        function(t, e) {
          t[e] = this.resolvedProperty[this.propertyIndex];
        },
        function(t, e) {
          this.resolvedProperty.toArray(t, e);
        }
      ],
      SetterByBindingTypeAndVersioning: [
        [
          function(t, e) {
            this.node[this.propertyName] = t[e];
          },
          function(t, e) {
            (this.node[this.propertyName] = t[e]),
              (this.targetObject.needsUpdate = !0);
          },
          function(t, e) {
            (this.node[this.propertyName] = t[e]),
              (this.targetObject.matrixWorldNeedsUpdate = !0);
          }
        ],
        [
          function(t, e) {
            for (
              var i = this.resolvedProperty, n = 0, r = i.length;
              n !== r;
              ++n
            )
              i[n] = t[e++];
          },
          function(t, e) {
            for (
              var i = this.resolvedProperty, n = 0, r = i.length;
              n !== r;
              ++n
            )
              i[n] = t[e++];
            this.targetObject.needsUpdate = !0;
          },
          function(t, e) {
            for (
              var i = this.resolvedProperty, n = 0, r = i.length;
              n !== r;
              ++n
            )
              i[n] = t[e++];
            this.targetObject.matrixWorldNeedsUpdate = !0;
          }
        ],
        [
          function(t, e) {
            this.resolvedProperty[this.propertyIndex] = t[e];
          },
          function(t, e) {
            (this.resolvedProperty[this.propertyIndex] = t[e]),
              (this.targetObject.needsUpdate = !0);
          },
          function(t, e) {
            (this.resolvedProperty[this.propertyIndex] = t[e]),
              (this.targetObject.matrixWorldNeedsUpdate = !0);
          }
        ],
        [
          function(t, e) {
            this.resolvedProperty.fromArray(t, e);
          },
          function(t, e) {
            this.resolvedProperty.fromArray(t, e),
              (this.targetObject.needsUpdate = !0);
          },
          function(t, e) {
            this.resolvedProperty.fromArray(t, e),
              (this.targetObject.matrixWorldNeedsUpdate = !0);
          }
        ]
      ]
    }),
    (nn.Composite = function(t, e, i) {
      (i = i || nn.parseTrackName(e)),
        (this._targetGroup = t),
        (this._bindings = t.subscribe_(e, i));
    }),
    (nn.Composite.prototype = {
      constructor: nn.Composite,
      getValue: function(t, e) {
        this.bind();
        var i = this._bindings[this._targetGroup.nCachedObjects_];
        void 0 !== i && i.getValue(t, e);
      },
      setValue: function(t, e) {
        for (
          var i = this._bindings,
            n = this._targetGroup.nCachedObjects_,
            r = i.length;
          n !== r;
          ++n
        )
          i[n].setValue(t, e);
      },
      bind: function() {
        for (
          var t = this._bindings,
            e = this._targetGroup.nCachedObjects_,
            i = t.length;
          e !== i;
          ++e
        )
          t[e].bind();
      },
      unbind: function() {
        for (
          var t = this._bindings,
            e = this._targetGroup.nCachedObjects_,
            i = t.length;
          e !== i;
          ++e
        )
          t[e].unbind();
      }
    }),
    (nn.create = function(t, e, i) {
      return t && t.isAnimationObjectGroup
        ? new nn.Composite(t, e, i)
        : new nn(t, e, i);
    }),
    (nn.parseTrackName = function(t) {
      var e = /^((?:\w+[\/:])*)(\w+)?(?:\.(\w+)(?:\[(.+)\])?)?\.(\w+)(?:\[(.+)\])?$/.exec(
        t
      );
      if (!e) throw Error('cannot parse trackName at all: ' + t);
      if (
        ((e = {
          nodeName: e[2],
          objectName: e[3],
          objectIndex: e[4],
          propertyName: e[5],
          propertyIndex: e[6]
        }),
        null === e.propertyName || 0 === e.propertyName.length)
      )
        throw Error('can not parse propertyName from trackName: ' + t);
      return e;
    }),
    (nn.findNode = function(t, e) {
      if (
        !e ||
        '' === e ||
        'root' === e ||
        '.' === e ||
        -1 === e ||
        e === t.name ||
        e === t.uuid
      )
        return t;
      if (t.skeleton) {
        var i = (function(t) {
          for (var i = 0; i < t.bones.length; i++) {
            var n = t.bones[i];
            if (n.name === e) return n;
          }
          return null;
        })(t.skeleton);
        if (i) return i;
      }
      if (t.children) {
        var n = function(t) {
          for (var i = 0; i < t.length; i++) {
            var r = t[i];
            if (r.name === e || r.uuid === e || (r = n(r.children))) return r;
          }
          return null;
        };
        if ((i = n(t.children))) return i;
      }
      return null;
    }),
    (rn.prototype = {
      constructor: rn,
      isAnimationObjectGroup: !0,
      add: function() {
        for (
          var t = this._objects,
            e = t.length,
            i = this.nCachedObjects_,
            n = this._indicesByUUID,
            r = this._paths,
            a = this._parsedPaths,
            o = this._bindings,
            s = o.length,
            c = 0,
            h = arguments.length;
          c !== h;
          ++c
        ) {
          var l = arguments[c],
            u = l.uuid,
            d = n[u];
          if (void 0 === d) {
            (d = e++), (n[u] = d), t.push(l);
            for (var u = 0, p = s; u !== p; ++u)
              o[u].push(new nn(l, r[u], a[u]));
          } else if (d < i) {
            var f = t[d],
              m = --i,
              p = t[m];
            for (
              n[p.uuid] = d, t[d] = p, n[u] = m, t[m] = l, u = 0, p = s;
              u !== p;
              ++u
            ) {
              var g = o[u],
                v = g[d];
              (g[d] = g[m]),
                void 0 === v && (v = new nn(l, r[u], a[u])),
                (g[m] = v);
            }
          } else
            t[d] !== f &&
              console.error(
                'Different objects with the same UUID detected. Clean the caches or recreate your infrastructure when reloading scenes...'
              );
        }
        this.nCachedObjects_ = i;
      },
      remove: function() {
        for (
          var t = this._objects,
            e = this.nCachedObjects_,
            i = this._indicesByUUID,
            n = this._bindings,
            r = n.length,
            a = 0,
            o = arguments.length;
          a !== o;
          ++a
        ) {
          var s = arguments[a],
            c = s.uuid,
            h = i[c];
          if (void 0 !== h && h >= e) {
            var l = e++,
              u = t[l];
            for (
              i[u.uuid] = h, t[h] = u, i[c] = l, t[l] = s, s = 0, c = r;
              s !== c;
              ++s
            ) {
              var u = n[s],
                d = u[h];
              (u[h] = u[l]), (u[l] = d);
            }
          }
        }
        this.nCachedObjects_ = e;
      },
      uncache: function() {
        for (
          var t = this._objects,
            e = t.length,
            i = this.nCachedObjects_,
            n = this._indicesByUUID,
            r = this._bindings,
            a = r.length,
            o = 0,
            s = arguments.length;
          o !== s;
          ++o
        ) {
          var c = arguments[o].uuid,
            h = n[c];
          if (void 0 !== h)
            if ((delete n[c], h < i)) {
              var c = --i,
                l = t[c],
                u = --e,
                d = t[u];
              for (
                n[l.uuid] = h,
                  t[h] = l,
                  n[d.uuid] = c,
                  t[c] = d,
                  t.pop(),
                  l = 0,
                  d = a;
                l !== d;
                ++l
              ) {
                var p = r[l],
                  f = p[u];
                (p[h] = p[c]), (p[c] = f), p.pop();
              }
            } else
              for (
                u = --e,
                  d = t[u],
                  n[d.uuid] = h,
                  t[h] = d,
                  t.pop(),
                  l = 0,
                  d = a;
                l !== d;
                ++l
              )
                (p = r[l]), (p[h] = p[u]), p.pop();
        }
        this.nCachedObjects_ = i;
      },
      subscribe_: function(t, e) {
        var i = this._bindingsIndicesByPath,
          n = i[t],
          r = this._bindings;
        if (void 0 !== n) return r[n];
        var a = this._paths,
          o = this._parsedPaths,
          s = this._objects,
          c = this.nCachedObjects_,
          h = Array(s.length),
          n = r.length;
        for (
          i[t] = n, a.push(t), o.push(e), r.push(h), i = c, n = s.length;
          i !== n;
          ++i
        )
          h[i] = new nn(s[i], t, e);
        return h;
      },
      unsubscribe_: function(t) {
        var e = this._bindingsIndicesByPath,
          i = e[t];
        if (void 0 !== i) {
          var n = this._paths,
            r = this._parsedPaths,
            a = this._bindings,
            o = a.length - 1,
            s = a[o];
          (e[t[o]] = i),
            (a[i] = s),
            a.pop(),
            (r[i] = r[o]),
            r.pop(),
            (n[i] = n[o]),
            n.pop();
        }
      }
    }),
    (an.prototype = {
      constructor: an,
      play: function() {
        return this._mixer._activateAction(this), this;
      },
      stop: function() {
        return this._mixer._deactivateAction(this), this.reset();
      },
      reset: function() {
        return (
          (this.paused = !1),
          (this.enabled = !0),
          (this.time = 0),
          (this._loopCount = -1),
          (this._startTime = null),
          this.stopFading().stopWarping()
        );
      },
      isRunning: function() {
        return (
          this.enabled &&
          !this.paused &&
          0 !== this.timeScale &&
          null === this._startTime &&
          this._mixer._isActiveAction(this)
        );
      },
      isScheduled: function() {
        return this._mixer._isActiveAction(this);
      },
      startAt: function(t) {
        return (this._startTime = t), this;
      },
      setLoop: function(t, e) {
        return (this.loop = t), (this.repetitions = e), this;
      },
      setEffectiveWeight: function(t) {
        return (
          (this.weight = t),
          (this._effectiveWeight = this.enabled ? t : 0),
          this.stopFading()
        );
      },
      getEffectiveWeight: function() {
        return this._effectiveWeight;
      },
      fadeIn: function(t) {
        return this._scheduleFading(t, 0, 1);
      },
      fadeOut: function(t) {
        return this._scheduleFading(t, 1, 0);
      },
      crossFadeFrom: function(t, e, i) {
        if ((t.fadeOut(e), this.fadeIn(e), i)) {
          i = this._clip.duration;
          var n = t._clip.duration,
            r = i / n;
          t.warp(1, n / i, e), this.warp(r, 1, e);
        }
        return this;
      },
      crossFadeTo: function(t, e, i) {
        return t.crossFadeFrom(this, e, i);
      },
      stopFading: function() {
        var t = this._weightInterpolant;
        return (
          null !== t &&
            ((this._weightInterpolant = null),
            this._mixer._takeBackControlInterpolant(t)),
          this
        );
      },
      setEffectiveTimeScale: function(t) {
        return (
          (this.timeScale = t),
          (this._effectiveTimeScale = this.paused ? 0 : t),
          this.stopWarping()
        );
      },
      getEffectiveTimeScale: function() {
        return this._effectiveTimeScale;
      },
      setDuration: function(t) {
        return (this.timeScale = this._clip.duration / t), this.stopWarping();
      },
      syncWith: function(t) {
        return (
          (this.time = t.time),
          (this.timeScale = t.timeScale),
          this.stopWarping()
        );
      },
      halt: function(t) {
        return this.warp(this._effectiveTimeScale, 0, t);
      },
      warp: function(t, e, i) {
        var n = this._mixer,
          r = n.time,
          a = this._timeScaleInterpolant,
          o = this.timeScale;
        return (
          null === a &&
            (this._timeScaleInterpolant = a = n._lendControlInterpolant()),
          (n = a.parameterPositions),
          (a = a.sampleValues),
          (n[0] = r),
          (n[1] = r + i),
          (a[0] = t / o),
          (a[1] = e / o),
          this
        );
      },
      stopWarping: function() {
        var t = this._timeScaleInterpolant;
        return (
          null !== t &&
            ((this._timeScaleInterpolant = null),
            this._mixer._takeBackControlInterpolant(t)),
          this
        );
      },
      getMixer: function() {
        return this._mixer;
      },
      getClip: function() {
        return this._clip;
      },
      getRoot: function() {
        return this._localRoot || this._mixer._root;
      },
      _update: function(t, e, i, n) {
        var r = this._startTime;
        if (null !== r) {
          if (0 > (e = (t - r) * i) || 0 === i) return;
          (this._startTime = null), (e *= i);
        }
        if (
          ((e *= this._updateTimeScale(t)),
          (i = this._updateTime(e)),
          0 < (t = this._updateWeight(t)))
        ) {
          e = this._interpolants;
          for (
            var r = this._propertyBindings, a = 0, o = e.length;
            a !== o;
            ++a
          )
            e[a].evaluate(i), r[a].accumulate(n, t);
        }
      },
      _updateWeight: function(t) {
        var e = 0;
        if (this.enabled) {
          var e = this.weight,
            i = this._weightInterpolant;
          if (null !== i) {
            var n = i.evaluate(t)[0],
              e = e * n;
            t > i.parameterPositions[1] &&
              (this.stopFading(), 0 === n && (this.enabled = !1));
          }
        }
        return (this._effectiveWeight = e);
      },
      _updateTimeScale: function(t) {
        var e = 0;
        if (!this.paused) {
          var e = this.timeScale,
            i = this._timeScaleInterpolant;
          if (null !== i) {
            var n = i.evaluate(t)[0],
              e = e * n;
            t > i.parameterPositions[1] &&
              (this.stopWarping(),
              0 === e ? (this.paused = !0) : (this.timeScale = e));
          }
        }
        return (this._effectiveTimeScale = e);
      },
      _updateTime: function(t) {
        var e = this.time + t;
        if (0 === t) return e;
        var i = this._clip.duration,
          n = this.loop,
          r = this._loopCount;
        if (2200 === n)
          t: {
            if (
              (-1 === r && ((this.loopCount = 0), this._setEndings(!0, !0, !1)),
              e >= i)
            )
              e = i;
            else {
              if (!(0 > e)) break t;
              e = 0;
            }
            this.clampWhenFinished ? (this.paused = !0) : (this.enabled = !1),
              this._mixer.dispatchEvent({
                type: 'finished',
                action: this,
                direction: 0 > t ? -1 : 1
              });
          }
        else {
          if (
            ((n = 2202 === n),
            -1 === r &&
              (0 <= t
                ? ((r = 0), this._setEndings(!0, 0 === this.repetitions, n))
                : this._setEndings(0 === this.repetitions, !0, n)),
            e >= i || 0 > e)
          ) {
            var a = Math.floor(e / i),
              e = e - i * a,
              r = r + Math.abs(a),
              o = this.repetitions - r;
            0 > o
              ? (this.clampWhenFinished
                  ? (this.paused = !0)
                  : (this.enabled = !1),
                (e = 0 < t ? i : 0),
                this._mixer.dispatchEvent({
                  type: 'finished',
                  action: this,
                  direction: 0 < t ? 1 : -1
                }))
              : (0 === o
                  ? ((t = 0 > t), this._setEndings(t, !t, n))
                  : this._setEndings(!1, !1, n),
                (this._loopCount = r),
                this._mixer.dispatchEvent({
                  type: 'loop',
                  action: this,
                  loopDelta: a
                }));
          }
          if (n && 1 == (1 & r)) return (this.time = e), i - e;
        }
        return (this.time = e);
      },
      _setEndings: function(t, e, i) {
        var n = this._interpolantSettings;
        i
          ? ((n.endingStart = 2401), (n.endingEnd = 2401))
          : ((n.endingStart = t ? (this.zeroSlopeAtStart ? 2401 : 2400) : 2402),
            (n.endingEnd = e ? (this.zeroSlopeAtEnd ? 2401 : 2400) : 2402));
      },
      _scheduleFading: function(t, e, i) {
        var n = this._mixer,
          r = n.time,
          a = this._weightInterpolant;
        return (
          null === a &&
            (this._weightInterpolant = a = n._lendControlInterpolant()),
          (n = a.parameterPositions),
          (a = a.sampleValues),
          (n[0] = r),
          (a[0] = e),
          (n[1] = r + t),
          (a[1] = i),
          this
        );
      }
    }),
    Object.assign(on.prototype, e.prototype, {
      clipAction: function(t, e) {
        var i = e || this._root,
          n = i.uuid,
          r = 'string' == typeof t ? Ri.findByName(i, t) : t,
          i = null !== r ? r.uuid : t,
          a = this._actionsByClip[i],
          o = null;
        if (void 0 !== a) {
          if (void 0 !== (o = a.actionByRoot[n])) return o;
          (o = a.knownActions[0]), null === r && (r = o._clip);
        }
        return null === r
          ? null
          : ((r = new an(this, r, e)),
            this._bindAction(r, o),
            this._addInactiveAction(r, i, n),
            r);
      },
      existingAction: function(t, e) {
        var i = e || this._root,
          n = i.uuid,
          i = 'string' == typeof t ? Ri.findByName(i, t) : t,
          i = this._actionsByClip[i ? i.uuid : t];
        return void 0 !== i ? i.actionByRoot[n] || null : null;
      },
      stopAllAction: function() {
        for (
          var t = this._actions,
            e = this._nActiveActions,
            i = this._bindings,
            n = this._nActiveBindings,
            r = (this._nActiveBindings = this._nActiveActions = 0);
          r !== e;
          ++r
        )
          t[r].reset();
        for (r = 0; r !== n; ++r) i[r].useCount = 0;
        return this;
      },
      update: function(t) {
        t *= this.timeScale;
        for (
          var e = this._actions,
            i = this._nActiveActions,
            n = (this.time += t),
            r = Math.sign(t),
            a = (this._accuIndex ^= 1),
            o = 0;
          o !== i;
          ++o
        ) {
          var s = e[o];
          s.enabled && s._update(n, t, r, a);
        }
        for (t = this._bindings, e = this._nActiveBindings, o = 0; o !== e; ++o)
          t[o].apply(a);
        return this;
      },
      getRoot: function() {
        return this._root;
      },
      uncacheClip: function(t) {
        var e = this._actions;
        t = t.uuid;
        var i = this._actionsByClip,
          n = i[t];
        if (void 0 !== n) {
          for (var n = n.knownActions, r = 0, a = n.length; r !== a; ++r) {
            var o = n[r];
            this._deactivateAction(o);
            var s = o._cacheIndex,
              c = e[e.length - 1];
            (o._cacheIndex = null),
              (o._byClipCacheIndex = null),
              (c._cacheIndex = s),
              (e[s] = c),
              e.pop(),
              this._removeInactiveBindingsForAction(o);
          }
          delete i[t];
        }
      },
      uncacheRoot: function(t) {
        t = t.uuid;
        var e,
          i = this._actionsByClip;
        for (e in i) {
          var n = i[e].actionByRoot[t];
          void 0 !== n &&
            (this._deactivateAction(n), this._removeInactiveAction(n));
        }
        if (void 0 !== (e = this._bindingsByRootAndName[t]))
          for (var r in e)
            (t = e[r]),
              t.restoreOriginalState(),
              this._removeInactiveBinding(t);
      },
      uncacheAction: function(t, e) {
        var i = this.existingAction(t, e);
        null !== i &&
          (this._deactivateAction(i), this._removeInactiveAction(i));
      }
    }),
    Object.assign(on.prototype, {
      _bindAction: function(t, e) {
        var i = t._localRoot || this._root,
          n = t._clip.tracks,
          r = n.length,
          a = t._propertyBindings,
          o = t._interpolants,
          s = i.uuid,
          c = this._bindingsByRootAndName,
          h = c[s];
        for (void 0 === h && ((h = {}), (c[s] = h)), c = 0; c !== r; ++c) {
          var l = n[c],
            u = l.name,
            d = h[u];
          if (void 0 === d) {
            if (void 0 !== (d = a[c])) {
              null === d._cacheIndex &&
                (++d.referenceCount, this._addInactiveBinding(d, s, u));
              continue;
            }
            (d = new en(
              nn.create(i, u, e && e._propertyBindings[c].binding.parsedPath),
              l.ValueTypeName,
              l.getValueSize()
            )),
              ++d.referenceCount,
              this._addInactiveBinding(d, s, u);
          }
          (a[c] = d), (o[c].resultBuffer = d.buffer);
        }
      },
      _activateAction: function(t) {
        if (!this._isActiveAction(t)) {
          if (null === t._cacheIndex) {
            var e = (t._localRoot || this._root).uuid,
              i = t._clip.uuid,
              n = this._actionsByClip[i];
            this._bindAction(t, n && n.knownActions[0]),
              this._addInactiveAction(t, i, e);
          }
          for (e = t._propertyBindings, i = 0, n = e.length; i !== n; ++i) {
            var r = e[i];
            0 == r.useCount++ && (this._lendBinding(r), r.saveOriginalState());
          }
          this._lendAction(t);
        }
      },
      _deactivateAction: function(t) {
        if (this._isActiveAction(t)) {
          for (var e = t._propertyBindings, i = 0, n = e.length; i !== n; ++i) {
            var r = e[i];
            0 == --r.useCount &&
              (r.restoreOriginalState(), this._takeBackBinding(r));
          }
          this._takeBackAction(t);
        }
      },
      _initMemoryManager: function() {
        (this._actions = []),
          (this._nActiveActions = 0),
          (this._actionsByClip = {}),
          (this._bindings = []),
          (this._nActiveBindings = 0),
          (this._bindingsByRootAndName = {}),
          (this._controlInterpolants = []),
          (this._nActiveControlInterpolants = 0);
        var t = this;
        this.stats = {
          actions: {
            get total() {
              return t._actions.length;
            },
            get inUse() {
              return t._nActiveActions;
            }
          },
          bindings: {
            get total() {
              return t._bindings.length;
            },
            get inUse() {
              return t._nActiveBindings;
            }
          },
          controlInterpolants: {
            get total() {
              return t._controlInterpolants.length;
            },
            get inUse() {
              return t._nActiveControlInterpolants;
            }
          }
        };
      },
      _isActiveAction: function(t) {
        return null !== (t = t._cacheIndex) && t < this._nActiveActions;
      },
      _addInactiveAction: function(t, e, i) {
        var n = this._actions,
          r = this._actionsByClip,
          a = r[e];
        void 0 === a
          ? ((a = { knownActions: [t], actionByRoot: {} }),
            (t._byClipCacheIndex = 0),
            (r[e] = a))
          : ((e = a.knownActions), (t._byClipCacheIndex = e.length), e.push(t)),
          (t._cacheIndex = n.length),
          n.push(t),
          (a.actionByRoot[i] = t);
      },
      _removeInactiveAction: function(t) {
        var e = this._actions,
          i = e[e.length - 1],
          n = t._cacheIndex;
        (i._cacheIndex = n), (e[n] = i), e.pop(), (t._cacheIndex = null);
        var i = t._clip.uuid,
          n = this._actionsByClip,
          r = n[i],
          a = r.knownActions,
          o = a[a.length - 1],
          s = t._byClipCacheIndex;
        (o._byClipCacheIndex = s),
          (a[s] = o),
          a.pop(),
          (t._byClipCacheIndex = null),
          delete r.actionByRoot[(e._localRoot || this._root).uuid],
          0 === a.length && delete n[i],
          this._removeInactiveBindingsForAction(t);
      },
      _removeInactiveBindingsForAction: function(t) {
        t = t._propertyBindings;
        for (var e = 0, i = t.length; e !== i; ++e) {
          var n = t[e];
          0 == --n.referenceCount && this._removeInactiveBinding(n);
        }
      },
      _lendAction: function(t) {
        var e = this._actions,
          i = t._cacheIndex,
          n = this._nActiveActions++,
          r = e[n];
        (t._cacheIndex = n), (e[n] = t), (r._cacheIndex = i), (e[i] = r);
      },
      _takeBackAction: function(t) {
        var e = this._actions,
          i = t._cacheIndex,
          n = --this._nActiveActions,
          r = e[n];
        (t._cacheIndex = n), (e[n] = t), (r._cacheIndex = i), (e[i] = r);
      },
      _addInactiveBinding: function(t, e, i) {
        var n = this._bindingsByRootAndName,
          r = n[e],
          a = this._bindings;
        void 0 === r && ((r = {}), (n[e] = r)),
          (r[i] = t),
          (t._cacheIndex = a.length),
          a.push(t);
      },
      _removeInactiveBinding: function(t) {
        var e = this._bindings,
          i = t.binding,
          n = i.rootNode.uuid,
          i = i.path,
          r = this._bindingsByRootAndName,
          a = r[n],
          o = e[e.length - 1];
        (t = t._cacheIndex),
          (o._cacheIndex = t),
          (e[t] = o),
          e.pop(),
          delete a[i];
        t: {
          for (var s in a) break t;
          delete r[n];
        }
      },
      _lendBinding: function(t) {
        var e = this._bindings,
          i = t._cacheIndex,
          n = this._nActiveBindings++,
          r = e[n];
        (t._cacheIndex = n), (e[n] = t), (r._cacheIndex = i), (e[i] = r);
      },
      _takeBackBinding: function(t) {
        var e = this._bindings,
          i = t._cacheIndex,
          n = --this._nActiveBindings,
          r = e[n];
        (t._cacheIndex = n), (e[n] = t), (r._cacheIndex = i), (e[i] = r);
      },
      _lendControlInterpolant: function() {
        var t = this._controlInterpolants,
          e = this._nActiveControlInterpolants++,
          i = t[e];
        return (
          void 0 === i &&
            ((i = new yi(
              new Float32Array(2),
              new Float32Array(2),
              1,
              this._controlInterpolantsResultBuffer
            )),
            (i.__cacheIndex = e),
            (t[e] = i)),
          i
        );
      },
      _takeBackControlInterpolant: function(t) {
        var e = this._controlInterpolants,
          i = t.__cacheIndex,
          n = --this._nActiveControlInterpolants,
          r = e[n];
        (t.__cacheIndex = n), (e[n] = t), (r.__cacheIndex = i), (e[i] = r);
      },
      _controlInterpolantsResultBuffer: new Float32Array(1)
    }),
    (sn.prototype = {
      constructor: sn,
      onUpdate: function(t) {
        return (this.dynamic = !0), (this.onUpdateCallback = t), this;
      }
    }),
    (cn.prototype = Object.create(mt.prototype)),
    (cn.prototype.constructor = cn),
    (cn.prototype.isInstancedBufferGeometry = !0),
    (cn.prototype.addGroup = function(t, e, i) {
      this.groups.push({ start: t, count: e, instances: i });
    }),
    (cn.prototype.copy = function(t) {
      var e = t.index;
      null !== e && this.setIndex(e.clone());
      var i,
        e = t.attributes;
      for (i in e) this.addAttribute(i, e[i].clone());
      for (t = t.groups, i = 0, e = t.length; i < e; i++) {
        var n = t[i];
        this.addGroup(n.start, n.count, n.instances);
      }
      return this;
    }),
    (hn.prototype = {
      constructor: hn,
      isInterleavedBufferAttribute: !0,
      get count() {
        return this.data.count;
      },
      get array() {
        return this.data.array;
      },
      setX: function(t, e) {
        return (this.data.array[t * this.data.stride + this.offset] = e), this;
      },
      setY: function(t, e) {
        return (
          (this.data.array[t * this.data.stride + this.offset + 1] = e), this
        );
      },
      setZ: function(t, e) {
        return (
          (this.data.array[t * this.data.stride + this.offset + 2] = e), this
        );
      },
      setW: function(t, e) {
        return (
          (this.data.array[t * this.data.stride + this.offset + 3] = e), this
        );
      },
      getX: function(t) {
        return this.data.array[t * this.data.stride + this.offset];
      },
      getY: function(t) {
        return this.data.array[t * this.data.stride + this.offset + 1];
      },
      getZ: function(t) {
        return this.data.array[t * this.data.stride + this.offset + 2];
      },
      getW: function(t) {
        return this.data.array[t * this.data.stride + this.offset + 3];
      },
      setXY: function(t, e, i) {
        return (
          (t = t * this.data.stride + this.offset),
          (this.data.array[t + 0] = e),
          (this.data.array[t + 1] = i),
          this
        );
      },
      setXYZ: function(t, e, i, n) {
        return (
          (t = t * this.data.stride + this.offset),
          (this.data.array[t + 0] = e),
          (this.data.array[t + 1] = i),
          (this.data.array[t + 2] = n),
          this
        );
      },
      setXYZW: function(t, e, i, n, r) {
        return (
          (t = t * this.data.stride + this.offset),
          (this.data.array[t + 0] = e),
          (this.data.array[t + 1] = i),
          (this.data.array[t + 2] = n),
          (this.data.array[t + 3] = r),
          this
        );
      }
    }),
    (ln.prototype = {
      constructor: ln,
      isInterleavedBuffer: !0,
      set needsUpdate(t) {
        !0 === t && this.version++;
      },
      setDynamic: function(t) {
        return (this.dynamic = t), this;
      },
      copy: function(t) {
        return (
          (this.array = new t.array.constructor(t.array)),
          (this.count = t.count),
          (this.stride = t.stride),
          (this.dynamic = t.dynamic),
          this
        );
      },
      copyAt: function(t, e, i) {
        (t *= this.stride), (i *= e.stride);
        for (var n = 0, r = this.stride; n < r; n++)
          this.array[t + n] = e.array[i + n];
        return this;
      },
      set: function(t, e) {
        return void 0 === e && (e = 0), this.array.set(t, e), this;
      },
      clone: function() {
        return new this.constructor().copy(this);
      }
    }),
    (un.prototype = Object.create(ln.prototype)),
    (un.prototype.constructor = un),
    (un.prototype.isInstancedInterleavedBuffer = !0),
    (un.prototype.copy = function(t) {
      return (
        ln.prototype.copy.call(this, t),
        (this.meshPerAttribute = t.meshPerAttribute),
        this
      );
    }),
    (dn.prototype = Object.create(ht.prototype)),
    (dn.prototype.constructor = dn),
    (dn.prototype.isInstancedBufferAttribute = !0),
    (dn.prototype.copy = function(t) {
      return (
        ht.prototype.copy.call(this, t),
        (this.meshPerAttribute = t.meshPerAttribute),
        this
      );
    }),
    (pn.prototype = {
      constructor: pn,
      linePrecision: 1,
      set: function(t, e) {
        this.ray.set(t, e);
      },
      setFromCamera: function(t, e) {
        e && e.isPerspectiveCamera
          ? (this.ray.origin.setFromMatrixPosition(e.matrixWorld),
            this.ray.direction
              .set(t.x, t.y, 0.5)
              .unproject(e)
              .sub(this.ray.origin)
              .normalize())
          : e && e.isOrthographicCamera
            ? (this.ray.origin
                .set(t.x, t.y, (e.near + e.far) / (e.near - e.far))
                .unproject(e),
              this.ray.direction
                .set(0, 0, -1)
                .transformDirection(e.matrixWorld))
            : console.error('THREE.Raycaster: Unsupported camera type.');
      },
      intersectObject: function(t, e) {
        var i = [];
        return mn(t, this, i, e), i.sort(fn), i;
      },
      intersectObjects: function(t, e) {
        var i = [];
        if (!1 === Array.isArray(t))
          return (
            console.warn(
              'THREE.Raycaster.intersectObjects: objects is not an Array.'
            ),
            i
          );
        for (var n = 0, r = t.length; n < r; n++) mn(t[n], this, i, e);
        return i.sort(fn), i;
      }
    }),
    (gn.prototype = {
      constructor: gn,
      start: function() {
        (this.oldTime = this.startTime = (performance || Date).now()),
          (this.running = !0);
      },
      stop: function() {
        this.getElapsedTime(), (this.running = !1);
      },
      getElapsedTime: function() {
        return this.getDelta(), this.elapsedTime;
      },
      getDelta: function() {
        var t = 0;
        if ((this.autoStart && !this.running && this.start(), this.running)) {
          var e = (performance || Date).now(),
            t = (e - this.oldTime) / 1e3;
          (this.oldTime = e), (this.elapsedTime += t);
        }
        return t;
      }
    }),
    (vn.prototype = {
      constructor: vn,
      set: function(t, e, i) {
        return (this.radius = t), (this.phi = e), (this.theta = i), this;
      },
      clone: function() {
        return new this.constructor().copy(this);
      },
      copy: function(t) {
        return (
          this.radius.copy(t.radius),
          this.phi.copy(t.phi),
          this.theta.copy(t.theta),
          this
        );
      },
      makeSafe: function() {
        return (
          (this.phi = Math.max(1e-6, Math.min(Math.PI - 1e-6, this.phi))), this
        );
      },
      setFromVector3: function(e) {
        return (
          (this.radius = e.length()),
          0 === this.radius
            ? (this.phi = this.theta = 0)
            : ((this.theta = Math.atan2(e.x, e.z)),
              (this.phi = Math.acos(t.Math.clamp(e.y / this.radius, -1, 1)))),
          this
        );
      }
    }),
    (yn.prototype = Object.create(gt.prototype)),
    (yn.prototype.constructor = yn),
    (yn.prototype.createAnimation = function(t, e, i, n) {
      (e = {
        start: e,
        end: i,
        length: i - e + 1,
        fps: n,
        duration: (i - e) / n,
        lastFrame: 0,
        currentFrame: 0,
        active: !1,
        time: 0,
        direction: 1,
        weight: 1,
        directionBackwards: !1,
        mirroredLoop: !1
      }),
        (this.animationsMap[t] = e),
        this.animationsList.push(e);
    }),
    (yn.prototype.autoCreateAnimations = function(t) {
      for (
        var e,
          i = /([a-z]+)_?(\d+)/i,
          n = {},
          r = this.geometry,
          a = 0,
          o = r.morphTargets.length;
        a < o;
        a++
      ) {
        var s = r.morphTargets[a].name.match(i);
        if (s && 1 < s.length) {
          var c = s[1];
          n[c] || (n[c] = { start: Infinity, end: -Infinity }),
            (s = n[c]),
            a < s.start && (s.start = a),
            a > s.end && (s.end = a),
            e || (e = c);
        }
      }
      for (c in n) (s = n[c]), this.createAnimation(c, s.start, s.end, t);
      this.firstAnimation = e;
    }),
    (yn.prototype.setAnimationDirectionForward = function(t) {
      (t = this.animationsMap[t]) &&
        ((t.direction = 1), (t.directionBackwards = !1));
    }),
    (yn.prototype.setAnimationDirectionBackward = function(t) {
      (t = this.animationsMap[t]) &&
        ((t.direction = -1), (t.directionBackwards = !0));
    }),
    (yn.prototype.setAnimationFPS = function(t, e) {
      var i = this.animationsMap[t];
      i && ((i.fps = e), (i.duration = (i.end - i.start) / i.fps));
    }),
    (yn.prototype.setAnimationDuration = function(t, e) {
      var i = this.animationsMap[t];
      i && ((i.duration = e), (i.fps = (i.end - i.start) / i.duration));
    }),
    (yn.prototype.setAnimationWeight = function(t, e) {
      var i = this.animationsMap[t];
      i && (i.weight = e);
    }),
    (yn.prototype.setAnimationTime = function(t, e) {
      var i = this.animationsMap[t];
      i && (i.time = e);
    }),
    (yn.prototype.getAnimationTime = function(t) {
      var e = 0;
      return (t = this.animationsMap[t]) && (e = t.time), e;
    }),
    (yn.prototype.getAnimationDuration = function(t) {
      var e = -1;
      return (t = this.animationsMap[t]) && (e = t.duration), e;
    }),
    (yn.prototype.playAnimation = function(t) {
      var e = this.animationsMap[t];
      e
        ? ((e.time = 0), (e.active = !0))
        : console.warn(
            'THREE.MorphBlendMesh: animation[' +
              t +
              '] undefined in .playAnimation()'
          );
    }),
    (yn.prototype.stopAnimation = function(t) {
      (t = this.animationsMap[t]) && (t.active = !1);
    }),
    (yn.prototype.update = function(e) {
      for (var i = 0, n = this.animationsList.length; i < n; i++) {
        var r = this.animationsList[i];
        if (r.active) {
          var a = r.duration / r.length;
          (r.time += r.direction * e),
            r.mirroredLoop
              ? (r.time > r.duration || 0 > r.time) &&
                ((r.direction *= -1),
                r.time > r.duration &&
                  ((r.time = r.duration), (r.directionBackwards = !0)),
                0 > r.time && ((r.time = 0), (r.directionBackwards = !1)))
              : ((r.time %= r.duration), 0 > r.time && (r.time += r.duration));
          var o =
              r.start + t.Math.clamp(Math.floor(r.time / a), 0, r.length - 1),
            s = r.weight;
          o !== r.currentFrame &&
            ((this.morphTargetInfluences[r.lastFrame] = 0),
            (this.morphTargetInfluences[r.currentFrame] = 1 * s),
            (this.morphTargetInfluences[o] = 0),
            (r.lastFrame = r.currentFrame),
            (r.currentFrame = o)),
            (a = (r.time % a) / a),
            r.directionBackwards && (a = 1 - a),
            r.currentFrame !== r.lastFrame
              ? ((this.morphTargetInfluences[r.currentFrame] = a * s),
                (this.morphTargetInfluences[r.lastFrame] = (1 - a) * s))
              : (this.morphTargetInfluences[r.currentFrame] = s);
        }
      }
    }),
    (xn.prototype = Object.create(rt.prototype)),
    (xn.prototype.constructor = xn),
    (xn.prototype.isImmediateRenderObject = !0),
    (_n.prototype = Object.create(se.prototype)),
    (_n.prototype.constructor = _n),
    (_n.prototype.update = (function() {
      var t = new c(),
        e = new c(),
        i = new Q();
      return function() {
        var n = ['a', 'b', 'c'];
        this.object.updateMatrixWorld(!0),
          i.getNormalMatrix(this.object.matrixWorld);
        var r = this.object.matrixWorld,
          a = this.geometry.attributes.position,
          o = this.object.geometry;
        if (o && o.isGeometry)
          for (
            var s = o.vertices, c = o.faces, h = (o = 0), l = c.length;
            h < l;
            h++
          )
            for (var u = c[h], d = 0, p = u.vertexNormals.length; d < p; d++) {
              var f = u.vertexNormals[d];
              t.copy(s[u[n[d]]]).applyMatrix4(r),
                e
                  .copy(f)
                  .applyMatrix3(i)
                  .normalize()
                  .multiplyScalar(this.size)
                  .add(t),
                a.setXYZ(o, t.x, t.y, t.z),
                (o += 1),
                a.setXYZ(o, e.x, e.y, e.z),
                (o += 1);
            }
        else if (o && o.isBufferGeometry)
          for (
            n = o.attributes.position,
              s = o.attributes.normal,
              d = o = 0,
              p = n.count;
            d < p;
            d++
          )
            t.set(n.getX(d), n.getY(d), n.getZ(d)).applyMatrix4(r),
              e.set(s.getX(d), s.getY(d), s.getZ(d)),
              e
                .applyMatrix3(i)
                .normalize()
                .multiplyScalar(this.size)
                .add(t),
              a.setXYZ(o, t.x, t.y, t.z),
              (o += 1),
              a.setXYZ(o, e.x, e.y, e.z),
              (o += 1);
        return (a.needsUpdate = !0), this;
      };
    })()),
    (bn.prototype = Object.create(rt.prototype)),
    (bn.prototype.constructor = bn),
    (bn.prototype.dispose = function() {
      this.cone.geometry.dispose(), this.cone.material.dispose();
    }),
    (bn.prototype.update = (function() {
      var t = new c(),
        e = new c();
      return function() {
        var i = this.light.distance ? this.light.distance : 1e3,
          n = i * Math.tan(this.light.angle);
        this.cone.scale.set(n, n, i),
          t.setFromMatrixPosition(this.light.matrixWorld),
          e.setFromMatrixPosition(this.light.target.matrixWorld),
          this.cone.lookAt(e.sub(t)),
          this.cone.material.color
            .copy(this.light.color)
            .multiplyScalar(this.light.intensity);
      };
    })()),
    (wn.prototype = Object.create(se.prototype)),
    (wn.prototype.constructor = wn),
    (wn.prototype.getBoneList = function(t) {
      var e = [];
      t && t.isBone && e.push(t);
      for (var i = 0; i < t.children.length; i++)
        e.push.apply(e, this.getBoneList(t.children[i]));
      return e;
    }),
    (wn.prototype.update = function() {
      for (
        var t = this.geometry,
          e = new h().getInverse(this.root.matrixWorld),
          i = new h(),
          n = 0,
          r = 0;
        r < this.bones.length;
        r++
      ) {
        var a = this.bones[r];
        a.parent &&
          a.parent.isBone &&
          (i.multiplyMatrices(e, a.matrixWorld),
          t.vertices[n].setFromMatrixPosition(i),
          i.multiplyMatrices(e, a.parent.matrixWorld),
          t.vertices[n + 1].setFromMatrixPosition(i),
          (n += 2));
      }
      (t.verticesNeedUpdate = !0), t.computeBoundingSphere();
    }),
    (Mn.prototype = Object.create(gt.prototype)),
    (Mn.prototype.constructor = Mn),
    (Mn.prototype.dispose = function() {
      this.geometry.dispose(), this.material.dispose();
    }),
    (Mn.prototype.update = function() {
      this.material.color
        .copy(this.light.color)
        .multiplyScalar(this.light.intensity);
    }),
    (En.prototype = Object.create(rt.prototype)),
    (En.prototype.constructor = En),
    (En.prototype.dispose = function() {
      this.lightSphere.geometry.dispose(), this.lightSphere.material.dispose();
    }),
    (En.prototype.update = (function() {
      var t = new c();
      return function() {
        this.colors[0]
          .copy(this.light.color)
          .multiplyScalar(this.light.intensity),
          this.colors[1]
            .copy(this.light.groundColor)
            .multiplyScalar(this.light.intensity),
          this.lightSphere.lookAt(
            t.setFromMatrixPosition(this.light.matrixWorld).negate()
          ),
          (this.lightSphere.geometry.colorsNeedUpdate = !0);
      };
    })()),
    (Sn.prototype = Object.create(se.prototype)),
    (Sn.prototype.constructor = Sn),
    (Sn.prototype.setColors = function() {
      console.error(
        'THREE.GridHelper: setColors() has been deprecated, pass them in the constructor instead.'
      );
    }),
    (Tn.prototype = Object.create(se.prototype)),
    (Tn.prototype.constructor = Tn),
    (Tn.prototype.update = (function() {
      var t = new c(),
        e = new c(),
        i = new Q();
      return function() {
        this.object.updateMatrixWorld(!0),
          i.getNormalMatrix(this.object.matrixWorld);
        for (
          var n = this.object.matrixWorld,
            r = this.geometry.attributes.position,
            a = this.object.geometry,
            o = a.vertices,
            a = a.faces,
            s = 0,
            c = 0,
            h = a.length;
          c < h;
          c++
        ) {
          var l = a[c],
            u = l.normal;
          t
            .copy(o[l.a])
            .add(o[l.b])
            .add(o[l.c])
            .divideScalar(3)
            .applyMatrix4(n),
            e
              .copy(u)
              .applyMatrix3(i)
              .normalize()
              .multiplyScalar(this.size)
              .add(t),
            r.setXYZ(s, t.x, t.y, t.z),
            (s += 1),
            r.setXYZ(s, e.x, e.y, e.z),
            (s += 1);
        }
        return (r.needsUpdate = !0), this;
      };
    })()),
    (An.prototype = Object.create(rt.prototype)),
    (An.prototype.constructor = An),
    (An.prototype.dispose = function() {
      var t = this.children[0],
        e = this.children[1];
      t.geometry.dispose(),
        t.material.dispose(),
        e.geometry.dispose(),
        e.material.dispose();
    }),
    (An.prototype.update = (function() {
      var t = new c(),
        e = new c(),
        i = new c();
      return function() {
        t.setFromMatrixPosition(this.light.matrixWorld),
          e.setFromMatrixPosition(this.light.target.matrixWorld),
          i.subVectors(e, t);
        var n = this.children[0],
          r = this.children[1];
        n.lookAt(i),
          n.material.color
            .copy(this.light.color)
            .multiplyScalar(this.light.intensity),
          r.lookAt(i),
          (r.scale.z = i.length());
      };
    })()),
    (Ln.prototype = Object.create(se.prototype)),
    (Ln.prototype.constructor = Ln),
    (Ln.prototype.update = (function() {
      function t(t, a, o, s) {
        if ((n.set(a, o, s).unproject(r), void 0 !== (t = i[t])))
          for (a = 0, o = t.length; a < o; a++) e.vertices[t[a]].copy(n);
      }
      var e,
        i,
        n = new c(),
        r = new xt();
      return function() {
        (e = this.geometry),
          (i = this.pointMap),
          r.projectionMatrix.copy(this.camera.projectionMatrix),
          t('c', 0, 0, -1),
          t('t', 0, 0, 1),
          t('n1', -1, -1, -1),
          t('n2', 1, -1, -1),
          t('n3', -1, 1, -1),
          t('n4', 1, 1, -1),
          t('f1', -1, -1, 1),
          t('f2', 1, -1, 1),
          t('f3', -1, 1, 1),
          t('f4', 1, 1, 1),
          t('u1', 0.7, 1.1, -1),
          t('u2', -0.7, 1.1, -1),
          t('u3', 0, 2, -1),
          t('cf1', -1, 0, 1),
          t('cf2', 1, 0, 1),
          t('cf3', 0, -1, 1),
          t('cf4', 0, 1, 1),
          t('cn1', -1, 0, -1),
          t('cn2', 1, 0, -1),
          t('cn3', 0, -1, -1),
          t('cn4', 0, 1, -1),
          (e.verticesNeedUpdate = !0);
      };
    })()),
    (Rn.prototype = Object.create(gt.prototype)),
    (Rn.prototype.constructor = Rn),
    (Rn.prototype.update = function() {
      this.box.setFromObject(this.object),
        this.box.size(this.scale),
        this.box.getCenter(this.position);
    }),
    (Pn.prototype = Object.create(se.prototype)),
    (Pn.prototype.constructor = Pn),
    (Pn.prototype.update = (function() {
      var t = new Z();
      return function(e) {
        if ((e && e.isBox3 ? t.copy(e) : t.setFromObject(e), !t.isEmpty())) {
          e = t.min;
          var i = t.max,
            n = this.geometry.attributes.position,
            r = n.array;
          (r[0] = i.x),
            (r[1] = i.y),
            (r[2] = i.z),
            (r[3] = e.x),
            (r[4] = i.y),
            (r[5] = i.z),
            (r[6] = e.x),
            (r[7] = e.y),
            (r[8] = i.z),
            (r[9] = i.x),
            (r[10] = e.y),
            (r[11] = i.z),
            (r[12] = i.x),
            (r[13] = i.y),
            (r[14] = e.z),
            (r[15] = e.x),
            (r[16] = i.y),
            (r[17] = e.z),
            (r[18] = e.x),
            (r[19] = e.y),
            (r[20] = e.z),
            (r[21] = i.x),
            (r[22] = e.y),
            (r[23] = e.z),
            (n.needsUpdate = !0),
            this.geometry.computeBoundingSphere();
        }
      };
    })());
  var nr = new mt();
  nr.addAttribute('position', new dt([0, 0, 0, 0, 1, 0], 3));
  var rr = new ze(0, 0.5, 1, 5, 1);
  rr.translate(0, -0.5, 0),
    (Cn.prototype = Object.create(rt.prototype)),
    (Cn.prototype.constructor = Cn),
    (Cn.prototype.setDirection = (function() {
      var t,
        e = new c();
      return function(i) {
        0.99999 < i.y
          ? this.quaternion.set(0, 0, 0, 1)
          : -0.99999 > i.y
            ? this.quaternion.set(1, 0, 0, 0)
            : (e.set(i.z, 0, -i.x).normalize(),
              (t = Math.acos(i.y)),
              this.quaternion.setFromAxisAngle(e, t));
      };
    })()),
    (Cn.prototype.setLength = function(t, e, i) {
      void 0 === e && (e = 0.2 * t),
        void 0 === i && (i = 0.2 * e),
        this.line.scale.set(1, Math.max(0, t - e), 1),
        this.line.updateMatrix(),
        this.cone.scale.set(i, e, i),
        (this.cone.position.y = t),
        this.cone.updateMatrix();
    }),
    (Cn.prototype.setColor = function(t) {
      this.line.material.color.copy(t), this.cone.material.color.copy(t);
    }),
    (In.prototype = Object.create(se.prototype)),
    (In.prototype.constructor = In),
    (t.CatmullRomCurve3 = (function() {
      function t() {}
      var e = new c(),
        i = new t(),
        n = new t(),
        r = new t();
      return (
        (t.prototype.init = function(t, e, i, n) {
          (this.c0 = t),
            (this.c1 = i),
            (this.c2 = -3 * t + 3 * e - 2 * i - n),
            (this.c3 = 2 * t - 2 * e + i + n);
        }),
        (t.prototype.initNonuniformCatmullRom = function(t, e, i, n, r, a, o) {
          this.init(
            e,
            i,
            ((e - t) / r - (i - t) / (r + a) + (i - e) / a) * a,
            ((i - e) / a - (n - e) / (a + o) + (n - i) / o) * a
          );
        }),
        (t.prototype.initCatmullRom = function(t, e, i, n, r) {
          this.init(e, i, r * (i - t), r * (n - e));
        }),
        (t.prototype.calc = function(t) {
          var e = t * t;
          return this.c0 + this.c1 * t + this.c2 * e + this.c3 * e * t;
        }),
        Ni.create(
          function(t) {
            (this.points = t || []), (this.closed = !1);
          },
          function(t) {
            var a,
              o,
              s = this.points;
            (o = s.length),
              2 > o && console.log('duh, you need at least 2 points'),
              (t *= o - (this.closed ? 0 : 1)),
              (a = Math.floor(t)),
              (t -= a),
              this.closed
                ? (a +=
                    0 < a
                      ? 0
                      : (Math.floor(Math.abs(a) / s.length) + 1) * s.length)
                : 0 === t && a === o - 1 && ((a = o - 2), (t = 1));
            var h, l, u;
            if (
              (this.closed || 0 < a
                ? (h = s[(a - 1) % o])
                : (e.subVectors(s[0], s[1]).add(s[0]), (h = e)),
              (l = s[a % o]),
              (u = s[(a + 1) % o]),
              this.closed || a + 2 < o
                ? (s = s[(a + 2) % o])
                : (e.subVectors(s[o - 1], s[o - 2]).add(s[o - 1]), (s = e)),
              void 0 === this.type ||
                'centripetal' === this.type ||
                'chordal' === this.type)
            ) {
              var d = 'chordal' === this.type ? 0.5 : 0.25;
              (o = Math.pow(h.distanceToSquared(l), d)),
                (a = Math.pow(l.distanceToSquared(u), d)),
                (d = Math.pow(u.distanceToSquared(s), d)),
                1e-4 > a && (a = 1),
                1e-4 > o && (o = a),
                1e-4 > d && (d = a),
                i.initNonuniformCatmullRom(h.x, l.x, u.x, s.x, o, a, d),
                n.initNonuniformCatmullRom(h.y, l.y, u.y, s.y, o, a, d),
                r.initNonuniformCatmullRom(h.z, l.z, u.z, s.z, o, a, d);
            } else
              'catmullrom' === this.type &&
                ((o = void 0 !== this.tension ? this.tension : 0.5),
                i.initCatmullRom(h.x, l.x, u.x, s.x, o),
                n.initCatmullRom(h.y, l.y, u.y, s.y, o),
                r.initCatmullRom(h.z, l.z, u.z, s.z, o));
            return new c(i.calc(t), n.calc(t), r.calc(t));
          }
        )
      );
    })()),
    (Un.prototype = Object.create(t.CatmullRomCurve3.prototype));
  var ar = Ni.create(
    function(t) {
      console.warn(
        'THREE.SplineCurve3 will be deprecated. Please use THREE.CatmullRomCurve3'
      ),
        (this.points = void 0 === t ? [] : t);
    },
    function(e) {
      var i = this.points;
      e *= i.length - 1;
      var n = Math.floor(e);
      e -= n;
      var r = i[0 == n ? n : n - 1],
        a = i[n],
        o = i[n > i.length - 2 ? i.length - 1 : n + 1],
        i = i[n > i.length - 3 ? i.length - 1 : n + 2],
        n = t.CurveUtils.interpolate;
      return new c(
        n(r.x, a.x, o.x, i.x, e),
        n(r.y, a.y, o.y, i.y, e),
        n(r.z, a.z, o.z, i.z, e)
      );
    }
  );
  (t.CubicBezierCurve3 = Ni.create(
    function(t, e, i, n) {
      (this.v0 = t), (this.v1 = e), (this.v2 = i), (this.v3 = n);
    },
    function(e) {
      var i = t.ShapeUtils.b3;
      return new c(
        i(e, this.v0.x, this.v1.x, this.v2.x, this.v3.x),
        i(e, this.v0.y, this.v1.y, this.v2.y, this.v3.y),
        i(e, this.v0.z, this.v1.z, this.v2.z, this.v3.z)
      );
    }
  )),
    (t.QuadraticBezierCurve3 = Ni.create(
      function(t, e, i) {
        (this.v0 = t), (this.v1 = e), (this.v2 = i);
      },
      function(e) {
        var i = t.ShapeUtils.b2;
        return new c(
          i(e, this.v0.x, this.v1.x, this.v2.x),
          i(e, this.v0.y, this.v1.y, this.v2.y),
          i(e, this.v0.z, this.v1.z, this.v2.z)
        );
      }
    )),
    (t.LineCurve3 = Ni.create(
      function(t, e) {
        (this.v1 = t), (this.v2 = e);
      },
      function(t) {
        if (1 === t) return this.v2.clone();
        var e = new c();
        return (
          e.subVectors(this.v2, this.v1), e.multiplyScalar(t), e.add(this.v1), e
        );
      }
    )),
    (Dn.prototype = Object.create(zi.prototype)),
    (Dn.prototype.constructor = Dn),
    (t.SceneUtils = {
      createMultiMaterialObject: function(t, e) {
        for (var i = new le(), n = 0, r = e.length; n < r; n++)
          i.add(new gt(t, e[n]));
        return i;
      },
      detach: function(t, e, i) {
        t.applyMatrix(e.matrixWorld), e.remove(t), i.add(t);
      },
      attach: function(t, e, i) {
        var n = new h();
        n.getInverse(i.matrixWorld), t.applyMatrix(n), e.remove(t), i.add(t);
      }
    }),
    Object.assign(k.prototype, {
      center: function(t) {
        return (
          console.warn(
            'THREE.Box2: .center() has been renamed to .getCenter().'
          ),
          this.getCenter(t)
        );
      },
      empty: function() {
        return (
          console.warn('THREE.Box2: .empty() has been renamed to .isEmpty().'),
          this.isEmpty()
        );
      },
      isIntersectionBox: function(t) {
        return (
          console.warn(
            'THREE.Box2: .isIntersectionBox() has been renamed to .intersectsBox().'
          ),
          this.intersectsBox(t)
        );
      },
      size: function(t) {
        return (
          console.warn('THREE.Box2: .size() has been renamed to .getSize().'),
          this.getSize(t)
        );
      }
    }),
    Object.assign(Z.prototype, {
      center: function(t) {
        return (
          console.warn(
            'THREE.Box3: .center() has been renamed to .getCenter().'
          ),
          this.getCenter(t)
        );
      },
      empty: function() {
        return (
          console.warn('THREE.Box3: .empty() has been renamed to .isEmpty().'),
          this.isEmpty()
        );
      },
      isIntersectionBox: function(t) {
        return (
          console.warn(
            'THREE.Box3: .isIntersectionBox() has been renamed to .intersectsBox().'
          ),
          this.intersectsBox(t)
        );
      },
      isIntersectionSphere: function(t) {
        return (
          console.warn(
            'THREE.Box3: .isIntersectionSphere() has been renamed to .intersectsSphere().'
          ),
          this.intersectsSphere(t)
        );
      },
      size: function(t) {
        return (
          console.warn('THREE.Box3: .size() has been renamed to .getSize().'),
          this.getSize(t)
        );
      }
    }),
    Object.assign(at.prototype, {
      center: function(t) {
        return (
          console.warn(
            'THREE.Line3: .center() has been renamed to .getCenter().'
          ),
          this.getCenter(t)
        );
      }
    }),
    Object.assign(Q.prototype, {
      multiplyVector3: function(t) {
        return (
          console.warn(
            'THREE.Matrix3: .multiplyVector3() has been removed. Use vector.applyMatrix3( matrix ) instead.'
          ),
          t.applyMatrix3(this)
        );
      },
      multiplyVector3Array: function(t) {
        return (
          console.warn(
            'THREE.Matrix3: .multiplyVector3Array() has been renamed. Use matrix.applyToVector3Array( array ) instead.'
          ),
          this.applyToVector3Array(t)
        );
      }
    }),
    Object.assign(h.prototype, {
      extractPosition: function(t) {
        return (
          console.warn(
            'THREE.Matrix4: .extractPosition() has been renamed to .copyPosition().'
          ),
          this.copyPosition(t)
        );
      },
      setRotationFromQuaternion: function(t) {
        return (
          console.warn(
            'THREE.Matrix4: .setRotationFromQuaternion() has been renamed to .makeRotationFromQuaternion().'
          ),
          this.makeRotationFromQuaternion(t)
        );
      },
      multiplyVector3: function(t) {
        return (
          console.warn(
            'THREE.Matrix4: .multiplyVector3() has been removed. Use vector.applyMatrix4( matrix ) or vector.applyProjection( matrix ) instead.'
          ),
          t.applyProjection(this)
        );
      },
      multiplyVector4: function(t) {
        return (
          console.warn(
            'THREE.Matrix4: .multiplyVector4() has been removed. Use vector.applyMatrix4( matrix ) instead.'
          ),
          t.applyMatrix4(this)
        );
      },
      multiplyVector3Array: function(t) {
        return (
          console.warn(
            'THREE.Matrix4: .multiplyVector3Array() has been renamed. Use matrix.applyToVector3Array( array ) instead.'
          ),
          this.applyToVector3Array(t)
        );
      },
      rotateAxis: function(t) {
        console.warn(
          'THREE.Matrix4: .rotateAxis() has been removed. Use Vector3.transformDirection( matrix ) instead.'
        ),
          t.transformDirection(this);
      },
      crossVector: function(t) {
        return (
          console.warn(
            'THREE.Matrix4: .crossVector() has been removed. Use vector.applyMatrix4( matrix ) instead.'
          ),
          t.applyMatrix4(this)
        );
      },
      translate: function() {
        console.error('THREE.Matrix4: .translate() has been removed.');
      },
      rotateX: function() {
        console.error('THREE.Matrix4: .rotateX() has been removed.');
      },
      rotateY: function() {
        console.error('THREE.Matrix4: .rotateY() has been removed.');
      },
      rotateZ: function() {
        console.error('THREE.Matrix4: .rotateZ() has been removed.');
      },
      rotateByAxis: function() {
        console.error('THREE.Matrix4: .rotateByAxis() has been removed.');
      }
    }),
    Object.assign(K.prototype, {
      isIntersectionLine: function(t) {
        return (
          console.warn(
            'THREE.Plane: .isIntersectionLine() has been renamed to .intersectsLine().'
          ),
          this.intersectsLine(t)
        );
      }
    }),
    Object.assign(s.prototype, {
      multiplyVector3: function(t) {
        return (
          console.warn(
            'THREE.Quaternion: .multiplyVector3() has been removed. Use is now vector.applyQuaternion( quaternion ) instead.'
          ),
          t.applyQuaternion(this)
        );
      }
    }),
    Object.assign(et.prototype, {
      isIntersectionBox: function(t) {
        return (
          console.warn(
            'THREE.Ray: .isIntersectionBox() has been renamed to .intersectsBox().'
          ),
          this.intersectsBox(t)
        );
      },
      isIntersectionPlane: function(t) {
        return (
          console.warn(
            'THREE.Ray: .isIntersectionPlane() has been renamed to .intersectsPlane().'
          ),
          this.intersectsPlane(t)
        );
      },
      isIntersectionSphere: function(t) {
        return (
          console.warn(
            'THREE.Ray: .isIntersectionSphere() has been renamed to .intersectsSphere().'
          ),
          this.intersectsSphere(t)
        );
      }
    }),
    Object.assign(Vi.prototype, {
      extrude: function(t) {
        return (
          console.warn(
            'THREE.Shape: .extrude() has been removed. Use ExtrudeGeometry() instead.'
          ),
          new Ae(this, t)
        );
      },
      makeGeometry: function(t) {
        return (
          console.warn(
            'THREE.Shape: .makeGeometry() has been removed. Use ShapeGeometry() instead.'
          ),
          new Fe(this, t)
        );
      }
    }),
    Object.assign(c.prototype, {
      setEulerFromRotationMatrix: function() {
        console.error(
          'THREE.Vector3: .setEulerFromRotationMatrix() has been removed. Use Euler.setFromRotationMatrix() instead.'
        );
      },
      setEulerFromQuaternion: function() {
        console.error(
          'THREE.Vector3: .setEulerFromQuaternion() has been removed. Use Euler.setFromQuaternion() instead.'
        );
      },
      getPositionFromMatrix: function(t) {
        return (
          console.warn(
            'THREE.Vector3: .getPositionFromMatrix() has been renamed to .setFromMatrixPosition().'
          ),
          this.setFromMatrixPosition(t)
        );
      },
      getScaleFromMatrix: function(t) {
        return (
          console.warn(
            'THREE.Vector3: .getScaleFromMatrix() has been renamed to .setFromMatrixScale().'
          ),
          this.setFromMatrixScale(t)
        );
      },
      getColumnFromMatrix: function(t, e) {
        return (
          console.warn(
            'THREE.Vector3: .getColumnFromMatrix() has been renamed to .setFromMatrixColumn().'
          ),
          this.setFromMatrixColumn(e, t)
        );
      }
    }),
    Object.assign(rt.prototype, {
      getChildByName: function(t) {
        return (
          console.warn(
            'THREE.Object3D: .getChildByName() has been renamed to .getObjectByName().'
          ),
          this.getObjectByName(t)
        );
      },
      renderDepth: function() {
        console.warn(
          'THREE.Object3D: .renderDepth has been removed. Use .renderOrder, instead.'
        );
      },
      translate: function(t, e) {
        return (
          console.warn(
            'THREE.Object3D: .translate() has been removed. Use .translateOnAxis( axis, distance ) instead.'
          ),
          this.translateOnAxis(e, t)
        );
      }
    }),
    Object.defineProperties(rt.prototype, {
      eulerOrder: {
        get: function() {
          return (
            console.warn('THREE.Object3D: .eulerOrder is now .rotation.order.'),
            this.rotation.order
          );
        },
        set: function(t) {
          console.warn('THREE.Object3D: .eulerOrder is now .rotation.order.'),
            (this.rotation.order = t);
        }
      },
      useQuaternion: {
        get: function() {
          console.warn(
            'THREE.Object3D: .useQuaternion has been removed. The library now uses quaternions by default.'
          );
        },
        set: function() {
          console.warn(
            'THREE.Object3D: .useQuaternion has been removed. The library now uses quaternions by default.'
          );
        }
      }
    }),
    Object.defineProperties(te.prototype, {
      objects: {
        get: function() {
          return (
            console.warn('THREE.LOD: .objects has been renamed to .levels.'),
            this.levels
          );
        }
      }
    }),
    (_t.prototype.setLens = function(t, e) {
      console.warn(
        'THREE.PerspectiveCamera.setLens is deprecated. Use .setFocalLength and .filmGauge for a photographic setup.'
      ),
        void 0 !== e && (this.filmGauge = e),
        this.setFocalLength(t);
    }),
    Object.defineProperties(si.prototype, {
      onlyShadow: {
        set: function() {
          console.warn('THREE.Light: .onlyShadow has been removed.');
        }
      },
      shadowCameraFov: {
        set: function(t) {
          console.warn(
            'THREE.Light: .shadowCameraFov is now .shadow.camera.fov.'
          ),
            (this.shadow.camera.fov = t);
        }
      },
      shadowCameraLeft: {
        set: function(t) {
          console.warn(
            'THREE.Light: .shadowCameraLeft is now .shadow.camera.left.'
          ),
            (this.shadow.camera.left = t);
        }
      },
      shadowCameraRight: {
        set: function(t) {
          console.warn(
            'THREE.Light: .shadowCameraRight is now .shadow.camera.right.'
          ),
            (this.shadow.camera.right = t);
        }
      },
      shadowCameraTop: {
        set: function(t) {
          console.warn(
            'THREE.Light: .shadowCameraTop is now .shadow.camera.top.'
          ),
            (this.shadow.camera.top = t);
        }
      },
      shadowCameraBottom: {
        set: function(t) {
          console.warn(
            'THREE.Light: .shadowCameraBottom is now .shadow.camera.bottom.'
          ),
            (this.shadow.camera.bottom = t);
        }
      },
      shadowCameraNear: {
        set: function(t) {
          console.warn(
            'THREE.Light: .shadowCameraNear is now .shadow.camera.near.'
          ),
            (this.shadow.camera.near = t);
        }
      },
      shadowCameraFar: {
        set: function(t) {
          console.warn(
            'THREE.Light: .shadowCameraFar is now .shadow.camera.far.'
          ),
            (this.shadow.camera.far = t);
        }
      },
      shadowCameraVisible: {
        set: function() {
          console.warn(
            'THREE.Light: .shadowCameraVisible has been removed. Use new THREE.CameraHelper( light.shadow.camera ) instead.'
          );
        }
      },
      shadowBias: {
        set: function(t) {
          console.warn('THREE.Light: .shadowBias is now .shadow.bias.'),
            (this.shadow.bias = t);
        }
      },
      shadowDarkness: {
        set: function() {
          console.warn('THREE.Light: .shadowDarkness has been removed.');
        }
      },
      shadowMapWidth: {
        set: function(t) {
          console.warn(
            'THREE.Light: .shadowMapWidth is now .shadow.mapSize.width.'
          ),
            (this.shadow.mapSize.width = t);
        }
      },
      shadowMapHeight: {
        set: function(t) {
          console.warn(
            'THREE.Light: .shadowMapHeight is now .shadow.mapSize.height.'
          ),
            (this.shadow.mapSize.height = t);
        }
      }
    }),
    Object.defineProperties(ht.prototype, {
      length: {
        get: function() {
          return (
            console.warn(
              'THREE.BufferAttribute: .length has been deprecated. Please use .count.'
            ),
            this.array.length
          );
        }
      }
    }),
    Object.assign(mt.prototype, {
      addIndex: function(t) {
        console.warn(
          'THREE.BufferGeometry: .addIndex() has been renamed to .setIndex().'
        ),
          this.setIndex(t);
      },
      addDrawCall: function(t, e, i) {
        void 0 !== i &&
          console.warn(
            'THREE.BufferGeometry: .addDrawCall() no longer supports indexOffset.'
          ),
          console.warn(
            'THREE.BufferGeometry: .addDrawCall() is now .addGroup().'
          ),
          this.addGroup(t, e);
      },
      clearDrawCalls: function() {
        console.warn(
          'THREE.BufferGeometry: .clearDrawCalls() is now .clearGroups().'
        ),
          this.clearGroups();
      },
      computeTangents: function() {
        console.warn(
          'THREE.BufferGeometry: .computeTangents() has been removed.'
        );
      },
      computeOffsets: function() {
        console.warn(
          'THREE.BufferGeometry: .computeOffsets() has been removed.'
        );
      }
    }),
    Object.defineProperties(mt.prototype, {
      drawcalls: {
        get: function() {
          return (
            console.error(
              'THREE.BufferGeometry: .drawcalls has been renamed to .groups.'
            ),
            this.groups
          );
        }
      },
      offsets: {
        get: function() {
          return (
            console.warn(
              'THREE.BufferGeometry: .offsets has been renamed to .groups.'
            ),
            this.groups
          );
        }
      }
    }),
    Object.defineProperties(X.prototype, {
      wrapAround: {
        get: function() {
          console.warn(
            'THREE.' + this.type + ': .wrapAround has been removed.'
          );
        },
        set: function() {
          console.warn(
            'THREE.' + this.type + ': .wrapAround has been removed.'
          );
        }
      },
      wrapRGB: {
        get: function() {
          return (
            console.warn('THREE.' + this.type + ': .wrapRGB has been removed.'),
            new V()
          );
        }
      }
    }),
    Object.defineProperties(Je.prototype, {
      metal: {
        get: function() {
          return (
            console.warn(
              'THREE.MeshPhongMaterial: .metal has been removed. Use THREE.MeshStandardMaterial instead.'
            ),
            !1
          );
        },
        set: function() {
          console.warn(
            'THREE.MeshPhongMaterial: .metal has been removed. Use THREE.MeshStandardMaterial instead'
          );
        }
      }
    }),
    Object.defineProperties(q.prototype, {
      derivatives: {
        get: function() {
          return (
            console.warn(
              'THREE.ShaderMaterial: .derivatives has been moved to .extensions.derivatives.'
            ),
            this.extensions.derivatives
          );
        },
        set: function(t) {
          console.warn(
            'THREE. ShaderMaterial: .derivatives has been moved to .extensions.derivatives.'
          ),
            (this.extensions.derivatives = t);
        }
      }
    }),
    (e.prototype = Object.assign(
      Object.create({
        constructor: e,
        apply: function(t) {
          console.warn(
            'THREE.EventDispatcher: .apply is deprecated, just inherit or Object.assign the prototype to mix-in.'
          ),
            Object.assign(t, this);
        }
      }),
      e.prototype
    )),
    Object.assign(qt.prototype, {
      supportsFloatTextures: function() {
        return (
          console.warn(
            "THREE.WebGLRenderer: .supportsFloatTextures() is now .extensions.get( 'OES_texture_float' )."
          ),
          this.extensions.get('OES_texture_float')
        );
      },
      supportsHalfFloatTextures: function() {
        return (
          console.warn(
            "THREE.WebGLRenderer: .supportsHalfFloatTextures() is now .extensions.get( 'OES_texture_half_float' )."
          ),
          this.extensions.get('OES_texture_half_float')
        );
      },
      supportsStandardDerivatives: function() {
        return (
          console.warn(
            "THREE.WebGLRenderer: .supportsStandardDerivatives() is now .extensions.get( 'OES_standard_derivatives' )."
          ),
          this.extensions.get('OES_standard_derivatives')
        );
      },
      supportsCompressedTextureS3TC: function() {
        return (
          console.warn(
            "THREE.WebGLRenderer: .supportsCompressedTextureS3TC() is now .extensions.get( 'WEBGL_compressed_texture_s3tc' )."
          ),
          this.extensions.get('WEBGL_compressed_texture_s3tc')
        );
      },
      supportsCompressedTexturePVRTC: function() {
        return (
          console.warn(
            "THREE.WebGLRenderer: .supportsCompressedTexturePVRTC() is now .extensions.get( 'WEBGL_compressed_texture_pvrtc' )."
          ),
          this.extensions.get('WEBGL_compressed_texture_pvrtc')
        );
      },
      supportsBlendMinMax: function() {
        return (
          console.warn(
            "THREE.WebGLRenderer: .supportsBlendMinMax() is now .extensions.get( 'EXT_blend_minmax' )."
          ),
          this.extensions.get('EXT_blend_minmax')
        );
      },
      supportsVertexTextures: function() {
        return this.capabilities.vertexTextures;
      },
      supportsInstancedArrays: function() {
        return (
          console.warn(
            "THREE.WebGLRenderer: .supportsInstancedArrays() is now .extensions.get( 'ANGLE_instanced_arrays' )."
          ),
          this.extensions.get('ANGLE_instanced_arrays')
        );
      },
      enableScissorTest: function(t) {
        console.warn(
          'THREE.WebGLRenderer: .enableScissorTest() is now .setScissorTest().'
        ),
          this.setScissorTest(t);
      },
      initMaterial: function() {
        console.warn('THREE.WebGLRenderer: .initMaterial() has been removed.');
      },
      addPrePlugin: function() {
        console.warn('THREE.WebGLRenderer: .addPrePlugin() has been removed.');
      },
      addPostPlugin: function() {
        console.warn('THREE.WebGLRenderer: .addPostPlugin() has been removed.');
      },
      updateShadowMap: function() {
        console.warn(
          'THREE.WebGLRenderer: .updateShadowMap() has been removed.'
        );
      }
    }),
    Object.defineProperties(qt.prototype, {
      shadowMapEnabled: {
        get: function() {
          return this.shadowMap.enabled;
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderer: .shadowMapEnabled is now .shadowMap.enabled.'
          ),
            (this.shadowMap.enabled = t);
        }
      },
      shadowMapType: {
        get: function() {
          return this.shadowMap.type;
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderer: .shadowMapType is now .shadowMap.type.'
          ),
            (this.shadowMap.type = t);
        }
      },
      shadowMapCullFace: {
        get: function() {
          return this.shadowMap.cullFace;
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderer: .shadowMapCullFace is now .shadowMap.cullFace.'
          ),
            (this.shadowMap.cullFace = t);
        }
      }
    }),
    Object.defineProperties(tt.prototype, {
      cullFace: {
        get: function() {
          return this.renderReverseSided ? 2 : 1;
        },
        set: function(t) {
          (t = 1 !== t),
            console.warn(
              'WebGLRenderer: .shadowMap.cullFace is deprecated. Set .shadowMap.renderReverseSided to ' +
                t +
                '.'
            ),
            (this.renderReverseSided = t);
        }
      }
    }),
    Object.defineProperties(a.prototype, {
      wrapS: {
        get: function() {
          return (
            console.warn(
              'THREE.WebGLRenderTarget: .wrapS is now .texture.wrapS.'
            ),
            this.texture.wrapS
          );
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderTarget: .wrapS is now .texture.wrapS.'
          ),
            (this.texture.wrapS = t);
        }
      },
      wrapT: {
        get: function() {
          return (
            console.warn(
              'THREE.WebGLRenderTarget: .wrapT is now .texture.wrapT.'
            ),
            this.texture.wrapT
          );
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderTarget: .wrapT is now .texture.wrapT.'
          ),
            (this.texture.wrapT = t);
        }
      },
      magFilter: {
        get: function() {
          return (
            console.warn(
              'THREE.WebGLRenderTarget: .magFilter is now .texture.magFilter.'
            ),
            this.texture.magFilter
          );
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderTarget: .magFilter is now .texture.magFilter.'
          ),
            (this.texture.magFilter = t);
        }
      },
      minFilter: {
        get: function() {
          return (
            console.warn(
              'THREE.WebGLRenderTarget: .minFilter is now .texture.minFilter.'
            ),
            this.texture.minFilter
          );
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderTarget: .minFilter is now .texture.minFilter.'
          ),
            (this.texture.minFilter = t);
        }
      },
      anisotropy: {
        get: function() {
          return (
            console.warn(
              'THREE.WebGLRenderTarget: .anisotropy is now .texture.anisotropy.'
            ),
            this.texture.anisotropy
          );
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderTarget: .anisotropy is now .texture.anisotropy.'
          ),
            (this.texture.anisotropy = t);
        }
      },
      offset: {
        get: function() {
          return (
            console.warn(
              'THREE.WebGLRenderTarget: .offset is now .texture.offset.'
            ),
            this.texture.offset
          );
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderTarget: .offset is now .texture.offset.'
          ),
            (this.texture.offset = t);
        }
      },
      repeat: {
        get: function() {
          return (
            console.warn(
              'THREE.WebGLRenderTarget: .repeat is now .texture.repeat.'
            ),
            this.texture.repeat
          );
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderTarget: .repeat is now .texture.repeat.'
          ),
            (this.texture.repeat = t);
        }
      },
      format: {
        get: function() {
          return (
            console.warn(
              'THREE.WebGLRenderTarget: .format is now .texture.format.'
            ),
            this.texture.format
          );
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderTarget: .format is now .texture.format.'
          ),
            (this.texture.format = t);
        }
      },
      type: {
        get: function() {
          return (
            console.warn(
              'THREE.WebGLRenderTarget: .type is now .texture.type.'
            ),
            this.texture.type
          );
        },
        set: function(t) {
          console.warn('THREE.WebGLRenderTarget: .type is now .texture.type.'),
            (this.texture.type = t);
        }
      },
      generateMipmaps: {
        get: function() {
          return (
            console.warn(
              'THREE.WebGLRenderTarget: .generateMipmaps is now .texture.generateMipmaps.'
            ),
            this.texture.generateMipmaps
          );
        },
        set: function(t) {
          console.warn(
            'THREE.WebGLRenderTarget: .generateMipmaps is now .texture.generateMipmaps.'
          ),
            (this.texture.generateMipmaps = t);
        }
      }
    }),
    Object.assign(Ki.prototype, {
      load: function(t) {
        console.warn(
          'THREE.Audio: .load has been deprecated. Please use THREE.AudioLoader.'
        );
        var e = this;
        return (
          new Yi().load(t, function(t) {
            e.setBuffer(t);
          }),
          this
        );
      }
    }),
    Object.assign(tn.prototype, {
      getData: function() {
        return (
          console.warn(
            'THREE.AudioAnalyser: .getData() is now .getFrequencyData().'
          ),
          this.getFrequencyData()
        );
      }
    }),
    Object.defineProperty(t, 'AudioContext', {
      get: function() {
        return t.getAudioContext();
      }
    }),
    (t.WebGLRenderTargetCube = o),
    (t.WebGLRenderTarget = a),
    (t.WebGLRenderer = qt),
    (t.ShaderLib = qn),
    (t.UniformsLib = Xn),
    (t.ShaderChunk = Wn),
    (t.FogExp2 = Yt),
    (t.Fog = Zt),
    (t.Scene = Jt),
    (t.LensFlare = Qt),
    (t.Sprite = $t),
    (t.LOD = te),
    (t.SkinnedMesh = re),
    (t.Skeleton = ie),
    (t.Bone = ne),
    (t.Mesh = gt),
    (t.LineSegments = se),
    (t.Line = oe),
    (t.Points = he),
    (t.Group = le),
    (t.VideoTexture = ue),
    (t.DataTexture = ee),
    (t.CompressedTexture = de),
    (t.CubeTexture = l),
    (t.CanvasTexture = pe),
    (t.DepthTexture = fe),
    (t.TextureIdCount = function() {
      return Bn++;
    }),
    (t.Texture = n),
    (t.MaterialIdCount = function() {
      return Yn++;
    }),
    (t.CompressedTextureLoader = ii),
    (t.BinaryTextureLoader = ni),
    (t.DataTextureLoader = ni),
    (t.CubeTextureLoader = ai),
    (t.TextureLoader = oi),
    (t.ObjectLoader = Di),
    (t.MaterialLoader = Pi),
    (t.BufferGeometryLoader = Ci),
    (t.LoadingManager = ti),
    (t.JSONLoader = Ui),
    (t.ImageLoader = ri),
    (t.FontLoader = Xi),
    (t.XHRLoader = ei),
    (t.Loader = Ii),
    (t.AudioLoader = Yi),
    (t.SpotLightShadow = li),
    (t.SpotLight = ui),
    (t.PointLight = di),
    (t.HemisphereLight = ci),
    (t.DirectionalLightShadow = pi),
    (t.DirectionalLight = fi),
    (t.AmbientLight = mi),
    (t.LightShadow = hi),
    (t.Light = si),
    (t.StereoCamera = Zi),
    (t.PerspectiveCamera = _t),
    (t.OrthographicCamera = bt),
    (t.CubeCamera = Ji),
    (t.Camera = xt),
    (t.AudioListener = Qi),
    (t.PositionalAudio = $i),
    (t.getAudioContext = qi),
    (t.AudioAnalyser = tn),
    (t.Audio = Ki),
    (t.VectorKeyframeTrack = bi),
    (t.StringKeyframeTrack = Si),
    (t.QuaternionKeyframeTrack = Mi),
    (t.NumberKeyframeTrack = Ei),
    (t.ColorKeyframeTrack = Ai),
    (t.BooleanKeyframeTrack = Ti),
    (t.PropertyMixer = en),
    (t.PropertyBinding = nn),
    (t.KeyframeTrack = Li),
    (t.AnimationObjectGroup = rn),
    (t.AnimationMixer = on),
    (t.AnimationClip = Ri),
    (t.Uniform = sn),
    (t.InstancedBufferGeometry = cn),
    (t.BufferGeometry = mt),
    (t.GeometryIdCount = function() {
      return Jn++;
    }),
    (t.Geometry = pt),
    (t.InterleavedBufferAttribute = hn),
    (t.InstancedInterleavedBuffer = un),
    (t.InterleavedBuffer = ln),
    (t.InstancedBufferAttribute = dn),
    (t.DynamicBufferAttribute = function(t, e) {
      return (
        console.warn(
          'THREE.DynamicBufferAttribute has been removed. Use new THREE.BufferAttribute().setDynamic( true ) instead.'
        ),
        new ht(t, e).setDynamic(!0)
      );
    }),
    (t.Float64Attribute = function(t, e) {
      return new ht(new Float64Array(t), e);
    }),
    (t.Float32Attribute = dt),
    (t.Uint32Attribute = ut),
    (t.Int32Attribute = function(t, e) {
      return new ht(new Int32Array(t), e);
    }),
    (t.Uint16Attribute = lt),
    (t.Int16Attribute = function(t, e) {
      return new ht(new Int16Array(t), e);
    }),
    (t.Uint8ClampedAttribute = function(t, e) {
      return new ht(new Uint8ClampedArray(t), e);
    }),
    (t.Uint8Attribute = function(t, e) {
      return new ht(new Uint8Array(t), e);
    }),
    (t.Int8Attribute = function(t, e) {
      return new ht(new Int8Array(t), e);
    }),
    (t.BufferAttribute = ht),
    (t.Face3 = st),
    (t.Object3DIdCount = function() {
      return Zn++;
    }),
    (t.Object3D = rt),
    (t.Raycaster = pn),
    (t.Layers = nt),
    (t.EventDispatcher = e),
    (t.Clock = gn),
    (t.QuaternionLinearInterpolant = wi),
    (t.LinearInterpolant = yi),
    (t.DiscreteInterpolant = xi),
    (t.CubicInterpolant = vi),
    (t.Interpolant = gi),
    (t.Triangle = ot),
    (t.Spline = function(t) {
      function e(t, e, i, n, r, a, o) {
        return (
          (t = 0.5 * (i - t)),
          (n = 0.5 * (n - e)),
          (2 * (e - i) + t + n) * o + (-3 * (e - i) - 2 * t - n) * a + t * r + e
        );
      }
      this.points = t;
      var i,
        n,
        r,
        a,
        o,
        s,
        h,
        l,
        u,
        d = [],
        p = { x: 0, y: 0, z: 0 };
      (this.initFromArray = function(t) {
        this.points = [];
        for (var e = 0; e < t.length; e++)
          this.points[e] = { x: t[e][0], y: t[e][1], z: t[e][2] };
      }),
        (this.getPoint = function(t) {
          return (
            (i = (this.points.length - 1) * t),
            (n = Math.floor(i)),
            (r = i - n),
            (d[0] = 0 === n ? n : n - 1),
            (d[1] = n),
            (d[2] =
              n > this.points.length - 2 ? this.points.length - 1 : n + 1),
            (d[3] =
              n > this.points.length - 3 ? this.points.length - 1 : n + 2),
            (s = this.points[d[0]]),
            (h = this.points[d[1]]),
            (l = this.points[d[2]]),
            (u = this.points[d[3]]),
            (a = r * r),
            (o = r * a),
            (p.x = e(s.x, h.x, l.x, u.x, r, a, o)),
            (p.y = e(s.y, h.y, l.y, u.y, r, a, o)),
            (p.z = e(s.z, h.z, l.z, u.z, r, a, o)),
            p
          );
        }),
        (this.getControlPointsArray = function() {
          var t,
            e,
            i = this.points.length,
            n = [];
          for (t = 0; t < i; t++)
            (e = this.points[t]), (n[t] = [e.x, e.y, e.z]);
          return n;
        }),
        (this.getLength = function(t) {
          var e,
            i,
            n,
            r = 0,
            a = new c(),
            o = new c(),
            s = [],
            h = 0;
          for (
            s[0] = 0,
              t || (t = 100),
              i = this.points.length * t,
              a.copy(this.points[0]),
              t = 1;
            t < i;
            t++
          )
            (e = t / i),
              (n = this.getPoint(e)),
              o.copy(n),
              (h += o.distanceTo(a)),
              a.copy(n),
              (e *= this.points.length - 1),
              (e = Math.floor(e)) !== r && ((s[e] = h), (r = e));
          return (s[s.length] = h), { chunks: s, total: h };
        }),
        (this.reparametrizeByArcLength = function(t) {
          var e,
            i,
            n,
            r,
            a,
            o,
            s = [],
            h = new c(),
            l = this.getLength();
          for (
            s.push(h.copy(this.points[0]).clone()), e = 1;
            e < this.points.length;
            e++
          ) {
            for (
              i = l.chunks[e] - l.chunks[e - 1],
                o = Math.ceil(t * i / l.total),
                r = (e - 1) / (this.points.length - 1),
                a = e / (this.points.length - 1),
                i = 1;
              i < o - 1;
              i++
            )
              (n = r + 1 / o * i * (a - r)),
                (n = this.getPoint(n)),
                s.push(h.copy(n).clone());
            s.push(h.copy(this.points[e]).clone());
          }
          this.points = s;
        });
    }),
    (t.Spherical = vn),
    (t.Plane = K),
    (t.Frustum = $),
    (t.Sphere = J),
    (t.Ray = et),
    (t.Matrix4 = h),
    (t.Matrix3 = Q),
    (t.Box3 = Z),
    (t.Box2 = k),
    (t.Line3 = at),
    (t.Euler = it),
    (t.Vector4 = r),
    (t.Vector3 = c),
    (t.Vector2 = i),
    (t.Quaternion = s),
    (t.Color = V),
    (t.MorphBlendMesh = yn),
    (t.ImmediateRenderObject = xn),
    (t.VertexNormalsHelper = _n),
    (t.SpotLightHelper = bn),
    (t.SkeletonHelper = wn),
    (t.PointLightHelper = Mn),
    (t.HemisphereLightHelper = En),
    (t.GridHelper = Sn),
    (t.FaceNormalsHelper = Tn),
    (t.DirectionalLightHelper = An),
    (t.CameraHelper = Ln),
    (t.BoundingBoxHelper = Rn),
    (t.BoxHelper = Pn),
    (t.ArrowHelper = Cn),
    (t.AxisHelper = In),
    (t.ClosedSplineCurve3 = Un),
    (t.SplineCurve3 = ar),
    (t.ArcCurve = Dn),
    (t.EllipseCurve = zi),
    (t.SplineCurve = Bi),
    (t.CubicBezierCurve = Gi),
    (t.QuadraticBezierCurve = Hi),
    (t.LineCurve = Fi),
    (t.Shape = Vi),
    (t.ShapePath = ji),
    (t.Path = ki),
    (t.Font = Wi),
    (t.CurvePath = Oi),
    (t.Curve = Ni),
    (t.WireframeGeometry = me),
    (t.ParametricGeometry = ge),
    (t.TetrahedronGeometry = ye),
    (t.OctahedronGeometry = xe),
    (t.IcosahedronGeometry = _e),
    (t.DodecahedronGeometry = be),
    (t.PolyhedronGeometry = ve),
    (t.TubeGeometry = we),
    (t.TorusKnotGeometry = Ee),
    (t.TorusKnotBufferGeometry = Me),
    (t.TorusGeometry = Te);
  (t.TorusBufferGeometry = Se),
    (t.TextGeometry = Le),
    (t.SphereBufferGeometry = Re),
    (t.SphereGeometry = Pe),
    (t.RingGeometry = Ie),
    (t.RingBufferGeometry = Ce),
    (t.PlaneBufferGeometry = yt),
    (t.PlaneGeometry = Ue),
    (t.LatheGeometry = Ne),
    (t.LatheBufferGeometry = De),
    (t.ShapeGeometry = Fe),
    (t.ExtrudeGeometry = Ae),
    (t.EdgesGeometry = Oe),
    (t.ConeGeometry = Ge),
    (t.ConeBufferGeometry = He),
    (t.CylinderGeometry = Be),
    (t.CylinderBufferGeometry = ze),
    (t.CircleBufferGeometry = Ve),
    (t.CircleGeometry = ke),
    (t.BoxBufferGeometry = vt),
    (t.BoxGeometry = je),
    (t.ShadowMaterial = We),
    (t.SpriteMaterial = Kt),
    (t.RawShaderMaterial = Xe),
    (t.ShaderMaterial = q),
    (t.PointsMaterial = ce),
    (t.MultiMaterial = qe),
    (t.MeshPhysicalMaterial = Ze),
    (t.MeshStandardMaterial = Ye),
    (t.MeshPhongMaterial = Je),
    (t.MeshNormalMaterial = Qe),
    (t.MeshLambertMaterial = Ke),
    (t.MeshDepthMaterial = Y),
    (t.MeshBasicMaterial = ct),
    (t.LineDashedMaterial = $e),
    (t.LineBasicMaterial = ae),
    (t.Material = X),
    (t.REVISION = '81'),
    (t.MOUSE = { LEFT: 0, MIDDLE: 1, RIGHT: 2 }),
    (t.CullFaceNone = 0),
    (t.CullFaceBack = 1),
    (t.CullFaceFront = 2),
    (t.CullFaceFrontBack = 3),
    (t.FrontFaceDirectionCW = 0),
    (t.FrontFaceDirectionCCW = 1),
    (t.BasicShadowMap = 0),
    (t.PCFShadowMap = 1),
    (t.PCFSoftShadowMap = 2),
    (t.FrontSide = 0),
    (t.BackSide = 1),
    (t.DoubleSide = 2),
    (t.FlatShading = 1),
    (t.SmoothShading = 2),
    (t.NoColors = 0),
    (t.FaceColors = 1),
    (t.VertexColors = 2),
    (t.NoBlending = 0),
    (t.NormalBlending = 1),
    (t.AdditiveBlending = 2),
    (t.SubtractiveBlending = 3),
    (t.MultiplyBlending = 4),
    (t.CustomBlending = 5),
    (t.BlendingMode = Nn),
    (t.AddEquation = 100),
    (t.SubtractEquation = 101),
    (t.ReverseSubtractEquation = 102),
    (t.MinEquation = 103),
    (t.MaxEquation = 104),
    (t.ZeroFactor = 200),
    (t.OneFactor = 201),
    (t.SrcColorFactor = 202),
    (t.OneMinusSrcColorFactor = 203),
    (t.SrcAlphaFactor = 204),
    (t.OneMinusSrcAlphaFactor = 205),
    (t.DstAlphaFactor = 206),
    (t.OneMinusDstAlphaFactor = 207),
    (t.DstColorFactor = 208),
    (t.OneMinusDstColorFactor = 209),
    (t.SrcAlphaSaturateFactor = 210),
    (t.NeverDepth = 0),
    (t.AlwaysDepth = 1),
    (t.LessDepth = 2),
    (t.LessEqualDepth = 3),
    (t.EqualDepth = 4),
    (t.GreaterEqualDepth = 5),
    (t.GreaterDepth = 6),
    (t.NotEqualDepth = 7),
    (t.MultiplyOperation = 0),
    (t.MixOperation = 1),
    (t.AddOperation = 2),
    (t.NoToneMapping = 0),
    (t.LinearToneMapping = 1),
    (t.ReinhardToneMapping = 2),
    (t.Uncharted2ToneMapping = 3),
    (t.CineonToneMapping = 4),
    (t.UVMapping = 300),
    (t.CubeReflectionMapping = 301),
    (t.CubeRefractionMapping = 302),
    (t.EquirectangularReflectionMapping = 303),
    (t.EquirectangularRefractionMapping = 304),
    (t.SphericalReflectionMapping = 305),
    (t.CubeUVReflectionMapping = 306),
    (t.CubeUVRefractionMapping = 307),
    (t.TextureMapping = Fn),
    (t.RepeatWrapping = 1e3),
    (t.ClampToEdgeWrapping = 1001),
    (t.MirroredRepeatWrapping = 1002),
    (t.TextureWrapping = On),
    (t.NearestFilter = 1003),
    (t.NearestMipMapNearestFilter = 1004),
    (t.NearestMipMapLinearFilter = 1005),
    (t.LinearFilter = 1006),
    (t.LinearMipMapNearestFilter = 1007),
    (t.LinearMipMapLinearFilter = 1008),
    (t.TextureFilter = zn),
    (t.UnsignedByteType = 1009),
    (t.ByteType = 1010),
    (t.ShortType = 1011),
    (t.UnsignedShortType = 1012),
    (t.IntType = 1013),
    (t.UnsignedIntType = 1014),
    (t.FloatType = 1015),
    (t.HalfFloatType = 1016),
    (t.UnsignedShort4444Type = 1017),
    (t.UnsignedShort5551Type = 1018),
    (t.UnsignedShort565Type = 1019),
    (t.UnsignedInt248Type = 1020),
    (t.AlphaFormat = 1021),
    (t.RGBFormat = 1022),
    (t.RGBAFormat = 1023),
    (t.LuminanceFormat = 1024),
    (t.LuminanceAlphaFormat = 1025),
    (t.RGBEFormat = 1023),
    (t.DepthFormat = 1026),
    (t.DepthStencilFormat = 1027),
    (t.RGB_S3TC_DXT1_Format = 2001),
    (t.RGBA_S3TC_DXT1_Format = 2002),
    (t.RGBA_S3TC_DXT3_Format = 2003),
    (t.RGBA_S3TC_DXT5_Format = 2004),
    (t.RGB_PVRTC_4BPPV1_Format = 2100),
    (t.RGB_PVRTC_2BPPV1_Format = 2101),
    (t.RGBA_PVRTC_4BPPV1_Format = 2102),
    (t.RGBA_PVRTC_2BPPV1_Format = 2103),
    (t.RGB_ETC1_Format = 2151),
    (t.LoopOnce = 2200),
    (t.LoopRepeat = 2201),
    (t.LoopPingPong = 2202),
    (t.InterpolateDiscrete = 2300),
    (t.InterpolateLinear = 2301),
    (t.InterpolateSmooth = 2302),
    (t.ZeroCurvatureEnding = 2400),
    (t.ZeroSlopeEnding = 2401),
    (t.WrapAroundEnding = 2402),
    (t.TrianglesDrawMode = 0),
    (t.TriangleStripDrawMode = 1),
    (t.TriangleFanDrawMode = 2),
    (t.LinearEncoding = 3e3),
    (t.sRGBEncoding = 3001),
    (t.GammaEncoding = 3007),
    (t.RGBEEncoding = 3002),
    (t.LogLuvEncoding = 3003),
    (t.RGBM7Encoding = 3004),
    (t.RGBM16Encoding = 3005),
    (t.RGBDEncoding = 3006),
    (t.BasicDepthPacking = 3200),
    (t.RGBADepthPacking = 3201),
    (t.CubeGeometry = je),
    (t.Face4 = function(t, e, i, n, r, a, o) {
      return (
        console.warn(
          'THREE.Face4 has been removed. A THREE.Face3 will be created instead.'
        ),
        new st(t, e, i, r, a, o)
      );
    }),
    (t.LineStrip = 0),
    (t.LinePieces = 1),
    (t.MeshFaceMaterial = qe),
    (t.PointCloud = function(t, e) {
      return (
        console.warn('THREE.PointCloud has been renamed to THREE.Points.'),
        new he(t, e)
      );
    }),
    (t.Particle = $t),
    (t.ParticleSystem = function(t, e) {
      return (
        console.warn('THREE.ParticleSystem has been renamed to THREE.Points.'),
        new he(t, e)
      );
    }),
    (t.PointCloudMaterial = function(t) {
      return (
        console.warn(
          'THREE.PointCloudMaterial has been renamed to THREE.PointsMaterial.'
        ),
        new ce(t)
      );
    }),
    (t.ParticleBasicMaterial = function(t) {
      return (
        console.warn(
          'THREE.ParticleBasicMaterial has been renamed to THREE.PointsMaterial.'
        ),
        new ce(t)
      );
    }),
    (t.ParticleSystemMaterial = function(t) {
      return (
        console.warn(
          'THREE.ParticleSystemMaterial has been renamed to THREE.PointsMaterial.'
        ),
        new ce(t)
      );
    }),
    (t.Vertex = function(t, e, i) {
      return (
        console.warn(
          'THREE.Vertex has been removed. Use THREE.Vector3 instead.'
        ),
        new c(t, e, i)
      );
    }),
    (t.EdgesHelper = function(t, e) {
      return (
        console.warn(
          'THREE.EdgesHelper has been removed. Use THREE.EdgesGeometry instead.'
        ),
        new se(
          new Oe(t.geometry),
          new ae({ color: void 0 !== e ? e : 16777215 })
        )
      );
    }),
    (t.WireframeHelper = function(t, e) {
      return (
        console.warn(
          'THREE.WireframeHelper has been removed. Use THREE.WireframeGeometry instead.'
        ),
        new se(
          new me(t.geometry),
          new ae({ color: void 0 !== e ? e : 16777215 })
        )
      );
    }),
    (t.GeometryUtils = {
      merge: function(t, e, i) {
        console.warn(
          'THREE.GeometryUtils: .merge() has been moved to Geometry. Use geometry.merge( geometry2, matrix, materialIndexOffset ) instead.'
        );
        var n;
        e.isMesh &&
          (e.matrixAutoUpdate && e.updateMatrix(),
          (n = e.matrix),
          (e = e.geometry)),
          t.merge(e, n, i);
      },
      center: function(t) {
        return (
          console.warn(
            'THREE.GeometryUtils: .center() has been moved to Geometry. Use geometry.center() instead.'
          ),
          t.center()
        );
      }
    }),
    (t.ImageUtils = {
      crossOrigin: void 0,
      loadTexture: function(t, e, i, n) {
        console.warn(
          'THREE.ImageUtils.loadTexture has been deprecated. Use THREE.TextureLoader() instead.'
        );
        var r = new oi();
        return (
          r.setCrossOrigin(this.crossOrigin),
          (t = r.load(t, i, void 0, n)),
          e && (t.mapping = e),
          t
        );
      },
      loadTextureCube: function(t, e, i, n) {
        console.warn(
          'THREE.ImageUtils.loadTextureCube has been deprecated. Use THREE.CubeTextureLoader() instead.'
        );
        var r = new ai();
        return (
          r.setCrossOrigin(this.crossOrigin),
          (t = r.load(t, i, void 0, n)),
          e && (t.mapping = e),
          t
        );
      },
      loadCompressedTexture: function() {
        console.error(
          'THREE.ImageUtils.loadCompressedTexture has been removed. Use THREE.DDSLoader instead.'
        );
      },
      loadCompressedTextureCube: function() {
        console.error(
          'THREE.ImageUtils.loadCompressedTextureCube has been removed. Use THREE.DDSLoader instead.'
        );
      }
    }),
    (t.Projector = function() {
      console.error(
        'THREE.Projector has been moved to /examples/js/renderers/Projector.js.'
      ),
        (this.projectVector = function(t, e) {
          console.warn(
            'THREE.Projector: .projectVector() is now vector.project().'
          ),
            t.project(e);
        }),
        (this.unprojectVector = function(t, e) {
          console.warn(
            'THREE.Projector: .unprojectVector() is now vector.unproject().'
          ),
            t.unproject(e);
        }),
        (this.pickingRay = function() {
          console.error(
            'THREE.Projector: .pickingRay() is now raycaster.setFromCamera().'
          );
        });
    }),
    (t.CanvasRenderer = function() {
      console.error(
        'THREE.CanvasRenderer has been moved to /examples/js/renderers/CanvasRenderer.js'
      ),
        (this.domElement = document.createElementNS(
          'http://www.w3.org/1999/xhtml',
          'canvas'
        )),
        (this.clear = function() {}),
        (this.render = function() {}),
        (this.setClearColor = function() {}),
        (this.setSize = function() {});
    }),
    Object.defineProperty(t, '__esModule', { value: !0 });
});
var Testimonials = function(t) {
  var e = this;
  (e.currentIndex = 0),
    (e.TOUCH_DEVICE = 'ontouchstart' in document.documentElement),
    (e.dom = {}),
    (e.dom.root = t),
    (e.dom.scrollContainer = e.dom.root.querySelector('.quotes')),
    (e.dom.testimonials = e.dom.root.querySelectorAll('.testimonial')),
    (e.dom.pageContainer = e.dom.root.querySelector('.pages')),
    (e.dom.pages = []),
    e.TOUCH_DEVICE && e.dom.scrollContainer.classList.add('scrollable');
  for (let t = 0; t < e.dom.testimonials.length; t++) {
    var i = document.createElement('div');
    i.classList.add('page'),
      (i.style.opacity = 0.35),
      e.dom.pageContainer.appendChild(i),
      i.addEventListener('click', function() {
        clearInterval(e.cycleInterval), e.showTestimonial(t, 500);
      }),
      e.dom.pages.push(i);
  }
  e.dom.scrollContainer.addEventListener('scroll', e._onScroll.bind(e)),
    e.dom.scrollContainer.addEventListener('wheel', function() {
      clearInterval(e.cycleInterval);
    }),
    e.dom.scrollContainer.addEventListener('touchmove', function() {
      clearInterval(e.cycleInterval);
    }),
    e._onScroll(),
    window.addEventListener('resize', function() {
      e.showTestimonial(e.currentIndex);
    }),
    (e.cycleInterval = setInterval(function() {
      e.currentIndex++,
        e.currentIndex >= e.dom.pages.length && (e.currentIndex = 0),
        e.showTestimonial(e.currentIndex, 500);
    }, 5e3));
};
Testimonials.prototype = {
  _onScroll: function(t) {
    var e = this,
      i = t ? t.target.scrollLeft : 0,
      n = i / e.dom.scrollContainer.offsetWidth,
      r = n - Math.floor(n);
    (e.currentIndex = Math.round(n)),
      e.dom.pages.forEach(function(t, i) {
        var n = 0;
        i == e.currentIndex && (n = Math.abs(2 * (r - 0.5))),
          (t.style.opacity = 0.65 * n + 0.35);
      });
  },
  showTestimonial: function(t, e) {
    var i = this,
      n = function(t, e, i, n) {
        return (t /= n / 2) < 1
          ? i / 2 * t * t * t + e
          : i / 2 * ((t -= 2) * t * t + 2) + e;
      };
    i.currentIndex = t;
    var r,
      a = i.currentIndex * i.dom.scrollContainer.offsetWidth;
    if (e != undefined && e > 0) {
      var o = i.dom.scrollContainer.scrollLeft,
        s = function(t) {
          r = r || t;
          var c = t - r,
            h = n(c, o, a - o, e);
          (i.dom.scrollContainer.scrollLeft = h),
            c < e
              ? requestAnimationFrame(s)
              : (i.dom.scrollContainer.scrollLeft = a);
        };
      requestAnimationFrame(s);
    } else i.dom.scrollContainer.scrollLeft = a;
  }
};
var Users = function(t) {
  var e = this;
  (e.currentIndex = 0),
    (e.TOUCH_DEVICE = 'ontouchstart' in document.documentElement),
    (e.dom = {}),
    (e.dom.root = t),
    (e.dom.scrollContainer = e.dom.root.querySelector(
      '.descriptions .scroll-container'
    )),
    (e.dom.photos = [].slice.call(
      e.dom.root.querySelectorAll('.photos .image')
    )),
    (e.dom.descriptions = [].slice.call(
      e.dom.root.querySelectorAll('.descriptions .user')
    )),
    (e.dom.prevButton = e.dom.root.querySelector('a.nav.prev')),
    (e.dom.nextButton = e.dom.root.querySelector('a.nav.next')),
    (e.coordinates = e.dom.descriptions.map(function(t) {
      return t.getAttribute('data-coordinates').split(',');
    })),
    (e.globe = new Globe(
      document.querySelector('section.users .globe'),
      500,
      function() {
        e.globe.addMarkers(e.coordinates);
        var t = e.globe.rotationForCoordinate(e.coordinates[0]);
        (e.globe.meshGroup.rotation.x = t[0]),
          (e.globe.meshGroup.rotation.y = t[1]);
      }
    )),
    e.TOUCH_DEVICE && e.dom.scrollContainer.classList.add('scrollable'),
    e.dom.scrollContainer.addEventListener('scroll', e._onScroll.bind(e)),
    e.dom.scrollContainer.addEventListener('wheel', function() {
      clearInterval(e.cycleInterval);
    }),
    e.dom.scrollContainer.addEventListener('touchmove', function() {
      clearInterval(e.cycleInterval);
    }),
    e._onScroll(),
    window.addEventListener('resize', function() {
      e.showUser(e.currentIndex);
    }),
    e.dom.prevButton.addEventListener('click', function() {
      clearInterval(e.cycleInterval), e.showPrevUser(500);
    }),
    e.dom.nextButton.addEventListener('click', function() {
      clearInterval(e.cycleInterval), e.showNextUser(500);
    }),
    (e.cycleInterval = setInterval(function() {
      e.showNextUser(500);
    }, 5e3));
};
Users.prototype = {
  _onScroll: function(t) {
    var e = this,
      i = t ? Math.max(t.target.scrollLeft, 0) : 0,
      n = i / e.dom.scrollContainer.offsetWidth,
      r = n - Math.floor(n),
      a = Math.floor(n);
    e.currentIndex = Math.round(n);
    var o =
        i /
        (e.dom.scrollContainer.scrollWidth - e.dom.scrollContainer.offsetWidth),
      s = o * (e.coordinates.length - 1),
      c = e.coordinates[Math.floor(s)],
      h =
        Math.floor(s) < e.coordinates.length - 1
          ? e.coordinates[Math.floor(s) + 1]
          : e.coordinates[Math.floor(s)],
      l = e._interpolatedArray(
        e.globe.rotationForCoordinate(c),
        e.globe.rotationForCoordinate(h),
        s - Math.floor(s)
      );
    e.globe.mesh &&
      ((e.globe.meshGroup.rotation.x = l[0]),
      (e.globe.meshGroup.rotation.y = l[1])),
      e.dom.photos.forEach(function(t, i) {
        var n = 1,
          o = 1;
        i == a + 1
          ? ((n = e._interpolatedValue(1.25, 1, r)), (o = r))
          : i > a + 1 && ((n = 1.25), (o = 0)),
          (t.style.opacity = o),
          (t.style.transform = 'scale(' + n + ')');
      });
  },
  _interpolatedValue: function(t, e, i) {
    return (e - t) * i + t;
  },
  _interpolatedArray: function(t, e, i) {
    var n = this;
    return t.map(function(t, r) {
      return n._interpolatedValue(t, e[r], i);
    });
  },
  showUser: function(t, e) {
    var i = this,
      n = function(t, e, i, n) {
        return (t /= n / 2) < 1
          ? i / 2 * t * t * t + e
          : i / 2 * ((t -= 2) * t * t + 2) + e;
      };
    i.currentIndex = t;
    var r,
      a = i.currentIndex * i.dom.scrollContainer.offsetWidth;
    if (e != undefined && e > 0) {
      var o = i.dom.scrollContainer.scrollLeft,
        s = function(t) {
          r = r || t;
          var c = t - r,
            h = n(c, o, a - o, e);
          (i.dom.scrollContainer.scrollLeft = h),
            c < e
              ? requestAnimationFrame(s)
              : (i.dom.scrollContainer.scrollLeft = a);
        };
      requestAnimationFrame(s);
    } else i.dom.scrollContainer.scrollLeft = a;
  },
  showPrevUser: function(t) {
    var e = this,
      i = e.currentIndex - 1;
    i < 0 && (i = e.dom.descriptions.length - 1), e.showUser(i, t);
  },
  showNextUser: function(t) {
    var e = this,
      i = e.currentIndex + 1;
    i > e.dom.descriptions.length - 1 && (i = 0), e.showUser(i, t);
  }
};
var modalOverlay = document.querySelector('.modal-overlay'),
  modal = modalOverlay.querySelector('.join-modal'),
  form = modal.querySelector('form'),
  submitButton = form.querySelector("input[type='submit']"),
  closeButton = modal.querySelector('a.close'),
  HASHES = ['#join', '#apply', '#request-access'],
  showJoinModal = function() {
    modalOverlay.classList.add('visible'),
      modal.querySelectorAll("input[type='text']")[0].focus();
  },
  hideJoinModal = function() {
    modalOverlay.classList.remove('visible'),
      history.replaceState(undefined, undefined, '#'),
      setTimeout(function() {
        submitButton.classList.remove('common-Button--disabled'),
          modal.classList.remove('success');
      }, 350);
  };
modalOverlay.addEventListener('click', hideJoinModal),
  closeButton.addEventListener('click', hideJoinModal),
  modal.addEventListener('click', function(t) {
    t.stopPropagation();
  }),
  document.addEventListener('keyup', function(t) {
    27 == t.keyCode && hideJoinModal();
  }),
  window.addEventListener('hashchange', function() {
    HASHES.indexOf(location.hash) > -1 && showJoinModal();
  }),
  document.addEventListener('DOMContentLoaded', function() {
    HASHES.indexOf(location.hash) > -1 && showJoinModal();
  }),
  form.addEventListener('submit', function(t) {
    t.preventDefault();
    var e = [].slice.call(form.querySelectorAll('*[required]')),
      i = !0;
    e.forEach(function(t) {
      t.validity.valid
        ? t.parentNode.classList.remove('invalid')
        : ((i = !1), t.parentNode.classList.add('invalid'));
    });
    var n = form.querySelector('[name="analytics_event_id"]');
    n &&
      window.Analytics &&
      window.Analytics.getPreviousEventId &&
      (n.value = window.Analytics.getPreviousEventId()),
      i &&
        (submitButton.classList.add('common-Button--disabled'),
        submitForm(form));
  });
var Globe = function(t, e, i) {
  var n = this;
  (n.RENDER_SCALE = 1),
    (n.LONGITUDE_ROTATION_OFFSET = -0.75),
    (n.LATITUDE_ROTATION_OFFSET = 0),
    (n.CONTAINER_PADDING = 20),
    (n.radius = e || 500),
    (n.markers = []),
    (n.markerPulseProgress = 0),
    (n.dom = {}),
    (n.dom.container = t),
    (n.containerSize = {
      width: n.dom.container.offsetWidth - 2 * n.CONTAINER_PADDING,
      height: n.dom.container.offsetHeight
    }),
    (n.scene = new THREE.Scene()),
    (n.camera = new THREE.OrthographicCamera(0, 0, 0, 0, 0, 0)),
    n._updateCamera(),
    n.camera.translateY(390),
    (n.renderer = new THREE.WebGLRenderer({
      antialias: window.devicePixelRatio < 2,
      alpha: !1
    })),
    n.renderer.setPixelRatio(window.devicePixelRatio),
    n.renderer.setSize(
      n.containerSize.width * n.RENDER_SCALE,
      n.containerSize.height * n.RENDER_SCALE
    ),
    n.renderer.setClearColor(16185852, 1),
    n.dom.container.appendChild(n.renderer.domElement),
    (n.meshGroup = new THREE.Group()),
    n.scene.add(n.meshGroup),
    n.setupLighting(),
    n.setupGlobe(i),
    window.addEventListener('resize', function() {
      (n.containerSize = {
        width: n.dom.container.offsetWidth - 2 * n.CONTAINER_PADDING,
        height: n.dom.container.offsetHeight
      }),
        n._updateCamera(),
        n.renderer.setSize(
          n.containerSize.width * n.RENDER_SCALE,
          n.containerSize.height * n.RENDER_SCALE
        );
    });
};
(Globe.prototype = {
  _updateCamera: function() {
    var t = this,
      e = t.containerSize.width / t.containerSize.height;
    (t.camera.left = -e * t.containerSize.height / 2),
      (t.camera.right = e * t.containerSize.height / 2),
      (t.camera.top = t.containerSize.height / 2),
      (t.camera.bottom = -t.containerSize.height / 2),
      (t.camera.near = 8 * -t.radius),
      (t.camera.far = 8 * t.radius),
      t.camera.updateProjectionMatrix();
  },
  setupLighting: function() {
    var t = this,
      e = new THREE.AmbientLight(16777215, 1);
    t.scene.add(e);
    var i = new THREE.DirectionalLight(16777215, 0.03);
    t.scene.add(i);
    var n = new THREE.PointLight(16777215, 0.2, 0, 2);
    (n.position.z = 500),
      (n.position.y = 800),
      (n.position.x = 0),
      t.scene.add(n);
  },
  setupGlobe: function(t) {
    var e = this;
    new THREE.TextureLoader().load('/img/v3/atlas/world-map.png', function(i) {
      (e.material = new THREE.MeshPhongMaterial({
        specular: new THREE.Color('rgb(255,255,255)'),
        shininess: 3,
        map: i
      })),
        (e.geometry = new THREE.SphereGeometry(e.radius, 64, 64)),
        (e.mesh = new THREE.Mesh(e.geometry, e.material)),
        e.meshGroup.add(e.mesh),
        (e.render = e.render.bind(e)),
        e.render(),
        t && t();
    });
  },
  addMarkers: function(t) {
    var e = this,
      i = (new THREE.Geometry(),
      new THREE.MeshBasicMaterial({ color: 6713064 })),
      n = new THREE.MeshBasicMaterial({
        color: 6713064,
        transparent: !0,
        opacity: 0.35
      });
    t.forEach(function(t) {
      var r = new THREE.Object3D(),
        a = e.coordinateToVector(t[0], t[1], e.radius, 0);
      r.lookAt(a), e.meshGroup.add(r);
      var o = new THREE.CircleGeometry(8, 32),
        s = new THREE.Mesh(o, i);
      s.position.set(0, 0, e.radius), r.add(s);
      var c = new THREE.CircleGeometry(32, 32),
        h = new THREE.Mesh(c, n);
      h.position.set(0, 0, e.radius),
        r.add(h),
        e.markers.push({ pointMesh: s, pulseMesh: h });
    });
  },
  rotationForCoordinate: function(t) {
    var e = this;
    return [
      t[0] * (Math.PI / 180) + e.LONGITUDE_ROTATION_OFFSET,
      (270 - t[1]) * (Math.PI / 180) + e.LATITUDE_ROTATION_OFFSET
    ];
  },
  coordinateToVector: function(t, e, i, n) {
    var r = t * Math.PI / 180,
      a = (e - 180) * Math.PI / 180,
      o = -(i + n) * Math.cos(r) * Math.cos(a),
      s = (i + n) * Math.sin(r),
      c = (i + n) * Math.cos(r) * Math.sin(a);
    return new THREE.Vector3(o, s, c);
  },
  render: function() {
    var t = this;
    requestAnimationFrame(t.render),
      (t.markerPulseProgress += 0.01),
      t.markerPulseProgress >= 1 && (t.markerPulseProgress = 0),
      t.markers.forEach(function(e) {
        e.pulseMesh.material.opacity = t._interpolatedValue(
          0.5,
          0,
          t.markerPulseProgress
        );
        var i = t._interpolatedValue(0.25, 1, t.markerPulseProgress);
        (e.pulseMesh.scale.x = i), (e.pulseMesh.scale.y = i);
      }),
      t.renderer.render(t.scene, t.camera);
  },
  _interpolatedValue: function(t, e, i) {
    return (e - t) * i + t;
  }
}),
  (function() {
    var t = Strut.isRetina ? '@2x' : '';
    Strut.supports.masks
      ? Strut.load.images(
          [
            'ground-shadow.png',
            'laptop-controls.svg',
            'laptop-device.jpg',
            'laptop-mask.svg?2',
            'laptop-screen' + t + '.jpg'
          ].map(function(t) {
            return '/img/v3/atlas/devices-dashboard/' + t;
          }),
          function() {
            document
              .querySelector('.devices-dashboard')
              .classList.add('loaded');
          }
        )
      : document.body.classList.add('mask-fallback');
  })();
var testimonials = new Testimonials(
    document.querySelector('section.testimonials')
  ),
  users = new Users(document.querySelector('section.users'));
