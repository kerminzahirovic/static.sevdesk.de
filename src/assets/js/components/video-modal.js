import $ from 'jquery';

class VideoModal {
  constructor(element = '.c-video-modal') {
    this.$element = $(element);
    if (this.$element.length > 0) {
      this.$iframe = this.$element.find('iframe');
      this.init();
    }
  }
  init() {
    // button to open the pop-up, with a href attribute #video-1
    $('.js-video-popup').on('click', this.initLink.bind(this));
    this.initModal();
  }

  initLink(event) {
    event.preventDefault();
    const $link = $(event.currentTarget);
    const linkID = $link.attr('href');
    const videoUrl = $(linkID).attr('data-src');

    if (linkID && videoUrl) {
      // put the iframe src to play
      this.showModal();
      this.$iframe.attr('src', videoUrl);
    }
  }

  showModal() {
    this.$element.modal('show');
  }

  initModal() {
    // remove src on modal close
    this.$element.on('hidden.bs.modal', e => {
      this.$iframe.attr('src', '');
    });
  }
}

export default new VideoModal();
