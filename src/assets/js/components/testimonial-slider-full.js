import $ from 'jquery';
import Siema from 'siema';

class TestimonialSliderFull {
  constructor(element) {
    this.$element = $(element);
    this.$container = this.$element.find('.fc-testimonial-slider-full__slider');
    this.$prev = this.$element.find('.js-prev');
    this.$next = this.$element.find('.js-next');
    this.swiperInstance = null;
    this.initEvents();
  }

  initEvents() {
    this.initSlider();
  }

  initSlider() {
    function addActiveClass(elements, perPage, current) {
      const activeItems = elements.slice(current, current + perPage);
      activeItems.forEach((slide, i) => {
        activeItems[i].classList.add('is-active');
      });
    }

    function cleanActiveClass(elements) {
      elements.forEach((slide, i) => {
        elements[i].classList.remove('is-active');
      });
    }

    function printSlideIndex() {
      cleanActiveClass(this.innerElements);
      addActiveClass(this.innerElements, this.perPage, this.currentSlide);
    }

    this.swiperInstance = new Siema({
      selector: this.$container[0],
      loop: true,
      perPage: 1,
      startIndex: 1,
      onInit: printSlideIndex,
      onChange: printSlideIndex,
      duration: 350
    });
    this.initButtons();
  }

  initButtons() {
    this.$prev.on('click', e => {
      e.preventDefault();
      this.swiperInstance.prev();
    });

    this.$next.on('click', e => {
      e.preventDefault();
      this.swiperInstance.next();
    });
  }
}

$('.fc-testimonial-slider-full').each(function() {
  new TestimonialSliderFull(this);
});
