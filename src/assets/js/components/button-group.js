import $ from 'jquery';

class ButtonGroup {
  constructor(selector, cb = () => {}) {
    this.$element = selector ? $(selector) : $('.c-button-group');
    this.callback = cb;
    if (this.$element.length) {
      this.initEvents();
    }
  }

  initEvents() {
    const self = this;
    this.$element.on('click', 'button', function(event) {
      event.preventDefault();
      const $this = $(this);
      $this.parent().find('button').removeClass('js-is-active');
      $this.addClass('js-is-active');
      self.callback($this);
    });
  }
}

export default ButtonGroup;
