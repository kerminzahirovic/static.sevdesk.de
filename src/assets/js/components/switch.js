import forEach from 'lodash/forEach';

class CustomSwitch {
  constructor(element = null, cb = () => {}) {
    if (!element) {
      return false;
    }
    this.element = element;
    this.callback = cb;
    this.init();

    return true;
  }

  init() {
    this.toggleSwitch();
  }

  toggleSwitch() {
    const label = this.element.querySelectorAll('.c-switch__label');
    const btn = this.element.querySelector('.c-switch__core');

    if (btn) {
      btn.addEventListener('click', event => {
        event.preventDefault();
        this.element.classList.toggle('is-checked');
        this.callback();
      });
    }

    forEach(label, lb => {
      lb.addEventListener('click', event => {
        event.preventDefault();
        event.stopPropagation();
        this.element.classList.toggle('is-checked');
        this.callback();
      });
    });
  }
}

export default CustomSwitch;
