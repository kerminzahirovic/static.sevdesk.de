class Carousel {
  constructor(element = '.c-testimonials') {
    this.currentIndex = 0;
    this.TOUCH_DEVICE = 'ontouchstart' in document.documentElement;
    this.dom = {};
    this.dom.root = document.querySelector(element);
    this.dom.scrollContainer = this.dom.root.querySelector('.container');
    this.dom.slides = [].slice.call(this.dom.root.querySelectorAll('.slide'));
    this.dom.prevButton = this.dom.root.querySelector('.btn-prev');
    this.dom.nextButton = this.dom.root.querySelector('.btn-next');

    if (this.TOUCH_DEVICE) {
      this.dom.scrollContainer.classList.add('is-scrollable');
    }

    // ['showPrev', 'showNext', 'showUser'].forEach(method => {
    //   this[method] = this[method].bind(this);
    // });

    this.attachEvents();
  }

  attachEvents() {
    this.dom.prevButton.addEventListener('click', e => {
      e.preventDefault();
      this.showPrev(500);
    })
    this.dom.nextButton.addEventListener('click', e => {
      e.preventDefault();
      this.showNext(500);
    })
  }

  showPrev(delay) {
    let index = this.currentIndex - 1;
    if (index < 0) {
      index = this.dom.slides.length - 1;
    }
    this.showUser(index, delay);
  }

  showNext(delay) {
    let index = this.currentIndex + 1;
    if (index > this.dom.slides.length - 1) {
      index = 0;
    }
    this.showUser(index, delay);
  }

  showUser(index, delay) {
    // e ?? is an offset or timmer
    this.currentIndex = index;
    let timeS;
    // calculate the moving left step, it decreases with time 50% is less then 1 and 50% is bigger then 1
    const calc = (time, scrollLeft, sizeOfFrame, delay) => {
      // t = t / n;
      return (time /= delay / 2) < 1
        ? sizeOfFrame / 2 * time * time * time + scrollLeft
        : sizeOfFrame / 2 * ((time -= 2) * time * time + 2) + scrollLeft
    };
    // distance from start to frame end of this frame
    const offset = this.currentIndex * this.dom.scrollContainer.offsetWidth;

    if (delay != undefined && delay > 0) {
      // the distance from start until the current frame start point
      const offset2 = this.dom.scrollContainer.scrollLeft;
      const doScroll = (timestamp) => {
        timeS = timeS || timestamp;
        const timeOffset = timestamp - timeS;
        const result = calc(timeOffset, offset2, offset - offset2, delay);
        this.dom.scrollContainer.scrollLeft = result;

        if (timeOffset < delay) {
          requestAnimationFrame(doScroll);
        } else {
          this.dom.scrollContainer.scrollLeft = offset;
        }
      };
      requestAnimationFrame(doScroll);
    } else {
      this.dom.scrollContainer.scrollLeft = offset;
    }

  }
}

new Carousel();
