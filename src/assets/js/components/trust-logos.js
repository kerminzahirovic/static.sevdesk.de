import forEach from 'lodash/forEach';
import Popper from 'popper.js';

class TrustLogos {
  constructor(element = '.c-trust-logos') {
    this.element = element;
    this.init();
  }

  init() {
    const containers = document.querySelectorAll(this.element);

    forEach(containers, container => {
      const items = container.querySelectorAll('.c-trust-logos__item');
      forEach(items, item => {
        const logo = item.querySelector('.c-trust-logos__media');
        const content = item.querySelector('.c-trust-logos__content');
        if (logo && content) {
          this.initPopper(logo, content);
        }
      });
    });
  }

  initPopper(referenceElement, popover) {
    const popper = new Popper(referenceElement, popover, {
      placement: 'top',
      modifiers: {
        flip: {
            behavior: ['left', 'bottom', 'top']
        },
    },
    });

  }
}

// export default new TrustLogos();
