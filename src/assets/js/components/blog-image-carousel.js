import Siema from 'siema';

class BlogImageCarousel {
  constructor(selector = '.c-blog-image-carousel') {
    this.elements = [].slice.call(document.querySelectorAll(selector));

    if (this.elements.length > 0) {
      this.initEvents();
    }
  }

  initEvents() {
    this.elements.forEach(element => {
      const slider = element.querySelector('.c-blog-image-carousel__slider');
      const btnPrev = element.querySelector('.js-prev');
      const btnNext = element.querySelector('.js-next');
      this.initSwiper(slider, btnPrev, btnNext);
    });
  }

  initSwiper(slider, prev, next) {
    const swiperInstance = new Siema({
      selector: slider,
      loop: true,
      threshold: 0,
      perPage: 1
      // onInit: printSlideIndex,
      // onChange: printSlideIndex
    });

    if (next && prev) {
      prev.addEventListener('click', e => {
        e.preventDefault();
        swiperInstance.prev();
      });

      next.addEventListener('click', e => {
        e.preventDefault();
        swiperInstance.next();
      });
    }
  }
}

export default new BlogImageCarousel();
