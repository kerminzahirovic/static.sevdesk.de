import $ from 'jquery';

class HeroTyped {
  constructor(element) {
    this.$element = $(element);
    this.typedInstance = null;
    this.init();
  }

  init() {
    if (typeof window.Typed === 'function') {
      this.initTypedJs();
    }
  }

  initTypedJs() {
    this.typedInstance = new window.Typed('#js-typed', {
      stringsElement: '#js-typed-strings',
      typeSpeed: 60,
      loop: true
    });
  }
}

$('.c-hero-typed').each(function init() {
  new HeroTyped(this);
});

