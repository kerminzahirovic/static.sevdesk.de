import forEach from 'lodash/forEach';

class Addons {
  constructor(element = '.c-addons') {
    this.element = element;
    this.init();
  }

  init() {
    const addons = document.querySelectorAll(this.element);
    jQuery('.c-addons__bpi').on('change', function() {
      if (this.value == 1) {
        if (document.querySelector('.c-addons__strpr-bp')) {
          document.querySelector('.c-addons__strpr-bp').remove()
        }
      } else if (this.value == 2) {
        var strPrice = '<span class="c-addons__strpr-bp"><span class="c-addons__value"><strong>9,80 €</strong></span> / Monat</span>'
        jQuery('.c-addons__strpr-bp').remove()
        jQuery(strPrice).insertBefore('.str-hook-bp')
      } else if (this.value == 3) {
        var strPrice = '<span class="c-addons__strpr-bp"><span class="c-addons__value"><strong>9,80 €</strong></span> / Monat</span>'
        jQuery('.c-addons__strpr-bp').remove()
        jQuery(strPrice).insertBefore('.str-hook-bp')
      }
    })

    jQuery('.c-addons__ni').on('change', function() {
      if (this.value == 1) {
        if (document.querySelector('.c-addons__strpr-ni')) {
          document.querySelector('.c-addons__strpr-ni').remove()
        }
      } else if (this.value == 2) {
        var strPrice = '<span class="c-addons__strpr-ni"><span class="c-addons__value"><strong>9,80 €</strong></span> / Monat</span>'
        jQuery('.c-addons__strpr-ni').remove()
        jQuery(strPrice).insertBefore('.str-hook-n')
      } else if (this.value == 3) {
        var strPrice = '<span class="c-addons__strpr-ni"><span class="c-addons__value"><strong>9,80 €</strong></span> / Monat</span>'
        jQuery('.c-addons__strpr-ni').remove()
        jQuery(strPrice).insertBefore('.str-hook-n')
      }
    })

  }
}

export default new Addons();
