import forEach from 'lodash/forEach';

class ArchiveList {
  constructor(element = '.c-archive-list') {
    this.element = element;
    this.init();
  }

  init() {
    const boxes = document.querySelectorAll(this.element);

    forEach(boxes, box => {
      const toggle = box.querySelector('.c-archive-list__toggle');

      if (toggle) {
        toggle.addEventListener('click', event => {
          event.preventDefault();
          box.classList.toggle('js-show-limit');
        });
      }
    });
  }
}

export default new ArchiveList();
