import forEach from 'lodash/forEach';

class ImageCarousel {
  constructor(element = '.c-image-carousel') {
    this.element = element;
    this.init();
  }

  init() {
    const slider = document.querySelectorAll(this.element);
    forEach(slider, item => {
      this.swiperInit(item);
    });
  }

  swiperInit() {

  }
}

export default new ImageCarousel();
