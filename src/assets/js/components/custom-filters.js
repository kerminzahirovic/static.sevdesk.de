import $ from 'jquery';

class CustomFilters {
  constructor(element) {
    this.$element = $(element);
    this.$items = this.$element.find('.js-filter-list-item');
    this.$filters = $('.c-custom-filters');
    this.$selects = $('.js-filter-select');
    this.$filter1 = this.$filters.find('.js-select-1');
    this.initEvents();
  }

  initEvents() {
    this.$selects.change(() => {
      this.$items.addClass('d-none');
      this.filterItems();
    });
  }

  filterItems() {
    const filter1Value = this.$filter1.val();
    const filter1Reg = new RegExp('\\b' + filter1Value + '\\b');

    const filtered = this.$items.filter(function() {
      return filter1Reg.test($(this).data('filter-tags'));
    });

    if (filtered.length) {
      filtered.removeClass('d-none');
    } else {
      //  show no results message
    }
  }
}

$('.js-list-for-filter').each(function() {
  // console.log('works');
  new CustomFilters(this);
});
