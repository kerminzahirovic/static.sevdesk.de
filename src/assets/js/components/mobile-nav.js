import $ from 'jquery';

/**
 * Inspired by slack
 * animation is done with css classes that use keyframe animation and we listen to animationend
 */
class MobileNav {
  constructor() {
    this.$selector = $('#mobile-nav');
    this.isOpen = false;

    if (this.$selector.length) {
      this.$btnOpen = $('.js-mobile-nav-trigger');
      this.$btnClose = $('.js-mobile-nav-close');
      this.$body = $('body');
      this.$html = $('html');
      this.initEvents();
    }
  }

  initEvents() {
    this.$btnOpen.on('click', event => {
      event.preventDefault();
      this.isOpen = true;
      this.$selector.removeClass('is-hidden');
      this.$body.addClass('js-mobile-nav-open');
      this.$html.addClass('js-mobile-nav-open');

      this.$btnOpen.attr('aria-expanded', 'true');

      setTimeout(() => {
        this.animate(this.$selector, 'slide-in--right', 'is-open');
        this.$selector.attr('tabindex', '-1').focus();
      }, 50);
    });

    this.$btnClose.on('click', event => {
      event.preventDefault();
      this.isOpen = false;
      this.animate(this.$selector, 'slide-out--right', 'is-hidden');
      this.$selector.removeClass('is-open');
      this.$body.removeClass('js-mobile-nav-open');
      this.$html.removeClass('js-mobile-nav-open');
      this.$selector.removeAttr('tabindex');
    });
  }

  animate(element, animationClass, finalClass) {
    element.addClass(`${animationClass} is-animating`).one('animationend', () => {
      element.removeClass(`${animationClass} is-animating`).addClass(finalClass);
    });
  }
}

export default new MobileNav();
