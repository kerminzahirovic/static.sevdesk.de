import Siema from 'siema';
import forEach from 'lodash/forEach';

class TrustCarousel {
  constructor(element = '.c-trust-carousel') {
    this.element = element;
    this.init();
  }

  init() {
    const slider = document.querySelectorAll(this.element);
    forEach(slider, item => {
      this.swiperInit(item);
    });
  }

  swiperInit(item) {
    const container = item.querySelector('.c-trust-carousel__slider');
    const prev = item.querySelector('.js-prev');
    const next = item.querySelector('.js-next');

    const swiperInstance = new Siema({
      selector: container,
      loop: true,
      perPage: {
        539: 2,
        719: 3,
        1140: 4
      }
    });

    prev.addEventListener('click', e => {
      e.preventDefault();
      swiperInstance.prev();
    });

    next.addEventListener('click', e => {
      e.preventDefault();
      swiperInstance.next();
    });

  }
}

export default new TrustCarousel();
