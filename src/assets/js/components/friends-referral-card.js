import $ from 'jquery';
import Cookie from 'js-cookie';

class FriendsReferral {
  constructor(element = '#friends-referral-card') {
    this.$element = $(element);
    if (!this.$element.length > 0) {
      return;
    }

    this.shareURL = Cookie.get('refLink');

    if (!this.shareURL) {
      this.showWarning();
    } else {
      this.$coupon = this.$element.find('.js-coupon');
      this.initEvents();
    }

  }

  initEvents() {
    this.createRefURL();
    this.socialLinks();
  }
  createRefURL() {
    if (this.$coupon.length > 0) {
      this.$coupon.text(this.shareURL);
    }
  }
  socialLinks() {
    const $email = this.$element.find('.js-email');
    const $fb = this.$element.find('.js-fb');
    const $twitter = this.$element.find('.js-twitter');

    const emailHref = 'mailto:?subject=sevDesk Rechnungsprogramm&body=Hallo, %0Dich habe die Online-Bürosoftware sevDesk entdeckt und würde es dir gerne empfehlen. %0D%0DsevDesk ist eine Buchhaltungs- und Rechnungsprogramm für Selbstständige und kleine Unternehmen. Rechnungen und Angebote schreiben geschieht im Handumdrehen und auch von unterwegs.%0D%0DTeste es einfach:';

    $email.attr('href', emailHref + this.shareURL);
    $fb.attr('href', 'https://www.facebook.com/sharer/sharer.php?u=' + this.shareURL);
    $twitter.attr('href', 'https://twitter.com/intent/tweet?url=' + this.shareURL);
  }

  showWarning() {
    this.$element.find('.js-success').addClass('d-none');
    this.$element.find('.js-error').removeClass('d-none');
  }
}

export default FriendsReferral;
