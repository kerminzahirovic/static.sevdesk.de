import $ from 'jquery';
import moment from 'moment';

if (document.querySelector('.l-advent-calendar')) {
  initAdventsCalendarJS()

}

function initAdventsCalendarJS() {
  function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
  }
  $( document ).ready(function() {
    var adDay = Number(zeroPad(moment().format('DD'), 2))
    var tuerchen = $('#day0' + adDay)

    var dd = ["c2V2ZGF5MQ==","c2V2ZGF5Mg==","c2V2ZGF5Mw==","c2V2ZGF5NA==","c2V2ZGF5NQ==","c2V2ZGF5Ng==","c2V2ZGF5Nw==","c2V2ZGF5OA==","c2V2ZGF5OQ==","c2V2ZGF5MTA=","c2V2ZGF5MTE=","c2V2ZGF5MTI=","c2V2ZGF5MTM=","c2V2ZGF5MTQ=","c2V2ZGF5MTU=","c2V2ZGF5MTY=","c2V2ZGF5MTc=","c2V2ZGF5MTg=","c2V2ZGF5MTk=","c2V2ZGF5MjA=","c2V2ZGF5MjE=","c2V2ZGF5MjI=","c2V2ZGF5MjM=","c2V2ZGF5MjQ="]
    dd.forEach((d) => {

      var n = Number(zeroPad(window.atob(d).match(/\d+/)[0]), 2)
      //var nNotPad = Number(window.atob(d).match(/\d+/)[0])
      if (n == adDay && tuerchen && moment().format('MM') == '12') {
        tuerchen.wrap(`<a href="/advent2019?advent=${d}"></a>`)
        tuerchen.addClass('today')
      }

      if (adDay > n && moment().format('MM')=='12') {
        $('#day0' + n).addClass('opened')
      }
      if (adDay < n) {
        $('#day0' + n + ' img').remove()
      }
    })
  });
}
