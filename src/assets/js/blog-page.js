import Vue from 'vue';
import Vuelidate from 'vuelidate';
import VueI18n from 'vue-i18n';

import './components/blog-image-carousel';
import './components/mql-box';
import { CURRENT_LANGUAGE } from './utils';


Vue.use(Vuelidate);
Vue.use(VueI18n);

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});


