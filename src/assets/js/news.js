import Vue from 'vue';

import newsfeedList from './vue-components/newsfeed-list.vue';

const newsListElem = document.getElementById('vue-news-list');
const newsJSON = document.querySelector('[data-news-list]');

if (newsListElem && newsJSON) {
  const newsListParsed = JSON.parse(newsJSON.innerHTML);
  // eslint-disable-next-line no-unused-vars
  const newsListInstance = new Vue({
    el: newsListElem,
    data() {
      return {
        news: Object.freeze(newsListParsed)
      };
    },
    components: { 'newsfeed-list': newsfeedList }
  });
}

const newsListRoadmapElem = document.getElementById('vue-news-roadmap');
const newsRoadmapJSON = document.querySelector('[data-news-roadmap]');

if (newsListRoadmapElem && newsRoadmapJSON) {
  const newsRoadmapParsed = JSON.parse(newsRoadmapJSON.innerHTML);
  // eslint-disable-next-line no-unused-vars
  const newsRoadmapInstance = new Vue({
    el: newsListRoadmapElem,
    data() {
      return {
        news: Object.freeze(newsRoadmapParsed)
      };
    },
    components: { 'newsfeed-list': newsfeedList }
  });
}
