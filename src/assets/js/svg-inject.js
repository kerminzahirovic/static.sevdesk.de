import SVGInjector from 'svg-injector-2';
// Elements to inject
const mySVGsToInject = document.querySelectorAll('img.js-inject-me');
// Options
const injectorOptions = {
  evalScripts: false
};

// Trigger the injection
const injector = new SVGInjector(injectorOptions);
injector.inject(mySVGsToInject);
