var modernBrowser = ('fetch' in window && 'assign' in Object);
console.log(modernBrowser);

function loadScript(src) {
  var scriptElement = document.createElement('script');
  scriptElement.async = false;
  scriptElement.src = src;
  document.head.appendChild(scriptElement);
}

if (!modernBrowser) {
  console.log('loading polyfill');
  loadScript('assets/js/polyfills/babel-polyfill.js');
  loadScript('assets/js/polyfills/picturefill.js');
}
