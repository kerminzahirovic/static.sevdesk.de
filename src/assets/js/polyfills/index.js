var modernBrowser = ('fetch' in window && 'assign' in Object);

function loadScript(src) {
  var scriptElement = document.createElement('script');
  scriptElement.async = false;
  scriptElement.src = src;
  document.head.appendChild(scriptElement);
}

if (!modernBrowser) {
  console.log('loading polyfill');
  loadScript('/content/themes/sevDesk/dist/js/polyfills/babel-polyfill.js');
  loadScript('/content/themes/sevDesk/dist/js/polyfills/picturefill.js');
}
