import { getUrlParameter } from './utils';

window.analytics.ready(() => {
  if (!document.getElementById('referral-page')) {
    return;
  }

  const refParam = getUrlParameter('sref');
  if (refParam) {
    // const userId = window.analytics.user().anonymousId();
    window.analytics.identify({
      referredByHash: refParam
    });
    window.analytics.track('Referred Click');
  }
});
