import Vue from 'vue';
import VueI18n from 'vue-i18n';

import { CURRENT_LANGUAGE } from './utils';
import calculatorVAT from './vue-components/calculator-vat.vue';
import calculatorVATes from './vue-components/calculator-vat-es.vue';
import calculatorTax from './vue-components/calculator-tax.vue';

// Create VueI18n instance
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});

const vatCalculator = document.getElementById('vue-calculator-vat');
if (vatCalculator) {
  // eslint-disable-next-line no-unused-vars
  const vatCalculatorInstance = new Vue({
    i18n,
    el: vatCalculator,
    components: { 'calculator-vat': calculatorVAT }
  });
}

const vatCalculatorES = document.getElementById('vue-calculator-vat-es');
if (vatCalculatorES) {
  // eslint-disable-next-line no-unused-vars
  const vatCalculatorInstance = new Vue({
    i18n,
    el: vatCalculatorES,
    components: { 'calculator-vat': calculatorVATes }
  });
}


const taxCalculator = document.getElementById('vue-calculator-tax');
if (taxCalculator) {
  const tabelJSON = document.querySelector('[data-tabel-tax-json]');
  // eslint-disable-next-line no-unused-vars
  const taxCalculatorInstance = new Vue({
    i18n,
    el: taxCalculator,
    data() {
      return {
        tabel: JSON.parse(tabelJSON.innerHTML) || []
      };
    },
    components: { 'calculator-tax': calculatorTax }
  });
}
