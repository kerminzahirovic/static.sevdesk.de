import Vue from 'vue';
import Vuelidate from 'vuelidate';
import VueI18n from 'vue-i18n';

import Newsletter from './vue-components/newsletter-hubspot.vue';
import { CURRENT_LANGUAGE } from './utils';

Vue.use(Vuelidate);
Vue.use(VueI18n);

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});

const newsletterElem = document.getElementById('vue-newsletter-hubspot');
if (newsletterElem) {
  // eslint-disable-next-line no-unused-vars
  const newsletterInstance = new Vue({
    i18n,
    el: newsletterElem,
    components: { 'c-vue-newsletter': Newsletter }
  });
}


