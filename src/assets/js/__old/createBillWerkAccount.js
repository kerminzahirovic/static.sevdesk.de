function createBillWerkAccount() {
  var country = 'DE';
  var method = $('input[name=paymentMethod]:checked').val();
  var order = null;
  var coupon = $('[name=coupon]').val();
  var planVariant = $('[name=planVariantId]').val();
  if ($('[name=country]').val() == 'sevdesk.de') {
    country = 'DE';
  }
  if ($('[name=country]').val() == 'sevdesk.at') {
    country = 'AT';
  }
  if ($('[name=country]').val() == 'sevdesk.ch') {
    country = 'CH';
  }
  if ($('[name=country]').val() == 'sevdesk.gr') {
    country = 'EL';
  }
  var customerData = {
    CompanyName: $('[name=company]').val(),
    FirstName: $('[name=firstname]').val(),
    LastName: $('[name=lastname]').val(),
    VatId: $('[name=vatnumber]').val(),
    EmailAddress: $('[name=email]').val(),
    Address: {
      Street: $('[name=street]').val(),
      PostalCode: $('[name=zip]').val(),
      City: $('[name=city]').val(),
      Country: country
    }
  };

  // Update Billing in sevDesk
  $.ajax({
    type: 'POST',
    url:
      endPoint +
      'api/v1/SubscriptionType/Factory/updateBillingInformation?cft=' +
      cftToken +
      '&token=' +
      apiToken,
    data: { data: JSON.stringify(customerData) },
    dataType: 'json',
    async: false,
    success: function(msg) {}
  });

  // Cart info
  var orderData = { Cart: { PlanVariantId: planVariant } };
  if (coupon.length > 0) {
    var couponData = { planVariantId: planVariant, coupon: coupon };

    // Check coupon
    $.ajax({
      type: 'POST',
      url:
        endPoint +
        'api/v1/SubscriptionType/Factory/checkCoupon?cft=' +
        cftToken +
        '&token=' +
        apiToken,
      data: couponData,
      dataType: 'json',
      async: false,
      success: function(msg) {
        if (msg.objects) {
          orderData.Cart.CouponCode = coupon;
        }
      }
    });
  }

  // Create order
  $.ajax({
    type: 'POST',
    url:
      endPoint +
      'api/v1/SubscriptionType/Factory/createOrder?cft=' +
      cftToken +
      '&token=' +
      apiToken,
    data: { data: JSON.stringify(orderData) },
    dataType: 'json',
    async: false,
    success: function(msg) {
      order = msg.objects;
    }
  });


  function payInteractive() {
    if (method === 'creditcard') {
      var year = parseInt(
        $('[name=creditcard-expiry]')
          .val()
          .substr(5, 2)
      );
      var month = parseInt(
        $('[name=creditcard-expiry]')
          .val()
          .substr(0, 2)
      );
      year += 2000;
      var number = $('[name=creditcard-number]')
        .val()
        .replace(/\s+/g, '');
      paymentData = {
        bearer: 'CreditCard:PayOne',
        cardNumber: number,
        expiryMonth: month,
        expiryYear: year,
        cardHolder: $('[name=creditcard-owner]').val(),
        cvc: $('[name=creditcard-cvc]').val()
      };
    } else if (method === 'elv') {
    } else {
      paymentData = {
        bearer: 'PayPal',
        emailAddress: $('[name=paypalMail]').val()
      };
    }
    if (order.CustomerToken) {
      var portalService = new BillwerkJS.Portal(order.CustomerToken);
      portalService.upgradePayInteractive(
        paymentService,
        paymentData,
        order,
        paymentHandler,
        errorHandler
      );
    } else {
      signupService.paySignupInteractive(
        paymentService,
        paymentData,
        order,
        paymentHandler,
        errorHandler
      );
    }
  }

  // Autologin
  getAutoLoginOTT(function(ott) {
    signupService = new BillwerkJS.Signup();
    paymentService = new BillwerkJS.Payment(
      {
        publicApiKey: publicApiKey,
        providerReturnUrl: endPoint + 'finalizeBooking.html?hash_login=' + ott
      },
      function() {
        payInteractive();
      },
      function(reason) {
        processStarted = false;
        closeModal();
      }
    );
  });
}
