import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Vuelidate from 'vuelidate';

import { CURRENT_LANGUAGE } from './utils';
import Tippy from './vue-components/directives/tippy';

import calculatorHourlyWage from './vue-components/calculator-hourly-wage.vue';
import calculatorHourlyRate from './vue-components/calculator-hourly-rate.vue';
import calculatorCompanyCar from './vue-components/calculator-company-car.vue';
import calculatorWorkingHours from './vue-components/calculator-working-hours.vue';

Vue.use(Vuelidate);
Vue.use(VueI18n);
Vue.use(Tippy);

// Create VueI18n instance
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});

// Stundenlohn - hourly wage
const hourlyWageCalculator = document.getElementById(
  'vue-calculator-hourly-wage'
);

if (hourlyWageCalculator) {
  // eslint-disable-next-line no-unused-vars
  const hourlyWageCalculatorInstance = new Vue({
    i18n,
    el: hourlyWageCalculator,
    components: { 'calculator-hourly-wage': calculatorHourlyWage }
  });
}


// Stundenlohn - hourly rate
const hourlyRateCalculator = document.getElementById(
  'vue-calculator-hourly-rate'
);

if (hourlyRateCalculator) {
  // eslint-disable-next-line no-unused-vars
  const hourlyRateCalculatorInstance = new Vue({
    i18n,
    el: hourlyRateCalculator,
    components: { 'calculator-hourly-rate': calculatorHourlyRate }
  });
}

// Stundenlohn - hourly rate
const companyCarCalculator = document.getElementById(
  'vue-calculator-company-car'
);

if (companyCarCalculator) {
  // eslint-disable-next-line no-unused-vars
  const companyCarCalculatorInstance = new Vue({
    i18n,
    el: companyCarCalculator,
    components: { 'calculator-company-car': calculatorCompanyCar }
  });
}

// Stundenlohn - working hours
const workingHoursCalculator = document.getElementById(
  'vue-calculator-working-hours'
);

if (workingHoursCalculator) {
  // eslint-disable-next-line no-unused-vars
  const workingHoursCalculatorInstance = new Vue({
    i18n,
    el: workingHoursCalculator,
    components: { 'calculator-working-hours': calculatorWorkingHours }
  });
}
