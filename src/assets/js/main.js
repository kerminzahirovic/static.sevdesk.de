window.Popper = require('popper.js').default;

import 'script-loader!lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes';
import 'script-loader!bootstrap/js/dist/util';
import 'script-loader!bootstrap/js/dist/modal';

import './svg-inject';
import './components';
import './vue-components';
