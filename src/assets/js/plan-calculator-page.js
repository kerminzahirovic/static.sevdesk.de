import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Vuelidate from 'vuelidate';

import { CURRENT_LANGUAGE } from './utils';
import PlanCalculator from './vue-components/calculator/calculator.vue';
import Tippy from './vue-components/directives/tippy';

Vue.use(Vuelidate);
Vue.use(VueI18n);
Vue.use(Tippy);

// Create VueI18n instance
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});

const planCalculator = document.getElementById('plan-calculator');

if (planCalculator) {
  // eslint-disable-next-line no-unused-vars
  const planCalculatorInstance = new Vue({
    i18n,
    el: planCalculator,
    data() {
      return {
        tabelA: window.tabelA,
        tabelB: window.tabelB,
        tabelC: window.tabelC
      };
    },
    components: { 'c-plan-calculator': PlanCalculator }
  });
}
