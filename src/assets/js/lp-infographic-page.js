import TimelineLite from 'TimelineLite';

// Stats Animation
const timeline1 = new TimelineLite();
timeline1
  .from('#lp-info-box-1', 1, {
    x: '-200',
    opacity: 0
  })
  .from('#lp-info-box-2', 1, {
    x: '200',
    opacity: 0
  })
  .staggerFrom('#stats-line-1', 1, {
    drawSVG: '0% 0%'
  })
  .staggerFrom('#stats-line-2', 1, {
    drawSVG: '0% 0%'
  });

// Graph Animation
const timeline2 = new TimelineLite();
timeline2
  .staggerFrom('#anim-bar-1', 0.5, {
    drawSVG: '0% 0%'
  })
  .staggerFrom('#anim-bar-2', 0.5, {
    drawSVG: '0% 0%'
  })
  .staggerFrom('#anim-bar-3', 0.5, {
    drawSVG: '0% 0%'
  })
  .staggerFrom('#anim-bar-4', 0.5, {
    drawSVG: '0% 0%'
  });

// Stats Animation
const timeline3 = new TimelineLite();
timeline3
  .staggerFrom('#cloud-info-graph-1', 1, {
    drawSVG: '0% 0%'
  })
  .staggerFrom('#cloud-info-graph-2', 1, {
    drawSVG: '0% 0%'
  })
  .staggerFrom('#cloud-info-graph-3', 1, {
    drawSVG: '0% 0%'
  });

// Slider Animation
const sliderPath = document.getElementById('slider-path')
const timeline4 = new TimelineLite();

if (sliderPath) {
  const sliderStart = sliderPath.getTotalLength();
  timeline4.from('#slider-point', 1, {
    x: '-' + sliderStart,
    opacity: 0
  })
  .from('#round-money-icon', 1, {
    y: '-20',
    opacity: 0
  }, '+=0.25')
  .from('#round-text', 1, {
    y: '10',
    opacity: 0
  }, '-=0.5')
  .from('#hard-thing-stat-1', 1, {
    drawSVG: '0% 0%'
  }, '-=0.5')
  .from('#hard-thing-stat-2', 1, {
    drawSVG: '0% 0%'
  }, '-=1');

}

// Scrollmagic
const controller = new window.ScrollMagic.Controller();

const scene1 = new window.ScrollMagic.Scene({
  triggerElement: '#lp-stats-section',
  reverse: false
})
  .setTween(timeline1)
  .addTo(controller);

const scene2 = new window.ScrollMagic.Scene({
  triggerElement: '#lp-clouds-graph',
  reverse: false
})
  .setTween(timeline2)
  .addTo(controller);

const scene3 = new window.ScrollMagic.Scene({
  triggerElement: '.lp-cloud-info',
  reverse: false
})
  .setTween(timeline3)
  .addTo(controller);

const scene4 = new window.ScrollMagic.Scene({
  triggerElement: '.lp-hard-thing__slider',
  reverse: false
})
  .setTween(timeline4)
  .addTo(controller);
