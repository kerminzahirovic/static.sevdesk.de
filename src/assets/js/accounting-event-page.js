import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Vuelidate from 'vuelidate';

import { CURRENT_LANGUAGE } from './utils';
import AccountingEventForm from './vue-components/accounting-event-form.vue';

import AccountingEventFormDate from './vue-components/accounting-event-form-date.vue';

Vue.use(Vuelidate);
Vue.use(VueI18n);

// Create VueI18n instance
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});

const accountingForm = document.getElementById('accounting-event');

if (accountingForm) {
  // eslint-disable-next-line no-unused-vars
  const branchFeedbackInstance = new Vue({
    i18n,
    el: accountingForm,
    components: { AccountingEventForm }
  });
}

// Date
const accountingFormDate = document.getElementById('accounting-event-date');
if (accountingFormDate) {
  // eslint-disable-next-line no-unused-vars
  const accountingFormDateInstance = new Vue({
    i18n,
    el: accountingFormDate,
    components: { AccountingEventFormDate }
  });
}
