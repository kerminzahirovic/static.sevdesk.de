import Vue from 'vue';
import VueI18n from 'vue-i18n';

import { CURRENT_LANGUAGE } from './utils';
import calculatorBusinessTax from './vue-components/calculator-business-tax.vue';

// Create VueI18n instance
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});


const businessTaxCalculatorElem = document.getElementById('vue-calculator-business-tax');
if (businessTaxCalculatorElem) {
  // eslint-disable-next-line no-unused-vars
  const businessTaxCalculatorInstance = new Vue({
    i18n,
    el: businessTaxCalculatorElem,
    components: { 'calculator-business-tax': calculatorBusinessTax }
  });
}
