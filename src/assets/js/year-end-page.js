import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Vuelidate from 'vuelidate';

import { CURRENT_LANGUAGE } from './utils';
import YearEndForm from './vue-components/year-end-form.vue';

Vue.use(Vuelidate);
Vue.use(VueI18n);

// Create VueI18n instance
const i18n = new VueI18n({
  locale: CURRENT_LANGUAGE
});

const yearEndForm = document.getElementById('year-end-form');

if (yearEndForm) {
  // eslint-disable-next-line no-unused-vars
  const yearEndInstance = new Vue({
    i18n,
    el: yearEndForm,
    components: { YearEndForm }
  });
}
