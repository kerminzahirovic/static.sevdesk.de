const assembler = require('fabricator-assemble');
const browserSync = require('browser-sync');
const csso = require('gulp-csso');
const del = require('del');
const gulp = require('gulp');
const gutil = require('gulp-util');
const gulpif = require('gulp-if');
// const imagemin = require('gulp-imagemin');
const prefix = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const reload = browserSync.reload;
const runSequence = require('run-sequence');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const webpack = require('webpack');
const rev = require('gulp-rev');
const size = require('gulp-size');

// configuration
const config = {
  dest: 'dist',
  dev: gutil.env.dev,
  styles: {
    browsers: 'last 3 versions',
    fabricator: {
      src: 'src/assets/fabricator/scss/fabricator.scss',
      watch: 'src/assets/fabricator/scss/**/*.scss',
      dest: 'dist/assets/fabricator/'
    },
    mainCss: {
      src: [
        'src/assets/scss/main.scss',
        'src/assets/scss/integrations.scss',
        'src/assets/scss/zendesk.scss',
        'src/assets/scss/app-login.scss',
        'src/assets/scss/blog.scss',
        'src/assets/scss/lp-infographic.scss',
        'src/assets/scss/birthday-signup.scss',
        'src/assets/scss/team.scss',
        'src/assets/scss/pricing.scss',
        'src/assets/scss/friends-referral.scss',
        'src/assets/scss/company-search.scss',
        'src/assets/scss/newsfeed.scss',
        'src/assets/scss/for-hugo.scss',
        'src/assets/scss/branch.scss',
        'src/assets/scss/book-download.scss'
      ],
      watch: 'src/assets/scss/**/*.scss',
      dest: 'dist/assets/css/'
    }
  },
  scripts: {
    fabricator: {
      src: './src/assets/fabricator/js/fabricator.js',
      watch: 'src/assets/fabricator/js/**/*',
      dest: 'dist/assets/fabricator/'
    },
    toolkit: {
      src: {
        'main': './src/assets/js/main.js',
        'checkout-page': './src/assets/js/checkout-page.js',
        'ref-page': './src/assets/js/ref-page.js',
        'blog-page': './src/assets/js/blog-page.js',
        'lp-infographic-page': './src/assets/js/lp-infographic-page.js',
        'branch-page': './src/assets/js/branch-page.js',
        'accounting-event-page': './src/assets/js/accounting-event-page.js',
        'year-end-page': './src/assets/js/year-end-page.js',
        'plan-calculator-page': './src/assets/js/plan-calculator-page.js',
        'friends-ref-page': './src/assets/js/friends-ref-page.js',
        'company-search-page': './src/assets/js/company-search-page.js',
        'calculator-vat': './src/assets/js/calculator-vat.js',
        'calculator-business-tax': './src/assets/js/calculator-business-tax.js',
        'calculator-other': './src/assets/js/calculator-other.js',
        'calculator-income-tax': './src/assets/js/calculator-income-tax.js',
        'news': './src/assets/js/news.js',
        'workshop': './src/assets/js/workshop-page.js',
        'newsletter': './src/assets/js/newsletter.js',
        'newsletter-hubspot': './src/assets/js/newsletter-hubspot.js'
      },
      watch: 'src/assets/js/**/*',
      dest: 'dist/assets/js'
    }
  },
  images: {
    main: {
      src: ['src/images/**/*', 'src/favicon.ico'],
      watch: 'src/images/**/*',
      dest: 'dist/images/'
    },
    demo: {
      src: ['src/demo-images/**/*'],
      watch: 'src/demo-images/**/*',
      dest: 'dist/demo-images/'
    }
  },
  templates: {
    watch: 'src/**/*.{html,md,json,yml}'
  }
};

// clean
gulp.task('clean', del.bind(null, [config.dest]));

// styles
gulp.task('styles:fabricator', () => {
  gulp
    .src(config.styles.fabricator.src)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(prefix(config.styles.browsers))
    .pipe(gulpif(!config.dev, csso()))
    .pipe(rename('f.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.styles.fabricator.dest))
    .pipe(gulpif(config.dev, reload({ stream: true })));
});

gulp.task('styles:main', () => {
  const s = size({ showFiles: true });

  gulp
    .src(config.styles.mainCss.src)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(prefix(config.styles.browsers))
    .pipe(gulpif(!config.dev, csso()))
    .pipe(gulpif(config.dev, sourcemaps.write('./maps')))
    .pipe(gulpif(!config.dev, s))
    .pipe(gulp.dest(config.styles.mainCss.dest))
    .pipe(gulpif(config.dev, reload({ stream: true })));
});

gulp.task('styles', ['styles:fabricator', 'styles:main']);

// scripts
const webpackConfig = require('./webpack.config')(config);

gulp.task('scripts:vendor', done => {
  return gulp
    .src('./src/assets/js/vendor/**/*')
    .pipe(gulp.dest('./dist/assets/js/vendor/'));
});

gulp.task('scripts:polyfills', done => {
  return gulp
    .src('./src/assets/js/polyfills/**/*')
    .pipe(gulp.dest('./dist/assets/js/polyfills/'));
});

gulp.task('scripts:other', done => {
  return gulp
    .src('./src/assets/js/analytics.js')
    .pipe(gulp.dest('./dist/assets/js/'));
});

gulp.task('scripts', done => {
  webpack(webpackConfig, (err, stats) => {
    if (err) {
      gutil.log(gutil.colors.red(err()));
    }
    const result = stats.toJson();
    if (result.errors.length) {
      result.errors.forEach(error => {
        gutil.log(gutil.colors.red(error));
      });
    }
    done();
  });
});

// images
gulp.task('images', ['favicon'], () => {
  return (
    gulp
      .src(config.images.main.src)
      // .pipe(imagemin())
      .pipe(gulp.dest(config.images.main.dest))
  );
});

gulp.task('demo-images', () => {
  return (
    gulp
      .src(config.images.demo.src)
      // .pipe(imagemin())
      .pipe(gulp.dest(config.images.demo.dest))
  );
});

gulp.task('favicon', () => {
  return gulp.src('src/favicon.ico').pipe(gulp.dest(config.dest));
});

// assembler
gulp.task('assembler', done => {
  assembler({
    logErrors: config.dev,
    dest: config.dest,
    helpers: {
      eq(left, right) {
        return left == right;
      }
    }
  });
  done();
});

// server
gulp.task('serve', () => {
  browserSync({
    server: {
      baseDir: config.dest
    },
    notify: false,
    logPrefix: 'FABRICATOR'
  });

  gulp.task('assembler:watch', ['assembler'], browserSync.reload);
  gulp.watch(config.templates.watch, ['assembler:watch']);

  gulp.task('styles:watch', ['styles']);
  gulp.watch(
    [config.styles.fabricator.watch, config.styles.mainCss.watch],
    ['styles:watch']
  );

  gulp.task(
    'scripts:watch',
    // ['scripts:other', 'scripts:polyfills', 'scripts'],
    ['scripts'],
    browserSync.reload
  );
  gulp.watch(
    [config.scripts.fabricator.watch, config.scripts.toolkit.watch],
    ['scripts:watch']
  );

  gulp.task('images:watch', ['images'], browserSync.reload);
  gulp.watch(config.images.main.watch, ['images:watch']);

  gulp.task('demo-images:watch', ['demo-images'], browserSync.reload);
  gulp.watch(config.images.demo.watch, ['demo-images:watch']);
});

// Manifest
gulp.task('clean-manifest', del.bind(null, ['build']));
gulp.task('manifest', ['clean-manifest'], () =>
  gulp
    .src(['dist/assets/css/*', 'dist/assets/js/**/*'], { base: 'dist/assets' })
    .pipe(gulp.dest('build/assets')) // copy original assets to build dir
    .pipe(rev())
    .pipe(gulp.dest('build/assets')) // write rev'd assets to build dir
    .pipe(
      rev.manifest({
        // base: 'build/assets',
        path: 'rev_manifest.json',
        merge: true
      })
    )
    .pipe(gulp.dest('build/assets'))
);

// default build task
gulp.task('default', ['clean'], () => {
  // define build tasks
  const tasks = [
    'styles',
    'scripts:vendor',
    'scripts:polyfills',
    'scripts:other',
    'scripts',
    'images',
    'demo-images',
    'assembler'
  ];

  // run build
  runSequence(tasks, () => {
    if (config.dev) {
      gulp.start('serve');
    } else {
      gulp.start('manifest');
    }
  });
});
