const path = require('path');
const webpack = require('webpack');
const CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin;
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

/**
 * Define plugins based on environment
 * @param {boolean} isDev If in development mode
 * @return {Array}
 */
function getPlugins(isDev) {
  const plugins = [new webpack.DefinePlugin({})];

  plugins.push(
    new CommonsChunkPlugin({
      names: ['js/vendor']
    })
  );

  plugins.push(
    new MomentLocalesPlugin({
      localesToKeep: ['de']
    })
  );

  if (isDev) {
    plugins.push(new webpack.NoErrorsPlugin());
  } else {
    // For vuejs prod build
    plugins.push(
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production')
        }
      })
    );

    plugins.push(new webpack.optimize.ModuleConcatenationPlugin());
    plugins.push(new webpack.optimize.DedupePlugin());

    plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        minimize: true,
        sourceMap: false,
        compress: {
          warnings: false
        }
      })
    );

    plugins.push(new BundleAnalyzerPlugin());
  } // end else

  return plugins;
}

/**
 * Define loaders
 * @return {Array}
 */
function getLoaders() {
  const loaders = [
    {
      test: /(\.js)/,
      exclude: /node_modules\/(?!(dom7|swiper)\/).*/,
      use: {
        loader: 'babel-loader'
      }
    },
    {
      test: /(\.jpg|\.png)$/,
      use: {
        loader: 'url-loader?limit=10000'
      }
    },
    {
      test: /(\.vue)/,
      exclude: /(node_modules)/,
      use: {
        loader: 'vue-loader'
      }
    },
    {
      test: /\.json/,
      use: {
        loader: 'json-loader'
      }
    },
    {
      test: /\.html$/,
      exclude: /node_modules/,
      use: {
        loader: 'html-loader'
      }
    }
  ];

  return loaders;
}

module.exports = config => {
  return {
    entry: {
      'js/vendor': [
        'lazysizes/plugins/bgset/ls.bgset',
        'lazysizes',
        'vue',
        'vuelidate',
        'vue-i18n',
        'axios',
        'svg-injector-2',
        'popper.js',
        'tippy.js/dist/tippy.standalone.js',
        'enquire.js',
        'siema',
        'js-cookie'
      ],
      'fabricator/f': config.scripts.fabricator.src,
      'js/main': config.scripts.toolkit.src.main,
      'js/checkout-page': config.scripts.toolkit.src['checkout-page'],
      'js/ref-page': config.scripts.toolkit.src['ref-page'],
      'js/blog-page': config.scripts.toolkit.src['blog-page'],
      'js/lp-infographic-page':
        config.scripts.toolkit.src['lp-infographic-page'],
      'js/branch-page': config.scripts.toolkit.src['branch-page'],
      'js/accounting-event-page':
        config.scripts.toolkit.src['accounting-event-page'],
      'js/year-end-page': config.scripts.toolkit.src['year-end-page'],
      'js/plan-calculator-page':
        config.scripts.toolkit.src['plan-calculator-page'],
      'js/friends-ref-page': config.scripts.toolkit.src['friends-ref-page'],
      'js/company-search-page':
        config.scripts.toolkit.src['company-search-page'],
      'js/calculator-vat': config.scripts.toolkit.src['calculator-vat'],
      'js/calculator-business-tax':
        config.scripts.toolkit.src['calculator-business-tax'],
      'js/calculator-other': config.scripts.toolkit.src['calculator-other'],
      'js/calculator-income-tax': config.scripts.toolkit.src['calculator-income-tax'],
      'js/news': config.scripts.toolkit.src.news,
      'js/workshop': config.scripts.toolkit.src.workshop,
      'js/newsletter': config.scripts.toolkit.src.newsletter,
      'js/newsletter-hubspot': config.scripts.toolkit.src['newsletter-hubspot']
    },
    output: {
      path: path.resolve(__dirname, config.dest, 'assets'),
      filename: '[name].js'
    },
    externals: {
      jquery: 'jQuery',
      TimelineLite: 'TimelineLite'
    },
    devtool: 'source-map',
    resolve: {
      extensions: ['.js'],
      alias: {
        '@Vendor': path.resolve(__dirname, 'src/assets/js/vendor'),
        vue$: 'vue/dist/vue.esm.js'
      }
    },
    plugins: getPlugins(config.dev),
    module: {
      rules: getLoaders()
    }
  };
};
